package br.com.ttireinf.main;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.hibernate.cfg.Configuration;

import br.com.tti.sefaz.connector.AbstractConnector;
import br.com.tti.sefaz.connector.Connector;
import br.com.tti.sefaz.connector.ConnectorFactory;
import br.com.tti.sefaz.connector.ConnectorNotifier;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.events.ChangeStateXmlEvent;
import br.com.tti.sefaz.remote.events.TTIEvent;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.MainParameters;
import br.com.ttireinf.xml.XMLGenerator;
import br.gov.esocial.reinf.schemas.evtfechamento.v1_03_02.Reinf.EvtFechaEvPer;
import br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri;
import br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02.Reinf.EvtReabreEvPer;
import br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom;
import br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs;
import br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ;

public class SisHospConnector extends AbstractConnector {

	public static String url;
	public static String user;
	public static String password;

	private Connector stub;
	private ManagerInterface manager;

	private EntityManager em;

	private EntityManagerFactory emf;

	public SisHospConnector() {
		this.createStub();

		Map props = new HashMap();
		props.put("javax.persistence.jdbc.url", url);
		props.put("javax.persistence.jdbc.user", user);
		props.put("javax.persistence.jdbc.password", password);
		this.emf = Persistence.createEntityManagerFactory("myreinf_fb", props);
		this.em = emf.createEntityManager();

		System.out.println("em: " + this.em);

	}

	private void createStub() {
		try {
			Registry rmi = LocateRegistry.getRegistry("localhost");
			stub = (Connector) UnicastRemoteObject.exportObject(this, 0);
			rmi.rebind("sishosp_connector", this.stub);

			manager = Locator.getManagerReference();
		} catch (RemoteException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}

	@Override
	public String getConnectorName() throws RemoteException {
		return "sishosp_connector";
	}

	@Override
	public Connector getStub() throws RemoteException {
		return this.stub;
	}

	@Override
	synchronized public void processEvent(TTIEvent eventdata) throws RemoteException {
		try {
			ChangeStateXmlEvent state = (ChangeStateXmlEvent) eventdata;
			MyLogger.getLog().info("Reinf:" + state.getKeyXml() + " STATUS: " + state.getState().toString());

			EntityManager emret = this.emf.createEntityManager();
			EntityTransaction t = emret.getTransaction();

			t.begin();
			if (state.getState().equals(XML_STATE.ERRO_SCHEMA_LOTE) || state.getState().equals(XML_STATE.AUTORIZADA)
					|| state.getState().equals(XML_STATE.ERRO_VALIDACAO)) {
				for (String key : this.eventsTable.keySet()) {
					if (state.getKeyXml().contains(key)) {
						Reinf_Eventos event = this.eventsTable.get(key);
						MyLogger.getLog().info("event: " + event);
						if (event != null) {

							Reinf_Eventos newevent = emret.find(Reinf_Eventos.class, event.getID_PK());

							newevent.setENV_STATUS("P");
							newevent.setRET_STATUS(state.getState().toString().substring(0, 9));

							if (state.getXMotivo() != null) {
								newevent.setRET_MSG(
										state.getXMotivo().substring(0, Math.min(state.getXMotivo().length(), 199)));
							}
							newevent.setRET_RECIBO(state.getNAutorizedProtocol());
							newevent.setRET_XML(state.getXmlProtocol());

							emret.merge(newevent);
							emret.flush();

						}
					}
				}
			}

			t.commit();
			emret.close();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

	}

	public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
		return xmlString.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
				replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
				.replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
				.replaceAll("(</)(\\w+:)(.*?>)", "$1$3"); /* remove closing tags prefix */
	}

	private String createID(EvtInfoContri evtinfo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return "ID1" + evtinfo.getIdeContri().getNrInsc() + "000000" + sdf.format(Calendar.getInstance().getTime())
				+ "00000";

	}
	
	private String createID(EvtReabreEvPer evtinfo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return "ID1" + evtinfo.getIdeContri().getNrInsc() + "000000" + sdf.format(Calendar.getInstance().getTime())
				+ "00000";

	}

	private String createID(EvtServTom evtinfo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return "ID1" + evtinfo.getIdeContri().getNrInsc() + "000000" + sdf.format(Calendar.getInstance().getTime())
				+ "00000";

	}

	private String createID(EvtFechaEvPer evtinfo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return "ID1" + evtinfo.getIdeContri().getNrInsc() + "000000" + sdf.format(Calendar.getInstance().getTime())
				+ "00000";

	}

	private Hashtable<String, Reinf_Eventos> eventsTable = new Hashtable<>();

	@Override
	public int processInput() throws RemoteException {
		/*
		 * List<Object[]> reinf_events =
		 * this.em.createNativeQuery(sql_select).getResultList(); for (Object[] objects
		 * : reinf_events) { for (Object object : objects) { if (object != null)
		 * System.out.println("class: " + object.getClass() + " value: " + object); } }
		 */
		System.out.println("calling");
		this.em.clear();
		EntityTransaction t = this.em.getTransaction();

		t.begin();

		List<Reinf_Eventos> events = this.em
				.createQuery("select e from Reinf_Eventos as e where e.ENV_STATUS='A'", Reinf_Eventos.class)
				.getResultList();
		for (Reinf_Eventos evento : events) {
			System.out.println(evento.getENV_STATUS());

			evento.setENV_STATUS("L");
			System.out.println("init");

			if ("R1000".equals(evento.getTIPO_EVENTO())) {
				try {
					EvtInfoContri evtinfo = this.em.find(EvtInfoContri.class,
							Long.parseLong(evento.getENV_INFO() + ""));

					this.em.detach(evtinfo);

					String cnpj = evtinfo.getIdeContri().getNrInsc();
					evtinfo.getIdeContri().setNrInsc(cnpj.substring(0, 8));

					evtinfo.setHjid2(0L);
					evtinfo.setId(createID(evtinfo));
					XMLGenerator gen = new XMLGenerator("br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02");
					String xml = gen.toXMLString(evtinfo);

					xml = removeXmlStringNamespaceAndPreamble(xml);

					System.out.println("xml: " + xml);

					xml = "<Reinf xmlns=\"http://www.reinf.esocial.gov.br/schemas/evtInfoContribuinte/v1_04_00\">" + xml
							+ "</Reinf>";

					xml = xml.replace("EvtInfoContri", "evtInfoContri");

					this.manager.sendXml(xml, new Hashtable<>());

					this.eventsTable.put(evtinfo.getId().replace("ID", ""), evento);
					MyLogger.getLog().info("this.eventsTable:" + this.eventsTable);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if ("R2010".equals(evento.getTIPO_EVENTO())) {
				try {
					EvtServTom evttoma = this.em.find(EvtServTom.class, Long.parseLong(evento.getENV_INFO() + ""));

					this.em.detach(evttoma);

					String cnpj = evttoma.getIdeContri().getNrInsc();
					evttoma.getIdeContri().setNrInsc(cnpj.substring(0, 8));

					evttoma.setHjid(0L);
					evttoma.setId(createID(evttoma));

					{
						List<Nfs> nfs = evttoma.getInfoServTom().getIdeEstabObra().getIdePrestServ().getNfs();
						for (Nfs nf : nfs) {
							nf.setHjid(0L);
							List<InfoTpServ> tps = nf.getInfoTpServ();
							for (InfoTpServ tp : tps) {
								tp.setHjid(0L);
							}
						}
					}

					XMLGenerator gen = new XMLGenerator("br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02");
					String xml = gen.toXMLString(evttoma);

					xml = removeXmlStringNamespaceAndPreamble(xml);

					System.out.println("xml: " + xml);

					xml = "<Reinf xmlns=\"http://www.reinf.esocial.gov.br/schemas/evtTomadorServicos/v1_04_00\">" + xml
							+ "</Reinf>";

					xml = xml.replace("EvtInfoContri", "evtInfoContri");

					this.manager.sendXml(xml, new Hashtable<>());

					this.eventsTable.put(evttoma.getId().replace("ID", ""), evento);
					MyLogger.getLog().info("this.eventsTable:" + this.eventsTable);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if ("R2099".equals(evento.getTIPO_EVENTO())) {
				try {
					EvtFechaEvPer evtfecha = this.em.find(EvtFechaEvPer.class,
							Long.parseLong(evento.getENV_INFO() + ""));

					this.em.detach(evtfecha);

					String cnpj = evtfecha.getIdeContri().getNrInsc();
					evtfecha.getIdeContri().setNrInsc(cnpj.substring(0, 8));

					evtfecha.setHjid(0L);
					evtfecha.setId(createID(evtfecha));

					{

					}

					XMLGenerator gen = new XMLGenerator("br.gov.esocial.reinf.schemas.evtfechamento.v1_03_02");
					String xml = gen.toXMLString(evtfecha);

					xml = removeXmlStringNamespaceAndPreamble(xml);

					System.out.println("xml: " + xml);

					xml = "<Reinf xmlns=\"http://www.reinf.esocial.gov.br/schemas/evtFechamento/v1_04_00\">" + xml
							+ "</Reinf>";

					xml = xml.replace("EvtFechaEvPer", "evtFechaEvPer");

					this.manager.sendXml(xml, new Hashtable<>());

					this.eventsTable.put(evtfecha.getId().replace("ID", ""), evento);
					MyLogger.getLog().info("this.eventsTable:" + this.eventsTable);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			
			if ("R2098".equals(evento.getTIPO_EVENTO())) {
				try {
					EvtReabreEvPer evtreabre = this.em.find(EvtReabreEvPer.class,
							Long.parseLong(evento.getENV_INFO() + ""));

					this.em.detach(evtreabre);

					String cnpj = evtreabre.getIdeContri().getNrInsc();
					evtreabre.getIdeContri().setNrInsc(cnpj.substring(0, 8));

					evtreabre.setHjid(0L);
					evtreabre.setId(createID(evtreabre));

					{

					}

					XMLGenerator gen = new XMLGenerator("br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02");
					String xml = gen.toXMLString(evtreabre);

					xml = removeXmlStringNamespaceAndPreamble(xml);

					System.out.println("xml: " + xml);

					xml = "<Reinf xmlns=\"http://www.reinf.esocial.gov.br/schemas/evtReabreEvPer/v1_04_00\">" + xml
							+ "</Reinf>";

					xml = xml.replace("EvtReabreEvPer", "evtReabreEvPer");

					this.manager.sendXml(xml, new Hashtable<>());

					this.eventsTable.put(evtreabre.getId().replace("ID", ""), evento);
					MyLogger.getLog().info("this.eventsTable:" + this.eventsTable);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			System.out.println("end");
			this.em.merge(evento);
			this.em.flush();

		}
		t.commit();
		return 10;
	}

	@Override
	public void register(ConnectorNotifier arg0) throws RemoteException {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		url = args[0];
		user = args[1];
		password = args[2];

		MainParameters.processArguments(args);
		try {
			ConnectorFactory.createConnector("br.com.ttireinf.main.SisHospConnector");
		} catch (Exception e) {
			MyLogger.getLog().info(e.getLocalizedMessage());
			System.exit(100);
		}
		MyLogger.getLog().info("Connector Iniciado");
	}

}
