package br.com.ttireinf.main;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "REINF_EVENTOS")
public class Reinf_Eventos implements Serializable {
	@Id
	private Integer ID_PK;

	private Integer ID_TITULO;

	private String TIPO_EVENTO;

	private Integer ENV_INFO;

	private String REINF_DT_EV;

	private String ENV_STATUS;

	private String RET_STATUS;

	@Lob
	private String RET_XML;

	private String RET_MSG;

	private String RET_RECIBO;

	public int getID_PK() {
		return ID_PK;
	}

	public void setID_PK(int iD_PK) {
		ID_PK = iD_PK;
	}

	public int getID_TITULO() {
		return ID_TITULO;
	}

	public void setID_TITULO(int iD_TITULO) {
		ID_TITULO = iD_TITULO;
	}

	public String getTIPO_EVENTO() {
		return TIPO_EVENTO;
	}

	public void setTIPO_EVENTO(String tIPO_EVENTO) {
		TIPO_EVENTO = tIPO_EVENTO;
	}

	public int getENV_INFO() {
		return ENV_INFO;
	}

	public void setENV_INFO(int eNV_INFO) {
		ENV_INFO = eNV_INFO;
	}

	public String getREINF_DT_EV() {
		return REINF_DT_EV;
	}

	public void setREINF_DT_EV(String rEINF_DT_EV) {
		REINF_DT_EV = rEINF_DT_EV;
	}

	public String getENV_STATUS() {
		return ENV_STATUS;
	}

	public void setENV_STATUS(String eNV_STATUS) {
		ENV_STATUS = eNV_STATUS;
	}

	public String getRET_STATUS() {
		return RET_STATUS;
	}

	public void setRET_STATUS(String rET_STATUS) {
		RET_STATUS = rET_STATUS;
	}

	public String getRET_XML() {
		return RET_XML;
	}

	public void setRET_XML(String rET_XML) {
		RET_XML = rET_XML;
	}

	public String getRET_MSG() {
		return RET_MSG;
	}

	public void setRET_MSG(String rET_MSG) {
		RET_MSG = rET_MSG;
	}

	public String getRET_RECIBO() {
		return RET_RECIBO;
	}

	public void setRET_RECIBO(String rET_RECIBO) {
		RET_RECIBO = rET_RECIBO;
	}

}
