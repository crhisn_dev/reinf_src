//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.25 �s 02:53:46 PM BRST 
//


package br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XMLGregorianCalendarAsDateTime;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Informa��es de processamento dos eventos
 * 
 * <p>Classe Java de TDadosProcessamentoEvento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TDadosProcessamentoEvento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nrProtEntr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="49"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dhProcess" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="tpEv">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="idEv">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="36"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="hash">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TDadosProcessamentoEvento", propOrder = {
    "nrProtEntr",
    "dhProcess",
    "tpEv",
    "idEv",
    "hash"
})
@Entity(name = "TDadosProcessamentoEvento")
@Table(name = "TDADOS_PROCESSAMENTO_EVENTO")
@Inheritance(strategy = InheritanceType.JOINED)
public class TDadosProcessamentoEvento
    implements Equals, HashCode
{

    @XmlElement(required = true)
    protected String nrProtEntr;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dhProcess;
    @XmlElement(required = true)
    protected String tpEv;
    @XmlElement(required = true)
    protected String idEv;
    @XmlElement(required = true)
    protected String hash;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Obt�m o valor da propriedade nrProtEntr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "NR_PROT_ENTR", length = 49)
    public String getNrProtEntr() {
        return nrProtEntr;
    }

    /**
     * Define o valor da propriedade nrProtEntr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrProtEntr(String value) {
        this.nrProtEntr = value;
    }

    /**
     * Obt�m o valor da propriedade dhProcess.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Transient
    public XMLGregorianCalendar getDhProcess() {
        return dhProcess;
    }

    /**
     * Define o valor da propriedade dhProcess.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDhProcess(XMLGregorianCalendar value) {
        this.dhProcess = value;
    }

    /**
     * Obt�m o valor da propriedade tpEv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "TP_EV", length = 6)
    public String getTpEv() {
        return tpEv;
    }

    /**
     * Define o valor da propriedade tpEv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTpEv(String value) {
        this.tpEv = value;
    }

    /**
     * Obt�m o valor da propriedade idEv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "ID_EV", length = 36)
    public String getIdEv() {
        return idEv;
    }

    /**
     * Define o valor da propriedade idEv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEv(String value) {
        this.idEv = value;
    }

    /**
     * Obt�m o valor da propriedade hash.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "HASH", length = 60)
    public String getHash() {
        return hash;
    }

    /**
     * Define o valor da propriedade hash.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHash(String value) {
        this.hash = value;
    }

    /**
     * Obt�m o valor da propriedade hjid.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Define o valor da propriedade hjid.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    @Basic
    @Column(name = "DH_PROCESS_ITEM")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDhProcessItem() {
        return XmlAdapterUtils.unmarshall(XMLGregorianCalendarAsDateTime.class, this.getDhProcess());
    }

    public void setDhProcessItem(Date target) {
        setDhProcess(XmlAdapterUtils.marshall(XMLGregorianCalendarAsDateTime.class, target));
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TDadosProcessamentoEvento)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TDadosProcessamentoEvento that = ((TDadosProcessamentoEvento) object);
        {
            String lhsNrProtEntr;
            lhsNrProtEntr = this.getNrProtEntr();
            String rhsNrProtEntr;
            rhsNrProtEntr = that.getNrProtEntr();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nrProtEntr", lhsNrProtEntr), LocatorUtils.property(thatLocator, "nrProtEntr", rhsNrProtEntr), lhsNrProtEntr, rhsNrProtEntr)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsDhProcess;
            lhsDhProcess = this.getDhProcess();
            XMLGregorianCalendar rhsDhProcess;
            rhsDhProcess = that.getDhProcess();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dhProcess", lhsDhProcess), LocatorUtils.property(thatLocator, "dhProcess", rhsDhProcess), lhsDhProcess, rhsDhProcess)) {
                return false;
            }
        }
        {
            String lhsTpEv;
            lhsTpEv = this.getTpEv();
            String rhsTpEv;
            rhsTpEv = that.getTpEv();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tpEv", lhsTpEv), LocatorUtils.property(thatLocator, "tpEv", rhsTpEv), lhsTpEv, rhsTpEv)) {
                return false;
            }
        }
        {
            String lhsIdEv;
            lhsIdEv = this.getIdEv();
            String rhsIdEv;
            rhsIdEv = that.getIdEv();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idEv", lhsIdEv), LocatorUtils.property(thatLocator, "idEv", rhsIdEv), lhsIdEv, rhsIdEv)) {
                return false;
            }
        }
        {
            String lhsHash;
            lhsHash = this.getHash();
            String rhsHash;
            rhsHash = that.getHash();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hash", lhsHash), LocatorUtils.property(thatLocator, "hash", rhsHash), lhsHash, rhsHash)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theNrProtEntr;
            theNrProtEntr = this.getNrProtEntr();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrProtEntr", theNrProtEntr), currentHashCode, theNrProtEntr);
        }
        {
            XMLGregorianCalendar theDhProcess;
            theDhProcess = this.getDhProcess();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dhProcess", theDhProcess), currentHashCode, theDhProcess);
        }
        {
            String theTpEv;
            theTpEv = this.getTpEv();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpEv", theTpEv), currentHashCode, theTpEv);
        }
        {
            String theIdEv;
            theIdEv = this.getIdEv();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idEv", theIdEv), currentHashCode, theIdEv);
        }
        {
            String theHash;
            theHash = this.getHash();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hash", theHash), currentHashCode, theHash);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
