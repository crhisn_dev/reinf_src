//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.25 �s 02:53:46 PM BRST 
//


package br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Classe Java de TStatus complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cdRetorno">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *               &lt;pattern value="[0|1|2]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="descRetorno">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="regOcorrs" type="{http://www.reinf.esocial.gov.br/schemas/evtTotalContrib/v1_04_00}TRegistroOcorrencias" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TStatus", propOrder = {
    "cdRetorno",
    "descRetorno",
    "regOcorrs"
})
@Entity(name = "TStatus")
@Table(name = "TSTATUS")
@Inheritance(strategy = InheritanceType.JOINED)
public class TStatus
    implements Equals, HashCode
{

    protected short cdRetorno;
    @XmlElement(required = true)
    protected String descRetorno;
    protected List<TRegistroOcorrencias> regOcorrs;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Obt�m o valor da propriedade cdRetorno.
     * 
     */
    @Basic
    @Column(name = "CD_RETORNO", scale = 0)
    public short getCdRetorno() {
        return cdRetorno;
    }

    /**
     * Define o valor da propriedade cdRetorno.
     * 
     */
    public void setCdRetorno(short value) {
        this.cdRetorno = value;
    }

    /**
     * Obt�m o valor da propriedade descRetorno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "DESC_RETORNO", length = 1000)
    public String getDescRetorno() {
        return descRetorno;
    }

    /**
     * Define o valor da propriedade descRetorno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRetorno(String value) {
        this.descRetorno = value;
    }

    /**
     * Gets the value of the regOcorrs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regOcorrs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegOcorrs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TRegistroOcorrencias }
     * 
     * 
     */
    @OneToMany(targetEntity = TRegistroOcorrencias.class, cascade = {
        CascadeType.ALL
    })
    @JoinColumn(name = "REG_OCORRS_TSTATUS_HJID")
    public List<TRegistroOcorrencias> getRegOcorrs() {
        if (regOcorrs == null) {
            regOcorrs = new ArrayList<TRegistroOcorrencias>();
        }
        return this.regOcorrs;
    }

    /**
     * 
     * 
     */
    public void setRegOcorrs(List<TRegistroOcorrencias> regOcorrs) {
        this.regOcorrs = regOcorrs;
    }

    /**
     * Obt�m o valor da propriedade hjid.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Define o valor da propriedade hjid.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TStatus)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TStatus that = ((TStatus) object);
        {
            short lhsCdRetorno;
            lhsCdRetorno = this.getCdRetorno();
            short rhsCdRetorno;
            rhsCdRetorno = that.getCdRetorno();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cdRetorno", lhsCdRetorno), LocatorUtils.property(thatLocator, "cdRetorno", rhsCdRetorno), lhsCdRetorno, rhsCdRetorno)) {
                return false;
            }
        }
        {
            String lhsDescRetorno;
            lhsDescRetorno = this.getDescRetorno();
            String rhsDescRetorno;
            rhsDescRetorno = that.getDescRetorno();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "descRetorno", lhsDescRetorno), LocatorUtils.property(thatLocator, "descRetorno", rhsDescRetorno), lhsDescRetorno, rhsDescRetorno)) {
                return false;
            }
        }
        {
            List<TRegistroOcorrencias> lhsRegOcorrs;
            lhsRegOcorrs = (((this.regOcorrs!= null)&&(!this.regOcorrs.isEmpty()))?this.getRegOcorrs():null);
            List<TRegistroOcorrencias> rhsRegOcorrs;
            rhsRegOcorrs = (((that.regOcorrs!= null)&&(!that.regOcorrs.isEmpty()))?that.getRegOcorrs():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "regOcorrs", lhsRegOcorrs), LocatorUtils.property(thatLocator, "regOcorrs", rhsRegOcorrs), lhsRegOcorrs, rhsRegOcorrs)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            short theCdRetorno;
            theCdRetorno = this.getCdRetorno();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cdRetorno", theCdRetorno), currentHashCode, theCdRetorno);
        }
        {
            String theDescRetorno;
            theDescRetorno = this.getDescRetorno();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "descRetorno", theDescRetorno), currentHashCode, theDescRetorno);
        }
        {
            List<TRegistroOcorrencias> theRegOcorrs;
            theRegOcorrs = (((this.regOcorrs!= null)&&(!this.regOcorrs.isEmpty()))?this.getRegOcorrs():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "regOcorrs", theRegOcorrs), currentHashCode, theRegOcorrs);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
