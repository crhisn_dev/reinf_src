//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.25 �s 02:53:46 PM BRST 
//


package br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib }
     * 
     */
    public Reinf.EvtTotalContrib createReinfEvtTotalContrib() {
        return new Reinf.EvtTotalContrib();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.InfoTotalContrib }
     * 
     */
    public Reinf.EvtTotalContrib.InfoTotalContrib createReinfEvtTotalContribInfoTotalContrib() {
        return new Reinf.EvtTotalContrib.InfoTotalContrib();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.InfoTotalContrib.RTom }
     * 
     */
    public Reinf.EvtTotalContrib.InfoTotalContrib.RTom createReinfEvtTotalContribInfoTotalContribRTom() {
        return new Reinf.EvtTotalContrib.InfoTotalContrib.RTom();
    }

    /**
     * Create an instance of {@link TDadosProcessamentoEvento }
     * 
     */
    public TDadosProcessamentoEvento createTDadosProcessamentoEvento() {
        return new TDadosProcessamentoEvento();
    }

    /**
     * Create an instance of {@link TRegistroOcorrencias }
     * 
     */
    public TRegistroOcorrencias createTRegistroOcorrencias() {
        return new TRegistroOcorrencias();
    }

    /**
     * Create an instance of {@link TStatus }
     * 
     */
    public TStatus createTStatus() {
        return new TStatus();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.IdeEvento }
     * 
     */
    public Reinf.EvtTotalContrib.IdeEvento createReinfEvtTotalContribIdeEvento() {
        return new Reinf.EvtTotalContrib.IdeEvento();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.IdeContri }
     * 
     */
    public Reinf.EvtTotalContrib.IdeContri createReinfEvtTotalContribIdeContri() {
        return new Reinf.EvtTotalContrib.IdeContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.IdeRecRetorno }
     * 
     */
    public Reinf.EvtTotalContrib.IdeRecRetorno createReinfEvtTotalContribIdeRecRetorno() {
        return new Reinf.EvtTotalContrib.IdeRecRetorno();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.InfoTotalContrib.RPrest }
     * 
     */
    public Reinf.EvtTotalContrib.InfoTotalContrib.RPrest createReinfEvtTotalContribInfoTotalContribRPrest() {
        return new Reinf.EvtTotalContrib.InfoTotalContrib.RPrest();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD }
     * 
     */
    public Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD createReinfEvtTotalContribInfoTotalContribRRecRepAD() {
        return new Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.InfoTotalContrib.RComl }
     * 
     */
    public Reinf.EvtTotalContrib.InfoTotalContrib.RComl createReinfEvtTotalContribInfoTotalContribRComl() {
        return new Reinf.EvtTotalContrib.InfoTotalContrib.RComl();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB }
     * 
     */
    public Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB createReinfEvtTotalContribInfoTotalContribRCPRB() {
        return new Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom }
     * 
     */
    public Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom createReinfEvtTotalContribInfoTotalContribRTomInfoCRTom() {
        return new Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom();
    }

}
