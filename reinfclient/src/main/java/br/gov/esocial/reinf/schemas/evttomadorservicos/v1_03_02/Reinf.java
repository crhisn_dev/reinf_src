//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.10 �s 09:36:18 AM BRT 
//

package br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
// import javax.xml.datatype.String;
// import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.StringAsDateTime;
import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtServTom">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="indRetif">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;minInclusive value="1"/>
 *                                   &lt;maxInclusive value="2"/>
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrRecibo" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="16"/>
 *                                   &lt;maxLength value="52"/>
 *                                   &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="perApur">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                   &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;minInclusive value="1"/>
 *                                   &lt;maxInclusive value="2"/>
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoServTom">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ideEstabObra">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="tpInscEstab">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                             &lt;minInclusive value="1"/>
 *                                             &lt;maxInclusive value="4"/>
 *                                             &lt;pattern value="[1|4]"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="nrInscEstab">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;pattern value="[0-9]{1,14}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="indObra">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                             &lt;minInclusive value="0"/>
 *                                             &lt;maxInclusive value="2"/>
 *                                             &lt;pattern value="[0-2]"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="idePrestServ">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="cnpjPrestador">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="3"/>
 *                                                       &lt;maxLength value="14"/>
 *                                                       &lt;pattern value="[0-9]{3,14}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalBruto">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalBaseRet">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalRetPrinc">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalRetAdic" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalNRetAdic" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="indCPRB">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                       &lt;minInclusive value="0"/>
 *                                                       &lt;maxInclusive value="1"/>
 *                                                       &lt;pattern value="[0|1]"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="nfs" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="serie">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="5"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="numDocto">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="15"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="dtEmissaoNF">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="vlrBruto">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="obs" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="250"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="infoTpServ" maxOccurs="9">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="tpServico">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;length value="9"/>
 *                                                                           &lt;pattern value="[0-9]{9}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrBaseRet">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrRetencao">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrRetSub" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrNRetPrinc" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos15" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos20" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos25" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrAdicional" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrNRetAdic" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="tpProcRetPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                                 &lt;minInclusive value="1"/>
 *                                                                 &lt;maxInclusive value="2"/>
 *                                                                 &lt;pattern value="[1|2]{1}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="nrProcRetPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="21"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="codSuspPrinc" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;pattern value="[0-9]{0,14}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="valorPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="tpProcRetAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                                 &lt;minInclusive value="1"/>
 *                                                                 &lt;maxInclusive value="2"/>
 *                                                                 &lt;pattern value="[1|2]{1}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="nrProcRetAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="21"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="codSuspAdic" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;pattern value="[0-9]{0,14}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="valorAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtServTom" })
@XmlRootElement(name = "Reinf")
// @Entity(name = "Reinf")
@Table(name = "REINF__4")
@Inheritance(strategy = InheritanceType.JOINED)
public class Reinf implements Equals, HashCode {

	@XmlElement(required = true)
	protected Reinf.EvtServTom evtServTom;
	@XmlAttribute(name = "Hjid")
	@Transient
	protected Long hjid;

	/**
	 * Obt�m o valor da propriedade evtServTom.
	 * 
	 * @return possible object is {@link Reinf.EvtServTom }
	 * 
	 */
	@ManyToOne(targetEntity = Reinf.EvtServTom.class, cascade = { CascadeType.ALL })
	@JoinColumn(name = "EVT_SERV_TOM_REINF___4_HJID")
	public Reinf.EvtServTom getEvtServTom() {
		return evtServTom;
	}

	/**
	 * Define o valor da propriedade evtServTom.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtServTom }
	 * 
	 */
	public void setEvtServTom(Reinf.EvtServTom value) {
		this.evtServTom = value;
	}

	/**
	 * Obt�m o valor da propriedade hjid.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */

	@Column(name = "HJID")

	public Long getHjid() {
		return hjid;
	}

	/**
	 * Define o valor da propriedade hjid.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setHjid(Long value) {
		this.hjid = value;
	}

	public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
			EqualsStrategy strategy) {
		if (!(object instanceof Reinf)) {
			return false;
		}
		if (this == object) {
			return true;
		}
		final Reinf that = ((Reinf) object);
		{
			Reinf.EvtServTom lhsEvtServTom;
			lhsEvtServTom = this.getEvtServTom();
			Reinf.EvtServTom rhsEvtServTom;
			rhsEvtServTom = that.getEvtServTom();
			if (!strategy.equals(LocatorUtils.property(thisLocator, "evtServTom", lhsEvtServTom),
					LocatorUtils.property(thatLocator, "evtServTom", rhsEvtServTom), lhsEvtServTom, rhsEvtServTom)) {
				return false;
			}
		}
		return true;
	}

	public boolean equals(Object object) {
		final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
		return equals(null, null, object, strategy);
	}

	public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
		int currentHashCode = 1;
		{
			Reinf.EvtServTom theEvtServTom;
			theEvtServTom = this.getEvtServTom();
			currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtServTom", theEvtServTom),
					currentHashCode, theEvtServTom);
		}
		return currentHashCode;
	}

	public int hashCode() {
		final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
		return this.hashCode(null, strategy);
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
	 * desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="indRetif">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;minInclusive value="1"/>
	 *                         &lt;maxInclusive value="2"/>
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrRecibo" minOccurs="0">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="16"/>
	 *                         &lt;maxLength value="52"/>
	 *                         &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="perApur">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;minInclusive value="1"/>
	 *                         &lt;maxInclusive value="2"/>
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="infoServTom">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="ideEstabObra">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="tpInscEstab">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                   &lt;minInclusive value="1"/>
	 *                                   &lt;maxInclusive value="4"/>
	 *                                   &lt;pattern value="[1|4]"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="nrInscEstab">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                   &lt;pattern value="[0-9]{1,14}"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="indObra">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                   &lt;minInclusive value="0"/>
	 *                                   &lt;maxInclusive value="2"/>
	 *                                   &lt;pattern value="[0-2]"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="idePrestServ">
	 *                               &lt;complexType>
	 *                                 &lt;complexContent>
	 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                     &lt;sequence>
	 *                                       &lt;element name="cnpjPrestador">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="3"/>
	 *                                             &lt;maxLength value="14"/>
	 *                                             &lt;pattern value="[0-9]{3,14}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalBruto">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalBaseRet">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalRetPrinc">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalRetAdic" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalNRetAdic" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="indCPRB">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                             &lt;minInclusive value="0"/>
	 *                                             &lt;maxInclusive value="1"/>
	 *                                             &lt;pattern value="[0|1]"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="nfs" maxOccurs="unbounded">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="serie">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="5"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="numDocto">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="15"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="dtEmissaoNF">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="vlrBruto">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="obs" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="250"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="infoTpServ" maxOccurs="9">
	 *                                                   &lt;complexType>
	 *                                                     &lt;complexContent>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                         &lt;sequence>
	 *                                                           &lt;element name="tpServico">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;length value="9"/>
	 *                                                                 &lt;pattern value="[0-9]{9}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrBaseRet">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrRetencao">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrRetSub" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrNRetPrinc" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos15" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos20" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos25" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrAdicional" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrNRetAdic" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                         &lt;/sequence>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/complexContent>
	 *                                                   &lt;/complexType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="tpProcRetPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                                       &lt;minInclusive value="1"/>
	 *                                                       &lt;maxInclusive value="2"/>
	 *                                                       &lt;pattern value="[1|2]{1}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="nrProcRetPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="21"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="codSuspPrinc" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;pattern value="[0-9]{0,14}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="valorPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="tpProcRetAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                                       &lt;minInclusive value="1"/>
	 *                                                       &lt;maxInclusive value="2"/>
	 *                                                       &lt;pattern value="[1|2]{1}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="nrProcRetAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="21"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="codSuspAdic" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;pattern value="[0-9]{0,14}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="valorAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                     &lt;/sequence>
	 *                                   &lt;/restriction>
	 *                                 &lt;/complexContent>
	 *                               &lt;/complexType>
	 *                             &lt;/element>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri", "infoServTom" })
	@Entity(name = "Reinf$EvtServTom")
	@Table(name = "EVT_SERV_TOM")
	@Inheritance(strategy = InheritanceType.JOINED)
	@XmlRootElement(name = "evtServTom")
	public static class EvtServTom implements Equals, HashCode {

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtServTom.IdeEvento ideEvento;

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtServTom.IdeContri ideContri;
		@XmlElement(required = true)
		protected Reinf.EvtServTom.InfoServTom infoServTom;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;
		@XmlAttribute(name = "Hjid")
		@Id
		@Column(name = "ID")
		protected Long hjid;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtServTom.IdeEvento }
		 * 
		 */
		@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.IdeEvento.class, cascade = {
				CascadeType.ALL })
		@JoinColumn(name = "IDE_EVENTO_EVT_SERV_TOM_HJID")
		public Reinf.EvtServTom.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServTom.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtServTom.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtServTom.IdeContri }
		 * 
		 */
		// @ManyToOne(targetEntity =
		// br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.IdeContri.class,
		// cascade = {
		// CascadeType.ALL })
		// @JoinColumn(name = "IDE_CONTRI_EVT_SERV_TOM_HJID")
		public Reinf.EvtServTom.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServTom.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtServTom.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade infoServTom.
		 * 
		 * @return possible object is {@link Reinf.EvtServTom.InfoServTom }
		 * 
		 */
		@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.class, cascade = {
				CascadeType.ALL })
		@JoinColumn(name = "INFO_SERV_TOM_EVT_SERV_TOM_H_0")
		public Reinf.EvtServTom.InfoServTom getInfoServTom() {
			return infoServTom;
		}

		/**
		 * Define o valor da propriedade infoServTom.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServTom.InfoServTom }
		 * 
		 */
		public void setInfoServTom(Reinf.EvtServTom.InfoServTom value) {
			this.infoServTom = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		@Basic
		@Column(name = "ID", length = 36)
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * Obt�m o valor da propriedade hjid.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */

		@Column(name = "HJID")

		public Long getHjid() {
			return hjid;
		}

		/**
		 * Define o valor da propriedade hjid.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setHjid(Long value) {
			this.hjid = value;
		}

		public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
				EqualsStrategy strategy) {
			if (!(object instanceof Reinf.EvtServTom)) {
				return false;
			}
			if (this == object) {
				return true;
			}
			final Reinf.EvtServTom that = ((Reinf.EvtServTom) object);
			{
				Reinf.EvtServTom.IdeEvento lhsIdeEvento;
				lhsIdeEvento = this.getIdeEvento();
				Reinf.EvtServTom.IdeEvento rhsIdeEvento;
				rhsIdeEvento = that.getIdeEvento();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEvento", lhsIdeEvento),
						LocatorUtils.property(thatLocator, "ideEvento", rhsIdeEvento), lhsIdeEvento, rhsIdeEvento)) {
					return false;
				}
			}
			{
				Reinf.EvtServTom.IdeContri lhsIdeContri;
				lhsIdeContri = this.getIdeContri();
				Reinf.EvtServTom.IdeContri rhsIdeContri;
				rhsIdeContri = that.getIdeContri();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideContri", lhsIdeContri),
						LocatorUtils.property(thatLocator, "ideContri", rhsIdeContri), lhsIdeContri, rhsIdeContri)) {
					return false;
				}
			}
			{
				Reinf.EvtServTom.InfoServTom lhsInfoServTom;
				lhsInfoServTom = this.getInfoServTom();
				Reinf.EvtServTom.InfoServTom rhsInfoServTom;
				rhsInfoServTom = that.getInfoServTom();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "infoServTom", lhsInfoServTom),
						LocatorUtils.property(thatLocator, "infoServTom", rhsInfoServTom), lhsInfoServTom,
						rhsInfoServTom)) {
					return false;
				}
			}
			{
				String lhsId;
				lhsId = this.getId();
				String rhsId;
				rhsId = that.getId();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId),
						LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
					return false;
				}
			}
			return true;
		}

		public boolean equals(Object object) {
			final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
			return equals(null, null, object, strategy);
		}

		public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
			int currentHashCode = 1;
			{
				Reinf.EvtServTom.IdeEvento theIdeEvento;
				theIdeEvento = this.getIdeEvento();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEvento", theIdeEvento),
						currentHashCode, theIdeEvento);
			}
			{
				Reinf.EvtServTom.IdeContri theIdeContri;
				theIdeContri = this.getIdeContri();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideContri", theIdeContri),
						currentHashCode, theIdeContri);
			}
			{
				Reinf.EvtServTom.InfoServTom theInfoServTom;
				theInfoServTom = this.getInfoServTom();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoServTom", theInfoServTom),
						currentHashCode, theInfoServTom);
			}
			{
				String theId;
				theId = this.getId();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode,
						theId);
			}
			return currentHashCode;
		}

		public int hashCode() {
			final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
			return this.hashCode(null, strategy);
		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		// @Entity(name = "Reinf$EvtServTom$IdeContri")
		@Table(name = "IDE_CONTRI")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeContri implements Equals, HashCode {

			protected short tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			@Basic
			@Column(name = "TP_INSC", scale = 0)
			public short getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(short value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_INSC")
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */

			@Column(name = "HJID")

			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtServTom.IdeContri)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtServTom.IdeContri that = ((Reinf.EvtServTom.IdeContri) object);
				{
					short lhsTpInsc;
					lhsTpInsc = this.getTpInsc();
					short rhsTpInsc;
					rhsTpInsc = that.getTpInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInsc", lhsTpInsc),
							LocatorUtils.property(thatLocator, "tpInsc", rhsTpInsc), lhsTpInsc, rhsTpInsc)) {
						return false;
					}
				}
				{
					String lhsNrInsc;
					lhsNrInsc = this.getNrInsc();
					String rhsNrInsc;
					rhsNrInsc = that.getNrInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInsc", lhsNrInsc),
							LocatorUtils.property(thatLocator, "nrInsc", rhsNrInsc), lhsNrInsc, rhsNrInsc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					short theTpInsc;
					theTpInsc = this.getTpInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInsc", theTpInsc),
							currentHashCode, theTpInsc);
				}
				{
					String theNrInsc;
					theNrInsc = this.getNrInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInsc", theNrInsc),
							currentHashCode, theNrInsc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="indRetif">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;minInclusive value="1"/>
		 *               &lt;maxInclusive value="2"/>
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrRecibo" minOccurs="0">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="16"/>
		 *               &lt;maxLength value="52"/>
		 *               &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="perApur">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;minInclusive value="1"/>
		 *               &lt;maxInclusive value="2"/>
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "indRetif", "nrRecibo", "perApur", "tpAmb", "procEmi", "verProc" })
		// @Entity(name = "Reinf$EvtServTom$IdeEvento")
		@Table(name = "IDE_EVENTO")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeEvento implements Equals, HashCode {

			protected short indRetif;
			protected String nrRecibo;
			@XmlElement(required = true)
			protected String perApur;
			protected long tpAmb;
			protected short procEmi;
			@XmlElement(required = true)
			protected String verProc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade indRetif.
			 * 
			 */
			@Basic
			@Column(name = "IND_RETIF", scale = 0)
			public short getIndRetif() {
				return indRetif;
			}

			/**
			 * Define o valor da propriedade indRetif.
			 * 
			 */
			public void setIndRetif(short value) {
				this.indRetif = value;
			}

			/**
			 * Obt�m o valor da propriedade nrRecibo.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_RECIBO", length = 52)
			public String getNrRecibo() {
				return nrRecibo;
			}

			/**
			 * Define o valor da propriedade nrRecibo.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrRecibo(String value) {
				this.nrRecibo = value;
			}

			/**
			 * Obt�m o valor da propriedade perApur.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Transient
			public String getPerApur() {
				return perApur;
			}

			/**
			 * Define o valor da propriedade perApur.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPerApur(String value) {
				this.perApur = value;
			}

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			@Basic
			@Column(name = "TP_AMB", scale = 0)
			public long getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(long value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			@Basic
			@Column(name = "PROC_EMI", scale = 0)
			public short getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(short value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "VER_PROC", length = 20)
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */

			@Column(name = "HJID")

			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			/*
			 * @Basic
			 * 
			 * @Column(name = "PER_APUR_ITEM")
			 * 
			 * @Temporal(TemporalType.TIMESTAMP) public Date getPerApurItem() { return
			 * XmlAdapterUtils.unmarshall(StringAsDateTime.class, this.getPerApur()); }
			 * 
			 * public void setPerApurItem(Date target) {
			 * setPerApur(XmlAdapterUtils.marshall(StringAsDateTime.class, target)); }
			 */

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtServTom.IdeEvento)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtServTom.IdeEvento that = ((Reinf.EvtServTom.IdeEvento) object);
				{
					short lhsIndRetif;
					lhsIndRetif = this.getIndRetif();
					short rhsIndRetif;
					rhsIndRetif = that.getIndRetif();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "indRetif", lhsIndRetif),
							LocatorUtils.property(thatLocator, "indRetif", rhsIndRetif), lhsIndRetif, rhsIndRetif)) {
						return false;
					}
				}
				{
					String lhsNrRecibo;
					lhsNrRecibo = this.getNrRecibo();
					String rhsNrRecibo;
					rhsNrRecibo = that.getNrRecibo();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrRecibo", lhsNrRecibo),
							LocatorUtils.property(thatLocator, "nrRecibo", rhsNrRecibo), lhsNrRecibo, rhsNrRecibo)) {
						return false;
					}
				}
				{
					String lhsPerApur;
					lhsPerApur = this.getPerApur();
					String rhsPerApur;
					rhsPerApur = that.getPerApur();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "perApur", lhsPerApur),
							LocatorUtils.property(thatLocator, "perApur", rhsPerApur), lhsPerApur, rhsPerApur)) {
						return false;
					}
				}
				{
					long lhsTpAmb;
					lhsTpAmb = this.getTpAmb();
					long rhsTpAmb;
					rhsTpAmb = that.getTpAmb();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpAmb", lhsTpAmb),
							LocatorUtils.property(thatLocator, "tpAmb", rhsTpAmb), lhsTpAmb, rhsTpAmb)) {
						return false;
					}
				}
				{
					short lhsProcEmi;
					lhsProcEmi = this.getProcEmi();
					short rhsProcEmi;
					rhsProcEmi = that.getProcEmi();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "procEmi", lhsProcEmi),
							LocatorUtils.property(thatLocator, "procEmi", rhsProcEmi), lhsProcEmi, rhsProcEmi)) {
						return false;
					}
				}
				{
					String lhsVerProc;
					lhsVerProc = this.getVerProc();
					String rhsVerProc;
					rhsVerProc = that.getVerProc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "verProc", lhsVerProc),
							LocatorUtils.property(thatLocator, "verProc", rhsVerProc), lhsVerProc, rhsVerProc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					short theIndRetif;
					theIndRetif = this.getIndRetif();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indRetif", theIndRetif),
							currentHashCode, theIndRetif);
				}
				{
					String theNrRecibo;
					theNrRecibo = this.getNrRecibo();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrRecibo", theNrRecibo),
							currentHashCode, theNrRecibo);
				}
				{
					String thePerApur;
					thePerApur = this.getPerApur();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "perApur", thePerApur),
							currentHashCode, thePerApur);
				}
				{
					long theTpAmb;
					theTpAmb = this.getTpAmb();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpAmb", theTpAmb),
							currentHashCode, theTpAmb);
				}
				{
					short theProcEmi;
					theProcEmi = this.getProcEmi();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "procEmi", theProcEmi),
							currentHashCode, theProcEmi);
				}
				{
					String theVerProc;
					theVerProc = this.getVerProc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verProc", theVerProc),
							currentHashCode, theVerProc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="ideEstabObra">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="tpInscEstab">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                         &lt;minInclusive value="1"/>
		 *                         &lt;maxInclusive value="4"/>
		 *                         &lt;pattern value="[1|4]"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="nrInscEstab">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                         &lt;pattern value="[0-9]{1,14}"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="indObra">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                         &lt;minInclusive value="0"/>
		 *                         &lt;maxInclusive value="2"/>
		 *                         &lt;pattern value="[0-2]"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="idePrestServ">
		 *                     &lt;complexType>
		 *                       &lt;complexContent>
		 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                           &lt;sequence>
		 *                             &lt;element name="cnpjPrestador">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="3"/>
		 *                                   &lt;maxLength value="14"/>
		 *                                   &lt;pattern value="[0-9]{3,14}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalBruto">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalBaseRet">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalRetPrinc">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalRetAdic" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalNRetAdic" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="indCPRB">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                   &lt;minInclusive value="0"/>
		 *                                   &lt;maxInclusive value="1"/>
		 *                                   &lt;pattern value="[0|1]"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="nfs" maxOccurs="unbounded">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="serie">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="5"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="numDocto">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="15"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="dtEmissaoNF">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="vlrBruto">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="obs" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="250"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="infoTpServ" maxOccurs="9">
		 *                                         &lt;complexType>
		 *                                           &lt;complexContent>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                               &lt;sequence>
		 *                                                 &lt;element name="tpServico">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;length value="9"/>
		 *                                                       &lt;pattern value="[0-9]{9}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrBaseRet">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrRetencao">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrRetSub" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrNRetPrinc" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos15" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos20" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos25" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrAdicional" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrNRetAdic" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                               &lt;/sequence>
		 *                                             &lt;/restriction>
		 *                                           &lt;/complexContent>
		 *                                         &lt;/complexType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                             &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="tpProcRetPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                             &lt;minInclusive value="1"/>
		 *                                             &lt;maxInclusive value="2"/>
		 *                                             &lt;pattern value="[1|2]{1}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="nrProcRetPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="21"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="codSuspPrinc" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;pattern value="[0-9]{0,14}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="valorPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                             &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="tpProcRetAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                             &lt;minInclusive value="1"/>
		 *                                             &lt;maxInclusive value="2"/>
		 *                                             &lt;pattern value="[1|2]{1}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="nrProcRetAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="21"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="codSuspAdic" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;pattern value="[0-9]{0,14}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="valorAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                           &lt;/sequence>
		 *                         &lt;/restriction>
		 *                       &lt;/complexContent>
		 *                     &lt;/complexType>
		 *                   &lt;/element>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "ideEstabObra" })
		// @Entity(name = "Reinf$EvtServTom$InfoServTom")
		@Table(name = "INFO_SERV_TOM")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class InfoServTom implements Equals, HashCode {

			@XmlElement(required = true)
			protected Reinf.EvtServTom.InfoServTom.IdeEstabObra ideEstabObra;
			@XmlAttribute(name = "Hjid")
			@Transient
			@Id
			@Column(name="ID")
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade ideEstabObra.
			 * 
			 * @return possible object is {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra }
			 * 
			 */
			@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.class, cascade = {
					CascadeType.ALL })
			@JoinColumn(name = "IDE_ESTAB_OBRA_INFO_SERV_TOM_0")
			public Reinf.EvtServTom.InfoServTom.IdeEstabObra getIdeEstabObra() {
				return ideEstabObra;
			}

			/**
			 * Define o valor da propriedade ideEstabObra.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra }
			 * 
			 */
			public void setIdeEstabObra(Reinf.EvtServTom.InfoServTom.IdeEstabObra value) {
				this.ideEstabObra = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */

			@Column(name = "HJID")

			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtServTom.InfoServTom)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtServTom.InfoServTom that = ((Reinf.EvtServTom.InfoServTom) object);
				{
					Reinf.EvtServTom.InfoServTom.IdeEstabObra lhsIdeEstabObra;
					lhsIdeEstabObra = this.getIdeEstabObra();
					Reinf.EvtServTom.InfoServTom.IdeEstabObra rhsIdeEstabObra;
					rhsIdeEstabObra = that.getIdeEstabObra();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEstabObra", lhsIdeEstabObra),
							LocatorUtils.property(thatLocator, "ideEstabObra", rhsIdeEstabObra), lhsIdeEstabObra,
							rhsIdeEstabObra)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					Reinf.EvtServTom.InfoServTom.IdeEstabObra theIdeEstabObra;
					theIdeEstabObra = this.getIdeEstabObra();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEstabObra", theIdeEstabObra),
							currentHashCode, theIdeEstabObra);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
			 * desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="tpInscEstab">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *               &lt;minInclusive value="1"/>
			 *               &lt;maxInclusive value="4"/>
			 *               &lt;pattern value="[1|4]"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="nrInscEstab">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *               &lt;pattern value="[0-9]{1,14}"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="indObra">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *               &lt;minInclusive value="0"/>
			 *               &lt;maxInclusive value="2"/>
			 *               &lt;pattern value="[0-2]"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="idePrestServ">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="cnpjPrestador">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="3"/>
			 *                         &lt;maxLength value="14"/>
			 *                         &lt;pattern value="[0-9]{3,14}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalBruto">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalBaseRet">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalRetPrinc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalRetAdic" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalNRetAdic" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indCPRB">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;minInclusive value="0"/>
			 *                         &lt;maxInclusive value="1"/>
			 *                         &lt;pattern value="[0|1]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="nfs" maxOccurs="unbounded">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="serie">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="5"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="numDocto">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="15"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="dtEmissaoNF">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="vlrBruto">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="obs" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="250"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="infoTpServ" maxOccurs="9">
			 *                               &lt;complexType>
			 *                                 &lt;complexContent>
			 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                                     &lt;sequence>
			 *                                       &lt;element name="tpServico">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;length value="9"/>
			 *                                             &lt;pattern value="[0-9]{9}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrBaseRet">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrRetencao">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrRetSub" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrNRetPrinc" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos15" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos20" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos25" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrAdicional" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrNRetAdic" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                     &lt;/sequence>
			 *                                   &lt;/restriction>
			 *                                 &lt;/complexContent>
			 *                               &lt;/complexType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="tpProcRetPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                                   &lt;minInclusive value="1"/>
			 *                                   &lt;maxInclusive value="2"/>
			 *                                   &lt;pattern value="[1|2]{1}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nrProcRetPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="21"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codSuspPrinc" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="valorPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="tpProcRetAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                                   &lt;minInclusive value="1"/>
			 *                                   &lt;maxInclusive value="2"/>
			 *                                   &lt;pattern value="[1|2]{1}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nrProcRetAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="21"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codSuspAdic" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="valorAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "tpInscEstab", "nrInscEstab", "indObra", "idePrestServ" })
			// @Entity(name = "Reinf$EvtServTom$InfoServTom$IdeEstabObra")
			@Table(name = "IDE_ESTAB_OBRA")
			@Inheritance(strategy = InheritanceType.JOINED)
			@Embeddable
			public static class IdeEstabObra implements Equals, HashCode {

				protected short tpInscEstab;
				@XmlElement(required = true)
				protected String nrInscEstab;
				protected short indObra;
				@XmlElement(required = true)
				protected Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ idePrestServ;
				@XmlAttribute(name = "Hjid")
				@Transient
				protected Long hjid;

				/**
				 * Obt�m o valor da propriedade tpInscEstab.
				 * 
				 */
				@Basic
				@Column(name = "TP_INSC_ESTAB", scale = 0)
				public short getTpInscEstab() {
					return tpInscEstab;
				}

				/**
				 * Define o valor da propriedade tpInscEstab.
				 * 
				 */
				public void setTpInscEstab(short value) {
					this.tpInscEstab = value;
				}

				/**
				 * Obt�m o valor da propriedade nrInscEstab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				@Basic
				@Column(name = "NR_INSC_ESTAB")
				public String getNrInscEstab() {
					return nrInscEstab;
				}

				/**
				 * Define o valor da propriedade nrInscEstab.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setNrInscEstab(String value) {
					this.nrInscEstab = value;
				}

				/**
				 * Obt�m o valor da propriedade indObra.
				 * 
				 */
				@Basic
				@Column(name = "IND_OBRA", scale = 0)
				public short getIndObra() {
					return indObra;
				}

				/**
				 * Define o valor da propriedade indObra.
				 * 
				 */
				public void setIndObra(short value) {
					this.indObra = value;
				}

				/**
				 * Obt�m o valor da propriedade idePrestServ.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ }
				 * 
				 */
				@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.class, cascade = {
						CascadeType.ALL })
				@JoinColumn(name = "IDE_PREST_SERV_IDE_ESTAB_OBR_0")
				public Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ getIdePrestServ() {
					return idePrestServ;
				}

				/**
				 * Define o valor da propriedade idePrestServ.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ }
				 * 
				 */
				public void setIdePrestServ(Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ value) {
					this.idePrestServ = value;
				}

				/**
				 * Obt�m o valor da propriedade hjid.
				 * 
				 * @return possible object is {@link Long }
				 * 
				 */

				@Column(name = "HJID")

				public Long getHjid() {
					return hjid;
				}

				/**
				 * Define o valor da propriedade hjid.
				 * 
				 * @param value
				 *            allowed object is {@link Long }
				 * 
				 */
				public void setHjid(Long value) {
					this.hjid = value;
				}

				public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
						EqualsStrategy strategy) {
					if (!(object instanceof Reinf.EvtServTom.InfoServTom.IdeEstabObra)) {
						return false;
					}
					if (this == object) {
						return true;
					}
					final Reinf.EvtServTom.InfoServTom.IdeEstabObra that = ((Reinf.EvtServTom.InfoServTom.IdeEstabObra) object);
					{
						short lhsTpInscEstab;
						lhsTpInscEstab = this.getTpInscEstab();
						short rhsTpInscEstab;
						rhsTpInscEstab = that.getTpInscEstab();
						if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInscEstab", lhsTpInscEstab),
								LocatorUtils.property(thatLocator, "tpInscEstab", rhsTpInscEstab), lhsTpInscEstab,
								rhsTpInscEstab)) {
							return false;
						}
					}
					{
						String lhsNrInscEstab;
						lhsNrInscEstab = this.getNrInscEstab();
						String rhsNrInscEstab;
						rhsNrInscEstab = that.getNrInscEstab();
						if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInscEstab", lhsNrInscEstab),
								LocatorUtils.property(thatLocator, "nrInscEstab", rhsNrInscEstab), lhsNrInscEstab,
								rhsNrInscEstab)) {
							return false;
						}
					}
					{
						short lhsIndObra;
						lhsIndObra = this.getIndObra();
						short rhsIndObra;
						rhsIndObra = that.getIndObra();
						if (!strategy.equals(LocatorUtils.property(thisLocator, "indObra", lhsIndObra),
								LocatorUtils.property(thatLocator, "indObra", rhsIndObra), lhsIndObra, rhsIndObra)) {
							return false;
						}
					}
					{
						Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ lhsIdePrestServ;
						lhsIdePrestServ = this.getIdePrestServ();
						Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ rhsIdePrestServ;
						rhsIdePrestServ = that.getIdePrestServ();
						if (!strategy.equals(LocatorUtils.property(thisLocator, "idePrestServ", lhsIdePrestServ),
								LocatorUtils.property(thatLocator, "idePrestServ", rhsIdePrestServ), lhsIdePrestServ,
								rhsIdePrestServ)) {
							return false;
						}
					}
					return true;
				}

				public boolean equals(Object object) {
					final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
					return equals(null, null, object, strategy);
				}

				public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
					int currentHashCode = 1;
					{
						short theTpInscEstab;
						theTpInscEstab = this.getTpInscEstab();
						currentHashCode = strategy.hashCode(
								LocatorUtils.property(locator, "tpInscEstab", theTpInscEstab), currentHashCode,
								theTpInscEstab);
					}
					{
						String theNrInscEstab;
						theNrInscEstab = this.getNrInscEstab();
						currentHashCode = strategy.hashCode(
								LocatorUtils.property(locator, "nrInscEstab", theNrInscEstab), currentHashCode,
								theNrInscEstab);
					}
					{
						short theIndObra;
						theIndObra = this.getIndObra();
						currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indObra", theIndObra),
								currentHashCode, theIndObra);
					}
					{
						Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ theIdePrestServ;
						theIdePrestServ = this.getIdePrestServ();
						currentHashCode = strategy.hashCode(
								LocatorUtils.property(locator, "idePrestServ", theIdePrestServ), currentHashCode,
								theIdePrestServ);
					}
					return currentHashCode;
				}

				public int hashCode() {
					final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
					return this.hashCode(null, strategy);
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
				 * desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="cnpjPrestador">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="3"/>
				 *               &lt;maxLength value="14"/>
				 *               &lt;pattern value="[0-9]{3,14}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalBruto">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalBaseRet">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalRetPrinc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalRetAdic" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalNRetAdic" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indCPRB">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;minInclusive value="0"/>
				 *               &lt;maxInclusive value="1"/>
				 *               &lt;pattern value="[0|1]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="nfs" maxOccurs="unbounded">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="serie">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="5"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="numDocto">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="15"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="dtEmissaoNF">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="vlrBruto">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="obs" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="250"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="infoTpServ" maxOccurs="9">
				 *                     &lt;complexType>
				 *                       &lt;complexContent>
				 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                           &lt;sequence>
				 *                             &lt;element name="tpServico">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;length value="9"/>
				 *                                   &lt;pattern value="[0-9]{9}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrBaseRet">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrRetencao">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrRetSub" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrNRetPrinc" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos15" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos20" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos25" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrAdicional" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrNRetAdic" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                           &lt;/sequence>
				 *                         &lt;/restriction>
				 *                       &lt;/complexContent>
				 *                     &lt;/complexType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="tpProcRetPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *                         &lt;minInclusive value="1"/>
				 *                         &lt;maxInclusive value="2"/>
				 *                         &lt;pattern value="[1|2]{1}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nrProcRetPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="21"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codSuspPrinc" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="valorPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="tpProcRetAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *                         &lt;minInclusive value="1"/>
				 *                         &lt;maxInclusive value="2"/>
				 *                         &lt;pattern value="[1|2]{1}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nrProcRetAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="21"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codSuspAdic" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="valorAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "cnpjPrestador", "vlrTotalBruto", "vlrTotalBaseRet",
						"vlrTotalRetPrinc", "vlrTotalRetAdic", "vlrTotalNRetPrinc", "vlrTotalNRetAdic", "indCPRB",
						"nfs" })
				// @Entity(name = "Reinf$EvtServTom$InfoServTom$IdeEstabObra$IdePrestServ")
				@Table(name = "IDE_PREST_SERV")
				@Inheritance(strategy = InheritanceType.JOINED)
				@Embeddable
				public static class IdePrestServ implements Equals, HashCode {

					@XmlElement(required = true)
					protected String cnpjPrestador;
					@XmlElement(required = true)
					protected String vlrTotalBruto;
					@XmlElement(required = true)
					protected String vlrTotalBaseRet;
					@XmlElement(required = true)
					protected String vlrTotalRetPrinc;
					protected String vlrTotalRetAdic;
					protected String vlrTotalNRetPrinc;
					protected String vlrTotalNRetAdic;
					protected short indCPRB;
					@XmlElement(required = true)

					@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.class, cascade = {
							CascadeType.ALL }, fetch=FetchType.EAGER)					
					@JoinColumn(name = "NFS_IDE_TOMADOR_HJID")
					
					protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> nfs;

					/*@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "INFO_PROC_RET_PR_IDE_PREST_S_0")
					@LazyCollection(LazyCollectionOption.FALSE)
					protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> infoProcRetPr;

					@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "INFO_PROC_RET_AD_IDE_PREST_S_0")
					@LazyCollection(LazyCollectionOption.FALSE)
					protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> infoProcRetAd;
					*/
					
					@XmlAttribute(name = "Hjid")
					@Transient
					protected Long hjid;

					/**
					 * Obt�m o valor da propriedade cnpjPrestador.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "CNPJ_PRESTADOR", length = 14)
					public String getCnpjPrestador() {
						return cnpjPrestador;
					}

					/**
					 * Define o valor da propriedade cnpjPrestador.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setCnpjPrestador(String value) {
						this.cnpjPrestador = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalBruto.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_BRUTO", length = 17)
					public String getVlrTotalBruto() {
						return vlrTotalBruto;
					}

					/**
					 * Define o valor da propriedade vlrTotalBruto.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalBruto(String value) {
						this.vlrTotalBruto = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalBaseRet.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_BASE_RET", length = 17)
					public String getVlrTotalBaseRet() {
						return vlrTotalBaseRet;
					}

					/**
					 * Define o valor da propriedade vlrTotalBaseRet.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalBaseRet(String value) {
						this.vlrTotalBaseRet = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalRetPrinc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_RET_PRINC", length = 17)
					public String getVlrTotalRetPrinc() {
						return vlrTotalRetPrinc;
					}

					/**
					 * Define o valor da propriedade vlrTotalRetPrinc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalRetPrinc(String value) {
						this.vlrTotalRetPrinc = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalRetAdic.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_RET_ADIC", length = 17)
					public String getVlrTotalRetAdic() {
						return vlrTotalRetAdic;
					}

					/**
					 * Define o valor da propriedade vlrTotalRetAdic.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalRetAdic(String value) {
						this.vlrTotalRetAdic = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalNRetPrinc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_NRET_PRINC", length = 17)
					public String getVlrTotalNRetPrinc() {
						return vlrTotalNRetPrinc;
					}

					/**
					 * Define o valor da propriedade vlrTotalNRetPrinc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalNRetPrinc(String value) {
						this.vlrTotalNRetPrinc = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalNRetAdic.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_NRET_ADIC", length = 17)
					public String getVlrTotalNRetAdic() {
						return vlrTotalNRetAdic;
					}

					/**
					 * Define o valor da propriedade vlrTotalNRetAdic.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalNRetAdic(String value) {
						this.vlrTotalNRetAdic = value;
					}

					/**
					 * Obt�m o valor da propriedade indCPRB.
					 * 
					 */
					@Basic
					@Column(name = "IND_CPRB", scale = 0)
					public short getIndCPRB() {
						return indCPRB;
					}

					/**
					 * Define o valor da propriedade indCPRB.
					 * 
					 */
					public void setIndCPRB(short value) {
						this.indCPRB = value;
					}

					/**
					 * Gets the value of the nfs property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live list, not a snapshot.
					 * Therefore any modification you make to the returned list will be present
					 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
					 * for the nfs property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getNfs().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs }
					 * 
					 * 
					 */
					@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "NFS_IDE_PREST_SERV_HJID")
					public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> getNfs() {
						if (nfs == null) {
							nfs = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs>();
						}
						return this.nfs;
					}

					/**
					 * 
					 * 
					 */
					public void setNfs(List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> nfs) {
						this.nfs = nfs;
					}

					/**
					 * Gets the value of the infoProcRetPr property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live list, not a snapshot.
					 * Therefore any modification you make to the returned list will be present
					 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
					 * for the infoProcRetPr property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoProcRetPr().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr }
					 * 
					 * 
					 */
					/*@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "INFO_PROC_RET_PR_IDE_PREST_S_0")
					public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> getInfoProcRetPr() {
						if (infoProcRetPr == null) {
							infoProcRetPr = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr>();
						}
						return this.infoProcRetPr;
					}

					*//**
					 * 
					 * 
					 *//*
					public void setInfoProcRetPr(
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> infoProcRetPr) {
						this.infoProcRetPr = infoProcRetPr;
					}
*/
					/**
					 * Gets the value of the infoProcRetAd property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live list, not a snapshot.
					 * Therefore any modification you make to the returned list will be present
					 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
					 * for the infoProcRetAd property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoProcRetAd().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd }
					 * 
					 * 
					 */
					/*@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "INFO_PROC_RET_AD_IDE_PREST_S_0")
					public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> getInfoProcRetAd() {
						if (infoProcRetAd == null) {
							infoProcRetAd = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd>();
						}
						return this.infoProcRetAd;
					}

					*//**
					 * 
					 * 
					 *//*
					public void setInfoProcRetAd(
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> infoProcRetAd) {
						this.infoProcRetAd = infoProcRetAd;
					}*/

					/**
					 * Obt�m o valor da propriedade hjid.
					 * 
					 * @return possible object is {@link Long }
					 * 
					 */

					@Column(name = "HJID")

					public Long getHjid() {
						return hjid;
					}

					/**
					 * Define o valor da propriedade hjid.
					 * 
					 * @param value
					 *            allowed object is {@link Long }
					 * 
					 */
					public void setHjid(Long value) {
						this.hjid = value;
					}

					public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
							EqualsStrategy strategy) {
						if (!(object instanceof Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ)) {
							return false;
						}
						if (this == object) {
							return true;
						}
						final Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ that = ((Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ) object);
						{
							String lhsCnpjPrestador;
							lhsCnpjPrestador = this.getCnpjPrestador();
							String rhsCnpjPrestador;
							rhsCnpjPrestador = that.getCnpjPrestador();
							if (!strategy.equals(LocatorUtils.property(thisLocator, "cnpjPrestador", lhsCnpjPrestador),
									LocatorUtils.property(thatLocator, "cnpjPrestador", rhsCnpjPrestador),
									lhsCnpjPrestador, rhsCnpjPrestador)) {
								return false;
							}
						}
						{
							String lhsVlrTotalBruto;
							lhsVlrTotalBruto = this.getVlrTotalBruto();
							String rhsVlrTotalBruto;
							rhsVlrTotalBruto = that.getVlrTotalBruto();
							if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalBruto", lhsVlrTotalBruto),
									LocatorUtils.property(thatLocator, "vlrTotalBruto", rhsVlrTotalBruto),
									lhsVlrTotalBruto, rhsVlrTotalBruto)) {
								return false;
							}
						}
						{
							String lhsVlrTotalBaseRet;
							lhsVlrTotalBaseRet = this.getVlrTotalBaseRet();
							String rhsVlrTotalBaseRet;
							rhsVlrTotalBaseRet = that.getVlrTotalBaseRet();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalBaseRet", lhsVlrTotalBaseRet),
									LocatorUtils.property(thatLocator, "vlrTotalBaseRet", rhsVlrTotalBaseRet),
									lhsVlrTotalBaseRet, rhsVlrTotalBaseRet)) {
								return false;
							}
						}
						{
							String lhsVlrTotalRetPrinc;
							lhsVlrTotalRetPrinc = this.getVlrTotalRetPrinc();
							String rhsVlrTotalRetPrinc;
							rhsVlrTotalRetPrinc = that.getVlrTotalRetPrinc();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalRetPrinc", lhsVlrTotalRetPrinc),
									LocatorUtils.property(thatLocator, "vlrTotalRetPrinc", rhsVlrTotalRetPrinc),
									lhsVlrTotalRetPrinc, rhsVlrTotalRetPrinc)) {
								return false;
							}
						}
						{
							String lhsVlrTotalRetAdic;
							lhsVlrTotalRetAdic = this.getVlrTotalRetAdic();
							String rhsVlrTotalRetAdic;
							rhsVlrTotalRetAdic = that.getVlrTotalRetAdic();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalRetAdic", lhsVlrTotalRetAdic),
									LocatorUtils.property(thatLocator, "vlrTotalRetAdic", rhsVlrTotalRetAdic),
									lhsVlrTotalRetAdic, rhsVlrTotalRetAdic)) {
								return false;
							}
						}
						{
							String lhsVlrTotalNRetPrinc;
							lhsVlrTotalNRetPrinc = this.getVlrTotalNRetPrinc();
							String rhsVlrTotalNRetPrinc;
							rhsVlrTotalNRetPrinc = that.getVlrTotalNRetPrinc();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalNRetPrinc", lhsVlrTotalNRetPrinc),
									LocatorUtils.property(thatLocator, "vlrTotalNRetPrinc", rhsVlrTotalNRetPrinc),
									lhsVlrTotalNRetPrinc, rhsVlrTotalNRetPrinc)) {
								return false;
							}
						}
						{
							String lhsVlrTotalNRetAdic;
							lhsVlrTotalNRetAdic = this.getVlrTotalNRetAdic();
							String rhsVlrTotalNRetAdic;
							rhsVlrTotalNRetAdic = that.getVlrTotalNRetAdic();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalNRetAdic", lhsVlrTotalNRetAdic),
									LocatorUtils.property(thatLocator, "vlrTotalNRetAdic", rhsVlrTotalNRetAdic),
									lhsVlrTotalNRetAdic, rhsVlrTotalNRetAdic)) {
								return false;
							}
						}
						{
							short lhsIndCPRB;
							lhsIndCPRB = this.getIndCPRB();
							short rhsIndCPRB;
							rhsIndCPRB = that.getIndCPRB();
							if (!strategy.equals(LocatorUtils.property(thisLocator, "indCPRB", lhsIndCPRB),
									LocatorUtils.property(thatLocator, "indCPRB", rhsIndCPRB), lhsIndCPRB,
									rhsIndCPRB)) {
								return false;
							}
						}
						{
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> lhsNfs;
							lhsNfs = (((this.nfs != null) && (!this.nfs.isEmpty())) ? this.getNfs() : null);
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> rhsNfs;
							rhsNfs = (((that.nfs != null) && (!that.nfs.isEmpty())) ? that.getNfs() : null);
							if (!strategy.equals(LocatorUtils.property(thisLocator, "nfs", lhsNfs),
									LocatorUtils.property(thatLocator, "nfs", rhsNfs), lhsNfs, rhsNfs)) {
								return false;
							}
						}
						/*{
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> lhsInfoProcRetPr;
							lhsInfoProcRetPr = (((this.infoProcRetPr != null) && (!this.infoProcRetPr.isEmpty()))
									? this.getInfoProcRetPr()
									: null);
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> rhsInfoProcRetPr;
							rhsInfoProcRetPr = (((that.infoProcRetPr != null) && (!that.infoProcRetPr.isEmpty()))
									? that.getInfoProcRetPr()
									: null);
							if (!strategy.equals(LocatorUtils.property(thisLocator, "infoProcRetPr", lhsInfoProcRetPr),
									LocatorUtils.property(thatLocator, "infoProcRetPr", rhsInfoProcRetPr),
									lhsInfoProcRetPr, rhsInfoProcRetPr)) {
								return false;
							}
						}*/
						/*{
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> lhsInfoProcRetAd;
							lhsInfoProcRetAd = (((this.infoProcRetAd != null) && (!this.infoProcRetAd.isEmpty()))
									? this.getInfoProcRetAd()
									: null);
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> rhsInfoProcRetAd;
							rhsInfoProcRetAd = (((that.infoProcRetAd != null) && (!that.infoProcRetAd.isEmpty()))
									? that.getInfoProcRetAd()
									: null);
							if (!strategy.equals(LocatorUtils.property(thisLocator, "infoProcRetAd", lhsInfoProcRetAd),
									LocatorUtils.property(thatLocator, "infoProcRetAd", rhsInfoProcRetAd),
									lhsInfoProcRetAd, rhsInfoProcRetAd)) {
								return false;
							}
						}*/
						return true;
					}

					public boolean equals(Object object) {
						final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
						return equals(null, null, object, strategy);
					}

					public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
						int currentHashCode = 1;
						{
							String theCnpjPrestador;
							theCnpjPrestador = this.getCnpjPrestador();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "cnpjPrestador", theCnpjPrestador), currentHashCode,
									theCnpjPrestador);
						}
						{
							String theVlrTotalBruto;
							theVlrTotalBruto = this.getVlrTotalBruto();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalBruto", theVlrTotalBruto), currentHashCode,
									theVlrTotalBruto);
						}
						{
							String theVlrTotalBaseRet;
							theVlrTotalBaseRet = this.getVlrTotalBaseRet();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalBaseRet", theVlrTotalBaseRet),
									currentHashCode, theVlrTotalBaseRet);
						}
						{
							String theVlrTotalRetPrinc;
							theVlrTotalRetPrinc = this.getVlrTotalRetPrinc();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalRetPrinc", theVlrTotalRetPrinc),
									currentHashCode, theVlrTotalRetPrinc);
						}
						{
							String theVlrTotalRetAdic;
							theVlrTotalRetAdic = this.getVlrTotalRetAdic();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalRetAdic", theVlrTotalRetAdic),
									currentHashCode, theVlrTotalRetAdic);
						}
						{
							String theVlrTotalNRetPrinc;
							theVlrTotalNRetPrinc = this.getVlrTotalNRetPrinc();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalNRetPrinc", theVlrTotalNRetPrinc),
									currentHashCode, theVlrTotalNRetPrinc);
						}
						{
							String theVlrTotalNRetAdic;
							theVlrTotalNRetAdic = this.getVlrTotalNRetAdic();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalNRetAdic", theVlrTotalNRetAdic),
									currentHashCode, theVlrTotalNRetAdic);
						}
						{
							short theIndCPRB;
							theIndCPRB = this.getIndCPRB();
							currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indCPRB", theIndCPRB),
									currentHashCode, theIndCPRB);
						}
						{
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> theNfs;
							theNfs = (((this.nfs != null) && (!this.nfs.isEmpty())) ? this.getNfs() : null);
							currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nfs", theNfs),
									currentHashCode, theNfs);
						}
						/*{
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> theInfoProcRetPr;
							theInfoProcRetPr = (((this.infoProcRetPr != null) && (!this.infoProcRetPr.isEmpty()))
									? this.getInfoProcRetPr()
									: null);
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "infoProcRetPr", theInfoProcRetPr), currentHashCode,
									theInfoProcRetPr);
						}
						{
							List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> theInfoProcRetAd;
							theInfoProcRetAd = (((this.infoProcRetAd != null) && (!this.infoProcRetAd.isEmpty()))
									? this.getInfoProcRetAd()
									: null);
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "infoProcRetAd", theInfoProcRetAd), currentHashCode,
									theInfoProcRetAd);
						}*/
						return currentHashCode;
					}

					public int hashCode() {
						final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
						return this.hashCode(null, strategy);
					}

					/**
					 * Informa��es de processos relacionados a n�o reten��o de contribui��o
					 * previdenci�ria adicional
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
					 * desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="tpProcRetAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
					 *               &lt;minInclusive value="1"/>
					 *               &lt;maxInclusive value="2"/>
					 *               &lt;pattern value="[1|2]{1}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nrProcRetAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="21"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codSuspAdic" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="valorAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "tpProcRetAdic", "nrProcRetAdic", "codSuspAdic", "valorAdic" })
					@Entity(name = "Reinf$EvtServTom$InfoServTom$IdeEstabObra$IdePrestServ$InfoProcRetAd")
					@Table(name = "INFO_PROC_RET_AD")
					@Inheritance(strategy = InheritanceType.JOINED)
					public static class InfoProcRetAd implements Equals, HashCode {

						protected short tpProcRetAdic;
						@XmlElement(required = true)
						protected String nrProcRetAdic;
						protected String codSuspAdic;
						@XmlElement(required = true)
						protected String valorAdic;
						@XmlAttribute(name = "Hjid")
						@Id
						@Column(name = "ID")
						protected Long hjid;

						/**
						 * Obt�m o valor da propriedade tpProcRetAdic.
						 * 
						 */
						@Basic
						@Column(name = "TP_PROC_RET_ADIC", scale = 0)
						public short getTpProcRetAdic() {
							return tpProcRetAdic;
						}

						/**
						 * Define o valor da propriedade tpProcRetAdic.
						 * 
						 */
						public void setTpProcRetAdic(short value) {
							this.tpProcRetAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade nrProcRetAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "NR_PROC_RET_ADIC", length = 21)
						public String getNrProcRetAdic() {
							return nrProcRetAdic;
						}

						/**
						 * Define o valor da propriedade nrProcRetAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNrProcRetAdic(String value) {
							this.nrProcRetAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade codSuspAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "COD_SUSP_ADIC")
						public String getCodSuspAdic() {
							return codSuspAdic;
						}

						/**
						 * Define o valor da propriedade codSuspAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSuspAdic(String value) {
							this.codSuspAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade valorAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "VALOR_ADIC", length = 17)
						public String getValorAdic() {
							return valorAdic;
						}

						/**
						 * Define o valor da propriedade valorAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setValorAdic(String value) {
							this.valorAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade hjid.
						 * 
						 * @return possible object is {@link Long }
						 * 
						 */

						@Column(name = "HJID")

						public Long getHjid() {
							return hjid;
						}

						/**
						 * Define o valor da propriedade hjid.
						 * 
						 * @param value
						 *            allowed object is {@link Long }
						 * 
						 */
						public void setHjid(Long value) {
							this.hjid = value;
						}

						public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
								EqualsStrategy strategy) {
							if (!(object instanceof Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd)) {
								return false;
							}
							if (this == object) {
								return true;
							}
							final Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd that = ((Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd) object);
							{
								short lhsTpProcRetAdic;
								lhsTpProcRetAdic = this.getTpProcRetAdic();
								short rhsTpProcRetAdic;
								rhsTpProcRetAdic = that.getTpProcRetAdic();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "tpProcRetAdic", lhsTpProcRetAdic),
										LocatorUtils.property(thatLocator, "tpProcRetAdic", rhsTpProcRetAdic),
										lhsTpProcRetAdic, rhsTpProcRetAdic)) {
									return false;
								}
							}
							{
								String lhsNrProcRetAdic;
								lhsNrProcRetAdic = this.getNrProcRetAdic();
								String rhsNrProcRetAdic;
								rhsNrProcRetAdic = that.getNrProcRetAdic();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "nrProcRetAdic", lhsNrProcRetAdic),
										LocatorUtils.property(thatLocator, "nrProcRetAdic", rhsNrProcRetAdic),
										lhsNrProcRetAdic, rhsNrProcRetAdic)) {
									return false;
								}
							}
							{
								String lhsCodSuspAdic;
								lhsCodSuspAdic = this.getCodSuspAdic();
								String rhsCodSuspAdic;
								rhsCodSuspAdic = that.getCodSuspAdic();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "codSuspAdic", lhsCodSuspAdic),
										LocatorUtils.property(thatLocator, "codSuspAdic", rhsCodSuspAdic),
										lhsCodSuspAdic, rhsCodSuspAdic)) {
									return false;
								}
							}
							{
								String lhsValorAdic;
								lhsValorAdic = this.getValorAdic();
								String rhsValorAdic;
								rhsValorAdic = that.getValorAdic();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "valorAdic", lhsValorAdic),
										LocatorUtils.property(thatLocator, "valorAdic", rhsValorAdic), lhsValorAdic,
										rhsValorAdic)) {
									return false;
								}
							}
							return true;
						}

						public boolean equals(Object object) {
							final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
							return equals(null, null, object, strategy);
						}

						public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
							int currentHashCode = 1;
							{
								short theTpProcRetAdic;
								theTpProcRetAdic = this.getTpProcRetAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "tpProcRetAdic", theTpProcRetAdic),
										currentHashCode, theTpProcRetAdic);
							}
							{
								String theNrProcRetAdic;
								theNrProcRetAdic = this.getNrProcRetAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "nrProcRetAdic", theNrProcRetAdic),
										currentHashCode, theNrProcRetAdic);
							}
							{
								String theCodSuspAdic;
								theCodSuspAdic = this.getCodSuspAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "codSuspAdic", theCodSuspAdic), currentHashCode,
										theCodSuspAdic);
							}
							{
								String theValorAdic;
								theValorAdic = this.getValorAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "valorAdic", theValorAdic), currentHashCode,
										theValorAdic);
							}
							return currentHashCode;
						}

						public int hashCode() {
							final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
							return this.hashCode(null, strategy);
						}

					}

					/**
					 * Informa��es de processos relacionados a n�o reten��o de contribui��o
					 * previdenci�ria
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
					 * desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="tpProcRetPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
					 *               &lt;minInclusive value="1"/>
					 *               &lt;maxInclusive value="2"/>
					 *               &lt;pattern value="[1|2]{1}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nrProcRetPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="21"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codSuspPrinc" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="valorPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "tpProcRetPrinc", "nrProcRetPrinc", "codSuspPrinc",
							"valorPrinc" })
					@Entity(name = "Reinf$EvtServTom$InfoServTom$IdeEstabObra$IdePrestServ$InfoProcRetPr")
					@Table(name = "INFO_PROC_RET_PR")
					@Inheritance(strategy = InheritanceType.JOINED)
					public static class InfoProcRetPr implements Equals, HashCode {

						protected short tpProcRetPrinc;
						@XmlElement(required = true)
						protected String nrProcRetPrinc;
						protected String codSuspPrinc;
						@XmlElement(required = true)
						protected String valorPrinc;
						@XmlAttribute(name = "Hjid")

						@Id
						@Column(name = "ID")
						protected Long hjid;

						/**
						 * Obt�m o valor da propriedade tpProcRetPrinc.
						 * 
						 */
						@Basic
						@Column(name = "TP_PROC_RET_PRINC", scale = 0)
						public short getTpProcRetPrinc() {
							return tpProcRetPrinc;
						}

						/**
						 * Define o valor da propriedade tpProcRetPrinc.
						 * 
						 */
						public void setTpProcRetPrinc(short value) {
							this.tpProcRetPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade nrProcRetPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "NR_PROC_RET_PRINC", length = 21)
						public String getNrProcRetPrinc() {
							return nrProcRetPrinc;
						}

						/**
						 * Define o valor da propriedade nrProcRetPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNrProcRetPrinc(String value) {
							this.nrProcRetPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade codSuspPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "COD_SUSP_PRINC")
						public String getCodSuspPrinc() {
							return codSuspPrinc;
						}

						/**
						 * Define o valor da propriedade codSuspPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSuspPrinc(String value) {
							this.codSuspPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade valorPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "VALOR_PRINC", length = 17)
						public String getValorPrinc() {
							return valorPrinc;
						}

						/**
						 * Define o valor da propriedade valorPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setValorPrinc(String value) {
							this.valorPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade hjid.
						 * 
						 * @return possible object is {@link Long }
						 * 
						 */

						@Column(name = "HJID")

						public Long getHjid() {
							return hjid;
						}

						/**
						 * Define o valor da propriedade hjid.
						 * 
						 * @param value
						 *            allowed object is {@link Long }
						 * 
						 */
						public void setHjid(Long value) {
							this.hjid = value;
						}

						public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
								EqualsStrategy strategy) {
							if (!(object instanceof Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr)) {
								return false;
							}
							if (this == object) {
								return true;
							}
							final Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr that = ((Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr) object);
							{
								short lhsTpProcRetPrinc;
								lhsTpProcRetPrinc = this.getTpProcRetPrinc();
								short rhsTpProcRetPrinc;
								rhsTpProcRetPrinc = that.getTpProcRetPrinc();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "tpProcRetPrinc", lhsTpProcRetPrinc),
										LocatorUtils.property(thatLocator, "tpProcRetPrinc", rhsTpProcRetPrinc),
										lhsTpProcRetPrinc, rhsTpProcRetPrinc)) {
									return false;
								}
							}
							{
								String lhsNrProcRetPrinc;
								lhsNrProcRetPrinc = this.getNrProcRetPrinc();
								String rhsNrProcRetPrinc;
								rhsNrProcRetPrinc = that.getNrProcRetPrinc();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "nrProcRetPrinc", lhsNrProcRetPrinc),
										LocatorUtils.property(thatLocator, "nrProcRetPrinc", rhsNrProcRetPrinc),
										lhsNrProcRetPrinc, rhsNrProcRetPrinc)) {
									return false;
								}
							}
							{
								String lhsCodSuspPrinc;
								lhsCodSuspPrinc = this.getCodSuspPrinc();
								String rhsCodSuspPrinc;
								rhsCodSuspPrinc = that.getCodSuspPrinc();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "codSuspPrinc", lhsCodSuspPrinc),
										LocatorUtils.property(thatLocator, "codSuspPrinc", rhsCodSuspPrinc),
										lhsCodSuspPrinc, rhsCodSuspPrinc)) {
									return false;
								}
							}
							{
								String lhsValorPrinc;
								lhsValorPrinc = this.getValorPrinc();
								String rhsValorPrinc;
								rhsValorPrinc = that.getValorPrinc();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "valorPrinc", lhsValorPrinc),
										LocatorUtils.property(thatLocator, "valorPrinc", rhsValorPrinc), lhsValorPrinc,
										rhsValorPrinc)) {
									return false;
								}
							}
							return true;
						}

						public boolean equals(Object object) {
							final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
							return equals(null, null, object, strategy);
						}

						public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
							int currentHashCode = 1;
							{
								short theTpProcRetPrinc;
								theTpProcRetPrinc = this.getTpProcRetPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "tpProcRetPrinc", theTpProcRetPrinc),
										currentHashCode, theTpProcRetPrinc);
							}
							{
								String theNrProcRetPrinc;
								theNrProcRetPrinc = this.getNrProcRetPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "nrProcRetPrinc", theNrProcRetPrinc),
										currentHashCode, theNrProcRetPrinc);
							}
							{
								String theCodSuspPrinc;
								theCodSuspPrinc = this.getCodSuspPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "codSuspPrinc", theCodSuspPrinc),
										currentHashCode, theCodSuspPrinc);
							}
							{
								String theValorPrinc;
								theValorPrinc = this.getValorPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "valorPrinc", theValorPrinc), currentHashCode,
										theValorPrinc);
							}
							return currentHashCode;
						}

						public int hashCode() {
							final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
							return this.hashCode(null, strategy);
						}

					}

					/**
					 * 
					 * Detalhamento das notas fiscais de servi�os prestados pela empresa
					 * identificada no registro superior.
					 * 
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
					 * desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="serie">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="5"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="numDocto">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="15"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="dtEmissaoNF">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="vlrBruto">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="obs" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="250"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="infoTpServ" maxOccurs="9">
					 *           &lt;complexType>
					 *             &lt;complexContent>
					 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *                 &lt;sequence>
					 *                   &lt;element name="tpServico">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;length value="9"/>
					 *                         &lt;pattern value="[0-9]{9}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrBaseRet">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrRetencao">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrRetSub" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrNRetPrinc" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos15" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos20" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos25" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrAdicional" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrNRetAdic" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                 &lt;/sequence>
					 *               &lt;/restriction>
					 *             &lt;/complexContent>
					 *           &lt;/complexType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "serie", "numDocto", "dtEmissaoNF", "vlrBruto", "obs",
							"infoTpServ" })
					@Entity(name = "Reinf$EvtServTom$InfoServTom$IdeEstabObra$IdePrestServ$Nfs")
					@Table(name = "NFS")
					@Inheritance(strategy = InheritanceType.JOINED)
					public static class Nfs implements Equals, HashCode {

						@XmlElement(required = true)
						protected String serie;
						@XmlElement(required = true)
						protected String numDocto;
						@XmlElement(required = true)
						protected String dtEmissaoNF;
						@XmlElement(required = true)
						protected String vlrBruto;
						protected String obs;
						@XmlElement(required = true)
						@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ.class, cascade = {
								CascadeType.ALL },fetch=FetchType.EAGER)
						@JoinColumn(name = "INFO_TP_SERV_NFS_HJID")
						protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> infoTpServ;
						@XmlAttribute(name = "Hjid")
						@Id
						@Column(name = "ID")
						protected Long hjid;

						/**
						 * Obt�m o valor da propriedade serie.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "SERIE", length = 5)
						public String getSerie() {
							return serie;
						}

						/**
						 * Define o valor da propriedade serie.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setSerie(String value) {
							this.serie = value;
						}

						/**
						 * Obt�m o valor da propriedade numDocto.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "NUM_DOCTO", length = 15)
						public String getNumDocto() {
							return numDocto;
						}

						/**
						 * Define o valor da propriedade numDocto.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNumDocto(String value) {
							this.numDocto = value;
						}

						/**
						 * Obt�m o valor da propriedade dtEmissaoNF.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Transient
						public String getDtEmissaoNF() {
							return dtEmissaoNF;
						}

						/**
						 * Define o valor da propriedade dtEmissaoNF.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setDtEmissaoNF(String value) {
							this.dtEmissaoNF = value;
						}

						/**
						 * Obt�m o valor da propriedade vlrBruto.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "VLR_BRUTO", length = 17)
						public String getVlrBruto() {
							return vlrBruto;
						}

						/**
						 * Define o valor da propriedade vlrBruto.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setVlrBruto(String value) {
							this.vlrBruto = value;
						}

						/**
						 * Obt�m o valor da propriedade obs.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "OBS", length = 250)
						public String getObs() {
							return obs;
						}

						/**
						 * Define o valor da propriedade obs.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setObs(String value) {
							this.obs = value;
						}

						/**
						 * Gets the value of the infoTpServ property.
						 * 
						 * <p>
						 * This accessor method returns a reference to the live list, not a snapshot.
						 * Therefore any modification you make to the returned list will be present
						 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
						 * for the infoTpServ property.
						 * 
						 * <p>
						 * For example, to add a new item, do as follows:
						 * 
						 * <pre>
						 * getInfoTpServ().add(newItem);
						 * </pre>
						 * 
						 * 
						 * <p>
						 * Objects of the following type(s) are allowed in the list
						 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ }
						 * 
						 * 
						 */
						@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ.class, cascade = {
								CascadeType.ALL })
						@JoinColumn(name = "INFO_TP_SERV_NFS_HJID")
						public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> getInfoTpServ() {
							if (infoTpServ == null) {
								infoTpServ = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ>();
							}
							return this.infoTpServ;
						}

						/**
						 * 
						 * 
						 */
						public void setInfoTpServ(
								List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> infoTpServ) {
							this.infoTpServ = infoTpServ;
						}

						/**
						 * Obt�m o valor da propriedade hjid.
						 * 
						 * @return possible object is {@link Long }
						 * 
						 */

						@Column(name = "HJID")

						public Long getHjid() {
							return hjid;
						}

						/**
						 * Define o valor da propriedade hjid.
						 * 
						 * @param value
						 *            allowed object is {@link Long }
						 * 
						 */
						public void setHjid(Long value) {
							this.hjid = value;
						}

						/*
						 * @Basic
						 * 
						 * @Column(name = "DT_EMISSAO_NFITEM")
						 * 
						 * @Temporal(TemporalType.TIMESTAMP) public Date getDtEmissaoNFItem() { return
						 * XmlAdapterUtils.unmarshall(StringAsDateTime.class, this.getDtEmissaoNF()); }
						 * 
						 * public void setDtEmissaoNFItem(Date target) {
						 * setDtEmissaoNF(XmlAdapterUtils.marshall(StringAsDateTime.class, target)); }
						 */

						public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
								EqualsStrategy strategy) {
							if (!(object instanceof Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs)) {
								return false;
							}
							if (this == object) {
								return true;
							}
							final Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs that = ((Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs) object);
							{
								String lhsSerie;
								lhsSerie = this.getSerie();
								String rhsSerie;
								rhsSerie = that.getSerie();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "serie", lhsSerie),
										LocatorUtils.property(thatLocator, "serie", rhsSerie), lhsSerie, rhsSerie)) {
									return false;
								}
							}
							{
								String lhsNumDocto;
								lhsNumDocto = this.getNumDocto();
								String rhsNumDocto;
								rhsNumDocto = that.getNumDocto();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "numDocto", lhsNumDocto),
										LocatorUtils.property(thatLocator, "numDocto", rhsNumDocto), lhsNumDocto,
										rhsNumDocto)) {
									return false;
								}
							}
							{
								String lhsDtEmissaoNF;
								lhsDtEmissaoNF = this.getDtEmissaoNF();
								String rhsDtEmissaoNF;
								rhsDtEmissaoNF = that.getDtEmissaoNF();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "dtEmissaoNF", lhsDtEmissaoNF),
										LocatorUtils.property(thatLocator, "dtEmissaoNF", rhsDtEmissaoNF),
										lhsDtEmissaoNF, rhsDtEmissaoNF)) {
									return false;
								}
							}
							{
								String lhsVlrBruto;
								lhsVlrBruto = this.getVlrBruto();
								String rhsVlrBruto;
								rhsVlrBruto = that.getVlrBruto();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrBruto", lhsVlrBruto),
										LocatorUtils.property(thatLocator, "vlrBruto", rhsVlrBruto), lhsVlrBruto,
										rhsVlrBruto)) {
									return false;
								}
							}
							{
								String lhsObs;
								lhsObs = this.getObs();
								String rhsObs;
								rhsObs = that.getObs();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "obs", lhsObs),
										LocatorUtils.property(thatLocator, "obs", rhsObs), lhsObs, rhsObs)) {
									return false;
								}
							}
							{
								List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> lhsInfoTpServ;
								lhsInfoTpServ = (((this.infoTpServ != null) && (!this.infoTpServ.isEmpty()))
										? this.getInfoTpServ()
										: null);
								List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> rhsInfoTpServ;
								rhsInfoTpServ = (((that.infoTpServ != null) && (!that.infoTpServ.isEmpty()))
										? that.getInfoTpServ()
										: null);
								if (!strategy.equals(LocatorUtils.property(thisLocator, "infoTpServ", lhsInfoTpServ),
										LocatorUtils.property(thatLocator, "infoTpServ", rhsInfoTpServ), lhsInfoTpServ,
										rhsInfoTpServ)) {
									return false;
								}
							}
							return true;
						}

						public boolean equals(Object object) {
							final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
							return equals(null, null, object, strategy);
						}

						public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
							int currentHashCode = 1;
							{
								String theSerie;
								theSerie = this.getSerie();
								currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "serie", theSerie),
										currentHashCode, theSerie);
							}
							{
								String theNumDocto;
								theNumDocto = this.getNumDocto();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "numDocto", theNumDocto), currentHashCode,
										theNumDocto);
							}
							{
								String theDtEmissaoNF;
								theDtEmissaoNF = this.getDtEmissaoNF();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "dtEmissaoNF", theDtEmissaoNF), currentHashCode,
										theDtEmissaoNF);
							}
							{
								String theVlrBruto;
								theVlrBruto = this.getVlrBruto();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "vlrBruto", theVlrBruto), currentHashCode,
										theVlrBruto);
							}
							{
								String theObs;
								theObs = this.getObs();
								currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "obs", theObs),
										currentHashCode, theObs);
							}
							{
								List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> theInfoTpServ;
								theInfoTpServ = (((this.infoTpServ != null) && (!this.infoTpServ.isEmpty()))
										? this.getInfoTpServ()
										: null);
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "infoTpServ", theInfoTpServ), currentHashCode,
										theInfoTpServ);
							}
							return currentHashCode;
						}

						public int hashCode() {
							final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
							return this.hashCode(null, strategy);
						}

						/**
						 * Informa��es sobre os tipos de Servi�os constantes da Nota Fiscal
						 * 
						 * <p>
						 * Classe Java de anonymous complex type.
						 * 
						 * <p>
						 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
						 * desta classe.
						 * 
						 * <pre>
						 * &lt;complexType>
						 *   &lt;complexContent>
						 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
						 *       &lt;sequence>
						 *         &lt;element name="tpServico">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;length value="9"/>
						 *               &lt;pattern value="[0-9]{9}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrBaseRet">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrRetencao">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrRetSub" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrNRetPrinc" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos15" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos20" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos25" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrAdicional" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrNRetAdic" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *       &lt;/sequence>
						 *     &lt;/restriction>
						 *   &lt;/complexContent>
						 * &lt;/complexType>
						 * </pre>
						 * 
						 * 
						 */
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "tpServico", "vlrBaseRet", "vlrRetencao", "vlrRetSub",
								"vlrNRetPrinc", "vlrServicos15", "vlrServicos20", "vlrServicos25", "vlrAdicional",
								"vlrNRetAdic" })
						@Entity(name = "Reinf$EvtServTom$InfoServTom$IdeEstabObra$IdePrestServ$Nfs$InfoTpServ")
						@Table(name = "INFO_TP_SERV")
						@Inheritance(strategy = InheritanceType.JOINED)
						public static class InfoTpServ implements Equals, HashCode {

							@XmlElement(required = true)
							protected String tpServico;
							@XmlElement(required = true)
							protected String vlrBaseRet;
							@XmlElement(required = true)
							protected String vlrRetencao;
							protected String vlrRetSub;
							protected String vlrNRetPrinc;
							protected String vlrServicos15;
							protected String vlrServicos20;
							protected String vlrServicos25;
							protected String vlrAdicional;
							protected String vlrNRetAdic;
							@XmlAttribute(name = "Hjid")
							@Id
							@Column(name = "ID")
							protected Long hjid;

							/**
							 * Obt�m o valor da propriedade tpServico.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "TP_SERVICO", length = 9)
							public String getTpServico() {
								return tpServico;
							}

							/**
							 * Define o valor da propriedade tpServico.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setTpServico(String value) {
								this.tpServico = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrBaseRet.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_BASE_RET", length = 17)
							public String getVlrBaseRet() {
								return vlrBaseRet;
							}

							/**
							 * Define o valor da propriedade vlrBaseRet.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrBaseRet(String value) {
								this.vlrBaseRet = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrRetencao.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_RETENCAO", length = 17)
							public String getVlrRetencao() {
								return vlrRetencao;
							}

							/**
							 * Define o valor da propriedade vlrRetencao.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrRetencao(String value) {
								this.vlrRetencao = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrRetSub.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_RET_SUB", length = 17)
							public String getVlrRetSub() {
								return vlrRetSub;
							}

							/**
							 * Define o valor da propriedade vlrRetSub.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrRetSub(String value) {
								this.vlrRetSub = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrNRetPrinc.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_NRET_PRINC", length = 17)
							public String getVlrNRetPrinc() {
								return vlrNRetPrinc;
							}

							/**
							 * Define o valor da propriedade vlrNRetPrinc.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrNRetPrinc(String value) {
								this.vlrNRetPrinc = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos15.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_SERVICOS_15", length = 17)
							public String getVlrServicos15() {
								return vlrServicos15;
							}

							/**
							 * Define o valor da propriedade vlrServicos15.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos15(String value) {
								this.vlrServicos15 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos20.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_SERVICOS_20", length = 17)
							public String getVlrServicos20() {
								return vlrServicos20;
							}

							/**
							 * Define o valor da propriedade vlrServicos20.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos20(String value) {
								this.vlrServicos20 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos25.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_SERVICOS_25", length = 17)
							public String getVlrServicos25() {
								return vlrServicos25;
							}

							/**
							 * Define o valor da propriedade vlrServicos25.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos25(String value) {
								this.vlrServicos25 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrAdicional.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_ADICIONAL", length = 17)
							public String getVlrAdicional() {
								return vlrAdicional;
							}

							/**
							 * Define o valor da propriedade vlrAdicional.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrAdicional(String value) {
								this.vlrAdicional = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrNRetAdic.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_NRET_ADIC", length = 17)
							public String getVlrNRetAdic() {
								return vlrNRetAdic;
							}

							/**
							 * Define o valor da propriedade vlrNRetAdic.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrNRetAdic(String value) {
								this.vlrNRetAdic = value;
							}

							/**
							 * Obt�m o valor da propriedade hjid.
							 * 
							 * @return possible object is {@link Long }
							 * 
							 */

							@Column(name = "HJID")

							public Long getHjid() {
								return hjid;
							}

							/**
							 * Define o valor da propriedade hjid.
							 * 
							 * @param value
							 *            allowed object is {@link Long }
							 * 
							 */
							public void setHjid(Long value) {
								this.hjid = value;
							}

							public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
									EqualsStrategy strategy) {
								if (!(object instanceof Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ)) {
									return false;
								}
								if (this == object) {
									return true;
								}
								final Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ that = ((Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ) object);
								{
									String lhsTpServico;
									lhsTpServico = this.getTpServico();
									String rhsTpServico;
									rhsTpServico = that.getTpServico();
									if (!strategy.equals(LocatorUtils.property(thisLocator, "tpServico", lhsTpServico),
											LocatorUtils.property(thatLocator, "tpServico", rhsTpServico), lhsTpServico,
											rhsTpServico)) {
										return false;
									}
								}
								{
									String lhsVlrBaseRet;
									lhsVlrBaseRet = this.getVlrBaseRet();
									String rhsVlrBaseRet;
									rhsVlrBaseRet = that.getVlrBaseRet();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrBaseRet", lhsVlrBaseRet),
											LocatorUtils.property(thatLocator, "vlrBaseRet", rhsVlrBaseRet),
											lhsVlrBaseRet, rhsVlrBaseRet)) {
										return false;
									}
								}
								{
									String lhsVlrRetencao;
									lhsVlrRetencao = this.getVlrRetencao();
									String rhsVlrRetencao;
									rhsVlrRetencao = that.getVlrRetencao();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrRetencao", lhsVlrRetencao),
											LocatorUtils.property(thatLocator, "vlrRetencao", rhsVlrRetencao),
											lhsVlrRetencao, rhsVlrRetencao)) {
										return false;
									}
								}
								{
									String lhsVlrRetSub;
									lhsVlrRetSub = this.getVlrRetSub();
									String rhsVlrRetSub;
									rhsVlrRetSub = that.getVlrRetSub();
									if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrRetSub", lhsVlrRetSub),
											LocatorUtils.property(thatLocator, "vlrRetSub", rhsVlrRetSub), lhsVlrRetSub,
											rhsVlrRetSub)) {
										return false;
									}
								}
								{
									String lhsVlrNRetPrinc;
									lhsVlrNRetPrinc = this.getVlrNRetPrinc();
									String rhsVlrNRetPrinc;
									rhsVlrNRetPrinc = that.getVlrNRetPrinc();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrNRetPrinc", lhsVlrNRetPrinc),
											LocatorUtils.property(thatLocator, "vlrNRetPrinc", rhsVlrNRetPrinc),
											lhsVlrNRetPrinc, rhsVlrNRetPrinc)) {
										return false;
									}
								}
								{
									String lhsVlrServicos15;
									lhsVlrServicos15 = this.getVlrServicos15();
									String rhsVlrServicos15;
									rhsVlrServicos15 = that.getVlrServicos15();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrServicos15", lhsVlrServicos15),
											LocatorUtils.property(thatLocator, "vlrServicos15", rhsVlrServicos15),
											lhsVlrServicos15, rhsVlrServicos15)) {
										return false;
									}
								}
								{
									String lhsVlrServicos20;
									lhsVlrServicos20 = this.getVlrServicos20();
									String rhsVlrServicos20;
									rhsVlrServicos20 = that.getVlrServicos20();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrServicos20", lhsVlrServicos20),
											LocatorUtils.property(thatLocator, "vlrServicos20", rhsVlrServicos20),
											lhsVlrServicos20, rhsVlrServicos20)) {
										return false;
									}
								}
								{
									String lhsVlrServicos25;
									lhsVlrServicos25 = this.getVlrServicos25();
									String rhsVlrServicos25;
									rhsVlrServicos25 = that.getVlrServicos25();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrServicos25", lhsVlrServicos25),
											LocatorUtils.property(thatLocator, "vlrServicos25", rhsVlrServicos25),
											lhsVlrServicos25, rhsVlrServicos25)) {
										return false;
									}
								}
								{
									String lhsVlrAdicional;
									lhsVlrAdicional = this.getVlrAdicional();
									String rhsVlrAdicional;
									rhsVlrAdicional = that.getVlrAdicional();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrAdicional", lhsVlrAdicional),
											LocatorUtils.property(thatLocator, "vlrAdicional", rhsVlrAdicional),
											lhsVlrAdicional, rhsVlrAdicional)) {
										return false;
									}
								}
								{
									String lhsVlrNRetAdic;
									lhsVlrNRetAdic = this.getVlrNRetAdic();
									String rhsVlrNRetAdic;
									rhsVlrNRetAdic = that.getVlrNRetAdic();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrNRetAdic", lhsVlrNRetAdic),
											LocatorUtils.property(thatLocator, "vlrNRetAdic", rhsVlrNRetAdic),
											lhsVlrNRetAdic, rhsVlrNRetAdic)) {
										return false;
									}
								}
								return true;
							}

							public boolean equals(Object object) {
								final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
								return equals(null, null, object, strategy);
							}

							public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
								int currentHashCode = 1;
								{
									String theTpServico;
									theTpServico = this.getTpServico();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "tpServico", theTpServico), currentHashCode,
											theTpServico);
								}
								{
									String theVlrBaseRet;
									theVlrBaseRet = this.getVlrBaseRet();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrBaseRet", theVlrBaseRet),
											currentHashCode, theVlrBaseRet);
								}
								{
									String theVlrRetencao;
									theVlrRetencao = this.getVlrRetencao();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrRetencao", theVlrRetencao),
											currentHashCode, theVlrRetencao);
								}
								{
									String theVlrRetSub;
									theVlrRetSub = this.getVlrRetSub();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrRetSub", theVlrRetSub), currentHashCode,
											theVlrRetSub);
								}
								{
									String theVlrNRetPrinc;
									theVlrNRetPrinc = this.getVlrNRetPrinc();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrNRetPrinc", theVlrNRetPrinc),
											currentHashCode, theVlrNRetPrinc);
								}
								{
									String theVlrServicos15;
									theVlrServicos15 = this.getVlrServicos15();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrServicos15", theVlrServicos15),
											currentHashCode, theVlrServicos15);
								}
								{
									String theVlrServicos20;
									theVlrServicos20 = this.getVlrServicos20();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrServicos20", theVlrServicos20),
											currentHashCode, theVlrServicos20);
								}
								{
									String theVlrServicos25;
									theVlrServicos25 = this.getVlrServicos25();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrServicos25", theVlrServicos25),
											currentHashCode, theVlrServicos25);
								}
								{
									String theVlrAdicional;
									theVlrAdicional = this.getVlrAdicional();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrAdicional", theVlrAdicional),
											currentHashCode, theVlrAdicional);
								}
								{
									String theVlrNRetAdic;
									theVlrNRetAdic = this.getVlrNRetAdic();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrNRetAdic", theVlrNRetAdic),
											currentHashCode, theVlrNRetAdic);
								}
								return currentHashCode;
							}

							public int hashCode() {
								final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
								return this.hashCode(null, strategy);
							}

						}

					}

				}

			}

		}

	}

}
