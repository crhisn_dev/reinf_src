//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.25 �s 02:53:46 PM BRST 
//


package br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Define uma ocorrencia encontrada no processamento de um arquivo.
 * 
 * <p>Classe Java de TRegistroOcorrencias complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TRegistroOcorrencias">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tpOcorr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *               &lt;pattern value="[1|2]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="localErroAviso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="200"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codResp">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dscResp">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRegistroOcorrencias", propOrder = {
    "tpOcorr",
    "localErroAviso",
    "codResp",
    "dscResp"
})
@Entity(name = "TRegistroOcorrencias")
@Table(name = "TREGISTRO_OCORRENCIAS")
@Inheritance(strategy = InheritanceType.JOINED)
public class TRegistroOcorrencias
    implements Equals, HashCode
{

    protected short tpOcorr;
    @XmlElement(required = true)
    protected String localErroAviso;
    @XmlElement(required = true)
    protected String codResp;
    @XmlElement(required = true)
    protected String dscResp;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Obt�m o valor da propriedade tpOcorr.
     * 
     */
    @Basic
    @Column(name = "TP_OCORR", scale = 0)
    public short getTpOcorr() {
        return tpOcorr;
    }

    /**
     * Define o valor da propriedade tpOcorr.
     * 
     */
    public void setTpOcorr(short value) {
        this.tpOcorr = value;
    }

    /**
     * Obt�m o valor da propriedade localErroAviso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "LOCAL_ERRO_AVISO", length = 200)
    public String getLocalErroAviso() {
        return localErroAviso;
    }

    /**
     * Define o valor da propriedade localErroAviso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalErroAviso(String value) {
        this.localErroAviso = value;
    }

    /**
     * Obt�m o valor da propriedade codResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "COD_RESP", length = 6)
    public String getCodResp() {
        return codResp;
    }

    /**
     * Define o valor da propriedade codResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResp(String value) {
        this.codResp = value;
    }

    /**
     * Obt�m o valor da propriedade dscResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Basic
    @Column(name = "DSC_RESP", length = 999)
    public String getDscResp() {
        return dscResp;
    }

    /**
     * Define o valor da propriedade dscResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDscResp(String value) {
        this.dscResp = value;
    }

    /**
     * Obt�m o valor da propriedade hjid.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Define o valor da propriedade hjid.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TRegistroOcorrencias)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TRegistroOcorrencias that = ((TRegistroOcorrencias) object);
        {
            short lhsTpOcorr;
            lhsTpOcorr = this.getTpOcorr();
            short rhsTpOcorr;
            rhsTpOcorr = that.getTpOcorr();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tpOcorr", lhsTpOcorr), LocatorUtils.property(thatLocator, "tpOcorr", rhsTpOcorr), lhsTpOcorr, rhsTpOcorr)) {
                return false;
            }
        }
        {
            String lhsLocalErroAviso;
            lhsLocalErroAviso = this.getLocalErroAviso();
            String rhsLocalErroAviso;
            rhsLocalErroAviso = that.getLocalErroAviso();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "localErroAviso", lhsLocalErroAviso), LocatorUtils.property(thatLocator, "localErroAviso", rhsLocalErroAviso), lhsLocalErroAviso, rhsLocalErroAviso)) {
                return false;
            }
        }
        {
            String lhsCodResp;
            lhsCodResp = this.getCodResp();
            String rhsCodResp;
            rhsCodResp = that.getCodResp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "codResp", lhsCodResp), LocatorUtils.property(thatLocator, "codResp", rhsCodResp), lhsCodResp, rhsCodResp)) {
                return false;
            }
        }
        {
            String lhsDscResp;
            lhsDscResp = this.getDscResp();
            String rhsDscResp;
            rhsDscResp = that.getDscResp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dscResp", lhsDscResp), LocatorUtils.property(thatLocator, "dscResp", rhsDscResp), lhsDscResp, rhsDscResp)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            short theTpOcorr;
            theTpOcorr = this.getTpOcorr();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpOcorr", theTpOcorr), currentHashCode, theTpOcorr);
        }
        {
            String theLocalErroAviso;
            theLocalErroAviso = this.getLocalErroAviso();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "localErroAviso", theLocalErroAviso), currentHashCode, theLocalErroAviso);
        }
        {
            String theCodResp;
            theCodResp = this.getCodResp();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "codResp", theCodResp), currentHashCode, theCodResp);
        }
        {
            String theDscResp;
            theDscResp = this.getDscResp();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dscResp", theDscResp), currentHashCode, theDscResp);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
