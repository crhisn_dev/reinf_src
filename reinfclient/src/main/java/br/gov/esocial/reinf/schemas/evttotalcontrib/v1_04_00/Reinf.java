//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.25 �s 02:53:46 PM BRST 
//


package br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtTotalContrib">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="perApur" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="7"/>
 *                                   &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="8"/>
 *                                   &lt;maxLength value="14"/>
 *                                   &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideRecRetorno">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ideStatus" type="{http://www.reinf.esocial.gov.br/schemas/evtTotalContrib/v1_04_00}TStatus"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoRecEv" type="{http://www.reinf.esocial.gov.br/schemas/evtTotalContrib/v1_04_00}TDadosProcessamentoEvento"/>
 *                   &lt;element name="infoTotalContrib" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nrRecArqBase" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="52"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="indExistInfo">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1-3]{1}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="RTom" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="cnpjPrestador">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;pattern value="[0-9]{14}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="CNO" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;pattern value="[0-9]{12}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrTotalBaseRet">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="infoCRTom" maxOccurs="2">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="CRTom">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="1"/>
 *                                                       &lt;maxLength value="6"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="VlrCRTom">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="VlrCRTomSusp" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RPrest" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="tpInscTomador">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                             &lt;minInclusive value="1"/>
 *                                             &lt;maxInclusive value="4"/>
 *                                             &lt;pattern value="[1|4]"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="nrInscTomador">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;pattern value="[0-9]{14}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrTotalBaseRet">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrTotalRetPrinc">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrTotalRetAdic" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrTotalNRetAdic" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RRecRepAD" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CRRecRepAD">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="1"/>
 *                                             &lt;maxLength value="6"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrCRRecRepAD">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrCRRecRepADSusp" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RComl" maxOccurs="3" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CRComl">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="1"/>
 *                                             &lt;maxLength value="6"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrCRComl">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrCRComlSusp" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RCPRB" maxOccurs="4" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CRCPRB">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="1"/>
 *                                             &lt;maxLength value="6"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrCRCPRB">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="vlrCRCPRBSusp" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="4"/>
 *                                             &lt;maxLength value="17"/>
 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "evtTotalContrib"
})
@XmlRootElement(name = "Reinf")
@Entity(name = "Reinf")
@Table(name = "REINF")
@Inheritance(strategy = InheritanceType.JOINED)
public class Reinf
    implements Equals, HashCode
{

    @XmlElement(required = true)
    protected Reinf.EvtTotalContrib evtTotalContrib;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Obt�m o valor da propriedade evtTotalContrib.
     * 
     * @return
     *     possible object is
     *     {@link Reinf.EvtTotalContrib }
     *     
     */
    @ManyToOne(targetEntity = Reinf.EvtTotalContrib.class, cascade = {
        CascadeType.ALL
    })
    @JoinColumn(name = "EVT_TOTAL_CONTRIB_REINF_HJID")
    public Reinf.EvtTotalContrib getEvtTotalContrib() {
        return evtTotalContrib;
    }

    /**
     * Define o valor da propriedade evtTotalContrib.
     * 
     * @param value
     *     allowed object is
     *     {@link Reinf.EvtTotalContrib }
     *     
     */
    public void setEvtTotalContrib(Reinf.EvtTotalContrib value) {
        this.evtTotalContrib = value;
    }

    /**
     * Obt�m o valor da propriedade hjid.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Define o valor da propriedade hjid.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Reinf)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Reinf that = ((Reinf) object);
        {
            Reinf.EvtTotalContrib lhsEvtTotalContrib;
            lhsEvtTotalContrib = this.getEvtTotalContrib();
            Reinf.EvtTotalContrib rhsEvtTotalContrib;
            rhsEvtTotalContrib = that.getEvtTotalContrib();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "evtTotalContrib", lhsEvtTotalContrib), LocatorUtils.property(thatLocator, "evtTotalContrib", rhsEvtTotalContrib), lhsEvtTotalContrib, rhsEvtTotalContrib)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Reinf.EvtTotalContrib theEvtTotalContrib;
            theEvtTotalContrib = this.getEvtTotalContrib();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtTotalContrib", theEvtTotalContrib), currentHashCode, theEvtTotalContrib);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ideEvento">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="perApur" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;length value="7"/>
     *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ideContri">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tpInsc">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="nrInsc">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;minLength value="8"/>
     *                         &lt;maxLength value="14"/>
     *                         &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ideRecRetorno">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ideStatus" type="{http://www.reinf.esocial.gov.br/schemas/evtTotalContrib/v1_04_00}TStatus"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="infoRecEv" type="{http://www.reinf.esocial.gov.br/schemas/evtTotalContrib/v1_04_00}TDadosProcessamentoEvento"/>
     *         &lt;element name="infoTotalContrib" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nrRecArqBase" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;minLength value="1"/>
     *                         &lt;maxLength value="52"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="indExistInfo">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                         &lt;pattern value="[1-3]{1}"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="RTom" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="cnpjPrestador">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;pattern value="[0-9]{14}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="CNO" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;pattern value="[0-9]{12}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrTotalBaseRet">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="infoCRTom" maxOccurs="2">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="CRTom">
     *                                         &lt;simpleType>
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                             &lt;minLength value="1"/>
     *                                             &lt;maxLength value="6"/>
     *                                           &lt;/restriction>
     *                                         &lt;/simpleType>
     *                                       &lt;/element>
     *                                       &lt;element name="VlrCRTom">
     *                                         &lt;simpleType>
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                             &lt;minLength value="4"/>
     *                                             &lt;maxLength value="17"/>
     *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                           &lt;/restriction>
     *                                         &lt;/simpleType>
     *                                       &lt;/element>
     *                                       &lt;element name="VlrCRTomSusp" minOccurs="0">
     *                                         &lt;simpleType>
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                             &lt;minLength value="4"/>
     *                                             &lt;maxLength value="17"/>
     *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                           &lt;/restriction>
     *                                         &lt;/simpleType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="RPrest" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="tpInscTomador">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
     *                                   &lt;minInclusive value="1"/>
     *                                   &lt;maxInclusive value="4"/>
     *                                   &lt;pattern value="[1|4]"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="nrInscTomador">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;pattern value="[0-9]{14}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrTotalBaseRet">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrTotalRetPrinc">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrTotalRetAdic" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrTotalNRetAdic" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="RRecRepAD" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CRRecRepAD">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="1"/>
     *                                   &lt;maxLength value="6"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrCRRecRepAD">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrCRRecRepADSusp" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="RComl" maxOccurs="3" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CRComl">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="1"/>
     *                                   &lt;maxLength value="6"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrCRComl">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrCRComlSusp" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="RCPRB" maxOccurs="4" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CRCPRB">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="1"/>
     *                                   &lt;maxLength value="6"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrCRCPRB">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="vlrCRCPRBSusp" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;minLength value="4"/>
     *                                   &lt;maxLength value="17"/>
     *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="id" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
     *             &lt;length value="36"/>
     *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ideEvento",
        "ideContri",
        "ideRecRetorno",
        "infoRecEv",
        "infoTotalContrib"
    })
    @Entity(name = "Reinf$EvtTotalContrib")
    @Table(name = "EVT_TOTAL_CONTRIB")
    @Inheritance(strategy = InheritanceType.JOINED)
    public static class EvtTotalContrib
        implements Equals, HashCode
    {

        @XmlElement(required = true)
        protected Reinf.EvtTotalContrib.IdeEvento ideEvento;
        @XmlElement(required = true)
        protected Reinf.EvtTotalContrib.IdeContri ideContri;
        @XmlElement(required = true)
        protected Reinf.EvtTotalContrib.IdeRecRetorno ideRecRetorno;
        @XmlElement(required = true)
        protected TDadosProcessamentoEvento infoRecEv;
        protected Reinf.EvtTotalContrib.InfoTotalContrib infoTotalContrib;
        @XmlAttribute(name = "id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String id;
        @XmlAttribute(name = "Hjid")
        protected Long hjid;

        /**
         * Obt�m o valor da propriedade ideEvento.
         * 
         * @return
         *     possible object is
         *     {@link Reinf.EvtTotalContrib.IdeEvento }
         *     
         */
        @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.IdeEvento.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "IDE_EVENTO_EVT_TOTAL_CONTRIB_0")
        public Reinf.EvtTotalContrib.IdeEvento getIdeEvento() {
            return ideEvento;
        }

        /**
         * Define o valor da propriedade ideEvento.
         * 
         * @param value
         *     allowed object is
         *     {@link Reinf.EvtTotalContrib.IdeEvento }
         *     
         */
        public void setIdeEvento(Reinf.EvtTotalContrib.IdeEvento value) {
            this.ideEvento = value;
        }

        /**
         * Obt�m o valor da propriedade ideContri.
         * 
         * @return
         *     possible object is
         *     {@link Reinf.EvtTotalContrib.IdeContri }
         *     
         */
        @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.IdeContri.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "IDE_CONTRI_EVT_TOTAL_CONTRIB_0")
        public Reinf.EvtTotalContrib.IdeContri getIdeContri() {
            return ideContri;
        }

        /**
         * Define o valor da propriedade ideContri.
         * 
         * @param value
         *     allowed object is
         *     {@link Reinf.EvtTotalContrib.IdeContri }
         *     
         */
        public void setIdeContri(Reinf.EvtTotalContrib.IdeContri value) {
            this.ideContri = value;
        }

        /**
         * Obt�m o valor da propriedade ideRecRetorno.
         * 
         * @return
         *     possible object is
         *     {@link Reinf.EvtTotalContrib.IdeRecRetorno }
         *     
         */
        @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.IdeRecRetorno.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "IDE_REC_RETORNO_EVT_TOTAL_CO_0")
        public Reinf.EvtTotalContrib.IdeRecRetorno getIdeRecRetorno() {
            return ideRecRetorno;
        }

        /**
         * Define o valor da propriedade ideRecRetorno.
         * 
         * @param value
         *     allowed object is
         *     {@link Reinf.EvtTotalContrib.IdeRecRetorno }
         *     
         */
        public void setIdeRecRetorno(Reinf.EvtTotalContrib.IdeRecRetorno value) {
            this.ideRecRetorno = value;
        }

        /**
         * Obt�m o valor da propriedade infoRecEv.
         * 
         * @return
         *     possible object is
         *     {@link TDadosProcessamentoEvento }
         *     
         */
        @ManyToOne(targetEntity = TDadosProcessamentoEvento.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "INFO_REC_EV_EVT_TOTAL_CONTRI_0")
        public TDadosProcessamentoEvento getInfoRecEv() {
            return infoRecEv;
        }

        /**
         * Define o valor da propriedade infoRecEv.
         * 
         * @param value
         *     allowed object is
         *     {@link TDadosProcessamentoEvento }
         *     
         */
        public void setInfoRecEv(TDadosProcessamentoEvento value) {
            this.infoRecEv = value;
        }

        /**
         * Obt�m o valor da propriedade infoTotalContrib.
         * 
         * @return
         *     possible object is
         *     {@link Reinf.EvtTotalContrib.InfoTotalContrib }
         *     
         */
        @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.InfoTotalContrib.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "INFO_TOTAL_CONTRIB_EVT_TOTAL_0")
        public Reinf.EvtTotalContrib.InfoTotalContrib getInfoTotalContrib() {
            return infoTotalContrib;
        }

        /**
         * Define o valor da propriedade infoTotalContrib.
         * 
         * @param value
         *     allowed object is
         *     {@link Reinf.EvtTotalContrib.InfoTotalContrib }
         *     
         */
        public void setInfoTotalContrib(Reinf.EvtTotalContrib.InfoTotalContrib value) {
            this.infoTotalContrib = value;
        }

        /**
         * Obt�m o valor da propriedade id.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        @Basic
        @Column(name = "ID", length = 36)
        public String getId() {
            return id;
        }

        /**
         * Define o valor da propriedade id.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Obt�m o valor da propriedade hjid.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        @Id
        @Column(name = "HJID")
        @GeneratedValue(strategy = GenerationType.AUTO)
        public Long getHjid() {
            return hjid;
        }

        /**
         * Define o valor da propriedade hjid.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setHjid(Long value) {
            this.hjid = value;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof Reinf.EvtTotalContrib)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final Reinf.EvtTotalContrib that = ((Reinf.EvtTotalContrib) object);
            {
                Reinf.EvtTotalContrib.IdeEvento lhsIdeEvento;
                lhsIdeEvento = this.getIdeEvento();
                Reinf.EvtTotalContrib.IdeEvento rhsIdeEvento;
                rhsIdeEvento = that.getIdeEvento();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEvento", lhsIdeEvento), LocatorUtils.property(thatLocator, "ideEvento", rhsIdeEvento), lhsIdeEvento, rhsIdeEvento)) {
                    return false;
                }
            }
            {
                Reinf.EvtTotalContrib.IdeContri lhsIdeContri;
                lhsIdeContri = this.getIdeContri();
                Reinf.EvtTotalContrib.IdeContri rhsIdeContri;
                rhsIdeContri = that.getIdeContri();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "ideContri", lhsIdeContri), LocatorUtils.property(thatLocator, "ideContri", rhsIdeContri), lhsIdeContri, rhsIdeContri)) {
                    return false;
                }
            }
            {
                Reinf.EvtTotalContrib.IdeRecRetorno lhsIdeRecRetorno;
                lhsIdeRecRetorno = this.getIdeRecRetorno();
                Reinf.EvtTotalContrib.IdeRecRetorno rhsIdeRecRetorno;
                rhsIdeRecRetorno = that.getIdeRecRetorno();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "ideRecRetorno", lhsIdeRecRetorno), LocatorUtils.property(thatLocator, "ideRecRetorno", rhsIdeRecRetorno), lhsIdeRecRetorno, rhsIdeRecRetorno)) {
                    return false;
                }
            }
            {
                TDadosProcessamentoEvento lhsInfoRecEv;
                lhsInfoRecEv = this.getInfoRecEv();
                TDadosProcessamentoEvento rhsInfoRecEv;
                rhsInfoRecEv = that.getInfoRecEv();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "infoRecEv", lhsInfoRecEv), LocatorUtils.property(thatLocator, "infoRecEv", rhsInfoRecEv), lhsInfoRecEv, rhsInfoRecEv)) {
                    return false;
                }
            }
            {
                Reinf.EvtTotalContrib.InfoTotalContrib lhsInfoTotalContrib;
                lhsInfoTotalContrib = this.getInfoTotalContrib();
                Reinf.EvtTotalContrib.InfoTotalContrib rhsInfoTotalContrib;
                rhsInfoTotalContrib = that.getInfoTotalContrib();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "infoTotalContrib", lhsInfoTotalContrib), LocatorUtils.property(thatLocator, "infoTotalContrib", rhsInfoTotalContrib), lhsInfoTotalContrib, rhsInfoTotalContrib)) {
                    return false;
                }
            }
            {
                String lhsId;
                lhsId = this.getId();
                String rhsId;
                rhsId = that.getId();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
            int currentHashCode = 1;
            {
                Reinf.EvtTotalContrib.IdeEvento theIdeEvento;
                theIdeEvento = this.getIdeEvento();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEvento", theIdeEvento), currentHashCode, theIdeEvento);
            }
            {
                Reinf.EvtTotalContrib.IdeContri theIdeContri;
                theIdeContri = this.getIdeContri();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideContri", theIdeContri), currentHashCode, theIdeContri);
            }
            {
                Reinf.EvtTotalContrib.IdeRecRetorno theIdeRecRetorno;
                theIdeRecRetorno = this.getIdeRecRetorno();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideRecRetorno", theIdeRecRetorno), currentHashCode, theIdeRecRetorno);
            }
            {
                TDadosProcessamentoEvento theInfoRecEv;
                theInfoRecEv = this.getInfoRecEv();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoRecEv", theInfoRecEv), currentHashCode, theInfoRecEv);
            }
            {
                Reinf.EvtTotalContrib.InfoTotalContrib theInfoTotalContrib;
                theInfoTotalContrib = this.getInfoTotalContrib();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoTotalContrib", theInfoTotalContrib), currentHashCode, theInfoTotalContrib);
            }
            {
                String theId;
                theId = this.getId();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
            }
            return currentHashCode;
        }

        public int hashCode() {
            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
            return this.hashCode(null, strategy);
        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tpInsc">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="nrInsc">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;minLength value="8"/>
         *               &lt;maxLength value="14"/>
         *               &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tpInsc",
            "nrInsc"
        })
        @Entity(name = "Reinf$EvtTotalContrib$IdeContri")
        @Table(name = "IDE_CONTRI")
        @Inheritance(strategy = InheritanceType.JOINED)
        public static class IdeContri
            implements Equals, HashCode
        {

            protected short tpInsc;
            @XmlElement(required = true)
            protected String nrInsc;
            @XmlAttribute(name = "Hjid")
            protected Long hjid;

            /**
             * Obt�m o valor da propriedade tpInsc.
             * 
             */
            @Basic
            @Column(name = "TP_INSC", scale = 0)
            public short getTpInsc() {
                return tpInsc;
            }

            /**
             * Define o valor da propriedade tpInsc.
             * 
             */
            public void setTpInsc(short value) {
                this.tpInsc = value;
            }

            /**
             * Obt�m o valor da propriedade nrInsc.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            @Basic
            @Column(name = "NR_INSC", length = 14)
            public String getNrInsc() {
                return nrInsc;
            }

            /**
             * Define o valor da propriedade nrInsc.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNrInsc(String value) {
                this.nrInsc = value;
            }

            /**
             * Obt�m o valor da propriedade hjid.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            @Id
            @Column(name = "HJID")
            @GeneratedValue(strategy = GenerationType.AUTO)
            public Long getHjid() {
                return hjid;
            }

            /**
             * Define o valor da propriedade hjid.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setHjid(Long value) {
                this.hjid = value;
            }

            public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                if (!(object instanceof Reinf.EvtTotalContrib.IdeContri)) {
                    return false;
                }
                if (this == object) {
                    return true;
                }
                final Reinf.EvtTotalContrib.IdeContri that = ((Reinf.EvtTotalContrib.IdeContri) object);
                {
                    short lhsTpInsc;
                    lhsTpInsc = this.getTpInsc();
                    short rhsTpInsc;
                    rhsTpInsc = that.getTpInsc();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInsc", lhsTpInsc), LocatorUtils.property(thatLocator, "tpInsc", rhsTpInsc), lhsTpInsc, rhsTpInsc)) {
                        return false;
                    }
                }
                {
                    String lhsNrInsc;
                    lhsNrInsc = this.getNrInsc();
                    String rhsNrInsc;
                    rhsNrInsc = that.getNrInsc();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInsc", lhsNrInsc), LocatorUtils.property(thatLocator, "nrInsc", rhsNrInsc), lhsNrInsc, rhsNrInsc)) {
                        return false;
                    }
                }
                return true;
            }

            public boolean equals(Object object) {
                final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                return equals(null, null, object, strategy);
            }

            public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                int currentHashCode = 1;
                {
                    short theTpInsc;
                    theTpInsc = this.getTpInsc();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInsc", theTpInsc), currentHashCode, theTpInsc);
                }
                {
                    String theNrInsc;
                    theNrInsc = this.getNrInsc();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInsc", theNrInsc), currentHashCode, theNrInsc);
                }
                return currentHashCode;
            }

            public int hashCode() {
                final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                return this.hashCode(null, strategy);
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="perApur" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;length value="7"/>
         *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "perApur"
        })
        @Entity(name = "Reinf$EvtTotalContrib$IdeEvento")
        @Table(name = "IDE_EVENTO")
        @Inheritance(strategy = InheritanceType.JOINED)
        public static class IdeEvento
            implements Equals, HashCode
        {

            protected String perApur;
            @XmlAttribute(name = "Hjid")
            protected Long hjid;

            /**
             * Obt�m o valor da propriedade perApur.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            @Basic
            @Column(name = "PER_APUR", length = 7)
            public String getPerApur() {
                return perApur;
            }

            /**
             * Define o valor da propriedade perApur.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPerApur(String value) {
                this.perApur = value;
            }

            /**
             * Obt�m o valor da propriedade hjid.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            @Id
            @Column(name = "HJID")
            @GeneratedValue(strategy = GenerationType.AUTO)
            public Long getHjid() {
                return hjid;
            }

            /**
             * Define o valor da propriedade hjid.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setHjid(Long value) {
                this.hjid = value;
            }

            public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                if (!(object instanceof Reinf.EvtTotalContrib.IdeEvento)) {
                    return false;
                }
                if (this == object) {
                    return true;
                }
                final Reinf.EvtTotalContrib.IdeEvento that = ((Reinf.EvtTotalContrib.IdeEvento) object);
                {
                    String lhsPerApur;
                    lhsPerApur = this.getPerApur();
                    String rhsPerApur;
                    rhsPerApur = that.getPerApur();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "perApur", lhsPerApur), LocatorUtils.property(thatLocator, "perApur", rhsPerApur), lhsPerApur, rhsPerApur)) {
                        return false;
                    }
                }
                return true;
            }

            public boolean equals(Object object) {
                final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                return equals(null, null, object, strategy);
            }

            public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                int currentHashCode = 1;
                {
                    String thePerApur;
                    thePerApur = this.getPerApur();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "perApur", thePerApur), currentHashCode, thePerApur);
                }
                return currentHashCode;
            }

            public int hashCode() {
                final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                return this.hashCode(null, strategy);
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ideStatus" type="{http://www.reinf.esocial.gov.br/schemas/evtTotalContrib/v1_04_00}TStatus"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ideStatus"
        })
        @Entity(name = "Reinf$EvtTotalContrib$IdeRecRetorno")
        @Table(name = "IDE_REC_RETORNO")
        @Inheritance(strategy = InheritanceType.JOINED)
        public static class IdeRecRetorno
            implements Equals, HashCode
        {

            @XmlElement(required = true)
            protected TStatus ideStatus;
            @XmlAttribute(name = "Hjid")
            protected Long hjid;

            /**
             * Obt�m o valor da propriedade ideStatus.
             * 
             * @return
             *     possible object is
             *     {@link TStatus }
             *     
             */
            @ManyToOne(targetEntity = TStatus.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "IDE_STATUS_IDE_REC_RETORNO_H_0")
            public TStatus getIdeStatus() {
                return ideStatus;
            }

            /**
             * Define o valor da propriedade ideStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link TStatus }
             *     
             */
            public void setIdeStatus(TStatus value) {
                this.ideStatus = value;
            }

            /**
             * Obt�m o valor da propriedade hjid.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            @Id
            @Column(name = "HJID")
            @GeneratedValue(strategy = GenerationType.AUTO)
            public Long getHjid() {
                return hjid;
            }

            /**
             * Define o valor da propriedade hjid.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setHjid(Long value) {
                this.hjid = value;
            }

            public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                if (!(object instanceof Reinf.EvtTotalContrib.IdeRecRetorno)) {
                    return false;
                }
                if (this == object) {
                    return true;
                }
                final Reinf.EvtTotalContrib.IdeRecRetorno that = ((Reinf.EvtTotalContrib.IdeRecRetorno) object);
                {
                    TStatus lhsIdeStatus;
                    lhsIdeStatus = this.getIdeStatus();
                    TStatus rhsIdeStatus;
                    rhsIdeStatus = that.getIdeStatus();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "ideStatus", lhsIdeStatus), LocatorUtils.property(thatLocator, "ideStatus", rhsIdeStatus), lhsIdeStatus, rhsIdeStatus)) {
                        return false;
                    }
                }
                return true;
            }

            public boolean equals(Object object) {
                final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                return equals(null, null, object, strategy);
            }

            public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                int currentHashCode = 1;
                {
                    TStatus theIdeStatus;
                    theIdeStatus = this.getIdeStatus();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideStatus", theIdeStatus), currentHashCode, theIdeStatus);
                }
                return currentHashCode;
            }

            public int hashCode() {
                final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                return this.hashCode(null, strategy);
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nrRecArqBase" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;minLength value="1"/>
         *               &lt;maxLength value="52"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="indExistInfo">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *               &lt;pattern value="[1-3]{1}"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="RTom" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="cnpjPrestador">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;pattern value="[0-9]{14}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="CNO" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;pattern value="[0-9]{12}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrTotalBaseRet">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="infoCRTom" maxOccurs="2">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="CRTom">
         *                               &lt;simpleType>
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                   &lt;minLength value="1"/>
         *                                   &lt;maxLength value="6"/>
         *                                 &lt;/restriction>
         *                               &lt;/simpleType>
         *                             &lt;/element>
         *                             &lt;element name="VlrCRTom">
         *                               &lt;simpleType>
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                   &lt;minLength value="4"/>
         *                                   &lt;maxLength value="17"/>
         *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                                 &lt;/restriction>
         *                               &lt;/simpleType>
         *                             &lt;/element>
         *                             &lt;element name="VlrCRTomSusp" minOccurs="0">
         *                               &lt;simpleType>
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                   &lt;minLength value="4"/>
         *                                   &lt;maxLength value="17"/>
         *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                                 &lt;/restriction>
         *                               &lt;/simpleType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="RPrest" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="tpInscTomador">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
         *                         &lt;minInclusive value="1"/>
         *                         &lt;maxInclusive value="4"/>
         *                         &lt;pattern value="[1|4]"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="nrInscTomador">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;pattern value="[0-9]{14}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrTotalBaseRet">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrTotalRetPrinc">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrTotalRetAdic" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrTotalNRetAdic" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="RRecRepAD" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CRRecRepAD">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="1"/>
         *                         &lt;maxLength value="6"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrCRRecRepAD">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrCRRecRepADSusp" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="RComl" maxOccurs="3" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CRComl">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="1"/>
         *                         &lt;maxLength value="6"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrCRComl">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrCRComlSusp" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="RCPRB" maxOccurs="4" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CRCPRB">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="1"/>
         *                         &lt;maxLength value="6"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrCRCPRB">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="vlrCRCPRBSusp" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;minLength value="4"/>
         *                         &lt;maxLength value="17"/>
         *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nrRecArqBase",
            "indExistInfo",
            "rTom",
            "rPrest",
            "rRecRepAD",
            "rComl",
            "rcprb"
        })
        @Entity(name = "Reinf$EvtTotalContrib$InfoTotalContrib")
        @Table(name = "INFO_TOTAL_CONTRIB")
        @Inheritance(strategy = InheritanceType.JOINED)
        public static class InfoTotalContrib
            implements Equals, HashCode
        {

            protected String nrRecArqBase;
            protected long indExistInfo;
            @XmlElement(name = "RTom")
            protected List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom> rTom;
            @XmlElement(name = "RPrest")
            protected List<Reinf.EvtTotalContrib.InfoTotalContrib.RPrest> rPrest;
            @XmlElement(name = "RRecRepAD")
            protected List<Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD> rRecRepAD;
            @XmlElement(name = "RComl")
            protected List<Reinf.EvtTotalContrib.InfoTotalContrib.RComl> rComl;
            @XmlElement(name = "RCPRB")
            protected List<Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB> rcprb;
            @XmlAttribute(name = "Hjid")
            protected Long hjid;

            /**
             * Obt�m o valor da propriedade nrRecArqBase.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            @Basic
            @Column(name = "NR_REC_ARQ_BASE", length = 52)
            public String getNrRecArqBase() {
                return nrRecArqBase;
            }

            /**
             * Define o valor da propriedade nrRecArqBase.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNrRecArqBase(String value) {
                this.nrRecArqBase = value;
            }

            /**
             * Obt�m o valor da propriedade indExistInfo.
             * 
             */
            @Basic
            @Column(name = "IND_EXIST_INFO", scale = 0)
            public long getIndExistInfo() {
                return indExistInfo;
            }

            /**
             * Define o valor da propriedade indExistInfo.
             * 
             */
            public void setIndExistInfo(long value) {
                this.indExistInfo = value;
            }

            /**
             * Gets the value of the rTom property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rTom property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRTom().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Reinf.EvtTotalContrib.InfoTotalContrib.RTom }
             * 
             * 
             */
            @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.InfoTotalContrib.RTom.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "RTOM_INFO_TOTAL_CONTRIB_HJID")
            public List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom> getRTom() {
                if (rTom == null) {
                    rTom = new ArrayList<Reinf.EvtTotalContrib.InfoTotalContrib.RTom>();
                }
                return this.rTom;
            }

            /**
             * 
             * 
             */
            public void setRTom(List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom> rTom) {
                this.rTom = rTom;
            }

            /**
             * Gets the value of the rPrest property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rPrest property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRPrest().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Reinf.EvtTotalContrib.InfoTotalContrib.RPrest }
             * 
             * 
             */
            @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.InfoTotalContrib.RPrest.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "RPREST_INFO_TOTAL_CONTRIB_HJ_0")
            public List<Reinf.EvtTotalContrib.InfoTotalContrib.RPrest> getRPrest() {
                if (rPrest == null) {
                    rPrest = new ArrayList<Reinf.EvtTotalContrib.InfoTotalContrib.RPrest>();
                }
                return this.rPrest;
            }

            /**
             * 
             * 
             */
            public void setRPrest(List<Reinf.EvtTotalContrib.InfoTotalContrib.RPrest> rPrest) {
                this.rPrest = rPrest;
            }

            /**
             * Gets the value of the rRecRepAD property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rRecRepAD property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRRecRepAD().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD }
             * 
             * 
             */
            @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "RREC_REP_AD_INFO_TOTAL_CONTR_0")
            public List<Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD> getRRecRepAD() {
                if (rRecRepAD == null) {
                    rRecRepAD = new ArrayList<Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD>();
                }
                return this.rRecRepAD;
            }

            /**
             * 
             * 
             */
            public void setRRecRepAD(List<Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD> rRecRepAD) {
                this.rRecRepAD = rRecRepAD;
            }

            /**
             * Gets the value of the rComl property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rComl property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRComl().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Reinf.EvtTotalContrib.InfoTotalContrib.RComl }
             * 
             * 
             */
            @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.InfoTotalContrib.RComl.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "RCOML_INFO_TOTAL_CONTRIB_HJID")
            public List<Reinf.EvtTotalContrib.InfoTotalContrib.RComl> getRComl() {
                if (rComl == null) {
                    rComl = new ArrayList<Reinf.EvtTotalContrib.InfoTotalContrib.RComl>();
                }
                return this.rComl;
            }

            /**
             * 
             * 
             */
            public void setRComl(List<Reinf.EvtTotalContrib.InfoTotalContrib.RComl> rComl) {
                this.rComl = rComl;
            }

            /**
             * Gets the value of the rcprb property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rcprb property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRCPRB().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB }
             * 
             * 
             */
            @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "RCPRB_INFO_TOTAL_CONTRIB_HJID")
            public List<Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB> getRCPRB() {
                if (rcprb == null) {
                    rcprb = new ArrayList<Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB>();
                }
                return this.rcprb;
            }

            /**
             * 
             * 
             */
            public void setRCPRB(List<Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB> rcprb) {
                this.rcprb = rcprb;
            }

            /**
             * Obt�m o valor da propriedade hjid.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            @Id
            @Column(name = "HJID")
            @GeneratedValue(strategy = GenerationType.AUTO)
            public Long getHjid() {
                return hjid;
            }

            /**
             * Define o valor da propriedade hjid.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setHjid(Long value) {
                this.hjid = value;
            }

            public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                if (!(object instanceof Reinf.EvtTotalContrib.InfoTotalContrib)) {
                    return false;
                }
                if (this == object) {
                    return true;
                }
                final Reinf.EvtTotalContrib.InfoTotalContrib that = ((Reinf.EvtTotalContrib.InfoTotalContrib) object);
                {
                    String lhsNrRecArqBase;
                    lhsNrRecArqBase = this.getNrRecArqBase();
                    String rhsNrRecArqBase;
                    rhsNrRecArqBase = that.getNrRecArqBase();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "nrRecArqBase", lhsNrRecArqBase), LocatorUtils.property(thatLocator, "nrRecArqBase", rhsNrRecArqBase), lhsNrRecArqBase, rhsNrRecArqBase)) {
                        return false;
                    }
                }
                {
                    long lhsIndExistInfo;
                    lhsIndExistInfo = this.getIndExistInfo();
                    long rhsIndExistInfo;
                    rhsIndExistInfo = that.getIndExistInfo();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "indExistInfo", lhsIndExistInfo), LocatorUtils.property(thatLocator, "indExistInfo", rhsIndExistInfo), lhsIndExistInfo, rhsIndExistInfo)) {
                        return false;
                    }
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom> lhsRTom;
                    lhsRTom = (((this.rTom!= null)&&(!this.rTom.isEmpty()))?this.getRTom():null);
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom> rhsRTom;
                    rhsRTom = (((that.rTom!= null)&&(!that.rTom.isEmpty()))?that.getRTom():null);
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "rTom", lhsRTom), LocatorUtils.property(thatLocator, "rTom", rhsRTom), lhsRTom, rhsRTom)) {
                        return false;
                    }
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RPrest> lhsRPrest;
                    lhsRPrest = (((this.rPrest!= null)&&(!this.rPrest.isEmpty()))?this.getRPrest():null);
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RPrest> rhsRPrest;
                    rhsRPrest = (((that.rPrest!= null)&&(!that.rPrest.isEmpty()))?that.getRPrest():null);
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "rPrest", lhsRPrest), LocatorUtils.property(thatLocator, "rPrest", rhsRPrest), lhsRPrest, rhsRPrest)) {
                        return false;
                    }
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD> lhsRRecRepAD;
                    lhsRRecRepAD = (((this.rRecRepAD!= null)&&(!this.rRecRepAD.isEmpty()))?this.getRRecRepAD():null);
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD> rhsRRecRepAD;
                    rhsRRecRepAD = (((that.rRecRepAD!= null)&&(!that.rRecRepAD.isEmpty()))?that.getRRecRepAD():null);
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "rRecRepAD", lhsRRecRepAD), LocatorUtils.property(thatLocator, "rRecRepAD", rhsRRecRepAD), lhsRRecRepAD, rhsRRecRepAD)) {
                        return false;
                    }
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RComl> lhsRComl;
                    lhsRComl = (((this.rComl!= null)&&(!this.rComl.isEmpty()))?this.getRComl():null);
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RComl> rhsRComl;
                    rhsRComl = (((that.rComl!= null)&&(!that.rComl.isEmpty()))?that.getRComl():null);
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "rComl", lhsRComl), LocatorUtils.property(thatLocator, "rComl", rhsRComl), lhsRComl, rhsRComl)) {
                        return false;
                    }
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB> lhsRCPRB;
                    lhsRCPRB = (((this.rcprb!= null)&&(!this.rcprb.isEmpty()))?this.getRCPRB():null);
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB> rhsRCPRB;
                    rhsRCPRB = (((that.rcprb!= null)&&(!that.rcprb.isEmpty()))?that.getRCPRB():null);
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "rcprb", lhsRCPRB), LocatorUtils.property(thatLocator, "rcprb", rhsRCPRB), lhsRCPRB, rhsRCPRB)) {
                        return false;
                    }
                }
                return true;
            }

            public boolean equals(Object object) {
                final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                return equals(null, null, object, strategy);
            }

            public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                int currentHashCode = 1;
                {
                    String theNrRecArqBase;
                    theNrRecArqBase = this.getNrRecArqBase();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrRecArqBase", theNrRecArqBase), currentHashCode, theNrRecArqBase);
                }
                {
                    long theIndExistInfo;
                    theIndExistInfo = this.getIndExistInfo();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indExistInfo", theIndExistInfo), currentHashCode, theIndExistInfo);
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom> theRTom;
                    theRTom = (((this.rTom!= null)&&(!this.rTom.isEmpty()))?this.getRTom():null);
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rTom", theRTom), currentHashCode, theRTom);
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RPrest> theRPrest;
                    theRPrest = (((this.rPrest!= null)&&(!this.rPrest.isEmpty()))?this.getRPrest():null);
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rPrest", theRPrest), currentHashCode, theRPrest);
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD> theRRecRepAD;
                    theRRecRepAD = (((this.rRecRepAD!= null)&&(!this.rRecRepAD.isEmpty()))?this.getRRecRepAD():null);
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rRecRepAD", theRRecRepAD), currentHashCode, theRRecRepAD);
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RComl> theRComl;
                    theRComl = (((this.rComl!= null)&&(!this.rComl.isEmpty()))?this.getRComl():null);
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rComl", theRComl), currentHashCode, theRComl);
                }
                {
                    List<Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB> theRCPRB;
                    theRCPRB = (((this.rcprb!= null)&&(!this.rcprb.isEmpty()))?this.getRCPRB():null);
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rcprb", theRCPRB), currentHashCode, theRCPRB);
                }
                return currentHashCode;
            }

            public int hashCode() {
                final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                return this.hashCode(null, strategy);
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CRCPRB">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="1"/>
             *               &lt;maxLength value="6"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrCRCPRB">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrCRCPRBSusp" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "crcprb",
                "vlrCRCPRB",
                "vlrCRCPRBSusp"
            })
            @Entity(name = "Reinf$EvtTotalContrib$InfoTotalContrib$RCPRB")
            @Table(name = "RCPRB")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class RCPRB
                implements Equals, HashCode
            {

                @XmlElement(name = "CRCPRB", required = true)
                protected String crcprb;
                @XmlElement(required = true)
                protected String vlrCRCPRB;
                protected String vlrCRCPRBSusp;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade crcprb.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "CRCPRB", length = 6)
                public String getCRCPRB() {
                    return crcprb;
                }

                /**
                 * Define o valor da propriedade crcprb.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCRCPRB(String value) {
                    this.crcprb = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrCRCPRB.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_CRCPRB", length = 17)
                public String getVlrCRCPRB() {
                    return vlrCRCPRB;
                }

                /**
                 * Define o valor da propriedade vlrCRCPRB.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrCRCPRB(String value) {
                    this.vlrCRCPRB = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrCRCPRBSusp.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_CRCPRBSUSP", length = 17)
                public String getVlrCRCPRBSusp() {
                    return vlrCRCPRBSusp;
                }

                /**
                 * Define o valor da propriedade vlrCRCPRBSusp.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrCRCPRBSusp(String value) {
                    this.vlrCRCPRBSusp = value;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB that = ((Reinf.EvtTotalContrib.InfoTotalContrib.RCPRB) object);
                    {
                        String lhsCRCPRB;
                        lhsCRCPRB = this.getCRCPRB();
                        String rhsCRCPRB;
                        rhsCRCPRB = that.getCRCPRB();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "crcprb", lhsCRCPRB), LocatorUtils.property(thatLocator, "crcprb", rhsCRCPRB), lhsCRCPRB, rhsCRCPRB)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrCRCPRB;
                        lhsVlrCRCPRB = this.getVlrCRCPRB();
                        String rhsVlrCRCPRB;
                        rhsVlrCRCPRB = that.getVlrCRCPRB();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRCPRB", lhsVlrCRCPRB), LocatorUtils.property(thatLocator, "vlrCRCPRB", rhsVlrCRCPRB), lhsVlrCRCPRB, rhsVlrCRCPRB)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrCRCPRBSusp;
                        lhsVlrCRCPRBSusp = this.getVlrCRCPRBSusp();
                        String rhsVlrCRCPRBSusp;
                        rhsVlrCRCPRBSusp = that.getVlrCRCPRBSusp();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRCPRBSusp", lhsVlrCRCPRBSusp), LocatorUtils.property(thatLocator, "vlrCRCPRBSusp", rhsVlrCRCPRBSusp), lhsVlrCRCPRBSusp, rhsVlrCRCPRBSusp)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        String theCRCPRB;
                        theCRCPRB = this.getCRCPRB();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "crcprb", theCRCPRB), currentHashCode, theCRCPRB);
                    }
                    {
                        String theVlrCRCPRB;
                        theVlrCRCPRB = this.getVlrCRCPRB();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRCPRB", theVlrCRCPRB), currentHashCode, theVlrCRCPRB);
                    }
                    {
                        String theVlrCRCPRBSusp;
                        theVlrCRCPRBSusp = this.getVlrCRCPRBSusp();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRCPRBSusp", theVlrCRCPRBSusp), currentHashCode, theVlrCRCPRBSusp);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }

            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CRComl">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="1"/>
             *               &lt;maxLength value="6"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrCRComl">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrCRComlSusp" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "crComl",
                "vlrCRComl",
                "vlrCRComlSusp"
            })
            @Entity(name = "Reinf$EvtTotalContrib$InfoTotalContrib$RComl")
            @Table(name = "RCOML")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class RComl
                implements Equals, HashCode
            {

                @XmlElement(name = "CRComl", required = true)
                protected String crComl;
                @XmlElement(required = true)
                protected String vlrCRComl;
                protected String vlrCRComlSusp;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade crComl.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "CRCOML", length = 6)
                public String getCRComl() {
                    return crComl;
                }

                /**
                 * Define o valor da propriedade crComl.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCRComl(String value) {
                    this.crComl = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrCRComl.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_CRCOML", length = 17)
                public String getVlrCRComl() {
                    return vlrCRComl;
                }

                /**
                 * Define o valor da propriedade vlrCRComl.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrCRComl(String value) {
                    this.vlrCRComl = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrCRComlSusp.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_CRCOML_SUSP", length = 17)
                public String getVlrCRComlSusp() {
                    return vlrCRComlSusp;
                }

                /**
                 * Define o valor da propriedade vlrCRComlSusp.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrCRComlSusp(String value) {
                    this.vlrCRComlSusp = value;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtTotalContrib.InfoTotalContrib.RComl)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtTotalContrib.InfoTotalContrib.RComl that = ((Reinf.EvtTotalContrib.InfoTotalContrib.RComl) object);
                    {
                        String lhsCRComl;
                        lhsCRComl = this.getCRComl();
                        String rhsCRComl;
                        rhsCRComl = that.getCRComl();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "crComl", lhsCRComl), LocatorUtils.property(thatLocator, "crComl", rhsCRComl), lhsCRComl, rhsCRComl)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrCRComl;
                        lhsVlrCRComl = this.getVlrCRComl();
                        String rhsVlrCRComl;
                        rhsVlrCRComl = that.getVlrCRComl();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRComl", lhsVlrCRComl), LocatorUtils.property(thatLocator, "vlrCRComl", rhsVlrCRComl), lhsVlrCRComl, rhsVlrCRComl)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrCRComlSusp;
                        lhsVlrCRComlSusp = this.getVlrCRComlSusp();
                        String rhsVlrCRComlSusp;
                        rhsVlrCRComlSusp = that.getVlrCRComlSusp();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRComlSusp", lhsVlrCRComlSusp), LocatorUtils.property(thatLocator, "vlrCRComlSusp", rhsVlrCRComlSusp), lhsVlrCRComlSusp, rhsVlrCRComlSusp)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        String theCRComl;
                        theCRComl = this.getCRComl();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "crComl", theCRComl), currentHashCode, theCRComl);
                    }
                    {
                        String theVlrCRComl;
                        theVlrCRComl = this.getVlrCRComl();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRComl", theVlrCRComl), currentHashCode, theVlrCRComl);
                    }
                    {
                        String theVlrCRComlSusp;
                        theVlrCRComlSusp = this.getVlrCRComlSusp();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRComlSusp", theVlrCRComlSusp), currentHashCode, theVlrCRComlSusp);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }

            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="tpInscTomador">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
             *               &lt;minInclusive value="1"/>
             *               &lt;maxInclusive value="4"/>
             *               &lt;pattern value="[1|4]"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="nrInscTomador">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;pattern value="[0-9]{14}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrTotalBaseRet">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrTotalRetPrinc">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrTotalRetAdic" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrTotalNRetAdic" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "tpInscTomador",
                "nrInscTomador",
                "vlrTotalBaseRet",
                "vlrTotalRetPrinc",
                "vlrTotalRetAdic",
                "vlrTotalNRetPrinc",
                "vlrTotalNRetAdic"
            })
            @Entity(name = "Reinf$EvtTotalContrib$InfoTotalContrib$RPrest")
            @Table(name = "RPREST")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class RPrest
                implements Equals, HashCode
            {

                protected short tpInscTomador;
                @XmlElement(required = true)
                protected String nrInscTomador;
                @XmlElement(required = true)
                protected String vlrTotalBaseRet;
                @XmlElement(required = true)
                protected String vlrTotalRetPrinc;
                protected String vlrTotalRetAdic;
                protected String vlrTotalNRetPrinc;
                protected String vlrTotalNRetAdic;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade tpInscTomador.
                 * 
                 */
                @Basic
                @Column(name = "TP_INSC_TOMADOR", scale = 0)
                public short getTpInscTomador() {
                    return tpInscTomador;
                }

                /**
                 * Define o valor da propriedade tpInscTomador.
                 * 
                 */
                public void setTpInscTomador(short value) {
                    this.tpInscTomador = value;
                }

                /**
                 * Obt�m o valor da propriedade nrInscTomador.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "NR_INSC_TOMADOR")
                public String getNrInscTomador() {
                    return nrInscTomador;
                }

                /**
                 * Define o valor da propriedade nrInscTomador.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNrInscTomador(String value) {
                    this.nrInscTomador = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrTotalBaseRet.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_TOTAL_BASE_RET", length = 17)
                public String getVlrTotalBaseRet() {
                    return vlrTotalBaseRet;
                }

                /**
                 * Define o valor da propriedade vlrTotalBaseRet.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrTotalBaseRet(String value) {
                    this.vlrTotalBaseRet = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrTotalRetPrinc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_TOTAL_RET_PRINC", length = 17)
                public String getVlrTotalRetPrinc() {
                    return vlrTotalRetPrinc;
                }

                /**
                 * Define o valor da propriedade vlrTotalRetPrinc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrTotalRetPrinc(String value) {
                    this.vlrTotalRetPrinc = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrTotalRetAdic.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_TOTAL_RET_ADIC", length = 17)
                public String getVlrTotalRetAdic() {
                    return vlrTotalRetAdic;
                }

                /**
                 * Define o valor da propriedade vlrTotalRetAdic.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrTotalRetAdic(String value) {
                    this.vlrTotalRetAdic = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrTotalNRetPrinc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_TOTAL_NRET_PRINC", length = 17)
                public String getVlrTotalNRetPrinc() {
                    return vlrTotalNRetPrinc;
                }

                /**
                 * Define o valor da propriedade vlrTotalNRetPrinc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrTotalNRetPrinc(String value) {
                    this.vlrTotalNRetPrinc = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrTotalNRetAdic.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_TOTAL_NRET_ADIC", length = 17)
                public String getVlrTotalNRetAdic() {
                    return vlrTotalNRetAdic;
                }

                /**
                 * Define o valor da propriedade vlrTotalNRetAdic.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrTotalNRetAdic(String value) {
                    this.vlrTotalNRetAdic = value;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtTotalContrib.InfoTotalContrib.RPrest)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtTotalContrib.InfoTotalContrib.RPrest that = ((Reinf.EvtTotalContrib.InfoTotalContrib.RPrest) object);
                    {
                        short lhsTpInscTomador;
                        lhsTpInscTomador = this.getTpInscTomador();
                        short rhsTpInscTomador;
                        rhsTpInscTomador = that.getTpInscTomador();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInscTomador", lhsTpInscTomador), LocatorUtils.property(thatLocator, "tpInscTomador", rhsTpInscTomador), lhsTpInscTomador, rhsTpInscTomador)) {
                            return false;
                        }
                    }
                    {
                        String lhsNrInscTomador;
                        lhsNrInscTomador = this.getNrInscTomador();
                        String rhsNrInscTomador;
                        rhsNrInscTomador = that.getNrInscTomador();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInscTomador", lhsNrInscTomador), LocatorUtils.property(thatLocator, "nrInscTomador", rhsNrInscTomador), lhsNrInscTomador, rhsNrInscTomador)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrTotalBaseRet;
                        lhsVlrTotalBaseRet = this.getVlrTotalBaseRet();
                        String rhsVlrTotalBaseRet;
                        rhsVlrTotalBaseRet = that.getVlrTotalBaseRet();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalBaseRet", lhsVlrTotalBaseRet), LocatorUtils.property(thatLocator, "vlrTotalBaseRet", rhsVlrTotalBaseRet), lhsVlrTotalBaseRet, rhsVlrTotalBaseRet)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrTotalRetPrinc;
                        lhsVlrTotalRetPrinc = this.getVlrTotalRetPrinc();
                        String rhsVlrTotalRetPrinc;
                        rhsVlrTotalRetPrinc = that.getVlrTotalRetPrinc();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalRetPrinc", lhsVlrTotalRetPrinc), LocatorUtils.property(thatLocator, "vlrTotalRetPrinc", rhsVlrTotalRetPrinc), lhsVlrTotalRetPrinc, rhsVlrTotalRetPrinc)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrTotalRetAdic;
                        lhsVlrTotalRetAdic = this.getVlrTotalRetAdic();
                        String rhsVlrTotalRetAdic;
                        rhsVlrTotalRetAdic = that.getVlrTotalRetAdic();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalRetAdic", lhsVlrTotalRetAdic), LocatorUtils.property(thatLocator, "vlrTotalRetAdic", rhsVlrTotalRetAdic), lhsVlrTotalRetAdic, rhsVlrTotalRetAdic)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrTotalNRetPrinc;
                        lhsVlrTotalNRetPrinc = this.getVlrTotalNRetPrinc();
                        String rhsVlrTotalNRetPrinc;
                        rhsVlrTotalNRetPrinc = that.getVlrTotalNRetPrinc();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalNRetPrinc", lhsVlrTotalNRetPrinc), LocatorUtils.property(thatLocator, "vlrTotalNRetPrinc", rhsVlrTotalNRetPrinc), lhsVlrTotalNRetPrinc, rhsVlrTotalNRetPrinc)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrTotalNRetAdic;
                        lhsVlrTotalNRetAdic = this.getVlrTotalNRetAdic();
                        String rhsVlrTotalNRetAdic;
                        rhsVlrTotalNRetAdic = that.getVlrTotalNRetAdic();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalNRetAdic", lhsVlrTotalNRetAdic), LocatorUtils.property(thatLocator, "vlrTotalNRetAdic", rhsVlrTotalNRetAdic), lhsVlrTotalNRetAdic, rhsVlrTotalNRetAdic)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        short theTpInscTomador;
                        theTpInscTomador = this.getTpInscTomador();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInscTomador", theTpInscTomador), currentHashCode, theTpInscTomador);
                    }
                    {
                        String theNrInscTomador;
                        theNrInscTomador = this.getNrInscTomador();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInscTomador", theNrInscTomador), currentHashCode, theNrInscTomador);
                    }
                    {
                        String theVlrTotalBaseRet;
                        theVlrTotalBaseRet = this.getVlrTotalBaseRet();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrTotalBaseRet", theVlrTotalBaseRet), currentHashCode, theVlrTotalBaseRet);
                    }
                    {
                        String theVlrTotalRetPrinc;
                        theVlrTotalRetPrinc = this.getVlrTotalRetPrinc();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrTotalRetPrinc", theVlrTotalRetPrinc), currentHashCode, theVlrTotalRetPrinc);
                    }
                    {
                        String theVlrTotalRetAdic;
                        theVlrTotalRetAdic = this.getVlrTotalRetAdic();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrTotalRetAdic", theVlrTotalRetAdic), currentHashCode, theVlrTotalRetAdic);
                    }
                    {
                        String theVlrTotalNRetPrinc;
                        theVlrTotalNRetPrinc = this.getVlrTotalNRetPrinc();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrTotalNRetPrinc", theVlrTotalNRetPrinc), currentHashCode, theVlrTotalNRetPrinc);
                    }
                    {
                        String theVlrTotalNRetAdic;
                        theVlrTotalNRetAdic = this.getVlrTotalNRetAdic();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrTotalNRetAdic", theVlrTotalNRetAdic), currentHashCode, theVlrTotalNRetAdic);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }

            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CRRecRepAD">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="1"/>
             *               &lt;maxLength value="6"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrCRRecRepAD">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrCRRecRepADSusp" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "crRecRepAD",
                "vlrCRRecRepAD",
                "vlrCRRecRepADSusp"
            })
            @Entity(name = "Reinf$EvtTotalContrib$InfoTotalContrib$RRecRepAD")
            @Table(name = "RREC_REP_AD")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class RRecRepAD
                implements Equals, HashCode
            {

                @XmlElement(name = "CRRecRepAD", required = true)
                protected String crRecRepAD;
                @XmlElement(required = true)
                protected String vlrCRRecRepAD;
                protected String vlrCRRecRepADSusp;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade crRecRepAD.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "CRREC_REP_AD", length = 6)
                public String getCRRecRepAD() {
                    return crRecRepAD;
                }

                /**
                 * Define o valor da propriedade crRecRepAD.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCRRecRepAD(String value) {
                    this.crRecRepAD = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrCRRecRepAD.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_CRREC_REP_AD", length = 17)
                public String getVlrCRRecRepAD() {
                    return vlrCRRecRepAD;
                }

                /**
                 * Define o valor da propriedade vlrCRRecRepAD.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrCRRecRepAD(String value) {
                    this.vlrCRRecRepAD = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrCRRecRepADSusp.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_CRREC_REP_ADSUSP", length = 17)
                public String getVlrCRRecRepADSusp() {
                    return vlrCRRecRepADSusp;
                }

                /**
                 * Define o valor da propriedade vlrCRRecRepADSusp.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrCRRecRepADSusp(String value) {
                    this.vlrCRRecRepADSusp = value;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD that = ((Reinf.EvtTotalContrib.InfoTotalContrib.RRecRepAD) object);
                    {
                        String lhsCRRecRepAD;
                        lhsCRRecRepAD = this.getCRRecRepAD();
                        String rhsCRRecRepAD;
                        rhsCRRecRepAD = that.getCRRecRepAD();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "crRecRepAD", lhsCRRecRepAD), LocatorUtils.property(thatLocator, "crRecRepAD", rhsCRRecRepAD), lhsCRRecRepAD, rhsCRRecRepAD)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrCRRecRepAD;
                        lhsVlrCRRecRepAD = this.getVlrCRRecRepAD();
                        String rhsVlrCRRecRepAD;
                        rhsVlrCRRecRepAD = that.getVlrCRRecRepAD();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRRecRepAD", lhsVlrCRRecRepAD), LocatorUtils.property(thatLocator, "vlrCRRecRepAD", rhsVlrCRRecRepAD), lhsVlrCRRecRepAD, rhsVlrCRRecRepAD)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrCRRecRepADSusp;
                        lhsVlrCRRecRepADSusp = this.getVlrCRRecRepADSusp();
                        String rhsVlrCRRecRepADSusp;
                        rhsVlrCRRecRepADSusp = that.getVlrCRRecRepADSusp();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRRecRepADSusp", lhsVlrCRRecRepADSusp), LocatorUtils.property(thatLocator, "vlrCRRecRepADSusp", rhsVlrCRRecRepADSusp), lhsVlrCRRecRepADSusp, rhsVlrCRRecRepADSusp)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        String theCRRecRepAD;
                        theCRRecRepAD = this.getCRRecRepAD();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "crRecRepAD", theCRRecRepAD), currentHashCode, theCRRecRepAD);
                    }
                    {
                        String theVlrCRRecRepAD;
                        theVlrCRRecRepAD = this.getVlrCRRecRepAD();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRRecRepAD", theVlrCRRecRepAD), currentHashCode, theVlrCRRecRepAD);
                    }
                    {
                        String theVlrCRRecRepADSusp;
                        theVlrCRRecRepADSusp = this.getVlrCRRecRepADSusp();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRRecRepADSusp", theVlrCRRecRepADSusp), currentHashCode, theVlrCRRecRepADSusp);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }

            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="cnpjPrestador">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;pattern value="[0-9]{14}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="CNO" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;pattern value="[0-9]{12}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="vlrTotalBaseRet">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;minLength value="4"/>
             *               &lt;maxLength value="17"/>
             *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="infoCRTom" maxOccurs="2">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="CRTom">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;minLength value="1"/>
             *                         &lt;maxLength value="6"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="VlrCRTom">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;minLength value="4"/>
             *                         &lt;maxLength value="17"/>
             *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="VlrCRTomSusp" minOccurs="0">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;minLength value="4"/>
             *                         &lt;maxLength value="17"/>
             *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "cnpjPrestador",
                "cno",
                "vlrTotalBaseRet",
                "infoCRTom"
            })
            @Entity(name = "Reinf$EvtTotalContrib$InfoTotalContrib$RTom")
            @Table(name = "RTOM")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class RTom
                implements Equals, HashCode
            {

                @XmlElement(required = true)
                protected String cnpjPrestador;
                @XmlElement(name = "CNO")
                protected String cno;
                @XmlElement(required = true)
                protected String vlrTotalBaseRet;
                @XmlElement(required = true)
                protected List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom> infoCRTom;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade cnpjPrestador.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "CNPJ_PRESTADOR")
                public String getCnpjPrestador() {
                    return cnpjPrestador;
                }

                /**
                 * Define o valor da propriedade cnpjPrestador.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCnpjPrestador(String value) {
                    this.cnpjPrestador = value;
                }

                /**
                 * Obt�m o valor da propriedade cno.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "CNO")
                public String getCNO() {
                    return cno;
                }

                /**
                 * Define o valor da propriedade cno.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCNO(String value) {
                    this.cno = value;
                }

                /**
                 * Obt�m o valor da propriedade vlrTotalBaseRet.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                @Basic
                @Column(name = "VLR_TOTAL_BASE_RET", length = 17)
                public String getVlrTotalBaseRet() {
                    return vlrTotalBaseRet;
                }

                /**
                 * Define o valor da propriedade vlrTotalBaseRet.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVlrTotalBaseRet(String value) {
                    this.vlrTotalBaseRet = value;
                }

                /**
                 * Gets the value of the infoCRTom property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the infoCRTom property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getInfoCRTom().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom }
                 * 
                 * 
                 */
                @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttotalcontrib.v1_04_00.Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom.class, cascade = {
                    CascadeType.ALL
                })
                @JoinColumn(name = "INFO_CRTOM_RTOM_HJID")
                public List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom> getInfoCRTom() {
                    if (infoCRTom == null) {
                        infoCRTom = new ArrayList<Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom>();
                    }
                    return this.infoCRTom;
                }

                /**
                 * 
                 * 
                 */
                public void setInfoCRTom(List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom> infoCRTom) {
                    this.infoCRTom = infoCRTom;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtTotalContrib.InfoTotalContrib.RTom)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtTotalContrib.InfoTotalContrib.RTom that = ((Reinf.EvtTotalContrib.InfoTotalContrib.RTom) object);
                    {
                        String lhsCnpjPrestador;
                        lhsCnpjPrestador = this.getCnpjPrestador();
                        String rhsCnpjPrestador;
                        rhsCnpjPrestador = that.getCnpjPrestador();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "cnpjPrestador", lhsCnpjPrestador), LocatorUtils.property(thatLocator, "cnpjPrestador", rhsCnpjPrestador), lhsCnpjPrestador, rhsCnpjPrestador)) {
                            return false;
                        }
                    }
                    {
                        String lhsCNO;
                        lhsCNO = this.getCNO();
                        String rhsCNO;
                        rhsCNO = that.getCNO();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "cno", lhsCNO), LocatorUtils.property(thatLocator, "cno", rhsCNO), lhsCNO, rhsCNO)) {
                            return false;
                        }
                    }
                    {
                        String lhsVlrTotalBaseRet;
                        lhsVlrTotalBaseRet = this.getVlrTotalBaseRet();
                        String rhsVlrTotalBaseRet;
                        rhsVlrTotalBaseRet = that.getVlrTotalBaseRet();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalBaseRet", lhsVlrTotalBaseRet), LocatorUtils.property(thatLocator, "vlrTotalBaseRet", rhsVlrTotalBaseRet), lhsVlrTotalBaseRet, rhsVlrTotalBaseRet)) {
                            return false;
                        }
                    }
                    {
                        List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom> lhsInfoCRTom;
                        lhsInfoCRTom = (((this.infoCRTom!= null)&&(!this.infoCRTom.isEmpty()))?this.getInfoCRTom():null);
                        List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom> rhsInfoCRTom;
                        rhsInfoCRTom = (((that.infoCRTom!= null)&&(!that.infoCRTom.isEmpty()))?that.getInfoCRTom():null);
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "infoCRTom", lhsInfoCRTom), LocatorUtils.property(thatLocator, "infoCRTom", rhsInfoCRTom), lhsInfoCRTom, rhsInfoCRTom)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        String theCnpjPrestador;
                        theCnpjPrestador = this.getCnpjPrestador();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cnpjPrestador", theCnpjPrestador), currentHashCode, theCnpjPrestador);
                    }
                    {
                        String theCNO;
                        theCNO = this.getCNO();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cno", theCNO), currentHashCode, theCNO);
                    }
                    {
                        String theVlrTotalBaseRet;
                        theVlrTotalBaseRet = this.getVlrTotalBaseRet();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrTotalBaseRet", theVlrTotalBaseRet), currentHashCode, theVlrTotalBaseRet);
                    }
                    {
                        List<Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom> theInfoCRTom;
                        theInfoCRTom = (((this.infoCRTom!= null)&&(!this.infoCRTom.isEmpty()))?this.getInfoCRTom():null);
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoCRTom", theInfoCRTom), currentHashCode, theInfoCRTom);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }


                /**
                 * <p>Classe Java de anonymous complex type.
                 * 
                 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="CRTom">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;minLength value="1"/>
                 *               &lt;maxLength value="6"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="VlrCRTom">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;minLength value="4"/>
                 *               &lt;maxLength value="17"/>
                 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="VlrCRTomSusp" minOccurs="0">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;minLength value="4"/>
                 *               &lt;maxLength value="17"/>
                 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "crTom",
                    "vlrCRTom",
                    "vlrCRTomSusp"
                })
                @Entity(name = "Reinf$EvtTotalContrib$InfoTotalContrib$RTom$InfoCRTom")
                @Table(name = "INFO_CRTOM")
                @Inheritance(strategy = InheritanceType.JOINED)
                public static class InfoCRTom
                    implements Equals, HashCode
                {

                    @XmlElement(name = "CRTom", required = true)
                    protected String crTom;
                    @XmlElement(name = "VlrCRTom", required = true)
                    protected String vlrCRTom;
                    @XmlElement(name = "VlrCRTomSusp")
                    protected String vlrCRTomSusp;
                    @XmlAttribute(name = "Hjid")
                    protected Long hjid;

                    /**
                     * Obt�m o valor da propriedade crTom.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "CRTOM", length = 6)
                    public String getCRTom() {
                        return crTom;
                    }

                    /**
                     * Define o valor da propriedade crTom.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCRTom(String value) {
                        this.crTom = value;
                    }

                    /**
                     * Obt�m o valor da propriedade vlrCRTom.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "VLR_CRTOM", length = 17)
                    public String getVlrCRTom() {
                        return vlrCRTom;
                    }

                    /**
                     * Define o valor da propriedade vlrCRTom.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setVlrCRTom(String value) {
                        this.vlrCRTom = value;
                    }

                    /**
                     * Obt�m o valor da propriedade vlrCRTomSusp.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "VLR_CRTOM_SUSP", length = 17)
                    public String getVlrCRTomSusp() {
                        return vlrCRTomSusp;
                    }

                    /**
                     * Define o valor da propriedade vlrCRTomSusp.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setVlrCRTomSusp(String value) {
                        this.vlrCRTomSusp = value;
                    }

                    /**
                     * Obt�m o valor da propriedade hjid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Id
                    @Column(name = "HJID")
                    @GeneratedValue(strategy = GenerationType.AUTO)
                    public Long getHjid() {
                        return hjid;
                    }

                    /**
                     * Define o valor da propriedade hjid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setHjid(Long value) {
                        this.hjid = value;
                    }

                    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                        if (!(object instanceof Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom)) {
                            return false;
                        }
                        if (this == object) {
                            return true;
                        }
                        final Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom that = ((Reinf.EvtTotalContrib.InfoTotalContrib.RTom.InfoCRTom) object);
                        {
                            String lhsCRTom;
                            lhsCRTom = this.getCRTom();
                            String rhsCRTom;
                            rhsCRTom = that.getCRTom();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "crTom", lhsCRTom), LocatorUtils.property(thatLocator, "crTom", rhsCRTom), lhsCRTom, rhsCRTom)) {
                                return false;
                            }
                        }
                        {
                            String lhsVlrCRTom;
                            lhsVlrCRTom = this.getVlrCRTom();
                            String rhsVlrCRTom;
                            rhsVlrCRTom = that.getVlrCRTom();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRTom", lhsVlrCRTom), LocatorUtils.property(thatLocator, "vlrCRTom", rhsVlrCRTom), lhsVlrCRTom, rhsVlrCRTom)) {
                                return false;
                            }
                        }
                        {
                            String lhsVlrCRTomSusp;
                            lhsVlrCRTomSusp = this.getVlrCRTomSusp();
                            String rhsVlrCRTomSusp;
                            rhsVlrCRTomSusp = that.getVlrCRTomSusp();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrCRTomSusp", lhsVlrCRTomSusp), LocatorUtils.property(thatLocator, "vlrCRTomSusp", rhsVlrCRTomSusp), lhsVlrCRTomSusp, rhsVlrCRTomSusp)) {
                                return false;
                            }
                        }
                        return true;
                    }

                    public boolean equals(Object object) {
                        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                        return equals(null, null, object, strategy);
                    }

                    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                        int currentHashCode = 1;
                        {
                            String theCRTom;
                            theCRTom = this.getCRTom();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "crTom", theCRTom), currentHashCode, theCRTom);
                        }
                        {
                            String theVlrCRTom;
                            theVlrCRTom = this.getVlrCRTom();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRTom", theVlrCRTom), currentHashCode, theVlrCRTom);
                        }
                        {
                            String theVlrCRTomSusp;
                            theVlrCRTomSusp = this.getVlrCRTomSusp();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlrCRTomSusp", theVlrCRTomSusp), currentHashCode, theVlrCRTomSusp);
                        }
                        return currentHashCode;
                    }

                    public int hashCode() {
                        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                        return this.hashCode(null, strategy);
                    }

                }

            }

        }

    }

}
