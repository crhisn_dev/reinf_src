package grxtracking.persistence.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.SerializationUtils;

import br.com.ttireinf.xml.XMLGenerator;
import br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf;
import br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri;

import org.firebirdsql.jdbc.*;

public class TaskerTest {

	public static void main22(String[] args) {
		try {

			Class.forName("org.firebirdsql.jdbc.FBDriver");
			Connection connection = DriverManager.getConnection("jdbc:firebirdsql:localhost/3050:c:/SISHOSP.fdb",
					"SYSDBA", "crhisn2572");
			ResultSet result = connection.createStatement().executeQuery("select tdesc from TURNO");
			while (result.next()) {
				String d = result.getString("tdesc");
				System.out.println(d);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main2222(String[] args) {
		try {

			EntityManagerFactory emf = Persistence.createEntityManagerFactory("myreinf_fb");
			EntityManager em = emf.createEntityManager();
			System.out.println("em: " + em);

			EntityTransaction t = em.getTransaction();

			t.begin();

			TypedQuery<EvtInfoContri> q = em.createQuery("select a from Reinf$EvtInfoContri as a", EvtInfoContri.class);
			List<EvtInfoContri> rr = q.getResultList();
			rr.forEach(e -> {
				try {
					System.out.println(e.getId());
					XMLGenerator x = new XMLGenerator("br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02");

					System.out.println(x.toXMLString(e));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			});

			t.commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
