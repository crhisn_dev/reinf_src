package test;

import java.util.Hashtable;

import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.ReadFile;

public class TestSendXML {

	public void changeAndSend() {
		try {

			ManagerInterface manager = Locator.getManagerReference("localhost");
			

			String xml = ReadFile.readFile("C:/Users/Administrador.WIN-SERVIDOR/Downloads/REINF_100018101711575210.XML");

			manager.sendXml(xml, new Hashtable<String, String>());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendInCont() {
		try {
			ManagerInterface manager = Locator.getManagerReference();

			Hashtable<String, Object> props = new Hashtable<String, Object>();
			manager.changeToState("00473892000148", "", TPEMISS.NORMAL, props);

			TPEMISS type = TPEMISS.CONTINGENCE_EPEC;
			manager.leaveContingence("00473892000148", "", true, type);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		TestSendXML s = new TestSendXML();
		// s.sendInCont();
		s.changeAndSend();
	}
}
