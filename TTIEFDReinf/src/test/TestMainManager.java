package test;

import br.com.tti.sefaz.main.InitManager;

public class TestMainManager {

	public static void main(String[] args) {
		InitManager.main(new String[] { "-xml",
				"C:/TTICTe/conf/configuracao_cte.xml", "-rmiregistry",
				"-local", "-cfg",
				System.getProperty("user.dir") + "/xsd/cte_v1.04.xsd",
				"-xsdevento",
				System.getProperty("user.dir") + "/xsd/eventoCTe_v1.04.xsd",
				"-xsdepec",
				System.getProperty("user.dir") + "/xsd/evEPECCTe_v1.04.xsd" });

		/*
		 * InitManager.main(new String[] { "-xml",
		 * "C:/TTICTe/conf/configuracao_cte.xml", "-rmiregistry", "-local",
		 * "-cfg", System.getProperty("user.dir") + "/xsd/cte_v1.04.xsd",
		 * "-xsdevento", System.getProperty("user.dir") +
		 * "/xsd/eventoCTe_v1.04.xsd", "-xsdepec",
		 * System.getProperty("user.dir") + "/xsd/evEPECCTe_v1.04.xsd" });
		 */
	}
}
