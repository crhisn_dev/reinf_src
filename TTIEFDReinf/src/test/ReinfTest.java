package test;

import javax.xml.ws.BindingProvider;

import br.gov.fazenda.sped.ReceberLoteEventos.LoteEventos;
import br.gov.fazenda.sped.ReceberLoteEventosResponse.ReceberLoteEventosResult;
import br.com.tti.sefaz.util.ReadFile;
import br.gov.fazenda.sped.RecepcaoLoteReinf;
import br.gov.fazenda.sped.RecepcaoLoteReinf_Service;

public class ReinfTest {

	public static void ajustarPropriedadesFwd(String fileTrust, String fileKey, String passwordTrust,
			String passwordKey) {
		System.setProperty("javax.net.ssl.keyStore", fileKey);
		System.setProperty("javax.net.ssl.trustStore", fileTrust);
		System.setProperty("javax.net.ssl.keyStorePassword", passwordKey);
		System.setProperty("javax.net.ssl.trustStorePassword", passwordTrust);
		System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
	}

	public static void main(String[] args) {
		ajustarPropriedadesFwd("C:\\TTIRec\\certificados\\myroot.jks", "C:\\TTIRec\\certificados\\Tome-Equp_2018.pfx",
				"crhisn", "tome1234");

		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "true");

		RecepcaoLoteReinf_Service service = new RecepcaoLoteReinf_Service();
		LoteEventos loteEventos = new LoteEventos();

		{
			try {
				String data1 = ReadFile
						.readFile("G:\\TTIDesktop\\sidney_environment\\myworkspace\\TTIEFDReinf\\src\\a.xml");
				loteEventos.getContent().add(data1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		RecepcaoLoteReinf port = service.getBasicHttpBindingRecepcaoLoteReinf();

		String endpointURL = "https://preprodefdreinf.receita.fazenda.gov.br/RecepcaoLoteReinf.svc?singleWSDL";
		BindingProvider bp = (BindingProvider) port;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);

		ReceberLoteEventosResult response = port.receberLoteEventos(loteEventos);
		System.out.println(response.getContent());
	}
}
