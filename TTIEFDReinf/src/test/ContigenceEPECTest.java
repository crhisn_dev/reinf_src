package test;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import br.com.tti.sefaz.contingence.ContingenceController;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.remote.DBConfig;
import br.com.tti.sefaz.remote.SchemaVersionConfig;
import br.com.tti.sefaz.remote.SenderConfig;
import br.com.tti.sefaz.remote.ServicesConfig;
import br.com.tti.sefaz.sender.Sender;
import br.com.tti.sefaz.signer.Main;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.SYSTEM;
import br.com.tti.sefaz.systemconfig.XMLConfigSystem;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.util.ManagerFacadeMock;
import br.com.tti.sefaz.util.ModoOpManager;
import br.com.tti.sefaz.util.ReadFile;

public class ContigenceEPECTest {
	public static void main(String[] args) throws Exception {
		ManagerInterface mockmanager = new ManagerFacadeMock() {
			@Override
			public DBConfig getDBConfig() throws RemoteException {
				DBConfig db = new DBConfig();
				db.setDriver("com.mysql.jdbc.Driver");
				db.setPassword("tti");
				db.setUrl("jdbc:mysql://localhost:3307/tticte");
				db.setUser("root");
				return db;
			}

			@Override
			public Hashtable<Vector<String>, Vector<SchemaVersionConfig>> getSchemasVersion()
					throws RemoteException {
				Hashtable<Vector<String>, Vector<SchemaVersionConfig>> config = new Hashtable<Vector<String>, Vector<SchemaVersionConfig>>();
				Vector<String> ufs = new Vector<String>();
				ufs.add("epec");
				ufs.add("35");
				Vector<SchemaVersionConfig> schemas = new Vector<SchemaVersionConfig>();

				SchemaVersionConfig schema = new SchemaVersionConfig();
				schema.setSchemaName("eventoNFe");
				schema.setUf("35");
				schema.setValue("1.04");

				schemas.add(schema);
				config.put(ufs, schemas);
				return config;
			}

			@Override
			public String signForCNPJ__Reinf(String cnpj, String xml, String tag)
					throws RemoteException {
				Main mm = new Main(
						"C:\\TTIRec\\certificados\\tome_equipamentos_2013.pfx",
						"tome1234");
				String signed = "NAO FEZ NADA";
				try {
					signed = mm.myAssinaCTeEvent(xml);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return signed;
			}

			@Override
			public String sendXMLMessage(String idServico, String header,
					String data, Hashtable prop) throws RemoteException {
				Sender sender = new Sender();
				return sender.sendXMLMessage(idServico, header, data, prop);
			}

			@Override
			public SenderConfig getSenderConfig() throws RemoteException {
				SenderConfig senderconfig = new SenderConfig();
				Vector<String> cnpjs = new Vector<String>();
				cnpjs.add("44384832000124");
				senderconfig.setCnpjs(cnpjs);
				senderconfig
						.setPfxTransmission("C:\\TTIRec\\certificados\\tome_equipamentos_2013.pfx");
				senderconfig
						.setTrustStoreFile("C:\\TTIRec\\certificados\\myroot.jks");

				return senderconfig;
			}

			@Override
			public ServicesConfig getServiceConfig() throws RemoteException {
				XMLConfigSystem confsys = new XMLConfigSystem(
						"C:\\TTICTe\\conf\\configuracao_cte.xml");
				confsys.make();
				ServicesConfig services = confsys.getServiceConfig();
				return services;
			}

			@Override
			public Hashtable<String, CNPJData> getCNPJ() throws RemoteException {

				Hashtable<String, CNPJData> cnpjs = new Hashtable<String, CNPJData>();
				CNPJData info = new CNPJData();
				info.setCnpj("44384832000124");
				cnpjs.put("44384832000124", info);
				return cnpjs;
			}
		};

		MainParameters.processArguments(new String[] { "-xsdevento",
				System.getProperty("user.dir") + "/xsd/eventoCTe_v1.04.xsd",
				"-xsdepec",
				System.getProperty("user.dir") + "/xsd/evEPECCTe_v1.04.xsd" });
		Locator.setManager(mockmanager);

		ModoOpManager manager = new ModoOpManager();
		manager.saveModo("44384832000124", TPEMISS.CONTINGENCE_EPEC, null, null);

		ContingenceController controller = new ContingenceController(
				mockmanager);
		String keyXml = "CTe35130844384832000124570000000000031100000019";
		String xml = ReadFile
				.readFile("C:\\Users\\Administrador.WIN-SERVIDOR\\Documents\\XMLs\\CTe_44384832000124_000_000000003.xml");
		Hashtable<String, Object> props = new Hashtable<String, Object>();
		props.put("xJust", "Deu problemas com a red da empresa");
		controller.enterContingence("44384832000124", "homologacao", false,
				TPEMISS.CONTINGENCE_EPEC);

		controller.processInContingence(keyXml, "", xml, props);

	}
}
