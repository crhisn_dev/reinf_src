package br.com.tti.ldap.authenticator;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class ADAuthenticator {
	private String domain;
	private String ldapHost;
	private String searchBase;

	public ADAuthenticator() {
		ResourceBundle adprop = ResourceBundle.getBundle("ad");
		this.domain = adprop.getString("domain");
		this.ldapHost = adprop.getString("url");
		this.searchBase = adprop.getString("base");
	}

	public ADAuthenticator(String domain, String host, String dn) {
		this.domain = domain;
		this.ldapHost = host;
		this.searchBase = dn;
	}

	public Map authenticate(String user, String pass) throws Exception {
		String returnedAtts[] = { "sn", "givenName", "mail", "whenCreated" };
		String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user
				+ "))";

		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		searchCtls.setReturningAttributes(returnedAtts);

		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapHost);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
		env.put(Context.SECURITY_CREDENTIALS, pass);

		LdapContext ctxGC = null;
		boolean ldapUser = false;

		ctxGC = new InitialLdapContext(env, null);
		// Search objects in GC using filters
		NamingEnumeration answer = ctxGC.search(searchBase, searchFilter,
				searchCtls);
		while (answer.hasMoreElements()) {

			SearchResult sr = (SearchResult) answer.next();

			Attributes attrs = sr.getAttributes();
			Map amap = null;
			if (attrs != null) {
				amap = new HashMap();
				NamingEnumeration ne = attrs.getAll();
				while (ne.hasMore()) {
					Attribute attr = (Attribute) ne.next();
					amap.put(attr.getID(), attr.get());
					ldapUser = true;
				}	
				ne.close();
			}
			return amap;
		}

		return null;
	}

	public static void main(String[] args) {
		// ADAuthenticator auth = new ADAuthenticator();
		String url = "rar.23.3.orP.rotidE.FDP.xifnI/2fb6m1f3/selif/moc.gnidaolpu//:ptth";
		for (int i = url.length() - 1; i > 0; i--) {
			System.out.print(url.substring(i, i + 1));
		}

	}
}
