package br.com.tti.sefaz.xmlgenerate.nfe;

import javax.xml.bind.JAXBElement;

import br.com.taragona.nfe.classes.consulta.TRetConsSitNFe;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xmlgenerate.XMLWrapperQuery;

public class XMLWrapperQueryNFeImpl implements XMLWrapperQuery {

	private JAXBElement<TRetConsSitNFe> query;
	private XMLGenerator xmlg;
	private String xmlString;

	public XMLWrapperQueryNFeImpl(String xmlResult) throws Exception {
		super();
		xmlResult = this.getStringXML(xmlResult);
		this.xmlg = XMLGenerator
				.createXMLGenerator("br.com.taragona.nfe.classes.consulta");
		this.query = (JAXBElement<TRetConsSitNFe>) this.xmlg
				.toObject(xmlResult);
		this.xmlString = xmlResult;
	}

	private String getStringXML(String xml) {
		xml = xml.replace("ns2:", "");

		int pos1 = xml.indexOf("<retConsSitNFe");
		int pos2 = xml.indexOf("</nfeConsultaNF2Result>");

		if (pos1 != -1 && pos2 != -1)
			return xml.substring(pos1, pos2);
		return xml;
	}

	@Override
	public String getCStat() {
		return this.query.getValue().getInfProt().getCStat();
	}

	@Override
	public String getCh() {
		return this.query.getValue().getInfProt().getChNFe();
	}

	@Override
	public String getDhSefaz() {
		if (this.query.getValue().getInfProt().getDhRecbto() != null)
			return this.query.getValue().getInfProt().getDhRecbto().toString();
		return "";
	}

	@Override
	public String getProt() {
		return this.query.getValue().getInfProt().getNProt();
	}

	@Override
	public String getTpAmb() {
		return this.query.getValue().getInfProt().getTpAmb();
	}

	@Override
	public String getXMotivo() {
		return this.query.getValue().getInfProt().getXMotivo();
	}

	@Override
	public String getXmlProtocol() {
		return this.xmlString;
	}

	@Override
	public String getXml() {
		return this.xmlString;
	}

}
