package br.com.tti.sefaz.xmlgenerate.nfe;

import java.util.List;

import javax.xml.bind.JAXBElement;

import br.com.taragona.nfe.classes.recepcao.TRetEnviNFe;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.reinf.eventos.retorno.Reinf;
import br.com.tti.sefaz.reinf.eventos.retorno.TArquivoReinf;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xmlgenerate.XMLWrapperReturnSend;

public class XMLWrapperReturnNFeImpl implements XMLWrapperReturnSend {
	private br.com.tti.sefaz.reinf.eventos.retorno.Reinf ret;

	private XMLGenerator xmlGen;

	private String xml;

	public XMLWrapperReturnNFeImpl(String response) throws Exception {

		int p1 = response.indexOf("<Reinf");
		int p2 = response.indexOf("</Reinf>", response.indexOf("</Reinf>") + 1);
		XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.retorno");
		try {
			String substring = response.substring(p1, p2 + "</Reinf>".length());
			System.out.println(substring);
			this.ret = (Reinf) gen.toObject(substring);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String extractElement(String xml) {

		xml = xml.replace("ns2:", "");

		int pos1 = xml.indexOf("<retEnvi");
		int pos2 = xml.indexOf("</nfeRecepcaoLote2Result>");

		return xml.substring(pos1, pos2);
	}

	@Override
	public String getCStat() {

		try {

			List<TArquivoReinf> eventos = this.ret.getRetornoLoteEventos().getRetornoEventos().getEvento();
			for (TArquivoReinf evento : eventos) {

				System.out.println(evento.getAny());

				XMLGenerator gen2 = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.retorno.total");

				br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf rr = (br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf) gen2
						.getUnmarshaller().unmarshal(evento.getAny());
				return rr.getEvtTotal().getIdeRecRetorno().getIdeStatus().getDescRetorno();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "";
	}

	@Override
	public String getNumberRecibo() {

		try {

			List<TArquivoReinf> eventos = this.ret.getRetornoLoteEventos().getRetornoEventos().getEvento();
			for (TArquivoReinf evento : eventos) {

				System.out.println(evento.getAny());

				XMLGenerator gen2 = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.retorno.total");

				br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf rr = (br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf) gen2
						.getUnmarshaller().unmarshal(evento.getAny());

				if (rr.getEvtTotal().getInfoTotal() != null) {
					return rr.getEvtTotal().getInfoTotal().getNrRecArqBase();
				}
				if (rr.getEvtTotal().getInfoRecEv() != null) {
					return rr.getEvtTotal().getInfoRecEv().getNrProtEntr();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getTpAmb() {
		return "";
	}

	@Override
	public String getXMotivo() {

		try {

			List<TArquivoReinf> eventos = this.ret.getRetornoLoteEventos().getRetornoEventos().getEvento();
			for (TArquivoReinf evento : eventos) {

				System.out.println(evento.getAny());

				XMLGenerator gen2 = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.retorno.total");

				br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf rr = (br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf) gen2
						.getUnmarshaller().unmarshal(evento.getAny());

				if (rr.getEvtTotal().getIdeRecRetorno().getIdeStatus().getRegOcorrs() != null
						&& !rr.getEvtTotal().getIdeRecRetorno().getIdeStatus().getRegOcorrs().isEmpty()) {
					return rr.getEvtTotal().getIdeRecRetorno().getIdeStatus().getRegOcorrs().get(0).getDscResp();
				} else {
					return "";
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getDhRecbto() {
		try {

			List<TArquivoReinf> eventos = this.ret.getRetornoLoteEventos().getRetornoEventos().getEvento();
			for (TArquivoReinf evento : eventos) {

				System.out.println(evento.getAny());

				XMLGenerator gen2 = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.retorno.total");

				br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf rr = (br.com.tti.sefaz.reinf.eventos.retorno.total.Reinf) gen2
						.getUnmarshaller().unmarshal(evento.getAny());
				return rr.getEvtTotal().getIdeEvento().getPerApur();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getXml() {
		return this.xml;
	}

}
