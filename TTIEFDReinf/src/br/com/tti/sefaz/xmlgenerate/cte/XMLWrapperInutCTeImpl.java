package br.com.tti.sefaz.xmlgenerate.cte;

import javax.xml.bind.JAXBElement;

import br.com.tti.sefaz.classes.inutilizacao.TRetInutCTe;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xmlgenerate.XMLWrapperInut;

public class XMLWrapperInutCTeImpl implements XMLWrapperInut {

	private TRetInutCTe inut;
	private XMLGenerator gen;
	private String xml;

	public XMLWrapperInutCTeImpl(String xml) throws Exception {
		this.gen = XMLGenerator
				.createXMLGenerator("br.com.tti.sefaz.classes.inutilizacao");

		this.xml = this.getStringXML(xml);

		this.inut = (TRetInutCTe) this.gen.toObject(this.xml);
	}

	private String getStringXML(String xml) {
		int pos1 = xml.indexOf("<retInutC");
		int pos2 = xml.indexOf("</cteInutilizacaoCTResult>");

		if (pos1 != -1 && pos2 != -1)
			return xml.substring(pos1, pos2);
		return xml;
	}

	@Override
	public String getCStat() {
		// TODO Auto-generated method stub
		return this.inut.getInfInut().getCStat();
	}

	@Override
	public String getDhSefaz() {
		// TODO Auto-generated method stub
		if (this.inut.getInfInut().getDhRecbto() != null)
			return this.inut.getInfInut().getDhRecbto().toString();
		return null;
	}

	@Override
	public String getNFim() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNIni() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProtInut() {
		return this.inut.getInfInut().getNProt();
	}

	@Override
	public String getTpAmb() {
		return this.inut.getInfInut().getTpAmb();
	}

	@Override
	public String getXMotivo() {
		return this.inut.getInfInut().getXMotivo();
	}

	@Override
	public String getXml() {
		// TODO Auto-generated method stub
		return this.xml;
	}

	@Override
	public String getXmlProtocol() {
		// TODO Auto-generated method stub
		return this.inut.getInfInut().getNProt();
	}

}
