package br.com.tti.sefaz.xmlgenerate.cte;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.tti.sefaz.util.ReadFile;

public class ReadXML {

	private Vector<Hashtable<String, String>> values = new Vector<Hashtable<String, String>>();

	public void parseXML(String xml) {
		xml = xml.replace("ns2:", "");
		xml = xml.replace("xmlns:ns2=\"http://www.portalfiscal.inf.br/nfe\"",
				"");
		// xml = xml.replace(" xmlns=\"http://www.w3.org/2000/09/xmldsig#\"",
		// "");

		System.out.println(xml);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {

			Document doc = factory.newDocumentBuilder().parse(
					new ByteArrayInputStream(xml.getBytes()));

			NodeList elem = doc.getDocumentElement().getElementsByTagName(
					"infProt");

			int size1 = elem.getLength();
			for (int ii = 0; ii < size1; ii++) {
				int size = elem.item(ii).getChildNodes().getLength();
				Hashtable<String, String> val = new Hashtable<String, String>();
				for (int i = 0; i < size; i++) {
					String key = elem.item(ii).getChildNodes().item(i)
							.getLocalName();
					String value = elem.item(ii).getChildNodes().item(i)
							.getTextContent();
					/*
					 * value = value.replace(".", "").replace("/", "").replace(
					 * "-", "");
					 */
					if (key != null) {
						val.put(key, value);
					}
				}
				this.values.add(val);
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void readFile(String file) {

		String xml = null;
		try {
			xml = ReadFile.readFile(file);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		this.parseXML(xml);
	}

	public Vector<Hashtable<String, String>> getValues() {
		return values;
	}

	public void setValues(Vector<Hashtable<String, String>> values) {
		this.values = values;
	}

	public static void main(String[] args) {
		ReadXML r = new ReadXML();
		r.readFile("C:\\1.txt");
		System.out.println(r.getValues().toString());
	}
}