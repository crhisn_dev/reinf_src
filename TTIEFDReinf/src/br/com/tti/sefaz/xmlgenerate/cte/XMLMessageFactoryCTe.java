package br.com.tti.sefaz.xmlgenerate.cte;

import java.io.File;
import java.io.FileWriter;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;

import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.remote.SchemaVersionConfig;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.util.FileLocator;
import br.com.tti.sefaz.xmlgenerate.XMLMessageFactory;

public class XMLMessageFactoryCTe implements XMLMessageFactory {

	private Hashtable<Vector<String>, Vector<SchemaVersionConfig>> schemasVersion;

	// uf, schema name, value
	private Hashtable<String, Hashtable<String, String>> tableSchemas;

	public XMLMessageFactoryCTe(Hashtable<Vector<String>, Vector<SchemaVersionConfig>> schemasVersion) {
		super();
		this.schemasVersion = schemasVersion;
		this.tableSchemas = new Hashtable<String, Hashtable<String, String>>();
		this.processSchemas();
	}

	private void processSchemas() {
		for (Vector<String> ufs : this.schemasVersion.keySet()) {
			Hashtable<String, String> versions = new Hashtable<String, String>();
			Vector<SchemaVersionConfig> table = this.schemasVersion.get(ufs);

			for (SchemaVersionConfig svc : table) {
				versions.put(svc.getSchemaName(), svc.getValue());
			}

			for (String uf : ufs) {
				this.tableSchemas.put(uf, versions);
			}
		}
	}

	private String convertIdService(String ambient) {
		if (ambient.equals(SystemProperties.AMBIENT_HOMOLOGACAO))
			return "2";
		if (ambient.equals(SystemProperties.AMBIENT_PRODUCAO))
			return "1";
		return "3";
	}

	public static String createEmptyMessage(Vector<String> xmls) {
		// String version = "1.07";
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<enviNFe xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"" + "1.10\">";
		for (String str : xmls) {
			xml += str;
		}
		xml += "</enviNFe>";
		return xml;
	}

	@Override
	public String createSendMessageXML(String xmlLote, String uf, String ambient) {
		String version = this.tableSchemas.get(uf).get("enviNFe");

		String xml = "<ReceberLoteEventos xmlns=\"http://sped.fazenda.gov.br/\">" + "<loteEventos>"
				+ "<Reinf xmlns=\"http://www.reinf.esocial.gov.br/schemas/envioLoteEventos/v1_02_00\">"
				+ "<loteEventos>" + "<evento id=\"ID1111111110000002017102714215100001\">";
		xml += xmlLote;
		xml += "</evento>" + "</loteEventos>" + "</Reinf>" + "</loteEventos>" + "</ReceberLoteEventos>";
		return xml;
	}

	@Override
	public String createSendMessage(String id, Vector<String> xmls, String uf, String ambient, String idService) {
		String version = this.tableSchemas.get(uf).get("enviNFe");
		String xml = "<ReceberLoteEventos xmlns=\"http://sped.fazenda.gov.br/\">" + "<loteEventos>"
				+ "<Reinf xmlns=\"http://www.reinf.esocial.gov.br/schemas/envioLoteEventos/v1_02_00\">"
				+ "<loteEventos>" + "<evento id=\"ID1111111110000002017102714215100001\">";
		
		for (String str : xmls) {
			xml += str;
		}
		xml += "</enviCTe></cteDadosMsg>";
		return xml;
	}

	@Override
	public String createCallbackMessage(String id, String uf, String ambient) {
		String version = this.tableSchemas.get(uf).get("consReciNFe");
		String ambientType = this.convertIdService(ambient);

		String xml = "<cteDadosMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteRetRecepcao\"><consReciCTe xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\""
				+ version + "\"><tpAmb>" + ambientType + "</tpAmb><nRec>" + id + "</nRec></consReciCTe></cteDadosMsg>";

		return xml;
	}

	@Override
	public String createQueryXmlMessage(String keyXml, String uf, String ambient) {
		String version = this.tableSchemas.get(uf).get("consSitNFe");
		String ambientType = this.convertIdService(ambient);

		keyXml = keyXml.replace("CTe", "");

		String xml = "<cteDadosMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteConsulta\"><consSitCTe xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\""
				+ version + "\"><tpAmb>" + ambientType + "</tpAmb><xServ>CONSULTAR</xServ><chCTe>" + keyXml
				+ "</chCTe></consSitCTe></cteDadosMsg>";
		return xml;
	}

	@Override
	public String createCancelProt(String id, Hashtable<String, String> prop2) {
		if (!id.startsWith("CTe")) {
			id = "CTe" + id;
		}

		XMLData xmldata = null;
		try {
			xmldata = XMLDataCache.getInstance().findData(id);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		if (xmldata != null) {
			String xmlCancel = xmldata.getCancelProtocol();

			if (xmlCancel == null || xmlCancel.isEmpty()) {
				return null;
			}

			String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><procEventoCTe xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\"2.00\">";

			String xml = header + xmlCancel + "</procEventoCTe>";

			try {
				String filename = FileLocator.getInstance().findXMLCancelProt(xmldata.getCnpjEmit(),
						xmldata.getDateCreate(), xmldata.getKeyXml());

				MyLogger.getLog().info("cancel: " + filename);

				FileWriter fw = new FileWriter(new File(filename));
				fw.write(xml);
				fw.close();
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
			return xml;
		}
		MyLogger.getLog().info("Nota nao encontrada: " + id);
		return null;
	}

	@Override
	public String createCCeProt(String id, Hashtable<String, String> prop23) {
		if (!id.startsWith("CTe")) {
			id = "CTe" + id;
		}

		XMLData xmldata = null;
		try {
			xmldata = XMLDataCache.getInstance().findData(id);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		if (xmldata != null) {
			// inut protocol contains XMLs CCe
			String xmlCCe = xmldata.getInutProtocol();

			if (xmlCCe == null || xmlCCe.isEmpty()) {
				return null;
			}

			String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><procEventoCTe xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\"2.00\">";

			String xml = header + xmlCCe + "</procEventoCTe>";

			try {
				String filename = FileLocator.getInstance().findXMLCCeProt(xmldata.getCnpjEmit(),
						xmldata.getDateCreate(), xmldata.getKeyXml());

				MyLogger.getLog().info("cce: " + filename);

				FileWriter fw = new FileWriter(new File(filename));
				fw.write(xml);
				fw.close();
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
			return xml;
		}
		MyLogger.getLog().info("Nota nao encontrada: " + id);
		return null;
	}

	@Override
	public String createCancelMessage(String keyXml, String protocolNumber, String just, String uf, String ambient) {
		String version = this.tableSchemas.get(uf).get("cancNFe");
		String ambientType = this.convertIdService(ambient);

		String xml = "<cteDadosMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteCancelamento\"><cancCTe xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\""
				+ version + "\"><infCanc Id=\"ID" + keyXml.replace("CTe", "") + "\"><tpAmb>" + ambientType
				+ "</tpAmb><xServ>CANCELAR</xServ><chCTe>" + keyXml.replace("CTe", "") + "</chCTe><nProt>"
				+ protocolNumber + "</nProt><xJust>" + just + "</xJust></infCanc></cancCTe></cteDadosMsg>";
		return xml;
	}

	@Override
	public String createInutMessage(String uf, String ano, String cnpj, String mod, String serie, String ini,
			String fim, String just, String ambient) {
		String version = this.tableSchemas.get(uf).get("inutNFe");
		String ambientType = this.convertIdService(ambient);

		String xml = "<cteDadosMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteInutilizacao\"><inutCTe xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\""
				+ version + "\"><infInut Id=\"ID" + uf + cnpj + mod + serie + ini + fim + "\"><tpAmb>" + ambientType
				+ "</tpAmb><xServ>INUTILIZAR</xServ><cUF>" + uf + "</cUF><ano>" + ano + "</ano><CNPJ>" + cnpj
				+ "</CNPJ><mod>" + "57" + "</mod><serie>" + Integer.parseInt(serie) + "</serie><nCTIni>"
				+ Integer.parseInt(ini) + "</nCTIni><nCTFin>" + Integer.parseInt(fim) + "</nCTFin><xJust>" + just
				+ "</xJust></infInut></inutCTe></cteDadosMsg>";

		return xml;
	}

	private String getNameSpace(String idService) {
		if (idService.equals(SystemProperties.ID_SERVICO_RECEPCAO)) {
			return "\"http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcao\"";
		}

		if (idService.equals(SystemProperties.ID_SERVICO_RETRECEPCAO)) {
			return "\"http://www.portalfiscal.inf.br/cte/wsdl/CteRetRecepcao\"";
		}

		if (idService.equals(SystemProperties.ID_SERVICO_CONSULTA)) {
			return "\"http://www.portalfiscal.inf.br/cte/wsdl/CteConsulta\"";
		}

		if (idService.equals(SystemProperties.ID_SERVICO_CANCELAMENTO)) {
			return "\"http://www.portalfiscal.inf.br/cte/wsdl/CteCancelamento\"";
		}

		if (idService.equals(SystemProperties.ID_SERVICO_INUTILIZACAO)) {
			return "\"http://www.portalfiscal.inf.br/cte/wsdl/CteInutilizacao\"";
		}

		if (idService.equals(SystemProperties.ID_SERVICO_EVENTOS)) {
			return "\"http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcaoEvento\"";
		}

		return "";
	}

	@Override
	public String createHeader(String uf, String ambient, String idService) {
		String value = this.getSchemaVersion(uf, idService);
		String namespace = this.getNameSpace(idService);
		String xmlString = "<cteCabecMsg xmlns =" + namespace + ">" + "<cUF>" + uf + "</cUF>" + "<versaoDados>" + value
				+ "</versaoDados>" + "</cteCabecMsg>";

		/*
		 * String version = "1.02";
		 * 
		 * String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"
		 * standalone=\"yes\"?><cabecMsg versao=\"" + version + "\"
		 * xmlns=\"http://www.portalfiscal.inf.br/nfe\"><versaoDados>" + value +
		 * "</versaoDados></cabecMsg>";
		 */
		return xmlString;
	}

	public String getSchemaVersion(String uf, String idServ) {
		String schemaName = null;
		if (idServ.equals(SystemProperties.ID_SERVICO_RECEPCAO))
			schemaName = "enviNFe";
		if (idServ.equals(SystemProperties.ID_SERVICO_RETRECEPCAO))
			schemaName = "consReciNFe";
		if (idServ.equals(SystemProperties.ID_SERVICO_CONSULTA))
			schemaName = "consSitNFe";
		if (idServ.equals(SystemProperties.ID_SERVICO_CANCELAMENTO))
			schemaName = "cancNFe";
		if (idServ.equals(SystemProperties.ID_SERVICO_INUTILIZACAO))
			schemaName = "inutNFe";
		if (idServ.equals(SystemProperties.ID_SERVICO_EVENTOS))
			schemaName = "eventoNFe";

		if (!this.tableSchemas.containsKey(uf)) {
			return "VERSAO ESQUEMA NAO CADASTRADO PARA UF: " + uf;
		}

		return this.tableSchemas.get(uf).get(schemaName);
	}

	@Override
	public String createStatusService(String uf, String ambient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createCancelMessageXML(String xml, String uf, String ambient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createInutMessageXML(String xml, String ambient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createQueryXmlMessageXML(String xml, String uf, String ambient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createCallbackMessageXML(String xml, String uf, String ambient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File createXMLAutorizedProt(String id, Hashtable<String, String> prop2) {

		if (!id.startsWith("CTe")) {
			id = "CTe" + id;
		}

		XMLData xmldata = null;
		try {
			xmldata = XMLDataCache.getInstance().findData(id);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		if (xmldata != null) {
			String xmlNota = xmldata.getXmlString();
			String xmlProtocol = xmldata.getAutorizedProtocol();

			if (xmlNota == null || xmlNota.isEmpty()) {
				return null;
			}

			if (xmlProtocol != null) {
				xmlProtocol = this.repareProtocol(xmlProtocol);
			}

			String header = null;

			header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><cteProc xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\"2.00\">";

			String xml = header + xmlNota + xmlProtocol + "</cteProc>";

			try {
				String filename = FileLocator.getInstance().findXMLProt(xmldata.getCnpjEmit(), xmldata.getDateCreate(),
						xmldata.getKeyXml());

				FileWriter fw = new FileWriter(new File(filename));
				fw.write(xml);
				fw.close();

				return new File(filename);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
			return null;
		}
		MyLogger.getLog().info("Nota nao encontrada: " + id);
		return null;
	}

	private static String START_INFPROT = "<infProt";
	private static String END_INFPROT = "</infProt>";

	private String repareProtocol(String xml) {

		xml = xml.replace("TProtCte", "protCTe");
		xml = xml.replace("tProtCte", "protCTe");
		xml = xml.replace("xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\"", "");

		xml = xml.replace("xmlns=\"http://www.w3.org/2000/09/xmldsig#\"", "");
		xml = xml.replace("xmlns:ns2=\"http://www.portalfiscal.inf.br/cte\"", "");
		xml = xml.replace("ns2:", "");

		xml = xml.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");

		int pos1 = xml.indexOf(START_INFPROT);
		int pos2 = xml.indexOf(END_INFPROT);
		if (pos1 != -1 && pos2 != -2) {
			xml = xml.substring(pos1, pos2 + END_INFPROT.length());
		}

		xml = "<protCTe versao=\"2.00\">" + xml + "</protCTe>";
		return xml;
	}
}
