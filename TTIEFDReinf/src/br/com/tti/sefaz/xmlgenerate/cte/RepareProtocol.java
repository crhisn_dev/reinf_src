package br.com.tti.sefaz.xmlgenerate.cte;

import java.util.Hashtable;

public class RepareProtocol {

	private static String PROTOCOL_TAG_OPEN = "<ns2:retConsSitNFe";
	private static String PROTOCOL_TAG_CLOSE = "</ns2:retConsSitNFe>";

	public String repare(String xml) {

		String protocol = this.extractProtocol(xml);

		if (protocol == null) {
			return xml;
		}

		ReadXML read = new ReadXML();
		read.parseXML(protocol);

		String newProtocol = this.makeNewProtocol(read.getValues().get(0));

		return xml.replace(protocol, newProtocol);
	}

	private String makeNewProtocol(Hashtable<String, String> prop) {
		String prot = "<protNFe versao=\"2.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\">"
				+ "<infProt>" + "<tpAmb>"
				+ prop.get("tpAmb")
				+ "</tpAmb>"
				+ "<verAplic>"
				+ prop.get("verAplic")
				+ "</verAplic>"
				+ "<chNFe>"
				+ prop.get("chNFe")
				+ "</chNFe>"
				+ "<dhRecbto>"
				+ prop.get("dhRecbto")
				+ "</dhRecbto>"
				+ "<nProt>"
				+ prop.get("nProt")
				+ "</nProt>"
				+ "<digVal>"
				+ prop.get("digVal")
				+ "</digVal>"
				+ "<cStat>"
				+ prop.get("cStat")
				+ "</cStat>"
				+ "<xMotivo>"
				+ prop.get("xMotivo") + "</xMotivo>" + "</infProt></protNFe>";
		return prot;
	}

	private String extractProtocol(String xml) {

		int pos1 = xml.indexOf(PROTOCOL_TAG_OPEN);
		int pos2 = xml.indexOf(PROTOCOL_TAG_CLOSE)
				+ PROTOCOL_TAG_CLOSE.length();

		String protocol = null;
		if (pos1 != -1 && pos2 != -1) {
			protocol = xml.substring(pos1, pos2);
		}

		return protocol;
	}

/*	public static void main(String[] args) {
		String xml = ImprimirDANFEImpl
				.lerArquivo("D:\\Documents and Settings\\Administrador\\Meus documentos\\Downloads\\NFe_47165196000138_1_000000544_prot.xml");
		RepareProtocol p = new RepareProtocol();
		System.out.println(p.repare(xml));
	}*/
}
