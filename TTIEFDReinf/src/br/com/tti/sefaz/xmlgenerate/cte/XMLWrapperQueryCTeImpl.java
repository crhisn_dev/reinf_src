package br.com.tti.sefaz.xmlgenerate.cte;

import br.com.tti.sefaz.classes.consulta.TRetConsSitCTe;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xmlgenerate.XMLWrapperQuery;

public class XMLWrapperQueryCTeImpl implements XMLWrapperQuery {
	private TRetConsSitCTe query;
	private XMLGenerator xmlg;
	private String xmlString;

	public XMLWrapperQueryCTeImpl(String xmlResult) throws Exception {
		super();
		xmlResult = this.getStringXML(xmlResult);

		this.xmlString = xmlResult;
		this.xmlg = XMLGenerator
				.createXMLGenerator("br.com.tti.sefaz.classes.consulta");
		this.query = (TRetConsSitCTe) this.xmlg.toObject(xmlResult);
		this.xmlString = xmlResult;
	}

	private String getStringXML(String xml) {
		int pos1 = xml.indexOf("<retConsSitCTe");
		int pos2 = xml.indexOf("</cteConsultaCTResult>");

		if (pos1 != -1 && pos2 != -1)
			return xml.substring(pos1, pos2);
		return xml;
	}

	@Override
	public String getCStat() {
		return this.query.getCStat();
	}

	@Override
	public String getCh() {
		return this.query.getProtCTe().getInfProt().getChCTe();
	}

	@Override
	public String getDhSefaz() {
		if (this.query.getProtCTe() != null
				&& this.query.getProtCTe().getInfProt() != null
				&& this.query.getProtCTe().getInfProt().getDhRecbto() != null) {
			return this.query.getProtCTe().getInfProt().getDhRecbto()
					.toString();
		}
		return null;
	}

	@Override
	public String getProt() {
		if (this.query.getRetCancCTe() != null)
			return this.query.getRetCancCTe().getInfCanc().getNProt();

		if (this.query.getProtCTe() != null)
			return this.query.getProtCTe().getInfProt().getNProt();

		return null;
	}

	@Override
	public String getTpAmb() {
		return this.query.getTpAmb();
	}

	@Override
	public String getXMotivo() {
		return this.query.getXMotivo();
	}

	@Override
	public String getXmlProtocol() {
		this.xmlString = this.xmlString.replace("retConsSitCTe", "protCTe");
		return this.xmlString;
	}

	@Override
	public String getXml() {
		// TODO Auto-generated method stub
		return null;
	}

}
