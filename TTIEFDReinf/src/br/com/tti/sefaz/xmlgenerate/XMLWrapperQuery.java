package br.com.tti.sefaz.xmlgenerate;

public interface XMLWrapperQuery {
	public String getCStat();

	public String getXMotivo();

	public String getCh();

	public String getProt();

	public String getDhSefaz();

	public String getTpAmb();

	public String getXmlProtocol();

	public String getXml();

}
