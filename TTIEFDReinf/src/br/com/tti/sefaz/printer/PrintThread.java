package br.com.tti.sefaz.printer;

import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.tcteimpressao.remote.CTeImpressao;

public class PrintThread implements Runnable {

	private CTeImpressao cteprinter;
	private String keyxml;
	private String xml;
	private Hashtable<String, String> prop;
	private Vector<CTeImpressao> removecteprinter;

	public PrintThread(Vector<CTeImpressao> removecteprinter,
			CTeImpressao cteprinter, String keyxml, String xml,
			Hashtable<String, String> prop) {
		super();
		this.removecteprinter = removecteprinter;
		this.cteprinter = cteprinter;
		this.keyxml = keyxml;
		this.xml = xml;
		this.prop = prop;
	}

	@Override
	public void run() {
		try {
			this.cteprinter.imprinirCTe(this.keyxml, this.xml, this.prop);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			MyLogger.getLog().info(
					"Removing printer: " + cteprinter.toString() + " reason: "
							+ e.getLocalizedMessage());
			try {
				removecteprinter.add(cteprinter);
			} catch (Throwable t) {
				MyLogger.getLog().log(Level.INFO, t.getLocalizedMessage(), t);
			}
		}
	}

}
