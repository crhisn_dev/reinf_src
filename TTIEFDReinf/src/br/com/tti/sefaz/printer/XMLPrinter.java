package br.com.tti.sefaz.printer;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Hashtable;

import nfeimpressao.TaragonaInt;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.tcteimpressao.remote.CTeImpressao;

public interface XMLPrinter extends Remote {

	public void printXml(String keyXml, String xml,
			SystemProperties.TPEMISS modo, Hashtable<String, String> prop)
			throws RemoteException;

	public void rePrintXml(String keyXml, SystemProperties.TPEMISS modo)
			throws RemoteException;

	public void registrarImpressao(TaragonaInt imp) throws RemoteException;

	public void registrarCTeImpressao(CTeImpressao imp) throws RemoteException;

	public byte[] findPDF(String id) throws RemoteException;

}
