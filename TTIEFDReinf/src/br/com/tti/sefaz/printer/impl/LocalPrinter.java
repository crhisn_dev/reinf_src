package br.com.tti.sefaz.printer.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;

import org.w3c.dom.Element;

import nfeimpressao.TaragonaInt;
import br.com.taragona.nfe.util.PropriedadesSistema.MODO_OP;
import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.EventData;
import br.com.tti.sefaz.persistence.EventData.TIPO_EVENTO;
import br.com.tti.sefaz.persistence.PFXFile;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.printer.XMLPrinter;
import br.com.tti.sefaz.printer.generator.PrintGenerator;
import br.com.tti.sefaz.remote.SchemaVersionConfig;
import br.com.tti.sefaz.sender.email.EmailSender;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.FileLocator;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xml.evento.TEvento;
import br.com.tti.sefaz.xml.evento.TRetEvento;
import br.com.tti.sefaz.xml.evento.cce.EvCCeCTe;
import br.com.tti.sefaz.xml.evento.cce.EvCCeCTe.InfCorrecao;
import br.com.tti.sefaz.xmlgenerate.cte.XMLMessageFactoryCTe;
import br.com.tti.tcteimpressao.remote.CTeImpressao;

public class LocalPrinter implements XMLPrinter {

	private ManagerInterface manager;
	private XMLDataCache cache;
	private PrintGenerator generator;
	private ResourceBundle rb;
	private XMLMessageFactoryCTe factory;

	public LocalPrinter() {
		this.manager = Locator.getManagerReference();
		try {
			this.rb = ResourceBundle.getBundle("configuracao_impressoras");
		} catch (Exception e) {
			MyLogger.getLog()
					.info("problemas ao ler o arquivo configuracao_impressoras.properties");
		}

		try {
			Hashtable<Vector<String>, Vector<SchemaVersionConfig>> schemaVersion = this.manager
					.getSchemasVersion();

			this.factory = new XMLMessageFactoryCTe(schemaVersion);
		} catch (Exception e) {
			MyLogger.getLog()
					.info("problemas ao ler o arquivo configuracao_impressoras.properties");
		}
	}

	@Override
	public void printXml(String keyXml, String xml, TPEMISS modo,
			Hashtable<String, String> prop) throws RemoteException {
		this.manager.printXml(keyXml, xml, modo, prop);

	}

	public String convert(byte[] bytess, String source_encoding,
			String target_encoding) throws Exception {

		Charset charsetInput = Charset.forName(source_encoding);
		CharsetDecoder decoder = charsetInput.newDecoder();
		CharBuffer cbuf = decoder.decode(ByteBuffer.wrap(bytess));

		Charset charsetOutput = Charset.forName(target_encoding);
		CharsetEncoder encoder = charsetOutput.newEncoder();
		ByteBuffer bbuf = encoder.encode(CharBuffer.wrap(cbuf));

		String xmlutf = new String(bbuf.array(), 0, bbuf.limit(), charsetOutput);

		return xmlutf;
	}

	private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public void rePrintXml(String keyXml, TPEMISS modo) throws RemoteException {
		this.cache = XMLDataCache.getInstance();

		XMLData xmlData = null;
		try {
			xmlData = this.cache.findData(keyXml);
			if (this.generator != null) {
				this.generator.generatePrint(keyXml);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (xmlData != null) {
			if (!xmlData.getState().equals(XML_STATE.AUTORIZADA)
					&& !xmlData.getState().equals(XML_STATE.CANCELADA)) {
				throw new RemoteException("CT-e nao autorizado ou cancelado");
			}

			Hashtable<String, String> prop = new Hashtable<String, String>();
			prop.put("PROTOCOLO", xmlData.getNAutorizedProtocol() + " "
					+ xmlData.getDhSefaz());

			prop.put("IMPRIMIR", "true");
			prop.put("DATAGERADA", this.df.format(xmlData.getDateCreate()));
			prop.put("IMPRESSORA",
					(xmlData.getImpressora() != null ? xmlData.getImpressora()
							: ""));

			if (xmlData.getState().equals(XML_STATE.CANCELADA)) {
				prop.put("CANCELADA", xmlData.getKeyXml());
			}

			prop.put(
					"pdffile",
					FileLocator.getInstance().findPDF(xmlData.getCnpjEmit(),
							xmlData.getDateCreate(), xmlData.getKeyXml()));
			prop.put(
					"psfile",
					FileLocator.getInstance().findPS(xmlData.getCnpjEmit(),
							xmlData.getDateCreate(), xmlData.getKeyXml()));

			try {
				List<EventData> cceautor = this.manager.obterEventosSequence(
						null, xmlData.getKeyXml(), TIPO_EVENTO.CCE.getCodigo(),
						null);

				if (cceautor != null && !cceautor.isEmpty()) {

					EventData cce = cceautor.get(0);
					TEvento tevent = cce.getEvento();
					String xml = tevent.getInfEvento().getDetEvento()
							.getAnyxml();

					XMLGenerator gen = new XMLGenerator(
							"br.com.tti.sefaz.xml.evento.cce");
					EvCCeCTe cceevent = (EvCCeCTe) gen.toObject(xml);
					List<InfCorrecao> correcoes = cceevent.getInfCorrecao();

					String correcoesstr = "";
					if (correcoes != null) {
						for (InfCorrecao inf : correcoes) {
							correcoesstr = " Grupo: "
									+ inf.getGrupoAlterado()
									+ " Campo: "
									+ inf.getCampoAlterado()
									+ " Novo Valor: "
									+ inf.getValorAlterado()
									+ ((inf.getValorAlterado() != null && !inf
											.getValorAlterado().isEmpty()) ? " Posicao: "
											+ inf.getNroItemAlterado()
											: "") + " - " + correcoesstr;
						}
					}

					try {
						if (correcoesstr != null && !correcoesstr.isEmpty()) {
							correcoesstr = correcoesstr.trim() + "@";
							correcoesstr = correcoesstr.replace("-@", "");
						}
					} catch (Exception e) {

					}

					try {
						correcoesstr = this.convert(correcoesstr.getBytes(),
								"ISO-8859-1", "UTF-8");
					} catch (Exception e) {
						try {
							correcoesstr = this.convert(
									correcoesstr.getBytes(), "UTF-8",
									"ISO-8859-1");
						} catch (Exception e1) {
						}
					}

					String protocolostr = "";
					if (cce.getRetevento() != null) {
						String protocolo = cce.getRetevento().getInfEvento()
								.getNProt();
						String data = cce.getRetevento().getInfEvento()
								.getDhRegEvento();

						protocolostr = protocolo + " " + data;
					}

					correcoesstr = "Protocolo: " + protocolostr
							+ " Campos Corrigidos: " + correcoesstr;

					prop.put("CORRECAO", correcoesstr);

					prop.put("XMLCCE", cce.getXmlString());
					prop.put("XMLCCEPROT", cce.getProtocoloXML());
				}

			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}

			String xmlprint = xmlData.getXmlStringprint();

			if (xmlprint == null || xmlprint.isEmpty()) {
				xmlprint = xmlData.getXmlString();
			}

			this.manager.printXml(xmlData.getKeyXml(), xmlprint, modo, prop);
		}
	}

	public void dontRePrintXml(String keyXml, TPEMISS modo)
			throws RemoteException {
		this.cache = XMLDataCache.getInstance();

		XMLData xmlData = null;
		try {
			xmlData = this.cache.findData(keyXml);
			if (this.generator != null) {
				this.generator.generatePrint(keyXml);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (xmlData != null) {
			if (xmlData.getState().equals(XML_STATE.AUTORIZADA)
					|| xmlData.getState().equals(XML_STATE.CANCELADA)
					|| (xmlData.getState().equals(XML_STATE.GERADA) && xmlData
							.getModo().equals(
									SystemProperties.TPEMISS.CONTINGENCE))
					|| (xmlData.getState().equals(XML_STATE.EPEC_ENVIADO))) {

				Hashtable<String, String> prop = new Hashtable<String, String>();
				prop.put("PROTOCOLO", xmlData.getNAutorizedProtocol() + " "
						+ xmlData.getDhSefaz());

				// antes nao tinha a chave IMPRIMIR
				// prop.put("IMPRIMIR", "false");
				prop.put("DATAGERADA", this.df.format(xmlData.getDateCreate()));
				prop.put(
						"IMPRESSORA",
						(xmlData.getImpressora() != null ? xmlData
								.getImpressora() : ""));
				prop.put(
						"pdffile",
						FileLocator.getInstance().findPDF(
								xmlData.getCnpjEmit(), xmlData.getDateCreate(),
								xmlData.getKeyXml()));
				prop.put(
						"psfile",
						FileLocator.getInstance().findPS(xmlData.getCnpjEmit(),
								xmlData.getDateCreate(), xmlData.getKeyXml()));

				if (xmlData.getState().equals(XML_STATE.CANCELADA)) {
					prop.put("CANCELADA", xmlData.getKeyXml());
				}

				try {
					List<EventData> cceautor = this.manager
							.obterEventosSequence(null, xmlData.getKeyXml(),
									TIPO_EVENTO.CCE.getCodigo(), null);

					if (cceautor != null && !cceautor.isEmpty()) {

						EventData cce = cceautor.get(0);
						TEvento tevent = cce.getEvento();
						String xml = tevent.getInfEvento().getDetEvento()
								.getAnyxml();

						XMLGenerator gen = new XMLGenerator(
								"br.com.tti.sefaz.xml.evento.cce");
						EvCCeCTe cceevent = (EvCCeCTe) gen.toObject(xml);
						List<InfCorrecao> correcoes = cceevent.getInfCorrecao();

						String correcoesstr = "";
						if (correcoes != null) {
							for (InfCorrecao inf : correcoes) {
								correcoesstr = " Grupo: "
										+ inf.getGrupoAlterado()
										+ " Campo: "
										+ inf.getCampoAlterado()
										+ " Novo Valor: "
										+ inf.getValorAlterado()
										+ ((inf.getValorAlterado() != null && !inf
												.getValorAlterado().isEmpty()) ? " Posicao: "
												+ inf.getNroItemAlterado()
												: "") + " - " + correcoesstr;
							}
						}

						try {
							if (correcoesstr != null && !correcoesstr.isEmpty()) {
								correcoesstr = correcoesstr.trim() + "@";
								correcoesstr = correcoesstr.replace("-@", "");
							}
						} catch (Exception e) {

						}

						try {
							correcoesstr = this.convert(
									correcoesstr.getBytes(), "ISO-8859-1",
									"UTF-8");
						} catch (Exception e) {
							try {
								correcoesstr = this.convert(
										correcoesstr.getBytes(), "UTF-8",
										"ISO-8859-1");
							} catch (Exception e1) {
							}
						}

						String protocolostr = "";
						if (cce.getRetevento() != null) {
							String protocolo = cce.getRetevento()
									.getInfEvento().getNProt();
							String data = cce.getRetevento().getInfEvento()
									.getDhRegEvento();

							protocolostr = protocolo + " " + data;
						}

						correcoesstr = "Protocolo: " + protocolostr
								+ " Campos Corrigidos: " + correcoesstr;

						prop.put("CORRECAO", correcoesstr);
						prop.put("XMLCCE", cce.getXmlString());
						prop.put("XMLCCEPROT", cce.getProtocoloXML());

					}

				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(),
							e);
				}

				String xmlprint = xmlData.getXmlStringprint();

				if (xmlprint == null || xmlprint.isEmpty()) {
					xmlprint = xmlData.getXmlString();
				}

				this.manager
						.printXml(xmlData.getKeyXml(), xmlprint, modo, prop);
			} else {
				throw new RemoteException("CT-e nao autorizado");

			}
		}
	}

	@Override
	public void registrarImpressao(TaragonaInt imp) throws RemoteException {
		this.manager.registrarImpressao(imp);
	}

	public String getName(String id) {
		return null;
	}

	@Override
	public byte[] findPDF(String id) throws RemoteException {
		this.cache = XMLDataCache.getInstance();

		if (!id.startsWith("CTe")) {
			id = "CTe" + id;
		}

		XMLData xmldata = null;

		try {
			xmldata = this.cache.findData(id);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		MyLogger.getLog().info("find PDF: " + id);

		this.dontRePrintXml(id, xmldata.getModo());

		KeyXml key = new KeyXml(id);
		String pdffile = FileLocator.getInstance().findPDF(key.getCnpj(),
				xmldata.getDateCreate(), xmldata.getKeyXml());

		/*
		 * XMLData cte = null; try { cte =
		 * XMLDataCache.getInstance().findData(id);
		 * printer.printXml(cte.getKeyXml(), cte.getXmlString(), null); } catch
		 * (Exception e) { MyLogger.getLog().log(Level.INFO,
		 * e.getLocalizedMessage(), e); }
		 */

		// String pdffile = "C:\TTICTe\files/PDF" + "/" + id + ".pdf";

		File pdffilef = new File(pdffile);

		MyLogger.getLog().info("procurando PDF:" + pdffilef.getAbsolutePath());

		for (int i = 0; i <= 3; i++) {
			if (pdffilef.exists()) {
				break;
			}

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}

		if (pdffilef.exists()) {
			try {
				File file = new File(pdffile);
				byte buffer[] = new byte[(int) file.length()];
				BufferedInputStream input = new BufferedInputStream(
						new FileInputStream(pdffile));
				input.read(buffer, 0, buffer.length);
				input.close();

				return buffer;
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
		return null;
	}

	@Override
	public void registrarCTeImpressao(CTeImpressao imp) throws RemoteException {
		// TODO Auto-generated method stub

	}

	/*
	 * public static byte[] findPDFStatic(String id) throws RemoteException {
	 * MyLogger.getLog().info("find PDF: " + id);
	 * 
	 * KeyXml key = new KeyXml(id); String pdfdir =
	 * FileLocator.getInstance().findPDFDir(key.getCnpj(), null);
	 * 
	 * if (!id.startsWith("CTe")) { id = "CTe" + id; }
	 * 
	 * String pdffile = pdfdir + "/" + id + ".pdf"; File pdffilef = new
	 * File(pdffile);
	 * 
	 * for (int i = 0; i <= 1; i++) { if (pdffilef.exists()) { break; }
	 * 
	 * try { Thread.sleep(1000); } catch (InterruptedException e1) {
	 * e1.printStackTrace(); } }
	 * 
	 * if (pdffilef.exists()) { try { File file = new File(pdffile); byte
	 * buffer[] = new byte[(int) file.length()]; BufferedInputStream input = new
	 * BufferedInputStream( new FileInputStream(pdffile)); input.read(buffer, 0,
	 * buffer.length); input.close();
	 * 
	 * return buffer; } catch (Exception e) { MyLogger.getLog().log(Level.INFO,
	 * e.getLocalizedMessage(), e); } } return null; }
	 */

}
