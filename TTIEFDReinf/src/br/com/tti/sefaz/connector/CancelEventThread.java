package br.com.tti.sefaz.connector;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties;

public class CancelEventThread implements Runnable {

	private ManagerInterface manager;
	private String cnpj;
	private String uf;
	private String keyxml;
	private String just;
	private Hashtable<String, String> props;

	public CancelEventThread(ManagerInterface manager, String cnpj, String uf,
			String keyxml, String just, Hashtable<String, String> props) {
		super();
		this.manager = manager;
		this.cnpj = cnpj;
		this.uf = uf;
		this.keyxml = keyxml;
		this.just = just;
		this.props = props;
	}

	@Override
	public void run() {
		try {
			this.manager.cancelXml(this.cnpj, this.uf,
					SystemProperties.AMBIENT_PRODUCAO, this.keyxml, null,
					this.just, true, props);
		} catch (RemoteException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

	}

}
