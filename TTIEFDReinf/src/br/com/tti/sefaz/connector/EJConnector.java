package br.com.tti.sefaz.connector;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.Query;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.CTeEvento;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.printer.impl.LocalPrinter;
import br.com.tti.sefaz.remote.events.ChangeStateXmlEvent;
import br.com.tti.sefaz.remote.events.TTIEvent;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.Locator;

public class EJConnector extends AbstractConnector {

	private DAO<CTeEvento> daoeventsend;
	private DAO<CTeEvento> daoeventreturn;
	private Connector stub;
	private ManagerInterface manager;

	private Hashtable<String, CTeEvento> eventscache;

	public EJConnector() {
		try {
			this.daoeventsend = DaoFactory.createDAO(CTeEvento.class);
			this.daoeventreturn = DaoFactory.createDAO(CTeEvento.class);
			this.createStub();
			this.eventscache = new Hashtable<String, CTeEvento>();
			manager = Locator.getManagerReference();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			System.exit(100);
			MyLogger.getLog().info("Fatal error!!!... force exit");
		}
	}

	private void createStub() {
		try {
			Registry rmi = LocateRegistry.getRegistry("localhost");
			stub = (Connector) UnicastRemoteObject.exportObject(this, 0);
			rmi.rebind("jundaiconnector", this.stub);
		} catch (RemoteException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}

	@Override
	public String getConnectorName() throws RemoteException {

		return this.getClass().getCanonicalName();
	}

	@Override
	public Connector getStub() throws RemoteException {
		// TODO Auto-generated method stub
		return this.stub;
	}

	@Override
	public void processEvent(TTIEvent eventdata) throws RemoteException {
		ChangeStateXmlEvent state = (ChangeStateXmlEvent) eventdata;
		MyLogger.getLog().info(
				"CTE: " + state.getKeyXml() + " STATUS: "
						+ state.getState().toString());

		String key = state.getKeyXml();

		CTeEvento eventfake = this.eventscache.get(key);

		List<CTeEvento> events = new ArrayList<CTeEvento>();

		if (eventfake == null) {
			String sql = "SELECT o FROM CTeEvento as o WHERE (o.cte_key = '"
					+ key + "' OR o.cte_key = 'CTe" + key + "')";
			MyLogger.getLog().info("update query:" + sql);
			Query q = this.daoeventreturn.createQuery(sql);
			events = q.getResultList();
		} else {
			CTeEvento eventcached = this.daoeventreturn.findEntity(eventfake
					.getId_pk());
			events.add(eventcached);
		}

		for (CTeEvento event : events) {
			try {
				if (event.getCte_key() != null && !event.getCte_key().isEmpty()) {
					this.eventscache.put(event.getCte_key(), event);
				}

				if (state.getState().equals(XML_STATE.AUTORIZADA)) {
					event.setCte_cod_ret(state.getSefazCode() + "");
					event.setCte_use(state.getNAutorizedProtocol());
					event.setRet_status("6");
					event.setCte_msg_ret(state.getXMotivo());
					event.setCte_dt(state.getDateAutorized().toString());
					event.setEnv_status("P");
					event.setCte_xml_retorno(state.getXmlProtocol());

					// pdf don't more save
					/*
					 * byte[] pdffile = LocalPrinter.findPDFStatic(state
					 * .getKeyXml()); event.setDacte(pdffile);
					 */
				}

				if (state.getState().equals(XML_STATE.ERRO_VALIDACAO)) {
					// event.setCte_cod_ret(state.getSefazCode() + "");
					// event.setCte_use(state.getNAutorizedProtocol());
					event.setRet_status("16");

					if (state.getXMotivo() != null) {
						String error = state.getXMotivo();
						if (error.length() >= 100) {
							error = error.substring(0, 100);
						}

						event.setCte_msg_ret(error);
					}
					// event.setCte_dt(state.getDateAutorized());
					event.setEnv_status("P");
				}

				if (state.getState().equals(XML_STATE.CANCELADA)) {
					event.setCte_cod_ret(state.getSefazCode() + "");
					event.setCte_use(state.getNCancelProtocol());
					event.setRet_status("5");
					event.setCte_msg_ret(state.getXMotivo());
					event.setCte_dt(state.getDateCancel().toString());
					event.setEnv_status("P");
				}

				if (state.getState().equals(XML_STATE.REJEITADA)) {
					event.setCte_cod_ret(state.getSefazCode() + "");
					// event.setCte_use(state.getNAutorizedProtocol());
					event.setRet_status("4");
					event.setCte_msg_ret(state.getXMotivo());
					// event.setCte_dt(state.getDateAutorized());
					event.setEnv_status("P");

					/*
					 * byte[] pdffile = LocalPrinter.findPDFStatic(state
					 * .getKeyXml()); event.setDacte(pdffile);
					 */
				}

				this.daoeventreturn.updateEntity(event);
				this.daoeventreturn.flush();
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
				// continue;
			}
		}
	}

	@Override
	public int processInput() throws RemoteException {
		// MyLogger.getLog().info("Processing db");

		String sql = "SELECT TOP 80 * FROM CTeEvento (nolock) WHERE env_status = 'A' ORDER BY cte_dt_ev asc";
		/*
		 * Query q = this.daoeventsend .createQuery(sql);
		 */

		Query q = this.daoeventsend.createNativeQuery(sql);

		List<CTeEvento> events = q.getResultList();

		if (events != null && events.size() != 0) {
			MyLogger.getLog().info("Size event:" + events.size());
		}

		for (CTeEvento event : events) {

			if (event.getCte_key() != null && !event.getCte_key().isEmpty()) {
				this.eventscache.put(event.getCte_key(), event);
			}

			MyLogger.getLog().info("Event keyxml: " + event.getCte_key());
			MyLogger.getLog().info("Event type: " + event.getTipo_evento());

			/*
			 * if (event.getCte_xml() == null || event.getCte_xml().isEmpty()) {
			 * MyLogger.getLog().info( "*************************** Event xml: "
			 * + event.getCte_xml()); }
			 * 
			 * if (event.getTipo_evento().equals("1")) {
			 * 
			 * String xml = event.getCte_xml();
			 * 
			 * if (xml != null && !xml.isEmpty()) { try {
			 * MyLogger.getLog().info( "Sending XML: " + event.getCte_key());
			 * Hashtable<String, String> props = new Hashtable<String,
			 * String>(); props.put("ID_PK", event.getId_pk() + ""); Thread t =
			 * new Thread(new ProcessEventThread( this.manager, xml, props));
			 * t.start();
			 * 
			 * // this.manager.sendXml(xml); } catch (Exception e) {
			 * MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			 * continue; } }
			 * 
			 * }
			 */

			if (event.getTipo_evento().equals("3")) {
				String info = event.getEnv_info();
				try {
					String[] split = new String[2];
					if (info != null || !info.isEmpty()) {
						split = info.split("\\|");
					} else {
						info = event.getCte_key();
					}

					if (split.length >= 2) {
						MyLogger.getLog().info("Sending cancel info: " + info);
						KeyXml key = new KeyXml(split[0]);

						Hashtable<String, String> props = new Hashtable<String, String>();
						props.put("ID_PK", event.getId_pk() + "");

						Thread t = new Thread(new CancelEventThread(
								this.manager, key.getCnpj(), key.getUF(),
								split[0], split[1], props));
						t.start();

						/*
						 * this.manager.cancelXml(key.getCnpj(), key.getUF(),
						 * SystemProperties.AMBIENT_PRODUCAO, split[0], null,
						 * split[1], true);
						 */
					}
				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(),
							e);
					continue;
				}

			}

			event.setEnv_status("L");
			this.daoeventsend.updateEntity(event);
			this.daoeventsend.flush();
		}

		return events.size();
	}

	@Override
	public void register(ConnectorNotifier notifier) throws RemoteException {

	}

}
