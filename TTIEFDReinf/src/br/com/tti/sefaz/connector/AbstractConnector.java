package br.com.tti.sefaz.connector;

import java.rmi.RemoteException;
import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.remote.events.TTIEvent;

public abstract class AbstractConnector implements Connector, Runnable {

	private boolean run = true;

	@Override
	public abstract int processInput() throws RemoteException;

	@Override
	public abstract String getConnectorName() throws RemoteException;

	@Override
	public abstract Connector getStub() throws RemoteException;

	@Override
	public abstract void register(ConnectorNotifier notifier) throws RemoteException;

	@Override
	public abstract void processEvent(TTIEvent event) throws RemoteException;

	@Override
	public void run() {
		while (this.run) {
			try {
				int size = this.processInput();
				/*
				 * if (size >= 50) { Thread.sleep(2 * 60 * 1000L); } else if
				 * (size >= 20 && size < 50) { Thread.sleep(1 * 60 * 1000L); }
				 * else { Thread.sleep(30 * 1000L); }
				 */

				Thread.sleep(30 * 1000L);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					MyLogger.getLog().log(Level.INFO, e1.getLocalizedMessage(), e1);
				}
			}
		}
	}

	@Override
	public void startConnector() throws RemoteException {
		Thread t = new Thread(this);
		t.start();
	}

	@Override
	public void stopConnector() throws RemoteException {
		this.run = false;
	}

}
