package br.com.tti.sefaz.connector;

import java.util.Vector;
import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.remote.events.TTIEvent;

public class ConnectorNotifyThread implements Runnable {

	private Connector connector;
	private TTIEvent event;
	private Vector<Connector> toRemoveConnectors;

	public ConnectorNotifyThread(Connector connector, TTIEvent event,
			Vector<Connector> toRemoveConnectors) {
		super();
		this.connector = connector;
		this.event = event;
		this.toRemoveConnectors = toRemoveConnectors;
	}

	@Override
	public void run() {
		try {
			this.connector.processEvent(this.event);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			MyLogger.getLog().info(
					"****************************** Connector dont found: "
							+ this.connector.toString() + " adding to remove");
			this.toRemoveConnectors.add(this.connector);
		}
	}

}
