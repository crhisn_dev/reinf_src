package br.com.tti.sefaz.connector;

import java.io.File;
import java.io.FileFilter;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;

import com.sun.org.apache.bcel.internal.classfile.Field;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.events.TTIEvent;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.util.ReadFile;

public class ReadFileConnector extends AbstractConnector {
	private Connector stub;
	private ManagerInterface manager;

	public ReadFileConnector() {
		this.createStub();
	}

	private void createStub() {
		try {
			Registry rmi = LocateRegistry.getRegistry("localhost");
			stub = (Connector) UnicastRemoteObject.exportObject(this, 0);
			rmi.rebind("readfile_connector", this.stub);

			manager = Locator.getManagerReference();
		} catch (RemoteException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}

	@Override
	public int processInput() throws RemoteException {
		Vector<String> dirs = MainParameters.getDirs();
		if (!dirs.isEmpty()) {

			File input_file = new File(dirs.get(0));

			MyLogger.getLog().info("reading file: " + input_file.getAbsolutePath());

			File[] files = input_file.listFiles(new FileFilter() {

				@Override
				public boolean accept(File pathname) {
					return pathname.getName().toLowerCase().endsWith(".xml");
				}
			});

			for (File file : files) {
				try {
					MyLogger.getLog().info("processando file: " + file.getName());

					String xml = ReadFile.readFile(file.getAbsolutePath());
					Hashtable<String, String> prop = new Hashtable<>();
					this.manager.sendXml(xml, prop);
				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
				}

				file.delete();
			}
		}

		return 0;
	}

	@Override
	public String getConnectorName() throws RemoteException {

		return "readfile_connector";
	}

	@Override
	public Connector getStub() throws RemoteException {
		// TODO Auto-generated method stub
		return this.stub;
	}

	@Override
	public void register(ConnectorNotifier notifier) throws RemoteException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processEvent(TTIEvent event) throws RemoteException {
		// TODO Auto-generated method stub

	}

}
