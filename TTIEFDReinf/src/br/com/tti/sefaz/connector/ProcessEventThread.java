package br.com.tti.sefaz.connector;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;

public class ProcessEventThread implements Runnable {

	private ManagerInterface manager;
	private String xml;
	private Hashtable<String, String> props;

	public ProcessEventThread(ManagerInterface manager, String xml,
			Hashtable<String, String> props) {
		super();
		this.manager = manager;
		this.xml = xml;
		this.props = props;
	}

	@Override
	public void run() {
		try {
			this.manager.sendXml(this.xml, props);
		} catch (RemoteException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}

}
