package br.com.tti.sefaz.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;

import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.EventData;
import br.com.tti.sefaz.persistence.EventData.TIPO_EVENTO;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.xml.evento.TRetEvento;

public class EventCancelTest {

	public static void main(String[] args) {

		try {
			String keyXml = "CTe35171010783117000142570010000017371100000011";
			String protocol = "123456789012345";
			String just = "deu problema no cancelamento";

			ManagerInterface manager = Locator.getManagerReference();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-03:00");

			KeyXml kk = new KeyXml(keyXml);
			String datahora = sdf.format(Calendar.getInstance().getTime());
			String cnpjclient = kk.getCnpj();
			Hashtable<String, Object> params = new Hashtable<String, Object>();
			params.put("protocolo", protocol);
			params.put("xjust", just);

			TRetEvento ret = manager.sendEvent(keyXml, TIPO_EVENTO.CANCELAMENTO, datahora, cnpjclient, params);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
