package br.com.tti.sefaz.test;

import java.rmi.RemoteException;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.util.Locator;

public class QueryTest {

	public static void main(String[] args) {
		ManagerInterface ger = Locator.getManagerReference();
		try {
			ger.synchronizeXmlWithSefaz("CTe35130844384832000124570000000000114100000015");
		} catch (RemoteException e) {
			MyLogger.getLog().info(e.getLocalizedMessage());
		}
	}
}
