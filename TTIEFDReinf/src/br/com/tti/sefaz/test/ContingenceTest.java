package br.com.tti.sefaz.test;

import java.rmi.RemoteException;
import java.util.Hashtable;

import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.Locator;

public class ContingenceTest {

	public static void main(String[] args) {
		ManagerInterface manager = Locator.getManagerReference();

		TPEMISS type = TPEMISS.CONTINGENCE_EPEC;
		try {
			manager.changeToState("44384832000124", "", TPEMISS.NORMAL, null);
			
			manager.leaveContingence("44384832000124", "homologacao", true, type);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
