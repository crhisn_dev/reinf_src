package br.com.tti.sefaz.test;

import java.io.File;

import br.com.tti.sefaz.reinf.eventos.infocont.Reinf;
import br.com.tti.sefaz.signer.Main;
import br.com.tti.sefaz.xml.XMLGenerator;

public class TestReinfSign {

	public static void main(String[] args) {
		try {
			XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.infocont");
			Reinf reinf = (Reinf) gen.toObjectFromFile(
					new File("G:\\TTIDesktop\\sidney_environment\\myworkspace\\TTIEFDReinf\\src\\reinf.xml"));

			System.out.println(reinf.getEvtInfoContri().getId());

			Main main = new Main("C:\\TTIRec\\certificados\\Tome-Equp_2018.pfx", "tome1234");
			String xml = gen.toXMLString(reinf);
			String tag = "evtInfoContri";
			
			String xml_signed = main.myAssinaReinf(xml, tag);

			System.out.println(xml_signed);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
