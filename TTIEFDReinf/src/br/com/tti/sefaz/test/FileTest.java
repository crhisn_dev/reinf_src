package br.com.tti.sefaz.test;

import java.io.File;
import java.util.Date;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FileTest {

	public static void main(String[] args) {
		File dir = new File("G:\\TTIDesktop\\sidney_environment\\myworkspace\\TTIEFDReinf\\src");

		File dirFile = dir;

		Queue<File> q = new ConcurrentLinkedQueue<File>();
		q.add(dir);

		while (!q.isEmpty()) {
			dirFile = q.poll();

			for (File file : dirFile.listFiles()) {
				if (file.isFile()) {
					System.out.println(file.getAbsolutePath() + " date: " + new Date(file.lastModified()));
				} else if (file.isDirectory()) {
					q.add(file);
				}
			}

		}

	}
}
