package br.com.tti.sefaz.test;

import br.com.taragona.nfe.util.PropriedadesSistema;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.systemconfig.SystemConfigInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.util.Locator;

public class InutTest {

	public static void main(String[] args) {
		try {
			ManagerInterface ger = Locator.getManagerReference();

			String keyXml = "CTe35171010783117000142570010000017371100000011";
			String cnpj = "10783117000142";
			ger.inutXml(cnpj, "35", SystemProperties.AMBIENT_HOMOLOGACAO, keyXml, "deu algum problema", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
