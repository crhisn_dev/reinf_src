package br.com.tti.sefaz.test;

import java.util.Hashtable;

import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.ReadFile;

public class TestSendXML {

	public static void main(String[] args) {
		try {
			ManagerInterface manager = Locator.getManagerReference("2localhost");
			String xml = ReadFile
					.readFile("C:\\TTICTes\\cte_200.xml");
			manager.changeToState("10783117000142", "", TPEMISS.NORMAL, null);
			manager.sendXml(xml, new Hashtable<String, String>());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
