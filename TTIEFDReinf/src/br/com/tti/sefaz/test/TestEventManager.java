package br.com.tti.sefaz.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.EventData;
import br.com.tti.sefaz.persistence.EventData.TIPO_EVENTO;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.xml.evento.TRetEvento;

public class TestEventManager {

	public static void main(String[] args) {
		for (Object o : System.getProperties().keySet()) {
			System.out.println(o + " - " + System.getProperties().get(o));

		}

		try {
			ManagerInterface manager = Locator.getManagerReference();
			Hashtable<String, Object> params = new Hashtable<String, Object>();
			Vector<Hashtable<String, String>> infcorrec = new Vector<Hashtable<String, String>>();
			Hashtable<String, String> entry = new Hashtable<String, String>();

			entry.put("grupo", "ide");
			entry.put("campo", "UFIni");
			entry.put("valor", "21");
			infcorrec.add(entry);
			params.put("infcorrec", infcorrec);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			Hashtable<String, Object> props = new Hashtable<String, Object>();
			Date data = sdf.parse("2012/01/01");
			List<EventData> res = manager.recoverEventData(
					"CTe35171010783117000142570010000017371100000011",
					TIPO_EVENTO.CCE, data, props);
			for (EventData r : res) {
				if (r.getRetevento() != null) {
					System.out.println(r.getRetevento().getInfEvento()
							.getXMotivo());
				}
			}

			TRetEvento ret = manager.sendEvent(
					"CTe35171010783117000142570010000017371100000011",
					TIPO_EVENTO.CCE, "2013-03-03T22:00:00-03:00", "10783117000142",
					params);
			System.out.println(ret.getInfEvento().getVerAplic() + " "
					+ ret.getInfEvento().getXMotivo());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
