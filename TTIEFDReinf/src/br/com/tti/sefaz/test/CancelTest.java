package br.com.tti.sefaz.test;

import java.rmi.RemoteException;
import java.util.Hashtable;

import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.util.Locator;

public class CancelTest {

	public static void main(String[] args) {
		ManagerInterface ger = Locator.getManagerReference();

		try {
			Hashtable<String, String> props = new Hashtable<String, String>();
			props.put("ID_PK", "1126");
			ger
					.cancelXml(
							"10783117000142",
							"35",
							SystemProperties.AMBIENT_HOMOLOGACAO,
							"35171010783117000142570010000017371100000011",
							"10783117000142",
							"teste de cancelamento: 35345678901234567890123456789012345678901234",
							false, props);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
