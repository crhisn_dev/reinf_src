package br.com.tti.sefaz.remote;

import java.io.Serializable;

public class CNPJData implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cnpj;
	private String uf;
	private String xName;
	private String ambiente;

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getXName() {
		return xName;
	}

	public void setXName(String name) {
		xName = name;
	}

}
