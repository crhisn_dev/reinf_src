package br.com.tti.sefaz.remote.events;

import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;

public class ChangeModeOpEvent extends TTIEvent {

	private static final long serialVersionUID = 1L;

	private TPEMISS modo;

	private String cnpj;

	private String amb;

	public TPEMISS getModo() {
		return modo;
	}

	public void setModo(TPEMISS modo) {
		this.modo = modo;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getAmb() {
		return amb;
	}

	public void setAmb(String amb) {
		this.amb = amb;
	}
}
