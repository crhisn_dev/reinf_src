package br.com.tti.sefaz.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cte_evento")
// @Table(name = "CTE_EVENTO")
public class CTeEvento {

	@Id
	private long id_pk;
	@Column(length = 2)
	private String tipo_evento;
	@Column(length = 500)
	private String env_info;
	@Temporal(TemporalType.TIMESTAMP)
	private Date cte_dt_ev;
	@Column(length = 2)
	private String env_status;
	@Column(length = 10)
	private String ret_status;
	@Column(length = 44)
	private String cte_key;
	@Column(length = 28)
	private String cte_use;
	// @Temporal(TemporalType.DATE)
	private String cte_dt;
	/*
	 * @Lob private String cte_xml;
	 */
	@Lob
	private String cte_xml_retorno;
	@Column(length = 200)
	private String cte_msg_ret;
	@Column(length = 50)
	private String cte_cod_ret;

	/*
	 * @Lob private byte[] dacte;
	 */

	public long getId_pk() {
		return id_pk;
	}

	public void setId_pk(long id_pk) {
		this.id_pk = id_pk;
	}

	public String getTipo_evento() {
		return tipo_evento;
	}

	public void setTipo_evento(String tipo_evento) {
		this.tipo_evento = tipo_evento;
	}

	public String getEnv_info() {
		return env_info;
	}

	public void setEnv_info(String env_info) {
		this.env_info = env_info;
	}

	public Date getCte_dt_ev() {
		return cte_dt_ev;
	}

	public void setCte_dt_ev(Date cte_dt_ev) {
		this.cte_dt_ev = cte_dt_ev;
	}

	public String getEnv_status() {
		return env_status;
	}

	public void setEnv_status(String env_status) {
		this.env_status = env_status;
	}

	public String getRet_status() {
		return ret_status;
	}

	public void setRet_status(String ret_status) {
		this.ret_status = ret_status;
	}

	public String getCte_key() {
		return cte_key;
	}

	public void setCte_key(String cte_key) {
		this.cte_key = cte_key;
	}

	public String getCte_use() {
		return cte_use;
	}

	public void setCte_use(String cte_use) {
		this.cte_use = cte_use;
	}

	public String getCte_dt() {
		return cte_dt;
	}

	public void setCte_dt(String cte_dt) {
		this.cte_dt = cte_dt;
	}

	/*
	 * public String getCte_xml() { return cte_xml; }
	 * 
	 * public void setCte_xml(String cte_xml) { this.cte_xml = cte_xml; }
	 */

	public String getCte_xml_retorno() {
		return cte_xml_retorno;
	}

	public void setCte_xml_retorno(String cte_xml_retorno) {
		this.cte_xml_retorno = cte_xml_retorno;
	}

	public String getCte_msg_ret() {
		return cte_msg_ret;
	}

	public void setCte_msg_ret(String cte_msg_ret) {
		this.cte_msg_ret = cte_msg_ret;
	}

	public String getCte_cod_ret() {
		return cte_cod_ret;
	}

	public void setCte_cod_ret(String cte_cod_ret) {
		this.cte_cod_ret = cte_cod_ret;
	}

	/*
	 * public byte[] getDacte() { return dacte; }
	 * 
	 * public void setDacte(byte[] dacte) { this.dacte = dacte; }
	 */

}
