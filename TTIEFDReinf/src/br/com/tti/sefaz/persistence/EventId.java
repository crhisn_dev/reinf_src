package br.com.tti.sefaz.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class EventId implements Serializable {
	private String keyxml;
	private int nseq;

	public String getKeyxml() {
		return keyxml;
	}

	public void setKeyxml(String keyxml) {
		this.keyxml = keyxml;
	}

	public int getNseq() {
		return nseq;
	}

	public void setNseq(int nseq) {
		this.nseq = nseq;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keyxml == null) ? 0 : keyxml.hashCode());
		result = prime * result + nseq;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventId other = (EventId) obj;
		if (keyxml == null) {
			if (other.keyxml != null)
				return false;
		} else if (!keyxml.equals(other.keyxml))
			return false;
		if (nseq != other.nseq)
			return false;
		return true;
	}

}
