package br.com.tti.sefaz.persistence;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;

@Entity
public class ModeOperationData {

	@Id
	private String cnpj;
	private TPEMISS modo;
	private String xjust;
	private String dhcont;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public TPEMISS getModo() {
		return modo;
	}

	public void setModo(TPEMISS modo) {
		this.modo = modo;
	}

	public String getXjust() {
		return xjust;
	}

	public void setXjust(String xjust) {
		this.xjust = xjust;
	}

	public String getDhcont() {
		return dhcont;
	}

	public void setDhcont(String dhcont) {
		this.dhcont = dhcont;
	}

}
