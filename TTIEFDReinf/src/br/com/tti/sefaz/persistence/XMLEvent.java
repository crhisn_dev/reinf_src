package br.com.tti.sefaz.persistence;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;

import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.xml.epec.info.TEvento;

@Entity
public class XMLEvent {

	@EmbeddedId
	private EventId id;

	private XML_STATE status;

	@Embedded
	private TEvento evento;

	@Lob
	private String xmlprotocol;

	public EventId getId() {
		return id;
	}

	public void setId(EventId id) {
		this.id = id;
	}

	public XML_STATE getStatus() {
		return status;
	}

	public void setStatus(XML_STATE status) {
		this.status = status;
	}

	public TEvento getEvento() {
		return evento;
	}

	public void setEvento(TEvento evento) {
		this.evento = evento;
	}

	public String getXmlprotocol() {
		return xmlprotocol;
	}

	public void setXmlprotocol(String xmlprotocol) {
		this.xmlprotocol = xmlprotocol;
	}

}
