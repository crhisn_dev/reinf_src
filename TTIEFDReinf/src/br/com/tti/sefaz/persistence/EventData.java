package br.com.tti.sefaz.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.tti.sefaz.xml.evento.TEvento;
import br.com.tti.sefaz.xml.evento.TRetEvento;

@Entity
public class EventData implements Serializable {

	static public enum EVENTO_STATUS {
		GERADO, ENVIADO, REJEITADO, AUTORIZADO, ERRO
	}

	static public enum TIPO_EVENTO {
		TODOS("000000", "Todos"), EPEC("110113", "EPEC"), CANCELAMENTO(
				"110111", "Cancelamento"), REG_MULTI("110160",
				"Registro Multimodal"), CCE("110110", "Carta de Correcao");

		private final String codigo;
		private final String mensagem;

		private TIPO_EVENTO(String codigo, String mensagem) {
			this.codigo = codigo;
			this.mensagem = mensagem;
		}

		public String getCodigo() {
			return codigo;
		}

		public String getMensagem() {
			return mensagem;
		}
	}

	@EmbeddedId
	private EventId id;

	@Embedded
	private TEvento evento;

	@Embedded
	private TRetEvento retevento;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataGerada;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEnviada;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAutorizada;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAtualizada;

	@Lob
	private String xmlString;

	@Lob
	private String protocoloXML;

	private String descricao;

	private TIPO_EVENTO tipoEvento;

	private EVENTO_STATUS status;

	private String mensagem;

	public EventId getId() {
		return id;
	}

	public void setId(EventId id) {
		this.id = id;
	}

	public TEvento getEvento() {
		return evento;
	}

	public void setEvento(TEvento evento) {
		this.evento = evento;
	}

	public TRetEvento getRetevento() {
		return retevento;
	}

	public void setRetevento(TRetEvento retevento) {
		this.retevento = retevento;
	}

	public Date getDataGerada() {
		return dataGerada;
	}

	public void setDataGerada(Date dataGerada) {
		this.dataGerada = dataGerada;
	}

	public Date getDataEnviada() {
		return dataEnviada;
	}

	public void setDataEnviada(Date dataEnviada) {
		this.dataEnviada = dataEnviada;
	}

	public Date getDataAutorizada() {
		return dataAutorizada;
	}

	public void setDataAutorizada(Date dataAutorizada) {
		this.dataAutorizada = dataAutorizada;
	}

	public Date getDataAtualizada() {
		return dataAtualizada;
	}

	public void setDataAtualizada(Date dataAtualizada) {
		this.dataAtualizada = dataAtualizada;
	}

	public String getXmlString() {
		return xmlString;
	}

	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}

	public String getProtocoloXML() {
		return protocoloXML;
	}

	public void setProtocoloXML(String protocoloXML) {
		this.protocoloXML = protocoloXML;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TIPO_EVENTO getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(TIPO_EVENTO tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public EVENTO_STATUS getStatus() {
		return status;
	}

	public void setStatus(EVENTO_STATUS status) {
		this.status = status;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventData other = (EventData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
