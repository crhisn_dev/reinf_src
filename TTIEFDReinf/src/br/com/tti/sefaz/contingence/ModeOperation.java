package br.com.tti.sefaz.contingence;

import java.rmi.RemoteException;
import java.util.Hashtable;

import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;

public interface ModeOperation extends java.rmi.Remote {

	public void changeToState(String cnpj, String ambient, TPEMISS modo,
			Hashtable<String, Object> props) throws RemoteException;

	public TPEMISS getModo(String cnpj, String ambient) throws RemoteException;

	public Hashtable<String, String> getJust(String cnpj, String ambient, TPEMISS tpemiss)
			throws RemoteException;
}
