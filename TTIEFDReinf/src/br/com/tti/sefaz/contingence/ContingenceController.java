package br.com.tti.sefaz.contingence;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.logging.Level;

import br.com.tti.sefaz.contingence.impl.EPECContingenceStrategyImpl;
import br.com.tti.sefaz.contingence.impl.EnterContingenceNFe;
import br.com.tti.sefaz.contingence.impl.PMLeaveContingenceStrategy;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.DBConfig;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.ManagerFacadeMock;
import br.com.tti.sefaz.util.ModoOpManager;
import br.com.tti.sefaz.util.ReadFile;

public class ContingenceController {

	private ManagerInterface manager;
	private ModoOpManager opmanager;

	private EPECContingenceStrategyImpl epecstrategy;

	public ContingenceController(ManagerInterface manager) {
		super();
		this.manager = manager;
		this.opmanager = new ModoOpManager();

	}

	private EnterContingenceStrategy findEnterStrategy(String cnpj,
			String ambient, TPEMISS mode) {
		if (mode.equals(TPEMISS.CONTINGENCE_EPEC)) {
			try {
				return new EPECContingenceStrategyImpl(this.manager, cnpj,
						ambient);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
		return null;
	}

	private LeaveContingenceStrategy findLeaveStrategy(String cnpj,
			String ambient, TPEMISS mode) {
		if (mode.equals(TPEMISS.CONTINGENCE_EPEC)) {
			try {
				return new EPECContingenceStrategyImpl(this.manager, cnpj,
						ambient);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}

		if (mode.equals(TPEMISS.CONTINGENCE)) {
			try {
				return new PMLeaveContingenceStrategy(this.manager, cnpj);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
		return null;
	}

	private ProcessContingenceStrategy findProcessStrategy(String cnpj,
			String ambient, TPEMISS mode) {
		if (mode.equals(TPEMISS.CONTINGENCE_EPEC)) {
			if (this.epecstrategy == null) {
				try {
					this.epecstrategy = new EPECContingenceStrategyImpl(
							this.manager, cnpj, ambient);
				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(),
							e);
				}
			}
			return this.epecstrategy;
		}
		return null;

	}

	public void enterContingence(String cnpj, String ambient, boolean async,
			TPEMISS type) throws RemoteException {

		/*
		 * if (type.equals(TPEMISS.CONTINGENCE)) { EnterContingenceStrategy
		 * strategy = new EnterContingenceNFe( this.manager, cnpj, ambient); if
		 * (async) { Thread t = new Thread(strategy); t.start(); } else {
		 * strategy.enterContingence(); } }
		 */

		try {
			TPEMISS mode = this.opmanager.getModo(cnpj);
			final EnterContingenceStrategy strategy = this.findEnterStrategy(
					cnpj, ambient, mode);
			if (strategy == null) {
				MyLogger.getLog().info(
						"Strategy dont found for mode:" + mode + " and CNPJ: "
								+ cnpj);
				return;
			}

			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					strategy.enterContingence();
				}
			});
			t.start();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			throw new RemoteException(e.getLocalizedMessage(), e.getCause());
		}

	}

	public void leaveContingence(String cnpj, String ambient, boolean async,
			TPEMISS modo) throws RemoteException {
		try {
			MyLogger.getLog().info(
					"Leaving Contingence:" + cnpj + " modo: " + modo);

			TPEMISS currentmode = this.opmanager.getModo(cnpj);

			if (!TPEMISS.NORMAL.equals(currentmode)
					&& !TPEMISS.SVC_RS.equals(currentmode)
					&& !TPEMISS.SVC_SP.equals(currentmode)) {
				MyLogger.getLog().info(
						"Dont setting in correct state. Must be in a sending mode:"
								+ currentmode + " for CNPJ: " + cnpj);

				throw new RemoteException(
						"Deveria estar em modo do envio. Estado atual:"
								+ currentmode + " para o CNPJ: " + cnpj);
			}

			final LeaveContingenceStrategy strategy = this.findLeaveStrategy(
					cnpj, ambient, modo);
			if (strategy == null) {
				MyLogger.getLog().info(
						"Strategy dont found for mode:" + modo + " and CNPJ: "
								+ cnpj);

				throw new RemoteException("Strategy dont found for mode:"
						+ modo + " and CNPJ: " + cnpj);

			}

			MyLogger.getLog().info("async:" + async);
			if (async) {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						strategy.leaveContingence();
					}
				});
				t.start();
			} else {
				strategy.leaveContingence();
			}
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			throw new RemoteException(e.getLocalizedMessage(), e.getCause());
		}
	}

	public void processInContingence(final String keyXml, final String ambient,
			final String xml, final Hashtable<String, Object> props)
			throws RemoteException {
		try {
			KeyXml key = new KeyXml(keyXml);
			String cnpj = key.getCnpj();

			TPEMISS mode = this.opmanager.getModo(cnpj);

			if (mode == null) {
				MyLogger.getLog().info("Mode dont found for and CNPJ: " + cnpj);
				return;
			}

			final ProcessContingenceStrategy strategy = this
					.findProcessStrategy(cnpj, ambient, mode);

			if (strategy == null) {
				MyLogger.getLog().info(
						"Strategy dont found for mode:" + mode + " and CNPJ: "
								+ cnpj);
				return;
			}

			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						strategy.processInContingence(keyXml, xml, props);
					} catch (Exception e) {
						MyLogger.getLog().log(Level.INFO,
								e.getLocalizedMessage(), e);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			throw new RemoteException(e.getLocalizedMessage(), e.getCause());
		}
	}

}
