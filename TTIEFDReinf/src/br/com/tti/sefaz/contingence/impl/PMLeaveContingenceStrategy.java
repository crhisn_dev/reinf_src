package br.com.tti.sefaz.contingence.impl;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.Query;

import br.com.tti.sefaz.contingence.AbstractContingenceStrategy;
import br.com.tti.sefaz.contingence.LeaveContingenceStrategy;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.XMLEvent;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.KeyXml;

public class PMLeaveContingenceStrategy extends AbstractContingenceStrategy
		implements LeaveContingenceStrategy {

	private DAO<XMLEvent> daoevent;
	private String cnpj;

	public PMLeaveContingenceStrategy(ManagerInterface manager, String cnpj) {
		super(manager);
		this.daoevent = DaoFactory.createDAO(XMLEvent.class);
		this.cnpj = cnpj;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	@Override
	public void leaveContingence() {
		MyLogger.getLog().info("Starting leaving contingence Papel Moeda...");

		this.daoevent.clean();

		Query query = this.daoevent
				.createQuery("SELECT e FROM XMLData as e WHERE e.state = :status and e.cnpjEmit = :cnpj and e.modo = :modo");
		query.setParameter("status", XML_STATE.GERADA);
		query.setParameter("modo", TPEMISS.CONTINGENCE);
		query.setParameter("cnpj", this.cnpj);

		List<XMLData> results = query.getResultList();

		if (results != null) {
			for (XMLData xmldata : results) {
				KeyXml key = new KeyXml(xmldata.getKeyXml());

				TPEMISS emiss = TPEMISS.values()[Integer.parseInt(key
						.getTpEmiss())];

				if (emiss.equals(xmldata.getModo())) {
					MyLogger.getLog().info(
							"sending KeyXML:" + xmldata.getKeyXml());
					Hashtable<String, String> prop = new Hashtable<String, String>();
					try {
						this.manager.sendXml(xmldata.getKeyXml(),
								xmldata.getXmlString(), xmldata.getCnpjEmit(),
								xmldata.getCnpjDest(), xmldata.getDateEmiss(),
								"", xmldata.getUf(), xmldata.getAmbient(),
								false, false, prop);
					} catch (RemoteException e) {
						MyLogger.getLog().log(Level.INFO,
								e.getLocalizedMessage(), e);
					}
				} else {
					MyLogger.getLog().info(
							"Tipo de emissao na chave nao corresponde ao modo de operacao: "
									+ xmldata.getKeyXml());
					continue;
				}
			}
		}
	}
}
