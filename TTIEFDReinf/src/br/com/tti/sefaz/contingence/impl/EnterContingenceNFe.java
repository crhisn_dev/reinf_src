package br.com.tti.sefaz.contingence.impl;

import java.util.List;

import br.com.tti.sefaz.contingence.AbstractContingenceStrategy;
import br.com.tti.sefaz.contingence.EnterContingenceStrategy;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;

public class EnterContingenceNFe extends AbstractContingenceStrategy implements
		EnterContingenceStrategy {

	public EnterContingenceNFe(ManagerInterface manager, String cnpj,
			String ambient) {
		super(manager);
		this.manager = manager;
		this.daoXml = DaoFactory.createDAO(XMLData.class);
		
	}

	private void setUpContingece() {
		/*
		 * List<XMLData> xmls = this.findXMLDataWithStates(
		 * this.toContingenceStates, TPEMISS.NORMAL); for (XMLData xml : xmls) {
		 * xml.setModo(TPEMISS.CONTINGENCE); this.daoXml.updateEntity(xml); }
		 */
	}

	@Override
	public void enterContingence() {
		this.setUpContingece();
	}

	@Override
	public void run() {
		this.enterContingence();
	}

}
