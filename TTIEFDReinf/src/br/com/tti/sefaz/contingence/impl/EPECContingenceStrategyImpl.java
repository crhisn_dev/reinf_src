package br.com.tti.sefaz.contingence.impl;

import java.io.StringWriter;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.Query;
import javax.xml.bind.JAXBElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.toplink.essentials.internal.parsing.ExistsNode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import br.com.tti.sefaz.contingence.AbstractContingenceStrategy;
import br.com.tti.sefaz.contingence.EnterContingenceStrategy;
import br.com.tti.sefaz.contingence.LeaveContingenceStrategy;
import br.com.tti.sefaz.contingence.ProcessContingenceStrategy;
import br.com.tti.sefaz.cte.xmlv104c.TCTe;
import br.com.tti.sefaz.cte.xmlv104c.TImp;
import br.com.tti.sefaz.listeners.impl.LogDatabaseImpl;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.EventId;
import br.com.tti.sefaz.persistence.LogXML;
import br.com.tti.sefaz.persistence.TTIState;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.XMLEvent;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.remote.SchemaVersionConfig;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.FileLocator;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xml.XMLValidator;
import br.com.tti.sefaz.xml.epec.info.EvEPECCTe;
import br.com.tti.sefaz.xml.epec.info.EvEPECCTe.Toma04;
import br.com.tti.sefaz.xml.epec.info.TEvento;
import br.com.tti.sefaz.xml.epec.info.TEvento.InfEvento;
import br.com.tti.sefaz.xml.epec.info.TEvento.InfEvento.DetEvento;
import br.com.tti.sefaz.xml.epec.info.TRetEvento;
import br.com.tti.sefaz.xml.epec.info.TUf;
import br.com.tti.sefaz.xmlgenerate.cte.XMLMessageFactoryCTe;

public class EPECContingenceStrategyImpl extends AbstractContingenceStrategy
		implements EnterContingenceStrategy, LeaveContingenceStrategy,
		ProcessContingenceStrategy {

	public static String TPEVENTO = "110113";

	private XMLGenerator genxml;
	private XMLGenerator genepec;
	private SimpleDateFormat sdf;
	private ManagerInterface manager;
	private XMLMessageFactoryCTe factory;
	private XMLValidator valevento;
	private XMLValidator valepec;

	private DAO<XMLEvent> daoevent;
	private DAO<XMLData> daodata;
	private DAO<LogXML> daolog;
	private String cnpj;
	private String ambient;
	private LogDatabaseImpl logdatabase;
	private ResourceBundle epec_table;

	public EPECContingenceStrategyImpl(ManagerInterface manager, String cnpj,
			String ambient) throws Exception {
		super(manager);
		this.manager = manager;
		this.cnpj = cnpj;
		this.ambient = ambient;

		this.genxml = new XMLGenerator("br.com.tti.sefaz.cte.xmlv104c");
		this.genepec = new XMLGenerator("br.com.tti.sefaz.xml.epec.info");

		Hashtable<Vector<String>, Vector<SchemaVersionConfig>> schemaVersion = this.manager
				.getSchemasVersion();

		this.factory = new XMLMessageFactoryCTe(schemaVersion);
		this.daoevent = DaoFactory.createDAO(XMLEvent.class);
		this.daodata = DaoFactory.createDAO(XMLData.class);
		this.daolog = DaoFactory.createDAO(LogXML.class);

		this.sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		this.valevento = new XMLValidator(MainParameters.getXSDEvento());
		this.valepec = new XMLValidator(MainParameters.getXSDEPEC());

		this.logdatabase = new LogDatabaseImpl();

		try {
			this.epec_table = ResourceBundle.getBundle("tabela_epec");
		} catch (Exception e) {
			MyLogger.getLog()
					.info("Nao foi possivel carregar a tabela de EPEC!. Arquivo tabela_epec.properties");
			System.exit(3000);
		}
	}

	@Override
	public void run() {

	}

	private String sendXML(String uf, String cufheader, String cnpj,
			String ambient, String xml) throws Exception {
		String header = this.factory.createHeader(cufheader, null,
				SystemProperties.ID_SERVICO_EVENTOS);

		String data = "<cteDadosMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcaoEvento\">"
				+ xml + "</cteDadosMsg >";
		Hashtable<String, String> prop = new Hashtable<String, String>();

		prop.put("AMBIENT", ambient);
		prop.put("UF", uf);
		prop.put("CNPJ", cnpj);

		return this.manager.sendXMLMessage(SystemProperties.ID_SERVICO_EVENTOS,
				header, data, prop);
	}

	private String findVICMS(TImp tImp) {
		if (tImp.getICMS00() != null && tImp.getICMS00().getVICMS() != null) {
			return tImp.getICMS00().getVICMS();
		}

		if (tImp.getICMS20() != null && tImp.getICMS20().getVICMS() != null) {
			return tImp.getICMS20().getVICMS();
		}

		if (tImp.getICMS90() != null && tImp.getICMS90().getVICMS() != null) {
			return tImp.getICMS90().getVICMS();
		}

		if (tImp.getICMSOutraUF() != null
				&& tImp.getICMSOutraUF().getVICMSOutraUF() != null) {
			return tImp.getICMSOutraUF().getVICMSOutraUF();
		}

		return "0.00";

	}

	private Toma04 fillToma04(TCTe cte) {
		Toma04 toma04epec = new Toma04();

		String cnpjtoma = null;
		String cpftoma = null;
		String ietoma = null;
		String tomatype = null;
		TUf uftoma = null;

		if (cte.getInfCte().getIde().getToma03() != null) {
			tomatype = cte.getInfCte().getIde().getToma03().getToma();

			if (tomatype.equals("0")) {
				cnpjtoma = cte.getInfCte().getRem().getCNPJ();
				cpftoma = cte.getInfCte().getRem().getCPF();
				ietoma = cte.getInfCte().getRem().getIE();
				uftoma = TUf.valueOf(cte.getInfCte().getRem().getEnderReme()
						.getUF().toString());
			}
			if (tomatype.equals("3")) {
				cnpjtoma = cte.getInfCte().getDest().getCNPJ();
				cpftoma = cte.getInfCte().getDest().getCPF();
				ietoma = cte.getInfCte().getDest().getIE();
				uftoma = TUf.valueOf(cte.getInfCte().getDest().getEnderDest()
						.getUF().toString());
			}

		} else if (cte.getInfCte().getIde().getToma4() != null) {
			cnpjtoma = cte.getInfCte().getIde().getToma4().getCNPJ();
			cpftoma = cte.getInfCte().getIde().getToma4().getCPF();
			ietoma = cte.getInfCte().getIde().getToma4().getIE();
			tomatype = "4";
			uftoma = TUf.valueOf(cte.getInfCte().getIde().getToma4()
					.getEnderToma().getUF().toString());
		}

		if (cnpjtoma != null) {
			toma04epec.setCNPJ(cnpjtoma);
		}

		if (cpftoma != null) {
			toma04epec.setCPF(cpftoma);
		}

		toma04epec.setIE(ietoma);
		toma04epec.setToma(tomatype);
		toma04epec.setUF(uftoma);

		return toma04epec;
	}

	@Override
	synchronized public void processInContingence(String keyxml, String xml,
			Hashtable<String, Object> props) throws Exception {
		TCTe cte = ((JAXBElement<TCTe>) this.genxml.toObject(xml)).getValue();

		String uf = cte.getInfCte().getIde().getCMunEnv().substring(0, 2);

		XMLData xmldata = this.daodata.findEntity(keyxml);

		TEvento evento = new TEvento();
		InfEvento infevento = new InfEvento();
		evento.setInfEvento(infevento);

		infevento.setChCTe(cte.getInfCte().getId().replace("CTe", ""));
		infevento.setCNPJ(cte.getInfCte().getEmit().getCNPJ());
		infevento.setCOrgao(this.epec_table.getString(uf).replace("epec", ""));

		infevento
				.setDhEvento(this.sdf.format(Calendar.getInstance().getTime()));
		infevento.setTpEvento(TPEVENTO);
		infevento.setTpAmb(xmldata.getAmbient().contains("producao") ? "1"
				: "2");

		String version = this.factory.getSchemaVersion(infevento.getCOrgao(),
				SystemProperties.ID_SERVICO_EVENTOS);
		//version = "1.04";
		evento.setVersao(version);

		// /////////////////// dados EPEC ///////////////////////////

		EvEPECCTe evepec = new EvEPECCTe();
		evepec.setDescEvento("EPEC");
		evepec.setXJust(cte.getInfCte().getIde().getXJust() != null ? cte
				.getInfCte().getIde().getXJust()
				: "NAO FOI INFORMADA NENHUMA JUSTIFICATIVA");
		evepec.setVICMS(this.findVICMS(cte.getInfCte().getImp().getICMS()));
		evepec.setVTPrest(cte.getInfCte().getVPrest().getVTPrest());
		evepec.setVCarga(cte.getInfCte().getInfCTeNorm().getInfCarga()
				.getVCarga());
		Toma04 toma04epec = this.fillToma04(cte);
		evepec.setToma04(toma04epec);
		evepec.setModal("01");
		evepec.setUFFim(TUf.valueOf(cte.getInfCte().getIde().getUFFim()
				.toString()));
		evepec.setUFIni(TUf.valueOf(cte.getInfCte().getIde().getUFIni()
				.toString()));

		DetEvento detevento = new DetEvento();
		detevento.setVersaoEvento(version);
		detevento.setDetEvento(evepec);
		detevento.setAny(this.createAny(evepec));

		infevento.setDetEvento(detevento);

		String ID = this.createID(keyxml, infevento);
		infevento.setId(ID);

		// /////////// salva evento no banco /////////////////

		XMLEvent eventdata = new XMLEvent();
		EventId ideventdata = new EventId();
		ideventdata.setKeyxml(keyxml);
		ideventdata.setNseq(Integer.parseInt(infevento.getNSeqEvento()));
		eventdata.setId(ideventdata);
		eventdata.setEvento(evento);
		eventdata.setStatus(XML_STATE.GERADA);

		if (this.daoevent.findEntity(ideventdata) == null) {
			this.daoevent.saveEntity(eventdata);
		} else {
			this.daoevent.updateEntity(eventdata);
		}

		// ////////////// validando epec //////////////////////////////
		String xmlepec = this.convertToXML(detevento.getAny());
		boolean error = false;
		String messageerror = "";

		try {
			this.valepec.validateXml(xmlepec);
		} catch (SAXException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			error = true;
			messageerror = e.getLocalizedMessage();
		}

		if (error) {
			throw new Exception(messageerror);
		}

		// ////////////////// validando evento /////////////////////////
		String xmlEvent = this.convertToXML(evento);

		String signedXML = this.manager.signForCNPJ__Reinf(infevento.getCNPJ(),
				xmlEvent, SystemProperties.ID_SERVICO_EVENTOS);

		signedXML = new String(signedXML.getBytes(), "UTF-8");

		MyLogger.getLog().info(signedXML);

		error = false;
		messageerror = "";

		try {
			this.valevento.validateXml(signedXML);
		} catch (SAXException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			error = true;
			messageerror = e.getLocalizedMessage();
		}

		if (error) {
			throw new Exception(messageerror);
		}

		// ///////////////// enviando XML //////////////////////////////

		eventdata.setStatus(XML_STATE.TENTANDO_ENVIO);
		this.daoevent.updateEntity(eventdata);

		String resultXML = this.sendXML(this.epec_table.getString(uf), uf, cte
				.getInfCte().getEmit().getCNPJ(), xmldata.getAmbient(),
				signedXML);

		eventdata.setStatus(XML_STATE.ENVIADA);
		this.daoevent.updateEntity(eventdata);

		this.processResult(xmldata, eventdata, resultXML);
	}

	private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	private void processResult(XMLData xmldata, XMLEvent eventdata,
			String resultXML) throws Exception {
		int pos1 = resultXML.indexOf("<retEventoCTe");
		int pos2 = resultXML.indexOf("</retEventoCTe>");
		if (pos1 != -1 && pos2 != -1) {
			resultXML = resultXML.substring(pos1,
					pos2 + "</retEventoCTe>".length());
		}

		TRetEvento returnevento = (TRetEvento) this.genepec.toObject(resultXML);

		MyLogger.getLog().info(
				"cStat: " + returnevento.getInfEvento().getCStat()
						+ " xMotivo:"
						+ returnevento.getInfEvento().getXMotivo());

		eventdata.setXmlprotocol(resultXML);
		if (
		// "134".equals(returnevento.getInfEvento().getCStat())||
		// "135".equals(returnevento.getInfEvento().getCStat()) ||
		"136".equals(returnevento.getInfEvento().getCStat())) {
			eventdata.setStatus(XML_STATE.AUTORIZADA);
			xmldata.setState(XML_STATE.EPEC_ENVIADO);
			xmldata.setModo(TPEMISS.CONTINGENCE_EPEC);

			try {
				Hashtable<String, String> prop = new Hashtable<String, String>();
				prop.put("PROTOCOLO", returnevento.getInfEvento().getNProt()
						+ " "
						+ returnevento.getInfEvento().getDhRegEvento()
								.toString());
				prop.put("DATAGERADA", this.df.format(xmldata.getDateCreate()));
				prop.put(
						"IMPRESSORA",
						(xmldata.getImpressora() != null ? xmldata
								.getImpressora() : ""));
				prop.put(
						"pdffile",
						FileLocator.getInstance().findPDF(
								xmldata.getCnpjEmit(), xmldata.getDateCreate(),
								xmldata.getKeyXml()));
				prop.put(
						"psfile",
						FileLocator.getInstance().findPS(xmldata.getCnpjEmit(),
								xmldata.getDateCreate(), xmldata.getKeyXml()));

				this.manager.printXml(xmldata.getKeyXml(),
						xmldata.getXmlString(), TPEMISS.CONTINGENCE_EPEC, prop);

				prop.put(
						"pdffile",
						FileLocator.getInstance().findPDF(
								xmldata.getCnpjEmit(), xmldata.getDateCreate(),
								xmldata.getKeyXml()));
				prop.put(
						"pdffile",
						FileLocator.getInstance().findPS(xmldata.getCnpjEmit(),
								xmldata.getDateCreate(), xmldata.getKeyXml()));
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}

		} else {
			eventdata.setStatus(XML_STATE.REJEITADA);
			xmldata.setState(XML_STATE.EPEC_NAO_ENVIADO);
			xmldata.setXMessageError(returnevento.getInfEvento().getXMotivo());
		}

		this.daoevent.updateEntity(eventdata);
		this.daodata.updateEntity(xmldata);
		this.logdatabase.process(xmldata);
	}

	private String convertToXML(TEvento event) throws Exception {
		String xml = this.genepec.toXMLString(event);
		return xml;
	}

	private Element createAny(EvEPECCTe evepec) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document detepec = builder.newDocument();

		this.genepec.getMarshaller().marshal(evepec, detepec);

		Element any = detepec.getDocumentElement();
		return any;
	}

	private String convertToXML(Node node) throws Exception {
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		StringWriter buffer = new StringWriter();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(new DOMSource(node), new StreamResult(buffer));
		String str = buffer.toString();

		return str;
	}

	private int calulateNSeqEvento(String keyxml) {
		if (!keyxml.startsWith("CTe")) {
			keyxml = "CTe" + keyxml;
		}

		int nSeqEvent = 1;

		Query query = this.daoevent
				.createQuery("SELECT e FROM XMLEvent as e WHERE e.id.keyxml = :key and e.status = :status and e.evento.infEvento.tpEvento = :tipo");

		query.setParameter("status", XML_STATE.AUTORIZADA);
		query.setParameter("tipo", TPEVENTO);
		query.setParameter("key", keyxml);

		List<XMLEvent> results = query.getResultList();
		if (results != null && !results.isEmpty()) {
			nSeqEvent = results.size() + 1;
		}

		return nSeqEvent;
	}

	private String createID(String keyxml, InfEvento infevento) {
		String ID = "";
		int nseq = calulateNSeqEvento(keyxml);
		ID = "ID" + infevento.getTpEvento() + keyxml.replace("CTe", "")
				+ String.format("%02d", nseq);
		infevento.setNSeqEvento(nseq + "");
		return ID;
	}

	private boolean checkWasWithoutReturn(String keyxml) {
		Query q = this.daolog
				.createQuery("SELECT l FROM LogXML as l WHERE l.keyxml = :key and l.statusop = :status");
		q.setParameter("key", keyxml.startsWith("CTe") ? keyxml : "CTe"
				+ keyxml);
		q.setParameter("status", XML_STATE.ENVIADA);

		List<LogXML> result = q.getResultList();
		if (result != null) {
			return !result.isEmpty();
		}
		return false;
	}

	@Override
	public void leaveContingence() {
		MyLogger.getLog().info("Starting leaving contingence EPEC...");

		this.daoevent.clean();

		Query query = this.daoevent
				.createQuery("SELECT e FROM XMLData as e WHERE e.state = :status and e.cnpjEmit = :cnpj");
		query.setParameter("status", XML_STATE.EPEC_ENVIADO);
		query.setParameter("cnpj", this.cnpj);

		List<XMLData> results = query.getResultList();

		if (results != null) {
			for (XMLData xmldata : results) {
				KeyXml key = new KeyXml(xmldata.getKeyXml());

				TPEMISS emiss = TPEMISS.values()[Integer.parseInt(key
						.getTpEmiss())];

				if (emiss.equals(xmldata.getModo())) {
					MyLogger.getLog().info(
							"sending KeyXML:" + xmldata.getKeyXml());
					Hashtable<String, String> prop = new Hashtable<String, String>();
					prop.put("epec", "true");
					try {
						this.manager.sendXml(xmldata.getKeyXml(),
								xmldata.getXmlString(), xmldata.getCnpjEmit(),
								xmldata.getCnpjDest(), xmldata.getDateEmiss(),
								"", xmldata.getUf(), xmldata.getAmbient(),
								false, false, prop);
					} catch (RemoteException e) {
						MyLogger.getLog().log(Level.INFO,
								e.getLocalizedMessage(), e);
					}
				} else {
					MyLogger.getLog().info(
							"Tipo de emissao na chave nao corresponde ao modo de operacao: "
									+ xmldata.getKeyXml());
					continue;
				}
			}
		}
	}

	@Override
	public void enterContingence() {
		MyLogger.getLog().info("Starting leaving contingence EPEC...");

		this.daoevent.clean();

		Vector<TTIState> selectedstates = new Vector<TTIState>();
		selectedstates.add(new TTIState(XML_STATE.ENVIADA, -1));
		selectedstates.add(new TTIState(XML_STATE.ERRO_COMUNICACAO_SEFAZ, -1));

		List<XMLData> xmldatas = this.findXMLDataWithStates(selectedstates,
				TPEMISS.CONTINGENCE_EPEC, this.cnpj, this.ambient);

		for (XMLData xmldata : xmldatas) {
			if (this.checkWasWithoutReturn(xmldata.getKeyXml())) {

			}
		}
	}

}
