package br.com.tti.sefaz.contingence;

import java.rmi.Remote;
import java.rmi.RemoteException;

import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;

public interface ContingeceInterface extends Remote {

	public void enterContingence(String cnpj, String ambient, boolean async,
			TPEMISS modo) throws RemoteException;

	public void leaveContingence(String cnpj, String ambient, boolean async,
			TPEMISS type) throws RemoteException;

	public void processInContingence(String keyxml, String ambient, String xml)
			throws RemoteException;

}
