package br.com.tti.sefaz.contingence;

import java.rmi.RemoteException;
import java.util.Hashtable;

import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.StateSystem;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.Locator;

public class Contingence implements ContingeceInterface {

	private ManagerInterface manager;
	private ContingenceController controller;
	private StateSystem system;

	public Contingence() {
		this.manager = Locator.getManagerReference();
		assert this.manager != null;
		this.controller = new ContingenceController(this.manager);
		this.system = new StateSystem();
	}

	@Override
	public void enterContingence(String cnpj, String ambient, boolean async,
			TPEMISS modo) throws RemoteException {
		this.controller.enterContingence(cnpj, ambient, async, modo);
	}

	@Override
	public void leaveContingence(String cnpj, String ambient, boolean async,
			TPEMISS type) throws RemoteException {
		this.controller.leaveContingence(cnpj, ambient, async, type);
	}

	@Override
	public void processInContingence(String keyxml, String ambient, String xml)
			throws RemoteException {
		Hashtable<String, Object> props = new Hashtable<String, Object>();
		// props.put("AMBIENT", this.system.getAmbient());
		this.controller.processInContingence(keyxml, ambient, xml, props);
	}

}
