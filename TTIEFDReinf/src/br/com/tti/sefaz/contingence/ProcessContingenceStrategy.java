package br.com.tti.sefaz.contingence;

import java.util.Hashtable;

public interface ProcessContingenceStrategy {
	public void processInContingence(String keyxml, String xml,
			Hashtable<String, Object> props) throws Exception;

}
