package br.com.tti.sefaz.log;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import br.com.tti.sefaz.util.MainParameters;

public class MyLogger {

	private static Logger logger = null;

	public static Logger getLog() {
		if (logger == null) {
			logger = Logger.getLogger("br.com.tti.sefaz");

			// fh.setFormatter(new SimpleFormatter());
			// fh.setLevel(Level.ALL);
			// logger.addHandler(fh);

			for (Handler ch : logger.getHandlers()) {
				ch.setFormatter(new SimpleFormatter());
				if (MainParameters.isConsole()) {
					ch.setLevel(Level.OFF);
				} else {
					ch.setLevel(Level.ALL);
				}
			}

			if (MainParameters.isDebug()) {
				logger.setLevel(Level.FINEST);
			} else {
				logger.setLevel(Level.INFO);
			}
		}
		// logger.setLevel(Level.INFO);

		return logger;
	}

	public static void main(String[] args) {
		MyLogger.getLog().warning("warning");

		MyLogger.getLog().info("info");
		MyLogger.getLog().config("config");
		MyLogger.getLog().finest("finest");
		MyLogger.getLog().log(Level.INFO, "nfo2");
	}
}
