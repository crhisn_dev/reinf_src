package br.com.tti.sefaz.event;

import java.io.FileWriter;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.EventData;
import br.com.tti.sefaz.persistence.EventData.EVENTO_STATUS;
import br.com.tti.sefaz.persistence.EventData.TIPO_EVENTO;
import br.com.tti.sefaz.persistence.EventId;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.persistence.dao.XMLDaoJPA;
import br.com.tti.sefaz.remote.SchemaVersionConfig;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xml.XMLValidator;
import br.com.tti.sefaz.xml.evento.TEvento;
import br.com.tti.sefaz.xml.evento.TEvento.InfEvento;
import br.com.tti.sefaz.xml.evento.TEvento.InfEvento.DetEvento;
import br.com.tti.sefaz.xml.evento.TRetEvento;
import br.com.tti.sefaz.xml.evento.cancel.EvCancCTe;
import br.com.tti.sefaz.xml.evento.cce.EvCCeCTe;
import br.com.tti.sefaz.xml.evento.cce.EvCCeCTe.InfCorrecao;
import br.com.tti.sefaz.xmlgenerate.cte.XMLMessageFactoryCTe;

public class EventManagerController implements EventManager {

	private DAO<EventData> daoEvent;
	// private DAO<XMLData> daoXml;

	private XMLGenerator gen;
	private XMLGenerator genret;

	private String cnpj;
	private String UF;

	private SimpleDateFormat sdf;
	// private SenderDispatchController sender;
	// private ResourceBundle bundle;

	private String ambient;
	private String xmlFile;
	private String xCondUso = "Ciencia da Operacao";
	private List<EventManagerListener> listeners;
	private ManagerInterface manager;
	private XMLMessageFactoryCTe factory;

	/*
	 * public EventManagerImpl() { this.bundle =
	 * ResourceBundle.getBundle("configure"); this.ambient =
	 * this.bundle.getString("ambient"); this.xmlFile =
	 * this.bundle.getString("fileconfig");
	 * 
	 * String[] args = new String[3]; args[0] = "-xml"; args[1] = this.xmlFile;
	 * args[2] = "-localdb";
	 * 
	 * MainParameters.processArguments(args);
	 * 
	 * this.daoEvent = DaoFactory.createDAO(EventData.class); this.listeners =
	 * new ArrayList<EventManagerListener>();
	 * 
	 * this.gen = new XMLGenerator("br.com.tti.sefaz.event.xml.classes");
	 * this.genret = new XMLGenerator("br.com.tti.sefaz.event.xml.classes.ret");
	 * 
	 * this.factory = new ObjectFactory(); this.sdf = new
	 * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:00'-02:00'");
	 * 
	 * XMLConfigSystem configSystem = new XMLConfigSystem(
	 * MainParameters.getXml()); configSystem.make();
	 * 
	 * this.manager = Locator.getManagerReference(); }
	 */

	public EventManagerController() throws Exception {
		// this.bundle = ResourceBundle.getBundle("configure");
		this.daoEvent = DaoFactory.createDAO(EventData.class);
		// this.daoXml = DaoFactory.createDAO(XMLData.class);
		this.listeners = new ArrayList<EventManagerListener>();

		this.gen = new XMLGenerator("br.com.tti.sefaz.xml.evento");
		this.genret = new XMLGenerator("br.com.tti.sefaz.xml.evento");

		this.sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:00'-02:00'");

		this.manager = Locator.getManagerReference();

		Hashtable<Vector<String>, Vector<SchemaVersionConfig>> schemaVersion = this.manager
				.getSchemasVersion();

		this.factory = new XMLMessageFactoryCTe(schemaVersion);
		/*
		 * this.sender = new SenderDispatchController(
		 * configSystem.getSenderConfig(), configSystem.getServiceConfig());
		 */

	}

	/*
	 * public EventManagerImpl(SenderDispatchController sender) { this.bundle =
	 * ResourceBundle.getBundle("configure"); this.daoEvent =
	 * DaoFactory.createDAO(EventData.class); this.listeners = new
	 * ArrayList<EventManagerListener>();
	 * 
	 * this.gen = new XMLGenerator("br.com.tti.sefaz.event.xml.classes");
	 * this.genret = new XMLGenerator("br.com.tti.sefaz.event.xml.classes.ret");
	 * 
	 * this.factory = new ObjectFactory(); this.sdf = new
	 * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:00'-02:00'");
	 * 
	 * XMLConfigSystem configSystem = new XMLConfigSystem(
	 * MainParameters.getXml()); configSystem.make();
	 * 
	 * 
	 * this.sender = new SenderDispatchController(
	 * configSystem.getSenderConfig(), configSystem.getServiceConfig());
	 * 
	 * 
	 * this.sender = sender;
	 * 
	 * this.signer = new Signer(configSystem.getCertificates()); }
	 */

	@Override
	public List<EventData> obterEventos(String keyxml,
			Hashtable<String, String> props) {

		if (keyxml.startsWith("CTe")) {
			keyxml = keyxml.replace("CTe", "");
		}

		System.out.println("Find events for:" + keyxml);

		if (keyxml.startsWith("CTe")) {
			keyxml = keyxml.replace("CTe", "");
		}

		String sql = "SELECT o FROM EventData as o where o.evento.infEvento.chCTe = '"
				+ keyxml + "' order by o.evento.infEvento.dhEvento desc ";

		Query query = this.daoEvent.createQuery(sql);
		// query.setParameter("e", ESTADO_NFE.AUTORIZADA);

		return query.getResultList();
	}

	@Override
	public List<EventData> obterEventosSequence(DAO<EventData> daoevent,
			String keyxml, String codetipoevent, Hashtable<String, String> props) {

		if (daoevent == null) {
			MyLogger.getLog().info("creating new dao");
			daoevent = (XMLDaoJPA<EventData>) DaoFactory
					.createDAO(EventData.class);
		}

		if (keyxml.startsWith("CTe")) {
			keyxml = keyxml.replace("CTe", "");
		}

		String sql = "SELECT o FROM EventData as o where o.evento.infEvento.chCTe = '"
				+ keyxml
				+ "' and o.status = :e and o.evento.infEvento.tpEvento = :tipoevent order by o.evento.infEvento.nSeqEvento desc ";

		Query query = daoevent.createQuery(sql);
		query.setParameter("e", EVENTO_STATUS.AUTORIZADO);
		query.setParameter("tipoevent", codetipoevent);

		return query.getResultList();
	}

	public int obterNumberEventos(DAO<EventData> daoevent, String keyxml,
			Hashtable<String, String> props) {

		if (keyxml.startsWith("CTe")) {
			keyxml = keyxml.replace("CTe", "");
		}

		String sql = "SELECT o FROM EventData as o where o.evento.infEvento.chCTe = '"
				+ keyxml + "' order by o.evento.infEvento.nSeqEvento desc ";

		Query query = daoevent.createQuery(sql);
		// query.setParameter("tipoevent", tipoevent);

		List result = query.getResultList();

		if (result == null) {
			return 0;
		}

		return result.size();
	}

	public String createEventHeader(String uf) {
		/*
		 * String version = "1.04"; String cabecalho =
		 * "<cteCabecMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcaoEvento\">"
		 * + "<cUF>" + uf + "</cUF>" + "<versaoDados>" + version +
		 * "</versaoDados>" + "</cteCabecMsg>"; return cabecalho;
		 */
		String header = this.factory.createHeader(uf, null,
				SystemProperties.ID_SERVICO_EVENTOS);
		return header;
	}

	public TRetEvento processResult(EventData eventdata, String xml) {
		String endtag = "</retEventoCTe>";
		int pos1 = xml.indexOf("<retEventoCTe");
		int pos2 = xml.indexOf(endtag);

		if (pos1 != -1 && pos2 != -1) {
			xml = xml.substring(pos1, pos2 + endtag.length());
		}

		try {

			JAXBElement<TRetEvento> retenv = (JAXBElement<TRetEvento>) this.genret
					.toObject(xml);
			TRetEvento rett = retenv.getValue();

			String cstat = rett.getInfEvento().getCStat();

			MyLogger.getLog().info("cstat retorno:" + cstat);

			if (rett.getInfEvento().getCStat().equals("135")
					|| rett.getInfEvento().getCStat().equals("136")) {

				eventdata.setStatus(EVENTO_STATUS.AUTORIZADO);
				eventdata.setMensagem(rett.getInfEvento().getXMotivo());
				eventdata.setDataAutorizada(Calendar.getInstance().getTime());
				eventdata.setDataAtualizada(Calendar.getInstance().getTime());

			} else {
				eventdata.setStatus(EVENTO_STATUS.REJEITADO);
				eventdata.setMensagem(rett.getInfEvento().getXMotivo());
				eventdata.setDataAtualizada(Calendar.getInstance().getTime());
			}

			return retenv.getValue();

			/*
			 * } else { eventdata.setStatus(EVENTO_STATUS.ERRO);
			 * eventdata.setMensagem(rett.getInfEvento().getXMotivo());
			 * eventdata.setDataAtualizada(Calendar.getInstance().getTime()); }
			 */

			// return retenv.getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String repareXML(String cnpj, String xml) {

		xml = xml
				.replace(
						"xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"http://www.portalfiscal.inf.br/nfe\"",
						"xmlns=\"http://www.portalfiscal.inf.br/nfe\"");

		xml = xml.replace("ns2:", "");

		xml = xml
				.replace(
						"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>",
						"");

		xml = xml
				.replace("<evento versao=\"1.00\">",
						"<evento versao=\"1.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\">");

		/*
		 * try { FileWriter fw = new FileWriter("C:\\preassinado.xml");
		 * fw.write(xml); fw.close(); } catch (Exception e) {
		 * e.printStackTrace(); }
		 */

		try {

			xml = this.manager.signForCNPJ__Reinf(cnpj, xml,
					SystemProperties.ID_SERVICO_EVENTOS);

			/*
			 * xml = this.signer.signForCNPJ(cnpj, xml,
			 * SystemProperties.ID_SERVICO_DOWNLOAD_NF);
			 */

			xml = xml.replace(
					"xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\"",
					"xmlns=\"http://www.portalfiscal.inf.br/nfe\"");

			xml = xml
					.replace("<evento versao=\"1.00\">",
							"<evento versao=\"1.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\">");

			xml = xml
					.replace("<envEvento versao=\"1.00\">",
							"<envEvento versao=\"1.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\">");

			// XMLValidator v = new
			// XMLValidator("C:\\TTIRec\\PL\\PL\\EventoManifestaDestinat_v100\\envConfRecebto_v1.00.xsd");
			// v.validateXml(xml);

			/*
			 * FileWriter fw = new FileWriter("C:\\postpreassinado.xml");
			 * fw.write(xml); fw.close();
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	private String toXMLString(EventData info) {

		TEvento evento = info.getEvento();
		try {
			String xml = this.gen.toXMLString(evento);
			return xml;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	/*
	 * private String createXML(EventData info) { TEnvEvento envevento =
	 * this.factory.createTEnvEvento(); envevento.setVersao("1.00");
	 * envevento.getEvento().add(info.getEvent());
	 * 
	 * String xmlevento = null;
	 * 
	 * try { xmlevento = this.gen.toXMLString(envevento); } catch (Exception e)
	 * { e.printStackTrace(); }
	 * 
	 * return xmlevento; }
	 */

	/*
	 * @Override public String xmlCCe(String keyxml, int num) throws
	 * RemoteException { String sql =
	 * "SELECT o FROM EventoInfo as o where o.event.infEvento.chNFe = '" +
	 * keyxml.replace("NFe", "") +
	 * "' and o.estado = :e order by o.event.infEvento.nSeqEvento desc ";
	 * 
	 * System.out.println(sql);
	 * 
	 * Query query = this.daoEvent.createQuery(sql); query.setParameter("e",
	 * XML_STATE.AUTORIZADA);
	 * 
	 * List<EventData> result = query.getResultList(); if (result.size() > 0) {
	 * return result.get(0).getXmlString(); } return null; }
	 */

	public static void main2(String[] args) {
		SimpleDateFormat sdf1 = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:00'-03:00'");
		long currentlonf = Calendar.getInstance().getTimeInMillis()
				- (60 * 60 * 1000L);
		Date currentdata = new Date(currentlonf);
		System.out.println(sdf1.format(currentdata));
	}

	private void notify(EventData data) {
		for (EventManagerListener listener : this.listeners) {
			try {
				listener.process(data);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
	}

	@Override
	public void addListener(EventManagerListener listener) {
		// ttirec manager add listener
		this.listeners.add(listener);
	}

	private String toString(org.w3c.dom.Element element) throws Exception {
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		StringWriter buffer = new StringWriter();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(new DOMSource(element), new StreamResult(buffer));
		String str = buffer.toString();
		return str;
	}

	public Element createCancel(String protocolo, String justificativa)
			throws Exception {

		EvCancCTe evcanc = new EvCancCTe();

		evcanc.setDescEvento(TIPO_EVENTO.CANCELAMENTO.getMensagem());
		evcanc.setNProt(protocolo);
		evcanc.setXJust(justificativa);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document elemrodo = builder.newDocument();

		JAXBContext ctxt1 = JAXBContext
				.newInstance("br.com.tti.sefaz.xml.evento.cancel");
		Marshaller mars1 = ctxt1.createMarshaller();
		mars1.marshal(evcanc, elemrodo);
		return elemrodo.getDocumentElement();

	}

	public Element createCCe(Vector<Hashtable<String, String>> infcorrec)
			throws Exception {
		EvCCeCTe evcce = new EvCCeCTe();
		evcce.setDescEvento(TIPO_EVENTO.CCE.getMensagem());

		for (Hashtable<String, String> entry : infcorrec) {
			InfCorrecao corr = new InfCorrecao();
			corr.setCampoAlterado(entry.get("campo"));
			corr.setGrupoAlterado(entry.get("grupo"));
			corr.setValorAlterado(entry.get("valor"));
			corr.setNroItemAlterado(entry.containsKey("nro") ? entry.get("nro")
					: null);

			evcce.getInfCorrecao().add(corr);
		}

		evcce.setXCondUso("A Carta de Correcao e disciplinada pelo Art. 58-B do CONVENIO/SINIEF 06/89: Fica permitida a utilizacao de carta de correcao, para regularizacao de erro ocorrido na emissao de documentos fiscais relativos a prestacao de servico de transporte, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da prestacao;II - a correcao de dados cadastrais que implique mudanca do emitente, tomador, remetente ou do destinatario;III - a data de emissao ou de saida.");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document elemrodo = builder.newDocument();

		JAXBContext ctxt1 = JAXBContext
				.newInstance("br.com.tti.sefaz.xml.evento.cce");
		Marshaller mars1 = ctxt1.createMarshaller();
		mars1.marshal(evcce, elemrodo);
		return elemrodo.getDocumentElement();
	}

	@Override
	public TRetEvento sendEvent(String keyxml, TIPO_EVENTO tipoevento,
			String datahora, String cnpjclient, Hashtable<String, Object> params)
			throws Exception {

		KeyXml key = new KeyXml(keyxml);

		String schemaversion = this.factory.getSchemaVersion(key.getUF(),
				SystemProperties.ID_SERVICO_EVENTOS);

		XMLDaoJPA<EventData> daoEvent = (XMLDaoJPA<EventData>) DaoFactory
				.createDAO(EventData.class);

		XMLDaoJPA<XMLData> daoXml = (XMLDaoJPA<XMLData>) DaoFactory
				.createDAO(XMLData.class);

		EventData eventdata = new EventData();

		TEvento event = new TEvento();
		InfEvento infevent = new InfEvento();
		DetEvento detevent = new DetEvento();

		infevent.setDetEvento(detevent);
		infevent.setChCTe(keyxml.replace("CTe", ""));
		infevent.setCNPJ(cnpjclient);
		infevent.setCOrgao(key.getUF());
		infevent.setTpEvento(tipoevento.getCodigo());
		// infevent.setVerEvento(this.bundle.getString("versaoevento"));
		infevent.setDhEvento(datahora);

		detevent.setVersaoEvento(schemaversion);

		if (TIPO_EVENTO.CANCELAMENTO.equals(tipoevento)) {
			String protocolo = (params.containsKey("protocolo") ? params.get(
					"protocolo").toString() : "NAO INFORMADO");
			String justificativa = (params.containsKey("xjust") ? params.get(
					"xjust").toString() : "NAO INFORMADO");
			Element evcancel = this.createCancel(protocolo, justificativa);
			detevent.setAny(evcancel);
			detevent.setAnyxml(this.toString(evcancel));
		}

		if (TIPO_EVENTO.CCE.equals(tipoevento)) {
			Vector<Hashtable<String, String>> infcorrec = (Vector<Hashtable<String, String>>) params
					.get("infcorrec");
			Element evcce = this.createCCe(infcorrec);
			detevent.setAny(evcce);
			detevent.setAnyxml(this.toString(evcce));
		}

		event.setInfEvento(infevent);
		eventdata.setEvento(event);
		eventdata.setTipoEvento(tipoevento);

		Hashtable<String, String> props = new Hashtable<String, String>();
		props.put("UF", key.getUF());
		props.put("AMBIENT", params.get("ambient").toString());
		props.put("CNPJ", key.getCnpj());

		// /////////////////////////////////////////////////////////////////////////////////
		// ////////////////return this.processEventData(keyxml, tipoevent,
		// eventdata, props);
		// ///////////////////////////////////////////////////////////////////////////////

		List<EventData> oldevents = this.obterEventosSequence(daoEvent, keyxml,
				tipoevento.getCodigo(), null);

		int nseqn = 1;
		if (oldevents != null && oldevents.size() >= 1) {
			EventData lastevent = oldevents.get(0);

			String lastnumberb = lastevent.getEvento().getInfEvento()
					.getNSeqEvento();
			if (lastnumberb != null) {
				nseqn = Integer.parseInt(lastnumberb);
				nseqn = nseqn + 1;
			}
		}

		int quantity = 1;
		try {
			quantity = this.obterNumberEventos(daoEvent, keyxml, props);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		// //////////////////////////////////////////////////////////

		EventId idevent = new EventId();
		idevent.setKeyxml(keyxml);
		idevent.setNseq(quantity);
		eventdata.setId(idevent);

		eventdata.setStatus(EVENTO_STATUS.GERADO);
		eventdata.setDataGerada(Calendar.getInstance().getTime());

		eventdata.getEvento().getInfEvento().setNSeqEvento(nseqn + "");
		eventdata.getEvento().setVersao(schemaversion);

		String tipoamb = params.containsKey("ambient") ? (params.get("ambient")
				.toString().equals(SystemProperties.AMBIENT_HOMOLOGACAO) ? "2"
				: "1") : "NAO INFORMADO";

		eventdata.getEvento().getInfEvento().setTpAmb(tipoamb);

		String id = eventdata.getEvento().getInfEvento().getTpEvento()
				+ eventdata.getEvento().getInfEvento().getChCTe() + "0"
				+ eventdata.getEvento().getInfEvento().getNSeqEvento();
		eventdata.getEvento().setVersao(schemaversion);
		eventdata.getEvento().getInfEvento().setId("ID" + id);

		String xml = null;

		try {
			daoEvent.saveEntity(eventdata);
			daoEvent.flush();

			xml = this.toXMLString(eventdata);
			xml = this.repareXML(
					eventdata.getEvento().getInfEvento().getCNPJ(), xml);

			FileWriter fw = new FileWriter("ultimo_evento.xml");
			fw.write(xml);
			fw.close();

			eventdata.setXmlString(xml);

			XMLValidator val = new XMLValidator(MainParameters.getXSDEvento());
			String messageevent = val.validateMessage(xml);

			String messageinc = null;
			/*
			 * if (TIPO_EVENTO.CCE.equals(tipoevento)) { String anyxml =
			 * event.getInfEvento().getDetEvento().getAnyxml(); XMLValidator
			 * val2 = new XMLValidator(MainParameters.getXSDCCe()); messageinc =
			 * val2.validateMessage(anyxml); }
			 */

			if (TIPO_EVENTO.CANCELAMENTO.equals(tipoevento)) {
				String anyxml = event.getInfEvento().getDetEvento().getAnyxml();
				XMLValidator val2 = new XMLValidator(
						MainParameters.getXSDCancel());
				messageinc = val2.validateMessage(anyxml);
			}

			if (messageevent != null) {
				eventdata.setMensagem(messageevent);
				throw new Exception(messageevent);
			}

			if (messageinc != null) {
				eventdata.setMensagem(messageinc);
				throw new Exception(messageinc);
			}

		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			throw new RemoteException(e.getLocalizedMessage(),
					e.fillInStackTrace());
		}

		String resultxml = null;
		try {
			String header = this.createEventHeader(eventdata.getEvento()
					.getInfEvento().getCOrgao());

			resultxml = this.manager
					.sendXMLMessage(
							SystemProperties.ID_SERVICO_EVENTOS,
							header,
							"<cteDadosMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcaoEvento\">"
									+ xml + "</cteDadosMsg>", props);

			eventdata.setStatus(EVENTO_STATUS.ENVIADO);
			eventdata.setDataEnviada(Calendar.getInstance().getTime());

			this.notify(eventdata);

			daoEvent.updateEntity(eventdata);
			daoEvent.flush();

		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			throw new RemoteException(e.getLocalizedMessage(),
					e.fillInStackTrace());
		}

		if (resultxml != null) {
			// eventdata.setEstado(XML_STATE.AUTORIZADA);
			// eventdata.setDataAtualizada(Calendar.getInstance().getTime());

			TRetEvento ret = this.processResult(eventdata, resultxml);

			String endtag = "</retEventoCTe>";
			int pos1 = resultxml.indexOf("<retEventoCTe");
			int pos2 = resultxml.indexOf(endtag);

			if (pos1 != -1 && pos2 != -1) {
				resultxml = resultxml.substring(pos1, pos2 + endtag.length());
			}

			eventdata.setProtocoloXML(resultxml);

			if (ret != null) {
				eventdata.setRetevento(ret);

				MyLogger.getLog().info("Event Status:" + eventdata.getStatus());
				if (!keyxml.startsWith("CTe")) {
					keyxml = "CTe" + keyxml;
				}

				XMLData xmldata = daoXml.findEntity(keyxml);
				if (TIPO_EVENTO.CCE.equals(tipoevento)) {
					xmldata.setEvCCe(eventdata.getStatus());

					// FIXME: xml do CCE salvo no inut para consulta rapida
					xmldata.setInutProtocol(xml + resultxml);

					if (EVENTO_STATUS.AUTORIZADO.equals(eventdata.getStatus())) {
						MyLogger.getLog().info("gerando XML do CCe");
						this.factory.createCCeProt(keyxml, null);

						// aqui
					}
				}

				if (TIPO_EVENTO.CANCELAMENTO.equals(tipoevento)) {
					xmldata.setEvCancel(eventdata.getStatus());
					xmldata.setCancelProtocol(resultxml);

					if (EVENTO_STATUS.AUTORIZADO.equals(eventdata.getStatus())) {
						xmldata.setNCancelProtocol((ret.getInfEvento()
								.getNProt() != null ? ret.getInfEvento()
								.getNProt() : "SEM PROTOCOLO CANCELAMENTO"));
					}
				}

				if (TIPO_EVENTO.REG_MULTI.equals(tipoevento)) {
					xmldata.setEvMultimodal(eventdata.getStatus());

				}

				daoXml.updateEntity(xmldata);
				daoXml.flush();

			} else {
				TRetEvento retret = new TRetEvento();
				TRetEvento.InfEvento infret = new TRetEvento.InfEvento();
				infret.setCStat("901");
				infret.setXMotivo("Erro nao catalogado no TTICTe");
				infret.setDhRegEvento(Calendar.getInstance().getTime()
						.toGMTString());
				retret.setInfEvento(infret);
				eventdata.setRetevento(retret);
			}

			this.notify(eventdata);

			daoEvent.updateEntity(eventdata);
			daoEvent.flush();

			ret.setEventdata(eventdata);

			return ret;
		} else {
			throw new Exception("Evento sem retorno da sefaz");
		}

	}

	@Override
	public List<EventData> recoverEventData(String keyxml,
			TIPO_EVENTO tipoevento, Date data, Hashtable<String, Object> props) {

		MyLogger.getLog().info("SQL keyxml:" + keyxml);

		Query query = null;

		if (tipoevento == null && data == null && props == null) {
			query = this.daoEvent
					.createQuery("select e from EventData as e where e.id.keyxml = :key order by e.dataGerada desc");
			query.setParameter("key", keyxml);
		} /*
		 * else if (data == null) { query = this.daoEvent .createQuery(
		 * "select e from EventData as e where e.id.keyxml = :key and e.tipoEvento = :tipo order by e.dataGerada desc"
		 * ); query.setParameter("key", keyxml); query.setParameter("tipo",
		 * tipoevento); }
		 */
		else if (props != null && props.containsKey("estado")) {
			query = this.daoEvent
					.createQuery("select e from EventData as e where e.id.keyxml = :key and e.status = :estado order by e.dataGerada desc");
			query.setParameter("key", keyxml);
			query.setParameter("estado", props.get("estado"));
		} else {
			query = this.daoEvent
					.createQuery("select e from EventData as e where e.id.keyxml = :key and e.tipoEvento = :tipo and e.dataGerada >= :data order by e.dataGerada desc");
			query.setParameter("key", keyxml);
			query.setParameter("tipo", tipoevento);
			query.setParameter("data", data);
		}

		MyLogger.getLog().info("SQL:" + query.toString());

		List res = query.getResultList();

		MyLogger.getLog().info("SQL result size:" + res.size());

		return res;
	}

	/*
	 * // @Override public TRetEvento processEventData(String keyxml, String
	 * tipoevent, EventData eventdata, Hashtable<String, String> props) throws
	 * RemoteException {
	 * 
	 * List<EventData> oldevents = this.obterEventosSequence(this.daoEvent,
	 * keyxml, tipoevent, null);
	 * 
	 * int nseqn = 1; if (oldevents != null && oldevents.size() >= 1) {
	 * EventData lastevent = oldevents.get(0);
	 * 
	 * String lastnumberb = lastevent.getEvento().getInfEvento()
	 * .getNSeqEvento(); if (lastnumberb != null) { nseqn =
	 * Integer.parseInt(lastnumberb); nseqn = nseqn + 1; } }
	 * 
	 * int quantity = 1; try { quantity = this.obterNumberEventos(this.daoEvent,
	 * keyxml, props); } catch (Exception e) { MyLogger.getLog().log(Level.INFO,
	 * e.getLocalizedMessage(), e); }
	 * 
	 * // //////////////////////////////////////////////////////////
	 * 
	 * EventId idevent = new EventId(); idevent.setKeyxml(keyxml);
	 * idevent.setNseq(quantity); eventdata.setId(idevent);
	 * 
	 * eventdata.setStatus(EVENTO_STATUS.GERADO);
	 * eventdata.setDataGerada(Calendar.getInstance().getTime());
	 * 
	 * eventdata.getEvento().getInfEvento().setNSeqEvento(nseqn + "");
	 * eventdata.getEvento().setVersao("1.00");
	 * 
	 * String tipoamb = "1";
	 * eventdata.getEvento().getInfEvento().setTpAmb(tipoamb);
	 * 
	 * String id = eventdata.getEvento().getInfEvento().getTpEvento() +
	 * eventdata.getEvento().getInfEvento().getChCTe() + "0" +
	 * eventdata.getEvento().getInfEvento().getNSeqEvento();
	 * eventdata.getEvento().setVersao("1.00");
	 * eventdata.getEvento().getInfEvento().setId("ID" + id); String xml = null;
	 * try { this.daoEvent.saveEntity(eventdata); xml =
	 * this.toXMLString(eventdata); xml = this.repareXML(
	 * eventdata.getEvento().getInfEvento().getCNPJ(), xml);
	 * 
	 * 
	 * XMLValidator validator = new XMLValidator(
	 * "C:\\TTINFE2.0\\documentos\\EventoManifestaDestinat_v100\\envConfRecebto_v1.00.xsd"
	 * ); validator.validateXml(xml);
	 * 
	 * 
	 * FileWriter fw = new FileWriter("ultimo_evento.xml"); fw.write(xml);
	 * fw.close();
	 * 
	 * // this.notify(eventdata);
	 * 
	 * } catch (Exception e) { MyLogger.getLog().log(Level.INFO,
	 * e.getLocalizedMessage(), e); throw new
	 * RemoteException(e.getLocalizedMessage(), e.fillInStackTrace()); }
	 * 
	 * String resultxml = null; String header =
	 * this.createEventHeader(eventdata.getEvento()
	 * .getInfEvento().getCOrgao()); try { resultxml = this.manager
	 * .sendXMLMessage( SystemProperties.ID_SERVICO_EVENTOS, header,
	 * "<nfeDadosMsg xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento\">"
	 * + xml + "</nfeDadosMsg>", props);
	 * 
	 * MyLogger.getLog().info(resultxml);
	 * 
	 * eventdata.setStatus(EVENTO_STATUS.ENVIADO);
	 * eventdata.setDataEnviada(Calendar.getInstance().getTime());
	 * 
	 * this.daoEvent.updateEntity(eventdata); this.daoEvent.flush();
	 * 
	 * this.notify(eventdata);
	 * 
	 * } catch (Exception e) { MyLogger.getLog().log(Level.INFO,
	 * e.getLocalizedMessage(), e); throw new
	 * RemoteException(e.getLocalizedMessage(), e.fillInStackTrace()); }
	 * 
	 * if (resultxml != null) { // eventdata.setEstado(XML_STATE.AUTORIZADA); //
	 * eventdata.setDataAtualizada(Calendar.getInstance().getTime());
	 * 
	 * TRetEvento ret = this.processResult(eventdata, resultxml);
	 * 
	 * String endtag = "</retEnvEvento>"; int pos1 =
	 * resultxml.indexOf("<retEnvEvento"); int pos2 = resultxml.indexOf(endtag);
	 * 
	 * if (pos1 != -1 && pos2 != -1) { resultxml = resultxml.substring(pos1,
	 * pos2 + endtag.length()); }
	 * 
	 * eventdata.setProtocoloXML(resultxml); eventdata.setRetevento(ret);
	 * 
	 * this.daoEvent.updateEntity(eventdata); this.daoEvent.flush();
	 * 
	 * this.notify(eventdata);
	 * 
	 * return ret; } else { throw new
	 * RemoteException("Evento sem retorno da sefaz"); }
	 * 
	 * }
	 */
}
