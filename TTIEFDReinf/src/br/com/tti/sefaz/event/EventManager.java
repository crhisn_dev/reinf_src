package br.com.tti.sefaz.event;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import br.com.tti.sefaz.xml.evento.TRetEvento;
import br.com.tti.sefaz.persistence.EventData;
import br.com.tti.sefaz.persistence.EventData.TIPO_EVENTO;
import br.com.tti.sefaz.persistence.dao.DAO;

public interface EventManager extends Remote {

	public List<EventData> obterEventos(String keyxml,
			Hashtable<String, String> props) throws RemoteException;

	/*
	 * public String adicionarEvento(String keyxml, EventData info,
	 * Hashtable<String, String> props) throws Exception;
	 */

	public TRetEvento sendEvent(String keyxml, TIPO_EVENTO evento,
			String datahora, String cnpjclient, Hashtable<String, Object> params)
			throws Exception;

	/*
	 * public TRetEvento processEventData(String keyxml, String tipoevent,
	 * EventData eventdata, Hashtable<String, String> props) throws Exception;
	 */

	public void addListener(EventManagerListener listener) throws Exception;

	public List<EventData> recoverEventData(String keyxml,
			TIPO_EVENTO tipoevento, Date data, Hashtable<String, Object> props)
			throws Exception;

	public List<EventData> obterEventosSequence(DAO<EventData> daoevent,
			String keyxml, String codetipoevent, Hashtable<String, String> props)
			throws Exception;

}
