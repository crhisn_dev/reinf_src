package br.com.tti.sefaz.util;

import java.io.DataInputStream;
import java.io.FileInputStream;

public class ReadFile {

	public static String readFile(String path) throws Exception {
		FileInputStream f = new FileInputStream(path);

		DataInputStream dis = new DataInputStream(f);
		String line = dis.readLine();
		String all = new String();

		while (line != null) {
			all += line;
			line = dis.readLine();
		}

		f.close();
		return all;
	}
}
