package br.com.tti.sefaz.util;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LocatorNotifier extends Remote {

	public void notify(String message, Exception e) throws RemoteException;

}
