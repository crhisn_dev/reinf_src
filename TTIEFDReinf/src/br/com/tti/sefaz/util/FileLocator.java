package br.com.tti.sefaz.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.CNPJData;

public class FileLocator {

	private static FileLocator INSTANCE = null;

	public static FileLocator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new FileLocator();
		}
		return INSTANCE;
	}

	private ResourceBundle rb;
	private SimpleDateFormat sdf;

	public FileLocator() {
		this.rb = ResourceBundle.getBundle("pastas_cnpj");
		if (MainParameters.getCaminhoarquivos() != null) {
			this.sdf = new SimpleDateFormat(MainParameters.getCaminhoarquivos());
		}

		try {
			ManagerInterface manager = Locator.getManagerReference();
			Hashtable<String, CNPJData> cnpjs = manager.getCNPJ();
			for (String cnpj : cnpjs.keySet()) {
				if (!this.rb.containsKey(cnpj)) {
					MyLogger.getLog().info("CNPJ nao cadastrado no arquivo pastas_cnpj:" + cnpj);
					System.exit(1011);
				}
			}
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}

	public void setSDF(String path) {
		this.sdf = new SimpleDateFormat(path);
	}

	public String findPDFDir(String cnpj, Date date, Hashtable<String, Object> props) {
		String path_dir = this.rb.getString(cnpj);
		File f = new File(path_dir + (this.sdf != null ? File.separator + "PDF" + File.separator + this.sdf.format(date)
				: File.separator));
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	public String findPSDir(String cnpj, Date date, Hashtable<String, Object> props) {
		String path_dir = this.rb.getString(cnpj);
		File f = new File(path_dir
				+ (this.sdf != null ? File.separator + "PS" + File.separator + this.sdf.format(date) : File.separator));
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	public String findXMLDir(String cnpj, Date date, Hashtable<String, Object> props) {
		String path_dir = this.rb.getString(cnpj);
		File f = new File(path_dir + (this.sdf != null ? File.separator + "XML" + File.separator + this.sdf.format(date)
				: File.separator));
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	public String findXMLCancelDir(String cnpj, Date date, Hashtable<String, Object> props) {
		String path_dir = this.rb.getString(cnpj);
		File f = new File(path_dir + (this.sdf != null
				? File.separator + "CANCELAMENTO" + File.separator + this.sdf.format(date) : File.separator));
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	public String findXMLCCeDir(String cnpj, Date date, Hashtable<String, Object> props) {
		String path_dir = this.rb.getString(cnpj);
		File f = new File(path_dir + (this.sdf != null ? File.separator + "CCE" + File.separator + this.sdf.format(date)
				: File.separator));
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	public String findXML(String cnpj, Date date, String keyxml) {
		return this.findXMLDir(cnpj, date, null) + File.separator + keyxml + ".xml";
	}

	public String findXMLProt(String cnpj, Date date, String keyxml) {
		return this.findXMLDir(cnpj, date, null) + File.separator + keyxml + "_prot.xml";
	}

	public String findXMLCancelProt(String cnpj, Date date, String keyxml) {
		return this.findXMLCancelDir(cnpj, date, null) + File.separator + keyxml + "-procCancCTe.xml";
	}

	public String findXMLCCeProt(String cnpj, Date date, String keyxml) {
		return this.findXMLCCeDir(cnpj, date, null) + File.separator + keyxml + "-procCCeCTe.xml";
	}

	public String findPDF(String cnpj, Date date, String keyxml) {
		return this.findPDFDir(cnpj, date, null) + File.separator + keyxml + ".pdf";
	}

	public String findPS(String cnpj, Date date, String keyxml) {
		return this.findPSDir(cnpj, date, null) + File.separator + keyxml + ".ps";
	}

	public static void main(String[] args) {
		MainParameters.processArguments(args);
		System.out.println(MainParameters.getCaminhoarquivos());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyy" + File.separator + "MMM" + File.separator + "dd_MMM");
		System.out.println(sdf.format(Calendar.getInstance().getTime()) + File.separator);

		System.out
				.println(FileLocator.getInstance().findXML("50935436000140", Calendar.getInstance().getTime(), "123"));

	}

}
