package br.com.tti.sefaz.util;

import java.util.Random;

import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;

public class KeyXml {

	private String uf;
	private String cnpj;
	private String numeroNota;
	private String keyxml;
	private String serie;
	private String ddmm;
	private String modelo;
	private String tpEmiss;
	private String codigo;
	private String dv;
	private String[] componentes;
	private static Random r = new Random();

	public KeyXml(String keyxml) {
		this.keyxml = keyxml.replace("NFe", "").replace("CTe", "");
		String[] partes = this.descomporChaveNFe(this.keyxml);

		this.uf = partes[0];
		this.ddmm = partes[1];
		this.cnpj = partes[2];
		this.modelo = partes[3];
		this.serie = partes[4];
		this.numeroNota = partes[5];
		this.tpEmiss = partes[6];// .toString().substring(0, 1);
		this.codigo = partes[7];
		this.dv = partes[8];
	}

	public String reCreateKeyXml() {
		return this.uf + this.ddmm + this.cnpj + this.modelo + this.serie
				+ this.numeroNota + this.tpEmiss + this.codigo + this.dv;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public String getDdmm() {
		return ddmm;
	}

	public void setDdmm(String ddmm) {
		this.ddmm = ddmm;
	}

	public String getUF() {
		return this.uf;
	}

	public void setUF(String uf) {
		this.uf = uf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNumeroNota() {
		return numeroNota;
	}

	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}

	public String getChave() {
		return keyxml;
	}

	public void setChave(String chave) {
		this.keyxml = chave;
	}

	// UF, AAMM, CNPJ Emit, modelo, serie, numero nfe, codigo numerico, DV
	public String[] descomporChaveNFe(String keyxml) {
		keyxml = keyxml.replace("NFe", "").replace("CTe", "");

		this.componentes = new String[9];

		this.componentes[0] = keyxml.substring(0, 2);
		this.componentes[1] = keyxml.substring(2, 6);// .substring(0,2);
		this.componentes[2] = keyxml.substring(6, 20);
		this.componentes[3] = keyxml.substring(20, 22);
		this.componentes[4] = keyxml.substring(22, 25);
		this.componentes[5] = keyxml.substring(25, 34);
		this.componentes[6] = keyxml.substring(34, 35);
		this.componentes[7] = keyxml.substring(35, 43);
		this.componentes[8] = keyxml.substring(43, 44);

		return componentes;
	}

	public String[] getComponentes() {
		return componentes;
	}

	public static String generateString(int n) {
		String str = new String();
		for (int i = 0; i < n; i++) {
			str += r.nextInt(10);
		}
		return str;
	}

	public String withTpEmissWithCTe(TPEMISS tpemiss) {
		this.tpEmiss = tpemiss.ordinal() + "";
		this.reCalculateDV();
		return "CTe" + this.reCreateKeyXml();
	}

	public void reCalculateDV() {
		String rawnfe = this.uf + this.ddmm + this.cnpj + this.modelo
				+ this.serie + this.numeroNota + this.tpEmiss + this.codigo;
		this.dv = this.calcularDV(rawnfe);
	}

	private String calcularDV(String chave) {
		int counter = 2;
		int accumulated = 0;
		for (int i = chave.length() - 1; i >= 0; i--) {
			int digito = Integer.parseInt(chave.substring(i, i + 1));
			accumulated += (counter * digito);
			if (counter > 8) {
				counter = 2;
			} else {
				counter++;
			}
		}

		int resto = accumulated - (((int) (accumulated / 11)) * 11);
		if (resto == 0 || resto == 1) {
			return "0";
		} else {
			return (11 - resto) + "";
		}

	}

	public TPEMISS getTpEmis() {
		return TPEMISS.values()[Integer.parseInt(this.tpEmiss)];
	}

	public static void main(String[] args) {

		String nn = "35130709338123000101550020003439271000000018";
		KeyXml key = new KeyXml(nn);

		for (String comp : key.getComponentes()) {
			System.out.println("->" + comp);
		}

		System.out.println(key.getTpEmiss());

		key.setTpEmiss(TPEMISS.CONTINGENCE_EPEC.ordinal() + "");
		key.setSerie("002");
		key.reCalculateDV();

		System.out.println(nn);
		System.out.println(key.withTpEmissWithCTe(TPEMISS.CONTINGENCE_EPEC));
		System.out.println(key.withTpEmissWithCTe(TPEMISS.CONTINGENCE));
		System.out.println(key.withTpEmissWithCTe(TPEMISS.NORMAL));
		System.out.println(key.withTpEmissWithCTe(TPEMISS.SVC_RS));
		System.out.println(key.withTpEmissWithCTe(TPEMISS.SVC_SP));

	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getTpEmiss() {
		return tpEmiss;
	}

	public void setTpEmiss(String tpEmiss) {
		this.tpEmiss = tpEmiss;
	}

}