package br.com.tti.sefaz.util;

import java.util.Hashtable;

import br.com.tti.sefaz.persistence.ModeOperationData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;

public class ModoOpManager {
	private DAO<ModeOperationData> daoModo;

	public ModoOpManager() {
		this.daoModo = DaoFactory.createDAO(ModeOperationData.class);
	}

	public SystemProperties.TPEMISS getModo(String cnpj) {
		ModeOperationData modo = this.daoModo.findEntity(cnpj);
		if (modo != null)
			return modo.getModo();
		return null;
	}

	public Hashtable<String, String> getXJustDHCont(String cnpj) {
		ModeOperationData modoD = this.daoModo.findEntity(cnpj);
		Hashtable<String, String> modos = new Hashtable<String, String>();
		if (modoD != null) {
			modos.put("xjust", modoD.getXjust() != null ? modoD.getXjust() : "");
			modos.put("dhcont", modoD.getDhcont() != null ? modoD.getDhcont()
					: "");
		}
		return modos;
	}

	public void saveModo(String cnpj, TPEMISS modo, String xjust, String dhcont) {

		ModeOperationData modoD = this.daoModo.findEntity(cnpj);
		if (modoD != null) {
			modoD.setCnpj(cnpj);
			modoD.setModo(modo);
			modoD.setXjust(xjust);
			modoD.setDhcont(dhcont);
			this.daoModo.updateEntity(modoD);
		} else {
			modoD = new ModeOperationData();
			modoD.setCnpj(cnpj);
			modoD.setModo(modo);
			modoD.setXjust(xjust);
			modoD.setDhcont(dhcont);
			this.daoModo.saveEntity(modoD);
		}

	}

}
