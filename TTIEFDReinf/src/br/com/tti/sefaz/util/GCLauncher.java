package br.com.tti.sefaz.util;

import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;

public class GCLauncher implements Runnable {

	@Override
	public void run() {
		MyLogger.getLog().info("GC launched");

		while (true) {
			try {
				MyLogger.getLog().info("GC starting...");
				System.gc();
				MyLogger.getLog().info("GC finished.");
				Thread.sleep(10 * 60 * 1000L); // 10min
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
	}

	public static void start() {
		Thread t = new Thread(new GCLauncher());
		t.start();
	}

}
