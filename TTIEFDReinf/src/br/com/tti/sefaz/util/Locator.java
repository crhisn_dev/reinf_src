package br.com.tti.sefaz.util;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;

import br.com.tti.sefaz.externaldbaccess.ExternalDBAccess;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.events.TTIEvent;

public class Locator {
	public static String MANGER_REMOTE_NAME = "ttimanager";
	public static String ACCESS_REMOTE_NAME = "ttiaccess";

	private static ManagerInterface manager = null;
	private static ExternalDBAccess external = null;

	private static int TENTATIVES = 3;

	private static LocatorNotifier notifier;

	public static LocatorNotifier getNotifier() {
		return notifier;
	}

	public static void setNotifier(LocatorNotifier notifier) {
		Locator.notifier = notifier;
	}

	public static ExternalDBAccess getExternalAccess() {
		if (external == null) {
			try {
				Registry registry = LocateRegistry.getRegistry();
				external = (ExternalDBAccess) registry
						.lookup(ACCESS_REMOTE_NAME);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return external;
	}

	public static ExternalDBAccess getExternalAccess(String ip) {
		if (external == null) {
			try {
				Registry registry = LocateRegistry.getRegistry(ip);
				external = (ExternalDBAccess) registry
						.lookup(ACCESS_REMOTE_NAME);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return external;
	}

	private static void reCreate(String ip) {
		for (int i = 0; i < TENTATIVES; i++) {
			try {
				Thread.sleep(2 * 1000L);
				Registry registry = LocateRegistry.getRegistry(ip);
				manager = (ManagerInterface) registry
						.lookup(MANGER_REMOTE_NAME);
				if (manager != null)
					return;
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static ManagerInterface getManagerReference() {
		if (manager == null) {
			try {
				Registry registry = LocateRegistry.getRegistry();
				manager = (ManagerInterface) registry
						.lookup(MANGER_REMOTE_NAME);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				notify(e.getLocalizedMessage(), e);
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				notify(e.getLocalizedMessage(), e);
			}
		}

		try {
			manager.isActive();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			reCreate("localhost");
		}

		return manager;
	}

	private static void notify(String message, Exception exception) {
		if (notifier != null) {
			try {
				notifier.notify(message, exception);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static ManagerInterface getManagerReference(String ip) {
		if (manager == null) {
			try {
				Registry registry = LocateRegistry.getRegistry(ip);
				manager = (ManagerInterface) registry
						.lookup(MANGER_REMOTE_NAME);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				notify(e.getLocalizedMessage(), e);

			}
		}

		try {
			manager.isActive();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			notify(e.getLocalizedMessage(), e);
			reCreate(ip);
		}

		return manager;
	}

	public static void setManager(ManagerInterface manager) {
		Locator.manager = manager;
	}

	public static void setExternal(ExternalDBAccess external) {
		Locator.external = external;
	}

}
