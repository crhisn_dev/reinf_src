package br.com.tti.sefaz.cancelinut;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.logging.Level;

import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.contingence.ModeOperation;
import br.com.tti.sefaz.exceptions.MySenderException;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.Locator;

public class CancelInut implements CancelInutInterface, ModeOperation {

	private ManagerInterface manager;

	private CancelInutController controller;

	private DAO<XMLData> daodata;

	public CancelInut() {
		this.manager = Locator.getManagerReference();
		assert this.manager != null;
		this.controller = new CancelInutController(this.manager);
		this.daodata = DaoFactory.createDAO(XMLData.class);
	}

	@Override
	synchronized public String cancelXml(String cnpj, String uf,
			String ambient, String keyXml, String protocol, String just,
			boolean async, Hashtable<String, String> props)
			throws RemoteException {

		CNPJData cnpjdata = this.manager.getCNPJ().get(cnpj);
		if (cnpjdata == null) {
			throw new RemoteException("CNPJ n�o cadastrado!");
		}

		ambient = cnpjdata.getAmbiente();

		MyLogger.getLog().info("CNPJData cancel ambient:" + ambient);

		if (!keyXml.startsWith("CTe")) {
			keyXml = "CTe" + keyXml;
		}

		int iscancel = this.controller.isCancel(cnpj, uf, ambient, keyXml,
				protocol, just, async);

		try {
			just = new String(just.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			MyLogger.getLog().log(Level.INFO, e1.getLocalizedMessage(), e1);
		}

		try {
			String id_pk = null;
			if (props != null) {
				MyLogger.getLog().info("PROPS:" + props.toString());

				id_pk = props.get("ID_PK");

				if (id_pk != null && !id_pk.isEmpty()) {
					id_pk = id_pk.trim();
					XMLData xmld = XMLDataCache.getInstance().findData(keyXml);
					if (xmld != null) {
						// xmld.setDescription(id_pk);
						xmld.setId_pk_evento(Integer.parseInt(id_pk));
						XMLDataCache.getInstance().updateXMLData(xmld);
					}
				}
			}
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		this.daodata.clean();

		XMLData xmldata = this.daodata.findEntity(keyXml);

		MyLogger.getLog().info("iscancel:" + iscancel);
		MyLogger.getLog().info("protocol:" + xmldata.getNAutorizedProtocol());

		if (iscancel == 1) {
			try {
				return this.controller.cancelXml(cnpj, uf, ambient, keyXml,
						xmldata.getNAutorizedProtocol(), just, async, props);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.SEVERE, e.getLocalizedMessage(), e);
				throw new MySenderException(e, e.getLocalizedMessage());
			}
		} else if (iscancel == 0) {
			return this.inutXml(cnpj, uf, ambient, keyXml, just, async);
		}

		return "CTe ja cancelada";
	}

	@Override
	public String inutXml(String cnpj, String uf, String ambient,
			String keyXml, String justl, boolean async) throws RemoteException {

		CNPJData cnpjdata = this.manager.getCNPJ().get(cnpj);
		if (cnpjdata == null) {
			throw new RemoteException("CNPJ n�o cadastrado!");
		}

		ambient = cnpjdata.getAmbiente();

		MyLogger.getLog().info("CNPJData cancel ambient:" + ambient);

		try {
			return this.controller.inutXml(cnpj, uf, ambient, keyXml, justl,
					async);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new MySenderException(e, e.getLocalizedMessage());
		}
	}

	@Override
	public String inutXml(String nOp, String uf, String ambient, String ano,
			String cnpj, String mod, String serie, String ini, String fim,
			String just, boolean async) throws RemoteException {
		try {
			return this.controller.inutXml(nOp, uf, ambient, ano, cnpj, mod,
					serie, ini, fim, just, async);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new MySenderException(e, e.getLocalizedMessage());
		}
	}

	@Override
	public void changeToState(String cnpj, String ambient, TPEMISS modo,
			Hashtable<String, Object> props) throws RemoteException {
		this.controller.changeToModo(cnpj, modo, props.get("xjust").toString(),
				props.get("dhcont").toString());
	}

	@Override
	public TPEMISS getModo(String cnpj, String ambient) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, String> getJust(String cnpj, String ambient,
			TPEMISS tpemiss) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
