package br.com.tti.sefaz.cancelinut;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.logging.Level;

import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.ModoOpManager;

public class CancelInutController {

	private ManagerInterface manager;
	private CancelInutThreadFactory factory;
	private Hashtable<String, TPEMISS> modos;
	private XMLDataCache cacheXml;

	private ModoOpManager managerModo;

	public CancelInutController(ManagerInterface manager) {
		super();
		this.manager = manager;
		this.factory = CancelInutThreadFactory.getFactory(this.manager);
		this.cacheXml = XMLDataCache.getInstance();

		this.managerModo = new ModoOpManager();
		this.modos = new Hashtable<String, TPEMISS>();

		this.initModos();
	}

	private void initModos() {
		try {
			Hashtable<String, CNPJData> cnpjs = this.manager.getCNPJ();
			for (String cnpj : cnpjs.keySet()) {
				TPEMISS modo = this.managerModo.getModo(cnpj);
				if (modo == null) {
					modo = TPEMISS.NORMAL;
					this.managerModo.saveModo(cnpj, modo,"","");
				}
				this.modos.put(cnpj, modo);
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int isCancel(String cnpj, String uf, String ambient, String keyXml,
			String protocol, String just, boolean async) {
		try {
			this.manager.makeQueryState(uf, ambient, keyXml, true, false);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		try {
			XMLData xmldata = this.cacheXml.findData(keyXml);
			if (xmldata != null) {

				if (xmldata.getState().equals(XML_STATE.CANCELADA)) {
					return 2;
				} else if (xmldata.getState().equals(XML_STATE.AUTORIZADA)) {
					return 1;
				} else {
					return 0;
				}
			}

		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		return -1;
	}
	
	

	public String cancelXml(String cnpj, String uf, String ambient,
			String keyXml, String protocol, String just, boolean async,
			Hashtable<String, String> props) throws Exception {

		if (protocol == null) {
			XMLData xmld = this.cacheXml.findData(keyXml);
			if (xmld == null) {
				MyLogger.getLog().info("CTe nao encontrado:" + keyXml);
				return "Nota nao encontrada";
			}

			protocol = xmld.getNAutorizedProtocol();

			if (protocol == null) {
				this.manager.makeQueryState(uf, ambient, keyXml, true, false);
				xmld = this.cacheXml.findData(keyXml);
				protocol = xmld.getNAutorizedProtocol();
			}
			MyLogger.getLog().info("protocol:" + protocol);
			if (protocol == null) {
				return null;
			}
		}

		MyLogger.getLog().info("protocol:" + protocol);

		TPEMISS modo = this.modos.get(cnpj);

		CancelEventThread cancelT = this.factory.createCancelThreadEvent(cnpj, uf,
				ambient, keyXml, protocol, just, modo);

		if (async) {
			Thread t = new Thread(cancelT);
			t.start();
			return null;
		} else {
			return cancelT.initCancel();
		}

	}

	public String inutXml(String cnpj, String uf, String ambient,
			String keyXml, String justl, boolean asyn) throws Exception {
		TPEMISS modo = this.modos.get(cnpj);
		InutThread inutT = this.factory.createInutThread(uf, ambient, keyXml,
				justl, modo);
		if (asyn) {
			Thread t = new Thread(inutT);
			t.start();
			return null;
		} else {
			return inutT.initInut();
		}
	}

	public String inutXml(String nOp, String uf, String ambient, String ano,
			String cnpj, String mod, String serie, String ini, String fim,
			String just, boolean asyn) throws Exception {
		TPEMISS modo = this.modos.get(cnpj);

		InutThread inutT = this.factory.createInutThread(nOp, uf, ambient, ano,
				cnpj, mod, serie, ini, fim, just, modo);
		if (asyn) {
			Thread t = new Thread(inutT);
			t.start();
			return null;
		} else {
			return inutT.initInut();
		}
	}

	public void changeToModo(String cnpj, TPEMISS modo, String xjust, String  dhcont) {
		MyLogger.getLog().info(
				"Changing state: Cancel-Inut" + " for: " + cnpj + " with: "
						+ modo);

		this.managerModo.saveModo(cnpj, modo ,xjust, dhcont);
		this.modos.put(cnpj, modo);
	}
}
