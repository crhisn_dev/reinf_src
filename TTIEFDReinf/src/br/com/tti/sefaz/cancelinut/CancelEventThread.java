package br.com.tti.sefaz.cancelinut;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.Query;

import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.EventData;
import br.com.tti.sefaz.persistence.SefazState;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.EventData.EVENTO_STATUS;
import br.com.tti.sefaz.persistence.EventData.TIPO_EVENTO;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.sender.email.EmailSender;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.xml.evento.TRetEvento;
import br.com.tti.sefaz.xmlgenerate.XMLMessageFactory;

public class CancelEventThread extends CancelThread {
	private DAO<EventData> daoEvent;

	public CancelEventThread(ManagerInterface manager, XMLDataCache cacheXml,
			XMLMessageFactory factory, String cnpj, String uf, String ambient,
			String keyXml, String protocol, String just, TPEMISS modo) {
		super(manager, cacheXml, factory, cnpj, uf, ambient, keyXml, protocol,
				just, modo);
		this.daoEvent = DaoFactory.createDAO(EventData.class);

	}

	public List<EventData> obterEventosSequence(DAO<EventData> daoevent,
			String keyxml, String codetipoevent, Hashtable<String, String> props) {

		if (keyxml.startsWith("CTe")) {
			keyxml = keyxml.replace("CTe", "");
		}

		String sql = "SELECT o FROM EventData as o where o.evento.infEvento.chCTe = '"
				+ keyxml
				+ "' and o.status = :e and o.evento.infEvento.tpEvento = :tipoevent order by o.evento.infEvento.nSeqEvento desc ";

		Query query = daoevent.createQuery(sql);
		query.setParameter("e", EVENTO_STATUS.AUTORIZADO);
		query.setParameter("tipoevent", codetipoevent);

		return query.getResultList();
	}

	@Override
	public String makeCancel() throws Exception {
		MyLogger.getLog().info("Making cancel event: " + this.keyXml);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-02:00");

		KeyXml kk = new KeyXml(this.keyXml);
		String datahora = sdf.format(Calendar.getInstance().getTime());
		String cnpjclient = kk.getCnpj();
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		params.put("protocolo", this.protocol);
		params.put("xjust", this.just);

		TRetEvento ret = this.manager.sendEvent(this.keyXml,
				TIPO_EVENTO.CANCELAMENTO, datahora, cnpjclient, params);

		EventData eventdata = ret.getEventdata();

		MyLogger.getLog().info(
				"XML Cancel: " + this.keyXml + " with status "
						+ ret.getInfEvento().getCStat() + " motive: "
						+ ret.getInfEvento().getXMotivo());

		if (EVENTO_STATUS.AUTORIZADO.equals(eventdata.getStatus())) {
			this.cacheXml.updateCancelState(this.keyXml, ret.getInfEvento()
					.getNProt(), eventdata.getProtocoloXML(), ret
					.getInfEvento().getCStat(),
					ret.getInfEvento().getXMotivo(), ret.getInfEvento()
							.getDhRegEvento(), this.just);

			try {
				String keyxml = this.keyXml;
				String emailsdb = null;
				XMLData xmldata = this.cacheXml
						.findData((this.keyXml.startsWith("CTe") ? this.keyXml
								: "CTe" + this.keyXml));
				if (xmldata != null) {
					emailsdb = xmldata.getEmail();
				}
				Date date = xmldata.getDateCreate();

				this.manager.rePrintXml(xmldata.getKeyXml(), xmldata.getModo());

				EmailSender.getInstance().sendEmailCancel(keyxml, emailsdb,
						date);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		} else {
			this.cacheXml.updateStateSefaz(this.keyXml,
					XML_STATE.ERRO_CANCELADA, Calendar.getInstance().getTime(),
					ret.getInfEvento().getCStat(), ret.getInfEvento()
							.getXMotivo(), null);
		}

		/*
		 * if (this.alreadyCanceledStates.contains(state)) {
		 * this.cacheXml.updateState(this.keyXml, XML_STATE.CANCELADA,
		 * Calendar.getInstance().getTime());
		 * 
		 * try { String keyxml = this.keyXml; String emailsdb = null; XMLData
		 * xmldata = this.cacheXml .findData((this.keyXml.startsWith("CTe") ?
		 * this.keyXml : "CTe" + this.keyXml)); if (xmldata != null) { emailsdb
		 * = xmldata.getEmail(); } Date date = xmldata.getDateCreate();
		 * EmailSender.getInstance().sendEmailCancel(keyxml, emailsdb, date); }
		 * catch (Exception e) { MyLogger.getLog().log(Level.INFO,
		 * e.getLocalizedMessage(), e); } }
		 */

		/*
		 * if (this.schemaErrorStates.contains(state)) {
		 * this.cacheXml.updateStateSefaz(this.keyXml, XML_STATE.ERRO_CANCELADA,
		 * Calendar.getInstance().getTime(), cancel.getCStat(),
		 * cancel.getXMotivo(), null); }
		 */
		this.tryAgain = false;
		return ret.getInfEvento().getXMotivo();
	}
}
