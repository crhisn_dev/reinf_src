//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.08.07 at 09:32:09 AM BRT 
//


package br.com.tti.sefaz.reinf.eventos.cprb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.tti.sefaz.reinf.eventos.cprb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.tti.sefaz.reinf.eventos.cprb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB.InfoCPRB.IdeEstab }
     * 
     */
    public Reinf.EvtCPRB.InfoCPRB.IdeEstab createReinfEvtCPRBInfoCPRBIdeEstab() {
        return new Reinf.EvtCPRB.InfoCPRB.IdeEstab();
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod.InfoProc }
     * 
     */
    public Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod.InfoProc createReinfEvtCPRBInfoCPRBIdeEstabTipoCodInfoProc() {
        return new Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod.InfoProc();
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB.IdeContri }
     * 
     */
    public Reinf.EvtCPRB.IdeContri createReinfEvtCPRBIdeContri() {
        return new Reinf.EvtCPRB.IdeContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod }
     * 
     */
    public Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod createReinfEvtCPRBInfoCPRBIdeEstabTipoCod() {
        return new Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod();
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB.InfoCPRB }
     * 
     */
    public Reinf.EvtCPRB.InfoCPRB createReinfEvtCPRBInfoCPRB() {
        return new Reinf.EvtCPRB.InfoCPRB();
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod.TipoAjuste }
     * 
     */
    public Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod.TipoAjuste createReinfEvtCPRBInfoCPRBIdeEstabTipoCodTipoAjuste() {
        return new Reinf.EvtCPRB.InfoCPRB.IdeEstab.TipoCod.TipoAjuste();
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB }
     * 
     */
    public Reinf.EvtCPRB createReinfEvtCPRB() {
        return new Reinf.EvtCPRB();
    }

    /**
     * Create an instance of {@link Reinf.EvtCPRB.IdeEvento }
     * 
     */
    public Reinf.EvtCPRB.IdeEvento createReinfEvtCPRBIdeEvento() {
        return new Reinf.EvtCPRB.IdeEvento();
    }

}
