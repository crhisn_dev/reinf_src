//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2017.12.11 �s 11:48:53 AM BRST 
//

package br.com.tti.sefaz.reinf.eventos.infocont;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtInfoContri">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="1|2|3"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="8"/>
 *                                   &lt;maxLength value="14"/>
 *                                   &lt;pattern value="\d{8}|\d{11}|\d{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;choice>
 *                               &lt;element name="inclusao" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="idePeriodo">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="infoCadastro">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="classTrib">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="2"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indEscrituracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indDesoneracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indAcordoIsenMulta">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indSitPJ" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="[0-4]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="contato">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="nmCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cpfCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="11"/>
 *                                                                   &lt;pattern value="[0-9]{3,11}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneFixo" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneCel" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="cnpjSoftHouse">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{3,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmRazao">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="115"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmCont">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="telefone" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="infoEFR" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="ideEFR">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="S|N"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cnpjEFR" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="8"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{8,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="alteracao" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="idePeriodo">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="infoCadastro">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="classTrib">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="2"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indEscrituracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indDesoneracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indAcordoIsenMulta">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indSitPJ" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="[0-4]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="contato">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="nmCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cpfCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="11"/>
 *                                                                   &lt;pattern value="[0-9]{3,11}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneFixo" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneCel" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="cnpjSoftHouse">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{3,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmRazao">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="115"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmCont">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="telefone" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="infoEFR" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="ideEFR">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="S|N"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cnpjEFR" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="8"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{8,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="novaValidade" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="exclusao" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="idePeriodo">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/choice>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtInfoContri" })
@XmlRootElement(name = "Reinf")
public class Reinf {

	@XmlElement(required = true)
	protected Reinf.EvtInfoContri evtInfoContri;

	/**
	 * Obt�m o valor da propriedade evtInfoContri.
	 * 
	 * @return possible object is {@link Reinf.EvtInfoContri }
	 * 
	 */
	public Reinf.EvtInfoContri getEvtInfoContri() {
		return evtInfoContri;
	}

	/**
	 * Define o valor da propriedade evtInfoContri.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtInfoContri }
	 * 
	 */
	public void setEvtInfoContri(Reinf.EvtInfoContri value) {
		this.evtInfoContri = value;
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido
	 * dentro desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="1|2|3"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="8"/>
	 *                         &lt;maxLength value="14"/>
	 *                         &lt;pattern value="\d{8}|\d{11}|\d{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="infoContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;choice>
	 *                     &lt;element name="inclusao" minOccurs="0">
	 *                       &lt;complexType>
	 *                         &lt;complexContent>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                             &lt;sequence>
	 *                               &lt;element name="idePeriodo">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                               &lt;element name="infoCadastro">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="classTrib">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="2"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indEscrituracao">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="0|1"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indDesoneracao">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="0|1"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indAcordoIsenMulta">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="0|1"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indSitPJ" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="[0-4]"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="contato">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="nmCtt">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="70"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="cpfCtt">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="3"/>
	 *                                                         &lt;maxLength value="11"/>
	 *                                                         &lt;pattern value="[0-9]{3,11}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="foneFixo" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="13"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="foneCel" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="13"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="email" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="60"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="cnpjSoftHouse">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="3"/>
	 *                                                         &lt;maxLength value="14"/>
	 *                                                         &lt;pattern value="[0-9]{3,14}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="nmRazao">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="115"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="nmCont">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="70"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="telefone" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="13"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="email" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="60"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="infoEFR" minOccurs="0">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="ideEFR">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="S|N"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="cnpjEFR" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="8"/>
	 *                                                         &lt;maxLength value="14"/>
	 *                                                         &lt;pattern value="[0-9]{8,14}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                             &lt;/sequence>
	 *                           &lt;/restriction>
	 *                         &lt;/complexContent>
	 *                       &lt;/complexType>
	 *                     &lt;/element>
	 *                     &lt;element name="alteracao" minOccurs="0">
	 *                       &lt;complexType>
	 *                         &lt;complexContent>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                             &lt;sequence>
	 *                               &lt;element name="idePeriodo">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                               &lt;element name="infoCadastro">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="classTrib">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="2"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indEscrituracao">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="0|1"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indDesoneracao">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="0|1"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indAcordoIsenMulta">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="0|1"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indSitPJ" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                               &lt;pattern value="[0-4]"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="contato">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="nmCtt">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="70"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="cpfCtt">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="3"/>
	 *                                                         &lt;maxLength value="11"/>
	 *                                                         &lt;pattern value="[0-9]{3,11}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="foneFixo" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;maxLength value="13"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="foneCel" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;maxLength value="13"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="email" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="60"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="cnpjSoftHouse">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="3"/>
	 *                                                         &lt;maxLength value="14"/>
	 *                                                         &lt;pattern value="[0-9]{3,14}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="nmRazao">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="115"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="nmCont">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="70"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="telefone" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="13"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="email" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="1"/>
	 *                                                         &lt;maxLength value="60"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="infoEFR" minOccurs="0">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="ideEFR">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="S|N"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="cnpjEFR" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;minLength value="8"/>
	 *                                                         &lt;maxLength value="14"/>
	 *                                                         &lt;pattern value="[0-9]{8,14}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                               &lt;element name="novaValidade" minOccurs="0">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                             &lt;/sequence>
	 *                           &lt;/restriction>
	 *                         &lt;/complexContent>
	 *                       &lt;/complexType>
	 *                     &lt;/element>
	 *                     &lt;element name="exclusao" minOccurs="0">
	 *                       &lt;complexType>
	 *                         &lt;complexContent>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                             &lt;sequence>
	 *                               &lt;element name="idePeriodo">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;length value="7"/>
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                             &lt;/sequence>
	 *                           &lt;/restriction>
	 *                         &lt;/complexContent>
	 *                       &lt;/complexType>
	 *                     &lt;/element>
	 *                   &lt;/choice>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri", "infoContri" })
	@XmlRootElement(name = "evtInfoContri")
	public static class EvtInfoContri {

		@XmlElement(required = true)
		protected Reinf.EvtInfoContri.IdeEvento ideEvento;
		@XmlElement(required = true)
		protected Reinf.EvtInfoContri.IdeContri ideContri;
		@XmlElement(required = true)
		protected Reinf.EvtInfoContri.InfoContri infoContri;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtInfoContri.IdeEvento }
		 * 
		 */
		public Reinf.EvtInfoContri.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtInfoContri.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtInfoContri.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtInfoContri.IdeContri }
		 * 
		 */
		public Reinf.EvtInfoContri.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtInfoContri.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtInfoContri.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade infoContri.
		 * 
		 * @return possible object is {@link Reinf.EvtInfoContri.InfoContri }
		 * 
		 */
		public Reinf.EvtInfoContri.InfoContri getInfoContri() {
			return infoContri;
		}

		/**
		 * Define o valor da propriedade infoContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtInfoContri.InfoContri }
		 * 
		 */
		public void setInfoContri(Reinf.EvtInfoContri.InfoContri value) {
			this.infoContri = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="8"/>
		 *               &lt;maxLength value="14"/>
		 *               &lt;pattern value="\d{8}|\d{11}|\d{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		public static class IdeContri {

			protected long tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			public long getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(long value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="1|2|3"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpAmb", "procEmi", "verProc" })
		public static class IdeEvento {

			protected long tpAmb;
			protected long procEmi;
			@XmlElement(required = true)
			protected String verProc;

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			public long getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(long value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			public long getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(long value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;choice>
		 *           &lt;element name="inclusao" minOccurs="0">
		 *             &lt;complexType>
		 *               &lt;complexContent>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                   &lt;sequence>
		 *                     &lt;element name="idePeriodo">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                     &lt;element name="infoCadastro">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="classTrib">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="2"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indEscrituracao">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="0|1"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indDesoneracao">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="0|1"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indAcordoIsenMulta">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="0|1"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indSitPJ" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="[0-4]"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="contato">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="nmCtt">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="70"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="cpfCtt">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="3"/>
		 *                                               &lt;maxLength value="11"/>
		 *                                               &lt;pattern value="[0-9]{3,11}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="foneFixo" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="13"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="foneCel" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="13"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="email" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="60"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                               &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="cnpjSoftHouse">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="3"/>
		 *                                               &lt;maxLength value="14"/>
		 *                                               &lt;pattern value="[0-9]{3,14}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="nmRazao">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="115"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="nmCont">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="70"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="telefone" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="13"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="email" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="60"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                               &lt;element name="infoEFR" minOccurs="0">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="ideEFR">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="S|N"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="cnpjEFR" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="8"/>
		 *                                               &lt;maxLength value="14"/>
		 *                                               &lt;pattern value="[0-9]{8,14}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                   &lt;/sequence>
		 *                 &lt;/restriction>
		 *               &lt;/complexContent>
		 *             &lt;/complexType>
		 *           &lt;/element>
		 *           &lt;element name="alteracao" minOccurs="0">
		 *             &lt;complexType>
		 *               &lt;complexContent>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                   &lt;sequence>
		 *                     &lt;element name="idePeriodo">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                     &lt;element name="infoCadastro">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="classTrib">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="2"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indEscrituracao">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="0|1"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indDesoneracao">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="0|1"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indAcordoIsenMulta">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="0|1"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indSitPJ" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                                     &lt;pattern value="[0-4]"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="contato">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="nmCtt">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="70"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="cpfCtt">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="3"/>
		 *                                               &lt;maxLength value="11"/>
		 *                                               &lt;pattern value="[0-9]{3,11}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="foneFixo" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;maxLength value="13"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="foneCel" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;maxLength value="13"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="email" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="60"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                               &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="cnpjSoftHouse">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="3"/>
		 *                                               &lt;maxLength value="14"/>
		 *                                               &lt;pattern value="[0-9]{3,14}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="nmRazao">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="115"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="nmCont">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="70"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="telefone" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="13"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="email" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="1"/>
		 *                                               &lt;maxLength value="60"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                               &lt;element name="infoEFR" minOccurs="0">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="ideEFR">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="S|N"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="cnpjEFR" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;minLength value="8"/>
		 *                                               &lt;maxLength value="14"/>
		 *                                               &lt;pattern value="[0-9]{8,14}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                     &lt;element name="novaValidade" minOccurs="0">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                   &lt;/sequence>
		 *                 &lt;/restriction>
		 *               &lt;/complexContent>
		 *             &lt;/complexType>
		 *           &lt;/element>
		 *           &lt;element name="exclusao" minOccurs="0">
		 *             &lt;complexType>
		 *               &lt;complexContent>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                   &lt;sequence>
		 *                     &lt;element name="idePeriodo">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;length value="7"/>
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                   &lt;/sequence>
		 *                 &lt;/restriction>
		 *               &lt;/complexContent>
		 *             &lt;/complexType>
		 *           &lt;/element>
		 *         &lt;/choice>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "inclusao", "alteracao", "exclusao" })
		public static class InfoContri {

			protected Reinf.EvtInfoContri.InfoContri.Inclusao inclusao;
			protected Reinf.EvtInfoContri.InfoContri.Alteracao alteracao;
			protected Reinf.EvtInfoContri.InfoContri.Exclusao exclusao;

			/**
			 * Obt�m o valor da propriedade inclusao.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtInfoContri.InfoContri.Inclusao }
			 * 
			 */
			public Reinf.EvtInfoContri.InfoContri.Inclusao getInclusao() {
				return inclusao;
			}

			/**
			 * Define o valor da propriedade inclusao.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtInfoContri.InfoContri.Inclusao }
			 * 
			 */
			public void setInclusao(Reinf.EvtInfoContri.InfoContri.Inclusao value) {
				this.inclusao = value;
			}

			/**
			 * Obt�m o valor da propriedade alteracao.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtInfoContri.InfoContri.Alteracao }
			 * 
			 */
			public Reinf.EvtInfoContri.InfoContri.Alteracao getAlteracao() {
				return alteracao;
			}

			/**
			 * Define o valor da propriedade alteracao.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtInfoContri.InfoContri.Alteracao }
			 * 
			 */
			public void setAlteracao(Reinf.EvtInfoContri.InfoContri.Alteracao value) {
				this.alteracao = value;
			}

			/**
			 * Obt�m o valor da propriedade exclusao.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtInfoContri.InfoContri.Exclusao }
			 * 
			 */
			public Reinf.EvtInfoContri.InfoContri.Exclusao getExclusao() {
				return exclusao;
			}

			/**
			 * Define o valor da propriedade exclusao.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtInfoContri.InfoContri.Exclusao }
			 * 
			 */
			public void setExclusao(Reinf.EvtInfoContri.InfoContri.Exclusao value) {
				this.exclusao = value;
			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado
			 * contido dentro desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="idePeriodo">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *         &lt;element name="infoCadastro">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="classTrib">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="2"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indEscrituracao">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="0|1"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indDesoneracao">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="0|1"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indAcordoIsenMulta">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="0|1"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indSitPJ" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="[0-4]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="contato">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="nmCtt">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="70"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="cpfCtt">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="3"/>
			 *                                   &lt;maxLength value="11"/>
			 *                                   &lt;pattern value="[0-9]{3,11}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="foneFixo" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;maxLength value="13"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="foneCel" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;maxLength value="13"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="email" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="60"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="cnpjSoftHouse">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="3"/>
			 *                                   &lt;maxLength value="14"/>
			 *                                   &lt;pattern value="[0-9]{3,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nmRazao">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="115"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nmCont">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="70"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="telefone" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="13"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="email" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="60"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoEFR" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="ideEFR">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="S|N"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="cnpjEFR" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="8"/>
			 *                                   &lt;maxLength value="14"/>
			 *                                   &lt;pattern value="[0-9]{8,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *         &lt;element name="novaValidade" minOccurs="0">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "idePeriodo", "infoCadastro", "novaValidade" })
			public static class Alteracao {

				@XmlElement(required = true)
				protected Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo idePeriodo;
				@XmlElement(required = true)
				protected Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro infoCadastro;
				protected Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade novaValidade;

				/**
				 * Obt�m o valor da propriedade idePeriodo.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo }
				 * 
				 */
				public Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo getIdePeriodo() {
					return idePeriodo;
				}

				/**
				 * Define o valor da propriedade idePeriodo.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo }
				 * 
				 */
				public void setIdePeriodo(Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo value) {
					this.idePeriodo = value;
				}

				/**
				 * Obt�m o valor da propriedade infoCadastro.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro }
				 * 
				 */
				public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro getInfoCadastro() {
					return infoCadastro;
				}

				/**
				 * Define o valor da propriedade infoCadastro.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro }
				 * 
				 */
				public void setInfoCadastro(Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro value) {
					this.infoCadastro = value;
				}

				/**
				 * Obt�m o valor da propriedade novaValidade.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade }
				 * 
				 */
				public Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade getNovaValidade() {
					return novaValidade;
				}

				/**
				 * Define o valor da propriedade novaValidade.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade }
				 * 
				 */
				public void setNovaValidade(Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade value) {
					this.novaValidade = value;
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "iniValid", "fimValid" })
				public static class IdePeriodo {

					@XmlElement(required = true)
					protected String iniValid;
					protected String fimValid;

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setIniValid(String value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setFimValid(String value) {
						this.fimValid = value;
					}

				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="classTrib">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="2"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indEscrituracao">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="0|1"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indDesoneracao">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="0|1"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indAcordoIsenMulta">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="0|1"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indSitPJ" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="[0-4]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="contato">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="nmCtt">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="70"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="cpfCtt">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="3"/>
				 *                         &lt;maxLength value="11"/>
				 *                         &lt;pattern value="[0-9]{3,11}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="foneFixo" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;maxLength value="13"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="foneCel" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;maxLength value="13"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="email" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="60"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="cnpjSoftHouse">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="3"/>
				 *                         &lt;maxLength value="14"/>
				 *                         &lt;pattern value="[0-9]{3,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nmRazao">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="115"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nmCont">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="70"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="telefone" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="13"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="email" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="60"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoEFR" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="ideEFR">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="S|N"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="cnpjEFR" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="8"/>
				 *                         &lt;maxLength value="14"/>
				 *                         &lt;pattern value="[0-9]{8,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "classTrib", "indEscrituracao", "indDesoneracao",
						"indAcordoIsenMulta", "indSitPJ", "contato", "softHouse", "infoEFR" })
				public static class InfoCadastro {

					@XmlElement(required = true)
					protected String classTrib;
					protected long indEscrituracao;
					protected long indDesoneracao;
					protected long indAcordoIsenMulta;
					protected Long indSitPJ;
					@XmlElement(required = true)
					protected Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato contato;
					protected List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> softHouse;
					protected Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR infoEFR;

					/**
					 * Obt�m o valor da propriedade classTrib.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getClassTrib() {
						return classTrib;
					}

					/**
					 * Define o valor da propriedade classTrib.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setClassTrib(String value) {
						this.classTrib = value;
					}

					/**
					 * Obt�m o valor da propriedade indEscrituracao.
					 * 
					 */
					public long getIndEscrituracao() {
						return indEscrituracao;
					}

					/**
					 * Define o valor da propriedade indEscrituracao.
					 * 
					 */
					public void setIndEscrituracao(long value) {
						this.indEscrituracao = value;
					}

					/**
					 * Obt�m o valor da propriedade indDesoneracao.
					 * 
					 */
					public long getIndDesoneracao() {
						return indDesoneracao;
					}

					/**
					 * Define o valor da propriedade indDesoneracao.
					 * 
					 */
					public void setIndDesoneracao(long value) {
						this.indDesoneracao = value;
					}

					/**
					 * Obt�m o valor da propriedade indAcordoIsenMulta.
					 * 
					 */
					public long getIndAcordoIsenMulta() {
						return indAcordoIsenMulta;
					}

					/**
					 * Define o valor da propriedade indAcordoIsenMulta.
					 * 
					 */
					public void setIndAcordoIsenMulta(long value) {
						this.indAcordoIsenMulta = value;
					}

					/**
					 * Obt�m o valor da propriedade indSitPJ.
					 * 
					 * @return possible object is {@link Long }
					 * 
					 */
					public Long getIndSitPJ() {
						return indSitPJ;
					}

					/**
					 * Define o valor da propriedade indSitPJ.
					 * 
					 * @param value
					 *            allowed object is {@link Long }
					 * 
					 */
					public void setIndSitPJ(Long value) {
						this.indSitPJ = value;
					}

					/**
					 * Obt�m o valor da propriedade contato.
					 * 
					 * @return possible object is
					 *         {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato }
					 * 
					 */
					public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato getContato() {
						return contato;
					}

					/**
					 * Define o valor da propriedade contato.
					 * 
					 * @param value
					 *            allowed object is
					 *            {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato }
					 * 
					 */
					public void setContato(Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato value) {
						this.contato = value;
					}

					/**
					 * Gets the value of the softHouse property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the softHouse property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getSoftHouse().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse }
					 * 
					 * 
					 */
					public List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> getSoftHouse() {
						if (softHouse == null) {
							softHouse = new ArrayList<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse>();
						}
						return this.softHouse;
					}

					/**
					 * Obt�m o valor da propriedade infoEFR.
					 * 
					 * @return possible object is
					 *         {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR }
					 * 
					 */
					public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR getInfoEFR() {
						return infoEFR;
					}

					/**
					 * Define o valor da propriedade infoEFR.
					 * 
					 * @param value
					 *            allowed object is
					 *            {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR }
					 * 
					 */
					public void setInfoEFR(Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR value) {
						this.infoEFR = value;
					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="nmCtt">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="70"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="cpfCtt">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="3"/>
					 *               &lt;maxLength value="11"/>
					 *               &lt;pattern value="[0-9]{3,11}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="foneFixo" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;maxLength value="13"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="foneCel" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;maxLength value="13"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="email" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="60"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "nmCtt", "cpfCtt", "foneFixo", "foneCel", "email" })
					public static class Contato {

						@XmlElement(required = true)
						protected String nmCtt;
						@XmlElement(required = true)
						protected String cpfCtt;
						protected String foneFixo;
						protected String foneCel;
						protected String email;

						/**
						 * Obt�m o valor da propriedade nmCtt.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNmCtt() {
							return nmCtt;
						}

						/**
						 * Define o valor da propriedade nmCtt.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNmCtt(String value) {
							this.nmCtt = value;
						}

						/**
						 * Obt�m o valor da propriedade cpfCtt.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCpfCtt() {
							return cpfCtt;
						}

						/**
						 * Define o valor da propriedade cpfCtt.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCpfCtt(String value) {
							this.cpfCtt = value;
						}

						/**
						 * Obt�m o valor da propriedade foneFixo.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getFoneFixo() {
							return foneFixo;
						}

						/**
						 * Define o valor da propriedade foneFixo.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setFoneFixo(String value) {
							this.foneFixo = value;
						}

						/**
						 * Obt�m o valor da propriedade foneCel.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getFoneCel() {
							return foneCel;
						}

						/**
						 * Define o valor da propriedade foneCel.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setFoneCel(String value) {
							this.foneCel = value;
						}

						/**
						 * Obt�m o valor da propriedade email.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getEmail() {
							return email;
						}

						/**
						 * Define o valor da propriedade email.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setEmail(String value) {
							this.email = value;
						}

					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="ideEFR">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="S|N"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="cnpjEFR" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="8"/>
					 *               &lt;maxLength value="14"/>
					 *               &lt;pattern value="[0-9]{8,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "ideEFR", "cnpjEFR" })
					public static class InfoEFR {

						@XmlElement(required = true)
						protected String ideEFR;
						protected String cnpjEFR;

						/**
						 * Obt�m o valor da propriedade ideEFR.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIdeEFR() {
							return ideEFR;
						}

						/**
						 * Define o valor da propriedade ideEFR.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIdeEFR(String value) {
							this.ideEFR = value;
						}

						/**
						 * Obt�m o valor da propriedade cnpjEFR.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCnpjEFR() {
							return cnpjEFR;
						}

						/**
						 * Define o valor da propriedade cnpjEFR.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCnpjEFR(String value) {
							this.cnpjEFR = value;
						}

					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="cnpjSoftHouse">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="3"/>
					 *               &lt;maxLength value="14"/>
					 *               &lt;pattern value="[0-9]{3,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nmRazao">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="115"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nmCont">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="70"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="telefone" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="13"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="email" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="60"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "cnpjSoftHouse", "nmRazao", "nmCont", "telefone", "email" })
					public static class SoftHouse {

						@XmlElement(required = true)
						protected String cnpjSoftHouse;
						@XmlElement(required = true)
						protected String nmRazao;
						@XmlElement(required = true)
						protected String nmCont;
						protected String telefone;
						protected String email;

						/**
						 * Obt�m o valor da propriedade cnpjSoftHouse.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCnpjSoftHouse() {
							return cnpjSoftHouse;
						}

						/**
						 * Define o valor da propriedade cnpjSoftHouse.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCnpjSoftHouse(String value) {
							this.cnpjSoftHouse = value;
						}

						/**
						 * Obt�m o valor da propriedade nmRazao.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNmRazao() {
							return nmRazao;
						}

						/**
						 * Define o valor da propriedade nmRazao.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNmRazao(String value) {
							this.nmRazao = value;
						}

						/**
						 * Obt�m o valor da propriedade nmCont.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNmCont() {
							return nmCont;
						}

						/**
						 * Define o valor da propriedade nmCont.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNmCont(String value) {
							this.nmCont = value;
						}

						/**
						 * Obt�m o valor da propriedade telefone.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getTelefone() {
							return telefone;
						}

						/**
						 * Define o valor da propriedade telefone.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setTelefone(String value) {
							this.telefone = value;
						}

						/**
						 * Obt�m o valor da propriedade email.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getEmail() {
							return email;
						}

						/**
						 * Define o valor da propriedade email.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setEmail(String value) {
							this.email = value;
						}

					}

				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "iniValid", "fimValid" })
				public static class NovaValidade {

					@XmlElement(required = true)
					protected String iniValid;
					protected String fimValid;

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setIniValid(String value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setFimValid(String value) {
						this.fimValid = value;
					}

				}

			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado
			 * contido dentro desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="idePeriodo">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "idePeriodo" })
			public static class Exclusao {

				@XmlElement(required = true)
				protected Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo idePeriodo;

				/**
				 * Obt�m o valor da propriedade idePeriodo.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo }
				 * 
				 */
				public Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo getIdePeriodo() {
					return idePeriodo;
				}

				/**
				 * Define o valor da propriedade idePeriodo.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo }
				 * 
				 */
				public void setIdePeriodo(Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo value) {
					this.idePeriodo = value;
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "iniValid", "fimValid" })
				public static class IdePeriodo {

					@XmlElement(required = true)
					protected String iniValid;
					protected String fimValid;

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setIniValid(String value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setFimValid(String value) {
						this.fimValid = value;
					}

				}

			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado
			 * contido dentro desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="idePeriodo">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="7"/>
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *         &lt;element name="infoCadastro">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="classTrib">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;length value="2"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indEscrituracao">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="0|1"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indDesoneracao">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="0|1"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indAcordoIsenMulta">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="0|1"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indSitPJ" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *                         &lt;pattern value="[0-4]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="contato">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="nmCtt">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="70"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="cpfCtt">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="3"/>
			 *                                   &lt;maxLength value="11"/>
			 *                                   &lt;pattern value="[0-9]{3,11}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="foneFixo" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="13"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="foneCel" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="13"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="email" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="60"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="cnpjSoftHouse">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="3"/>
			 *                                   &lt;maxLength value="14"/>
			 *                                   &lt;pattern value="[0-9]{3,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nmRazao">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="115"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nmCont">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="70"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="telefone" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="13"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="email" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="60"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoEFR" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="ideEFR">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="S|N"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="cnpjEFR" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="8"/>
			 *                                   &lt;maxLength value="14"/>
			 *                                   &lt;pattern value="[0-9]{8,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "idePeriodo", "infoCadastro" })
			public static class Inclusao {

				@XmlElement(required = true)
				protected Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo idePeriodo;
				@XmlElement(required = true)
				protected Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro infoCadastro;

				/**
				 * Obt�m o valor da propriedade idePeriodo.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo }
				 * 
				 */
				public Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo getIdePeriodo() {
					return idePeriodo;
				}

				/**
				 * Define o valor da propriedade idePeriodo.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo }
				 * 
				 */
				public void setIdePeriodo(Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo value) {
					this.idePeriodo = value;
				}

				/**
				 * Obt�m o valor da propriedade infoCadastro.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro }
				 * 
				 */
				public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro getInfoCadastro() {
					return infoCadastro;
				}

				/**
				 * Define o valor da propriedade infoCadastro.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro }
				 * 
				 */
				public void setInfoCadastro(Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro value) {
					this.infoCadastro = value;
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="7"/>
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "iniValid", "fimValid" })
				public static class IdePeriodo {

					@XmlElement(required = true)
					protected String iniValid;
					protected String fimValid;

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setIniValid(String value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setFimValid(String value) {
						this.fimValid = value;
					}

				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="classTrib">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;length value="2"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indEscrituracao">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="0|1"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indDesoneracao">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="0|1"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indAcordoIsenMulta">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="0|1"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indSitPJ" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
				 *               &lt;pattern value="[0-4]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="contato">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="nmCtt">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="70"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="cpfCtt">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="3"/>
				 *                         &lt;maxLength value="11"/>
				 *                         &lt;pattern value="[0-9]{3,11}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="foneFixo" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="13"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="foneCel" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="13"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="email" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="60"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="cnpjSoftHouse">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="3"/>
				 *                         &lt;maxLength value="14"/>
				 *                         &lt;pattern value="[0-9]{3,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nmRazao">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="115"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nmCont">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="70"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="telefone" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="13"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="email" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="60"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoEFR" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="ideEFR">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="S|N"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="cnpjEFR" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="8"/>
				 *                         &lt;maxLength value="14"/>
				 *                         &lt;pattern value="[0-9]{8,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "classTrib", "indEscrituracao", "indDesoneracao",
						"indAcordoIsenMulta", "indSitPJ", "contato", "softHouse", "infoEFR" })
				public static class InfoCadastro {

					@XmlElement(required = true)
					protected String classTrib;
					protected long indEscrituracao;
					protected long indDesoneracao;
					protected long indAcordoIsenMulta;
					protected Long indSitPJ;
					@XmlElement(required = true)
					protected Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato contato;
					protected List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> softHouse;
					protected Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR infoEFR;

					/**
					 * Obt�m o valor da propriedade classTrib.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getClassTrib() {
						return classTrib;
					}

					/**
					 * Define o valor da propriedade classTrib.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setClassTrib(String value) {
						this.classTrib = value;
					}

					/**
					 * Obt�m o valor da propriedade indEscrituracao.
					 * 
					 */
					public long getIndEscrituracao() {
						return indEscrituracao;
					}

					/**
					 * Define o valor da propriedade indEscrituracao.
					 * 
					 */
					public void setIndEscrituracao(long value) {
						this.indEscrituracao = value;
					}

					/**
					 * Obt�m o valor da propriedade indDesoneracao.
					 * 
					 */
					public long getIndDesoneracao() {
						return indDesoneracao;
					}

					/**
					 * Define o valor da propriedade indDesoneracao.
					 * 
					 */
					public void setIndDesoneracao(long value) {
						this.indDesoneracao = value;
					}

					/**
					 * Obt�m o valor da propriedade indAcordoIsenMulta.
					 * 
					 */
					public long getIndAcordoIsenMulta() {
						return indAcordoIsenMulta;
					}

					/**
					 * Define o valor da propriedade indAcordoIsenMulta.
					 * 
					 */
					public void setIndAcordoIsenMulta(long value) {
						this.indAcordoIsenMulta = value;
					}

					/**
					 * Obt�m o valor da propriedade indSitPJ.
					 * 
					 * @return possible object is {@link Long }
					 * 
					 */
					public Long getIndSitPJ() {
						return indSitPJ;
					}

					/**
					 * Define o valor da propriedade indSitPJ.
					 * 
					 * @param value
					 *            allowed object is {@link Long }
					 * 
					 */
					public void setIndSitPJ(Long value) {
						this.indSitPJ = value;
					}

					/**
					 * Obt�m o valor da propriedade contato.
					 * 
					 * @return possible object is
					 *         {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato }
					 * 
					 */
					public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato getContato() {
						return contato;
					}

					/**
					 * Define o valor da propriedade contato.
					 * 
					 * @param value
					 *            allowed object is
					 *            {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato }
					 * 
					 */
					public void setContato(Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato value) {
						this.contato = value;
					}

					/**
					 * Gets the value of the softHouse property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the softHouse property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getSoftHouse().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse }
					 * 
					 * 
					 */
					public List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> getSoftHouse() {
						if (softHouse == null) {
							softHouse = new ArrayList<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse>();
						}
						return this.softHouse;
					}

					/**
					 * Obt�m o valor da propriedade infoEFR.
					 * 
					 * @return possible object is
					 *         {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR }
					 * 
					 */
					public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR getInfoEFR() {
						return infoEFR;
					}

					/**
					 * Define o valor da propriedade infoEFR.
					 * 
					 * @param value
					 *            allowed object is
					 *            {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR }
					 * 
					 */
					public void setInfoEFR(Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR value) {
						this.infoEFR = value;
					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="nmCtt">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="70"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="cpfCtt">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="3"/>
					 *               &lt;maxLength value="11"/>
					 *               &lt;pattern value="[0-9]{3,11}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="foneFixo" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="13"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="foneCel" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="13"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="email" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="60"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "nmCtt", "cpfCtt", "foneFixo", "foneCel", "email" })
					public static class Contato {

						@XmlElement(required = true)
						protected String nmCtt;
						@XmlElement(required = true)
						protected String cpfCtt;
						protected String foneFixo;
						protected String foneCel;
						protected String email;

						/**
						 * Obt�m o valor da propriedade nmCtt.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNmCtt() {
							return nmCtt;
						}

						/**
						 * Define o valor da propriedade nmCtt.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNmCtt(String value) {
							this.nmCtt = value;
						}

						/**
						 * Obt�m o valor da propriedade cpfCtt.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCpfCtt() {
							return cpfCtt;
						}

						/**
						 * Define o valor da propriedade cpfCtt.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCpfCtt(String value) {
							this.cpfCtt = value;
						}

						/**
						 * Obt�m o valor da propriedade foneFixo.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getFoneFixo() {
							return foneFixo;
						}

						/**
						 * Define o valor da propriedade foneFixo.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setFoneFixo(String value) {
							this.foneFixo = value;
						}

						/**
						 * Obt�m o valor da propriedade foneCel.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getFoneCel() {
							return foneCel;
						}

						/**
						 * Define o valor da propriedade foneCel.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setFoneCel(String value) {
							this.foneCel = value;
						}

						/**
						 * Obt�m o valor da propriedade email.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getEmail() {
							return email;
						}

						/**
						 * Define o valor da propriedade email.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setEmail(String value) {
							this.email = value;
						}

					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="ideEFR">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="S|N"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="cnpjEFR" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="8"/>
					 *               &lt;maxLength value="14"/>
					 *               &lt;pattern value="[0-9]{8,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "ideEFR", "cnpjEFR" })
					public static class InfoEFR {

						@XmlElement(required = true)
						protected String ideEFR;
						protected String cnpjEFR;

						/**
						 * Obt�m o valor da propriedade ideEFR.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIdeEFR() {
							return ideEFR;
						}

						/**
						 * Define o valor da propriedade ideEFR.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIdeEFR(String value) {
							this.ideEFR = value;
						}

						/**
						 * Obt�m o valor da propriedade cnpjEFR.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCnpjEFR() {
							return cnpjEFR;
						}

						/**
						 * Define o valor da propriedade cnpjEFR.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCnpjEFR(String value) {
							this.cnpjEFR = value;
						}

					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="cnpjSoftHouse">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="3"/>
					 *               &lt;maxLength value="14"/>
					 *               &lt;pattern value="[0-9]{3,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nmRazao">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="115"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nmCont">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="70"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="telefone" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="13"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="email" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="60"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "cnpjSoftHouse", "nmRazao", "nmCont", "telefone", "email" })
					public static class SoftHouse {

						@XmlElement(required = true)
						protected String cnpjSoftHouse;
						@XmlElement(required = true)
						protected String nmRazao;
						@XmlElement(required = true)
						protected String nmCont;
						protected String telefone;
						protected String email;

						/**
						 * Obt�m o valor da propriedade cnpjSoftHouse.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCnpjSoftHouse() {
							return cnpjSoftHouse;
						}

						/**
						 * Define o valor da propriedade cnpjSoftHouse.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCnpjSoftHouse(String value) {
							this.cnpjSoftHouse = value;
						}

						/**
						 * Obt�m o valor da propriedade nmRazao.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNmRazao() {
							return nmRazao;
						}

						/**
						 * Define o valor da propriedade nmRazao.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNmRazao(String value) {
							this.nmRazao = value;
						}

						/**
						 * Obt�m o valor da propriedade nmCont.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNmCont() {
							return nmCont;
						}

						/**
						 * Define o valor da propriedade nmCont.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNmCont(String value) {
							this.nmCont = value;
						}

						/**
						 * Obt�m o valor da propriedade telefone.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getTelefone() {
							return telefone;
						}

						/**
						 * Define o valor da propriedade telefone.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setTelefone(String value) {
							this.telefone = value;
						}

						/**
						 * Obt�m o valor da propriedade email.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getEmail() {
							return email;
						}

						/**
						 * Define o valor da propriedade email.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setEmail(String value) {
							this.email = value;
						}

					}

				}

			}

		}

	}

}
