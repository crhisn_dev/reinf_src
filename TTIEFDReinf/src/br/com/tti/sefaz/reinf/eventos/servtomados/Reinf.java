//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.02.06 �s 11:19:03 AM BRST 
//

package br.com.tti.sefaz.reinf.eventos.servtomados;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtServTom">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="indRetif">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;minInclusive value="1"/>
 *                                   &lt;maxInclusive value="2"/>
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrRecibo" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="16"/>
 *                                   &lt;maxLength value="52"/>
 *                                   &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="perApur">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                   &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;minInclusive value="1"/>
 *                                   &lt;maxInclusive value="3"/>
 *                                   &lt;pattern value="[1-3]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;minInclusive value="1"/>
 *                                   &lt;maxInclusive value="2"/>
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoServTom">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ideEstabObra">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="tpInscEstab">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                             &lt;minInclusive value="1"/>
 *                                             &lt;maxInclusive value="4"/>
 *                                             &lt;pattern value="[1|4]"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="nrInscEstab">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;minLength value="3"/>
 *                                             &lt;maxLength value="14"/>
 *                                             &lt;pattern value="[0-9]{3,14}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="indObra">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                             &lt;minInclusive value="0"/>
 *                                             &lt;maxInclusive value="2"/>
 *                                             &lt;pattern value="[0-2]"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="idePrestServ">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="cnpjPrestador">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="3"/>
 *                                                       &lt;maxLength value="14"/>
 *                                                       &lt;pattern value="[0-9]{3,14}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalBruto">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalBaseRet">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalRetPrinc">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalRetAdic" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalNRetAdic" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="indCPRB">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                       &lt;minInclusive value="0"/>
 *                                                       &lt;maxInclusive value="1"/>
 *                                                       &lt;pattern value="[0|1]"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="nfs" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="serie">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="5"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="numDocto">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="15"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="dtEmissaoNF">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="vlrBruto">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="obs" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="250"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="infoTpServ" maxOccurs="9">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="tpServico">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;length value="9"/>
 *                                                                           &lt;pattern value="[0-9]{9}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrBaseRet">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrRetencao">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrRetSub" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrNRetPrinc" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos15" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos20" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos25" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrAdicional" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrNRetAdic" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="tpProcRetPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                                 &lt;minInclusive value="1"/>
 *                                                                 &lt;maxInclusive value="2"/>
 *                                                                 &lt;pattern value="[1|2]{1}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="nrProcRetPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="21"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="codSuspPrinc" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;pattern value="[0-9]{0,14}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="valorPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="tpProcRetAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                                 &lt;minInclusive value="1"/>
 *                                                                 &lt;maxInclusive value="2"/>
 *                                                                 &lt;pattern value="[1|2]{1}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="nrProcRetAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="21"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="codSuspAdic" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;pattern value="[0-9]{0,14}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="valorAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtServTom" })
@XmlRootElement(name = "Reinf")
public class Reinf {

	@XmlElement(required = true)
	protected Reinf.EvtServTom evtServTom;

	/**
	 * Obt�m o valor da propriedade evtServTom.
	 * 
	 * @return possible object is {@link Reinf.EvtServTom }
	 * 
	 */
	public Reinf.EvtServTom getEvtServTom() {
		return evtServTom;
	}

	/**
	 * Define o valor da propriedade evtServTom.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtServTom }
	 * 
	 */
	public void setEvtServTom(Reinf.EvtServTom value) {
		this.evtServTom = value;
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido
	 * dentro desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="indRetif">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;minInclusive value="1"/>
	 *                         &lt;maxInclusive value="2"/>
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrRecibo" minOccurs="0">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="16"/>
	 *                         &lt;maxLength value="52"/>
	 *                         &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="perApur">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;minInclusive value="1"/>
	 *                         &lt;maxInclusive value="3"/>
	 *                         &lt;pattern value="[1-3]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;minInclusive value="1"/>
	 *                         &lt;maxInclusive value="2"/>
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="infoServTom">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="ideEstabObra">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="tpInscEstab">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                   &lt;minInclusive value="1"/>
	 *                                   &lt;maxInclusive value="4"/>
	 *                                   &lt;pattern value="[1|4]"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="nrInscEstab">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                   &lt;minLength value="3"/>
	 *                                   &lt;maxLength value="14"/>
	 *                                   &lt;pattern value="[0-9]{3,14}"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="indObra">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                   &lt;minInclusive value="0"/>
	 *                                   &lt;maxInclusive value="2"/>
	 *                                   &lt;pattern value="[0-2]"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="idePrestServ">
	 *                               &lt;complexType>
	 *                                 &lt;complexContent>
	 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                     &lt;sequence>
	 *                                       &lt;element name="cnpjPrestador">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="3"/>
	 *                                             &lt;maxLength value="14"/>
	 *                                             &lt;pattern value="[0-9]{3,14}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalBruto">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalBaseRet">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalRetPrinc">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalRetAdic" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalNRetAdic" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="indCPRB">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                             &lt;minInclusive value="0"/>
	 *                                             &lt;maxInclusive value="1"/>
	 *                                             &lt;pattern value="[0|1]"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="nfs" maxOccurs="unbounded">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="serie">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="5"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="numDocto">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="15"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="dtEmissaoNF">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="vlrBruto">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="obs" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="250"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="infoTpServ" maxOccurs="9">
	 *                                                   &lt;complexType>
	 *                                                     &lt;complexContent>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                         &lt;sequence>
	 *                                                           &lt;element name="tpServico">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;length value="9"/>
	 *                                                                 &lt;pattern value="[0-9]{9}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrBaseRet">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrRetencao">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrRetSub" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrNRetPrinc" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos15" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos20" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos25" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrAdicional" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrNRetAdic" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                         &lt;/sequence>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/complexContent>
	 *                                                   &lt;/complexType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="tpProcRetPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                                       &lt;minInclusive value="1"/>
	 *                                                       &lt;maxInclusive value="2"/>
	 *                                                       &lt;pattern value="[1|2]{1}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="nrProcRetPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="21"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="codSuspPrinc" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;pattern value="[0-9]{0,14}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="valorPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="tpProcRetAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                                       &lt;minInclusive value="1"/>
	 *                                                       &lt;maxInclusive value="2"/>
	 *                                                       &lt;pattern value="[1|2]{1}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="nrProcRetAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="21"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="codSuspAdic" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;pattern value="[0-9]{0,14}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="valorAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                     &lt;/sequence>
	 *                                   &lt;/restriction>
	 *                                 &lt;/complexContent>
	 *                               &lt;/complexType>
	 *                             &lt;/element>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri", "infoServTom" })
	@XmlRootElement(name = "evtServTom")
	public static class EvtServTom {

		@XmlElement(required = true)
		protected Reinf.EvtServTom.IdeEvento ideEvento;
		@XmlElement(required = true)
		protected Reinf.EvtServTom.IdeContri ideContri;
		@XmlElement(required = true)
		protected Reinf.EvtServTom.InfoServTom infoServTom;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtServTom.IdeEvento }
		 * 
		 */
		public Reinf.EvtServTom.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServTom.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtServTom.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtServTom.IdeContri }
		 * 
		 */
		public Reinf.EvtServTom.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServTom.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtServTom.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade infoServTom.
		 * 
		 * @return possible object is {@link Reinf.EvtServTom.InfoServTom }
		 * 
		 */
		public Reinf.EvtServTom.InfoServTom getInfoServTom() {
			return infoServTom;
		}

		/**
		 * Define o valor da propriedade infoServTom.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServTom.InfoServTom }
		 * 
		 */
		public void setInfoServTom(Reinf.EvtServTom.InfoServTom value) {
			this.infoServTom = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		public static class IdeContri {

			protected short tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			public short getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(short value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="indRetif">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;minInclusive value="1"/>
		 *               &lt;maxInclusive value="2"/>
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrRecibo" minOccurs="0">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="16"/>
		 *               &lt;maxLength value="52"/>
		 *               &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="perApur">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;minInclusive value="1"/>
		 *               &lt;maxInclusive value="3"/>
		 *               &lt;pattern value="[1-3]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;minInclusive value="1"/>
		 *               &lt;maxInclusive value="2"/>
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "indRetif", "nrRecibo", "perApur", "tpAmb", "procEmi", "verProc" })
		public static class IdeEvento {

			protected short indRetif;
			protected String nrRecibo;
			@XmlElement(required = true)
			protected XMLGregorianCalendar perApur;
			protected short tpAmb;
			protected short procEmi;
			@XmlElement(required = true)
			protected String verProc;

			/**
			 * Obt�m o valor da propriedade indRetif.
			 * 
			 */
			public short getIndRetif() {
				return indRetif;
			}

			/**
			 * Define o valor da propriedade indRetif.
			 * 
			 */
			public void setIndRetif(short value) {
				this.indRetif = value;
			}

			/**
			 * Obt�m o valor da propriedade nrRecibo.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNrRecibo() {
				return nrRecibo;
			}

			/**
			 * Define o valor da propriedade nrRecibo.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrRecibo(String value) {
				this.nrRecibo = value;
			}

			/**
			 * Obt�m o valor da propriedade perApur.
			 * 
			 * @return possible object is {@link XMLGregorianCalendar }
			 * 
			 */
			public XMLGregorianCalendar getPerApur() {
				return perApur;
			}

			/**
			 * Define o valor da propriedade perApur.
			 * 
			 * @param value
			 *            allowed object is {@link XMLGregorianCalendar }
			 * 
			 */
			public void setPerApur(XMLGregorianCalendar value) {
				this.perApur = value;
			}

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			public short getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(short value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			public short getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(short value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="ideEstabObra">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="tpInscEstab">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                         &lt;minInclusive value="1"/>
		 *                         &lt;maxInclusive value="4"/>
		 *                         &lt;pattern value="[1|4]"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="nrInscEstab">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                         &lt;minLength value="3"/>
		 *                         &lt;maxLength value="14"/>
		 *                         &lt;pattern value="[0-9]{3,14}"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="indObra">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                         &lt;minInclusive value="0"/>
		 *                         &lt;maxInclusive value="2"/>
		 *                         &lt;pattern value="[0-2]"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="idePrestServ">
		 *                     &lt;complexType>
		 *                       &lt;complexContent>
		 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                           &lt;sequence>
		 *                             &lt;element name="cnpjPrestador">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="3"/>
		 *                                   &lt;maxLength value="14"/>
		 *                                   &lt;pattern value="[0-9]{3,14}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalBruto">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalBaseRet">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalRetPrinc">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalRetAdic" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalNRetAdic" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="indCPRB">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                   &lt;minInclusive value="0"/>
		 *                                   &lt;maxInclusive value="1"/>
		 *                                   &lt;pattern value="[0|1]"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="nfs" maxOccurs="unbounded">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="serie">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="5"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="numDocto">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="15"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="dtEmissaoNF">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="vlrBruto">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="obs" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="250"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="infoTpServ" maxOccurs="9">
		 *                                         &lt;complexType>
		 *                                           &lt;complexContent>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                               &lt;sequence>
		 *                                                 &lt;element name="tpServico">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;length value="9"/>
		 *                                                       &lt;pattern value="[0-9]{9}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrBaseRet">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrRetencao">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrRetSub" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrNRetPrinc" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos15" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos20" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos25" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrAdicional" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrNRetAdic" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                               &lt;/sequence>
		 *                                             &lt;/restriction>
		 *                                           &lt;/complexContent>
		 *                                         &lt;/complexType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                             &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="tpProcRetPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                             &lt;minInclusive value="1"/>
		 *                                             &lt;maxInclusive value="2"/>
		 *                                             &lt;pattern value="[1|2]{1}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="nrProcRetPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="21"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="codSuspPrinc" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;pattern value="[0-9]{0,14}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="valorPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                             &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="tpProcRetAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                             &lt;minInclusive value="1"/>
		 *                                             &lt;maxInclusive value="2"/>
		 *                                             &lt;pattern value="[1|2]{1}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="nrProcRetAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="21"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="codSuspAdic" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;pattern value="[0-9]{0,14}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="valorAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                           &lt;/sequence>
		 *                         &lt;/restriction>
		 *                       &lt;/complexContent>
		 *                     &lt;/complexType>
		 *                   &lt;/element>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "ideEstabObra" })
		public static class InfoServTom {

			@XmlElement(required = true)
			protected Reinf.EvtServTom.InfoServTom.IdeEstabObra ideEstabObra;

			/**
			 * Obt�m o valor da propriedade ideEstabObra.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra }
			 * 
			 */
			public Reinf.EvtServTom.InfoServTom.IdeEstabObra getIdeEstabObra() {
				return ideEstabObra;
			}

			/**
			 * Define o valor da propriedade ideEstabObra.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra }
			 * 
			 */
			public void setIdeEstabObra(Reinf.EvtServTom.InfoServTom.IdeEstabObra value) {
				this.ideEstabObra = value;
			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado
			 * contido dentro desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="tpInscEstab">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *               &lt;minInclusive value="1"/>
			 *               &lt;maxInclusive value="4"/>
			 *               &lt;pattern value="[1|4]"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="nrInscEstab">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *               &lt;minLength value="3"/>
			 *               &lt;maxLength value="14"/>
			 *               &lt;pattern value="[0-9]{3,14}"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="indObra">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *               &lt;minInclusive value="0"/>
			 *               &lt;maxInclusive value="2"/>
			 *               &lt;pattern value="[0-2]"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="idePrestServ">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="cnpjPrestador">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="3"/>
			 *                         &lt;maxLength value="14"/>
			 *                         &lt;pattern value="[0-9]{3,14}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalBruto">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalBaseRet">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalRetPrinc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalRetAdic" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalNRetAdic" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indCPRB">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;minInclusive value="0"/>
			 *                         &lt;maxInclusive value="1"/>
			 *                         &lt;pattern value="[0|1]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="nfs" maxOccurs="unbounded">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="serie">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="5"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="numDocto">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="15"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="dtEmissaoNF">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="vlrBruto">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="obs" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="250"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="infoTpServ" maxOccurs="9">
			 *                               &lt;complexType>
			 *                                 &lt;complexContent>
			 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                                     &lt;sequence>
			 *                                       &lt;element name="tpServico">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;length value="9"/>
			 *                                             &lt;pattern value="[0-9]{9}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrBaseRet">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrRetencao">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrRetSub" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrNRetPrinc" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos15" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos20" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos25" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrAdicional" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrNRetAdic" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                     &lt;/sequence>
			 *                                   &lt;/restriction>
			 *                                 &lt;/complexContent>
			 *                               &lt;/complexType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="tpProcRetPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                                   &lt;minInclusive value="1"/>
			 *                                   &lt;maxInclusive value="2"/>
			 *                                   &lt;pattern value="[1|2]{1}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nrProcRetPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="21"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codSuspPrinc" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="valorPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="tpProcRetAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                                   &lt;minInclusive value="1"/>
			 *                                   &lt;maxInclusive value="2"/>
			 *                                   &lt;pattern value="[1|2]{1}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nrProcRetAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="21"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codSuspAdic" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="valorAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "tpInscEstab", "nrInscEstab", "indObra", "idePrestServ" })
			public static class IdeEstabObra {

				protected short tpInscEstab;
				@XmlElement(required = true)
				protected String nrInscEstab;
				protected short indObra;
				@XmlElement(required = true)
				protected Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ idePrestServ;

				/**
				 * Obt�m o valor da propriedade tpInscEstab.
				 * 
				 */
				public short getTpInscEstab() {
					return tpInscEstab;
				}

				/**
				 * Define o valor da propriedade tpInscEstab.
				 * 
				 */
				public void setTpInscEstab(short value) {
					this.tpInscEstab = value;
				}

				/**
				 * Obt�m o valor da propriedade nrInscEstab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getNrInscEstab() {
					return nrInscEstab;
				}

				/**
				 * Define o valor da propriedade nrInscEstab.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setNrInscEstab(String value) {
					this.nrInscEstab = value;
				}

				/**
				 * Obt�m o valor da propriedade indObra.
				 * 
				 */
				public short getIndObra() {
					return indObra;
				}

				/**
				 * Define o valor da propriedade indObra.
				 * 
				 */
				public void setIndObra(short value) {
					this.indObra = value;
				}

				/**
				 * Obt�m o valor da propriedade idePrestServ.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ }
				 * 
				 */
				public Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ getIdePrestServ() {
					return idePrestServ;
				}

				/**
				 * Define o valor da propriedade idePrestServ.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ }
				 * 
				 */
				public void setIdePrestServ(Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ value) {
					this.idePrestServ = value;
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="cnpjPrestador">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="3"/>
				 *               &lt;maxLength value="14"/>
				 *               &lt;pattern value="[0-9]{3,14}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalBruto">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalBaseRet">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalRetPrinc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalRetAdic" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalNRetAdic" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indCPRB">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;minInclusive value="0"/>
				 *               &lt;maxInclusive value="1"/>
				 *               &lt;pattern value="[0|1]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="nfs" maxOccurs="unbounded">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="serie">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="5"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="numDocto">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="15"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="dtEmissaoNF">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="vlrBruto">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="obs" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="250"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="infoTpServ" maxOccurs="9">
				 *                     &lt;complexType>
				 *                       &lt;complexContent>
				 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                           &lt;sequence>
				 *                             &lt;element name="tpServico">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;length value="9"/>
				 *                                   &lt;pattern value="[0-9]{9}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrBaseRet">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrRetencao">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrRetSub" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrNRetPrinc" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos15" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos20" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos25" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrAdicional" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrNRetAdic" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                           &lt;/sequence>
				 *                         &lt;/restriction>
				 *                       &lt;/complexContent>
				 *                     &lt;/complexType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="tpProcRetPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *                         &lt;minInclusive value="1"/>
				 *                         &lt;maxInclusive value="2"/>
				 *                         &lt;pattern value="[1|2]{1}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nrProcRetPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="21"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codSuspPrinc" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="valorPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="tpProcRetAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *                         &lt;minInclusive value="1"/>
				 *                         &lt;maxInclusive value="2"/>
				 *                         &lt;pattern value="[1|2]{1}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nrProcRetAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="21"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codSuspAdic" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="valorAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "cnpjPrestador", "vlrTotalBruto", "vlrTotalBaseRet",
						"vlrTotalRetPrinc", "vlrTotalRetAdic", "vlrTotalNRetPrinc", "vlrTotalNRetAdic", "indCPRB",
						"nfs", "infoProcRetPr", "infoProcRetAd" })
				public static class IdePrestServ {

					@XmlElement(required = true)
					protected String cnpjPrestador;
					@XmlElement(required = true)
					protected String vlrTotalBruto;
					@XmlElement(required = true)
					protected String vlrTotalBaseRet;
					@XmlElement(required = true)
					protected String vlrTotalRetPrinc;
					protected String vlrTotalRetAdic;
					protected String vlrTotalNRetPrinc;
					protected String vlrTotalNRetAdic;
					protected short indCPRB;
					@XmlElement(required = true)
					protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> nfs;
					protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> infoProcRetPr;
					protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> infoProcRetAd;

					/**
					 * Obt�m o valor da propriedade cnpjPrestador.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getCnpjPrestador() {
						return cnpjPrestador;
					}

					/**
					 * Define o valor da propriedade cnpjPrestador.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setCnpjPrestador(String value) {
						this.cnpjPrestador = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalBruto.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getVlrTotalBruto() {
						return vlrTotalBruto;
					}

					/**
					 * Define o valor da propriedade vlrTotalBruto.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalBruto(String value) {
						this.vlrTotalBruto = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalBaseRet.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getVlrTotalBaseRet() {
						return vlrTotalBaseRet;
					}

					/**
					 * Define o valor da propriedade vlrTotalBaseRet.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalBaseRet(String value) {
						this.vlrTotalBaseRet = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalRetPrinc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getVlrTotalRetPrinc() {
						return vlrTotalRetPrinc;
					}

					/**
					 * Define o valor da propriedade vlrTotalRetPrinc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalRetPrinc(String value) {
						this.vlrTotalRetPrinc = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalRetAdic.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getVlrTotalRetAdic() {
						return vlrTotalRetAdic;
					}

					/**
					 * Define o valor da propriedade vlrTotalRetAdic.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalRetAdic(String value) {
						this.vlrTotalRetAdic = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalNRetPrinc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getVlrTotalNRetPrinc() {
						return vlrTotalNRetPrinc;
					}

					/**
					 * Define o valor da propriedade vlrTotalNRetPrinc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalNRetPrinc(String value) {
						this.vlrTotalNRetPrinc = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalNRetAdic.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getVlrTotalNRetAdic() {
						return vlrTotalNRetAdic;
					}

					/**
					 * Define o valor da propriedade vlrTotalNRetAdic.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalNRetAdic(String value) {
						this.vlrTotalNRetAdic = value;
					}

					/**
					 * Obt�m o valor da propriedade indCPRB.
					 * 
					 */
					public short getIndCPRB() {
						return indCPRB;
					}

					/**
					 * Define o valor da propriedade indCPRB.
					 * 
					 */
					public void setIndCPRB(short value) {
						this.indCPRB = value;
					}

					/**
					 * Gets the value of the nfs property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the nfs property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getNfs().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs }
					 * 
					 * 
					 */
					public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> getNfs() {
						if (nfs == null) {
							nfs = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs>();
						}
						return this.nfs;
					}

					/**
					 * Gets the value of the infoProcRetPr property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the infoProcRetPr property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoProcRetPr().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr }
					 * 
					 * 
					 */
					public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> getInfoProcRetPr() {
						if (infoProcRetPr == null) {
							infoProcRetPr = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr>();
						}
						return this.infoProcRetPr;
					}

					/**
					 * Gets the value of the infoProcRetAd property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the infoProcRetAd property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoProcRetAd().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd }
					 * 
					 * 
					 */
					public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> getInfoProcRetAd() {
						if (infoProcRetAd == null) {
							infoProcRetAd = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd>();
						}
						return this.infoProcRetAd;
					}

					/**
					 * Informa��es de processos relacionados a n�o reten��o de
					 * contribui��o previdenci�ria adicional
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="tpProcRetAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
					 *               &lt;minInclusive value="1"/>
					 *               &lt;maxInclusive value="2"/>
					 *               &lt;pattern value="[1|2]{1}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nrProcRetAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="21"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codSuspAdic" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="valorAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "tpProcRetAdic", "nrProcRetAdic", "codSuspAdic", "valorAdic" })
					public static class InfoProcRetAd {

						protected short tpProcRetAdic;
						@XmlElement(required = true)
						protected String nrProcRetAdic;
						protected String codSuspAdic;
						@XmlElement(required = true)
						protected String valorAdic;

						/**
						 * Obt�m o valor da propriedade tpProcRetAdic.
						 * 
						 */
						public short getTpProcRetAdic() {
							return tpProcRetAdic;
						}

						/**
						 * Define o valor da propriedade tpProcRetAdic.
						 * 
						 */
						public void setTpProcRetAdic(short value) {
							this.tpProcRetAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade nrProcRetAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNrProcRetAdic() {
							return nrProcRetAdic;
						}

						/**
						 * Define o valor da propriedade nrProcRetAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNrProcRetAdic(String value) {
							this.nrProcRetAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade codSuspAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCodSuspAdic() {
							return codSuspAdic;
						}

						/**
						 * Define o valor da propriedade codSuspAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSuspAdic(String value) {
							this.codSuspAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade valorAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getValorAdic() {
							return valorAdic;
						}

						/**
						 * Define o valor da propriedade valorAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setValorAdic(String value) {
							this.valorAdic = value;
						}

					}

					/**
					 * Informa��es de processos relacionados a n�o reten��o de
					 * contribui��o previdenci�ria
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="tpProcRetPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
					 *               &lt;minInclusive value="1"/>
					 *               &lt;maxInclusive value="2"/>
					 *               &lt;pattern value="[1|2]{1}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nrProcRetPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="21"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codSuspPrinc" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="valorPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "tpProcRetPrinc", "nrProcRetPrinc", "codSuspPrinc",
							"valorPrinc" })
					public static class InfoProcRetPr {

						protected short tpProcRetPrinc;
						@XmlElement(required = true)
						protected String nrProcRetPrinc;
						protected String codSuspPrinc;
						@XmlElement(required = true)
						protected String valorPrinc;

						/**
						 * Obt�m o valor da propriedade tpProcRetPrinc.
						 * 
						 */
						public short getTpProcRetPrinc() {
							return tpProcRetPrinc;
						}

						/**
						 * Define o valor da propriedade tpProcRetPrinc.
						 * 
						 */
						public void setTpProcRetPrinc(short value) {
							this.tpProcRetPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade nrProcRetPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNrProcRetPrinc() {
							return nrProcRetPrinc;
						}

						/**
						 * Define o valor da propriedade nrProcRetPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNrProcRetPrinc(String value) {
							this.nrProcRetPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade codSuspPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCodSuspPrinc() {
							return codSuspPrinc;
						}

						/**
						 * Define o valor da propriedade codSuspPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSuspPrinc(String value) {
							this.codSuspPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade valorPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getValorPrinc() {
							return valorPrinc;
						}

						/**
						 * Define o valor da propriedade valorPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setValorPrinc(String value) {
							this.valorPrinc = value;
						}

					}

					/**
					 * 
					 * Detalhamento das notas fiscais de servi�os prestados pela
					 * empresa identificada no registro superior.
					 * 
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="serie">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="5"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="numDocto">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="15"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="dtEmissaoNF">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="vlrBruto">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="obs" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="250"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="infoTpServ" maxOccurs="9">
					 *           &lt;complexType>
					 *             &lt;complexContent>
					 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *                 &lt;sequence>
					 *                   &lt;element name="tpServico">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;length value="9"/>
					 *                         &lt;pattern value="[0-9]{9}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrBaseRet">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrRetencao">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrRetSub" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrNRetPrinc" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos15" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos20" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos25" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrAdicional" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrNRetAdic" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                 &lt;/sequence>
					 *               &lt;/restriction>
					 *             &lt;/complexContent>
					 *           &lt;/complexType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "serie", "numDocto", "dtEmissaoNF", "vlrBruto", "obs",
							"infoTpServ" })
					public static class Nfs {

						@XmlElement(required = true)
						protected String serie;
						@XmlElement(required = true)
						protected String numDocto;
						@XmlElement(required = true)
						protected XMLGregorianCalendar dtEmissaoNF;
						@XmlElement(required = true)
						protected String vlrBruto;
						protected String obs;
						@XmlElement(required = true)
						protected List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> infoTpServ;

						/**
						 * Obt�m o valor da propriedade serie.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getSerie() {
							return serie;
						}

						/**
						 * Define o valor da propriedade serie.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setSerie(String value) {
							this.serie = value;
						}

						/**
						 * Obt�m o valor da propriedade numDocto.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getNumDocto() {
							return numDocto;
						}

						/**
						 * Define o valor da propriedade numDocto.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNumDocto(String value) {
							this.numDocto = value;
						}

						/**
						 * Obt�m o valor da propriedade dtEmissaoNF.
						 * 
						 * @return possible object is
						 *         {@link XMLGregorianCalendar }
						 * 
						 */
						public XMLGregorianCalendar getDtEmissaoNF() {
							return dtEmissaoNF;
						}

						/**
						 * Define o valor da propriedade dtEmissaoNF.
						 * 
						 * @param value
						 *            allowed object is
						 *            {@link XMLGregorianCalendar }
						 * 
						 */
						public void setDtEmissaoNF(XMLGregorianCalendar value) {
							this.dtEmissaoNF = value;
						}

						/**
						 * Obt�m o valor da propriedade vlrBruto.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getVlrBruto() {
							return vlrBruto;
						}

						/**
						 * Define o valor da propriedade vlrBruto.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setVlrBruto(String value) {
							this.vlrBruto = value;
						}

						/**
						 * Obt�m o valor da propriedade obs.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getObs() {
							return obs;
						}

						/**
						 * Define o valor da propriedade obs.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setObs(String value) {
							this.obs = value;
						}

						/**
						 * Gets the value of the infoTpServ property.
						 * 
						 * <p>
						 * This accessor method returns a reference to the live
						 * list, not a snapshot. Therefore any modification you
						 * make to the returned list will be present inside the
						 * JAXB object. This is why there is not a
						 * <CODE>set</CODE> method for the infoTpServ property.
						 * 
						 * <p>
						 * For example, to add a new item, do as follows:
						 * 
						 * <pre>
						 * getInfoTpServ().add(newItem);
						 * </pre>
						 * 
						 * 
						 * <p>
						 * Objects of the following type(s) are allowed in the
						 * list
						 * {@link Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ }
						 * 
						 * 
						 */
						public List<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ> getInfoTpServ() {
							if (infoTpServ == null) {
								infoTpServ = new ArrayList<Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.InfoTpServ>();
							}
							return this.infoTpServ;
						}

						/**
						 * Informa��es sobre os tipos de Servi�os constantes da
						 * Nota Fiscal
						 * 
						 * <p>
						 * Classe Java de anonymous complex type.
						 * 
						 * <p>
						 * O seguinte fragmento do esquema especifica o conte�do
						 * esperado contido dentro desta classe.
						 * 
						 * <pre>
						 * &lt;complexType>
						 *   &lt;complexContent>
						 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
						 *       &lt;sequence>
						 *         &lt;element name="tpServico">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;length value="9"/>
						 *               &lt;pattern value="[0-9]{9}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrBaseRet">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrRetencao">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrRetSub" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrNRetPrinc" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos15" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos20" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos25" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrAdicional" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrNRetAdic" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *       &lt;/sequence>
						 *     &lt;/restriction>
						 *   &lt;/complexContent>
						 * &lt;/complexType>
						 * </pre>
						 * 
						 * 
						 */
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "tpServico", "vlrBaseRet", "vlrRetencao", "vlrRetSub",
								"vlrNRetPrinc", "vlrServicos15", "vlrServicos20", "vlrServicos25", "vlrAdicional",
								"vlrNRetAdic" })
						public static class InfoTpServ {

							@XmlElement(required = true)
							protected String tpServico;
							@XmlElement(required = true)
							protected String vlrBaseRet;
							@XmlElement(required = true)
							protected String vlrRetencao;
							protected String vlrRetSub;
							protected String vlrNRetPrinc;
							protected String vlrServicos15;
							protected String vlrServicos20;
							protected String vlrServicos25;
							protected String vlrAdicional;
							protected String vlrNRetAdic;

							/**
							 * Obt�m o valor da propriedade tpServico.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getTpServico() {
								return tpServico;
							}

							/**
							 * Define o valor da propriedade tpServico.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setTpServico(String value) {
								this.tpServico = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrBaseRet.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrBaseRet() {
								return vlrBaseRet;
							}

							/**
							 * Define o valor da propriedade vlrBaseRet.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrBaseRet(String value) {
								this.vlrBaseRet = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrRetencao.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrRetencao() {
								return vlrRetencao;
							}

							/**
							 * Define o valor da propriedade vlrRetencao.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrRetencao(String value) {
								this.vlrRetencao = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrRetSub.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrRetSub() {
								return vlrRetSub;
							}

							/**
							 * Define o valor da propriedade vlrRetSub.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrRetSub(String value) {
								this.vlrRetSub = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrNRetPrinc.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrNRetPrinc() {
								return vlrNRetPrinc;
							}

							/**
							 * Define o valor da propriedade vlrNRetPrinc.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrNRetPrinc(String value) {
								this.vlrNRetPrinc = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos15.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrServicos15() {
								return vlrServicos15;
							}

							/**
							 * Define o valor da propriedade vlrServicos15.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos15(String value) {
								this.vlrServicos15 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos20.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrServicos20() {
								return vlrServicos20;
							}

							/**
							 * Define o valor da propriedade vlrServicos20.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos20(String value) {
								this.vlrServicos20 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos25.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrServicos25() {
								return vlrServicos25;
							}

							/**
							 * Define o valor da propriedade vlrServicos25.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos25(String value) {
								this.vlrServicos25 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrAdicional.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrAdicional() {
								return vlrAdicional;
							}

							/**
							 * Define o valor da propriedade vlrAdicional.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrAdicional(String value) {
								this.vlrAdicional = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrNRetAdic.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							public String getVlrNRetAdic() {
								return vlrNRetAdic;
							}

							/**
							 * Define o valor da propriedade vlrNRetAdic.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrNRetAdic(String value) {
								this.vlrNRetAdic = value;
							}

						}

					}

				}

			}

		}

	}

}
