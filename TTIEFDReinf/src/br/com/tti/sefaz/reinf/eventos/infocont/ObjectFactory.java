//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2017.12.11 �s 11:48:53 AM BRST 
//


package br.com.tti.sefaz.reinf.eventos.infocont;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.tti.sefaz.reinf.eventos.infocont package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.tti.sefaz.reinf.eventos.infocont
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri }
     * 
     */
    public Reinf.EvtInfoContri createReinfEvtInfoContri() {
        return new Reinf.EvtInfoContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri createReinfEvtInfoContriInfoContri() {
        return new Reinf.EvtInfoContri.InfoContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Exclusao }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Exclusao createReinfEvtInfoContriInfoContriExclusao() {
        return new Reinf.EvtInfoContri.InfoContri.Exclusao();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Alteracao }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Alteracao createReinfEvtInfoContriInfoContriAlteracao() {
        return new Reinf.EvtInfoContri.InfoContri.Alteracao();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro createReinfEvtInfoContriInfoContriAlteracaoInfoCadastro() {
        return new Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Inclusao }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Inclusao createReinfEvtInfoContriInfoContriInclusao() {
        return new Reinf.EvtInfoContri.InfoContri.Inclusao();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro createReinfEvtInfoContriInfoContriInclusaoInfoCadastro() {
        return new Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.IdeEvento }
     * 
     */
    public Reinf.EvtInfoContri.IdeEvento createReinfEvtInfoContriIdeEvento() {
        return new Reinf.EvtInfoContri.IdeEvento();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.IdeContri }
     * 
     */
    public Reinf.EvtInfoContri.IdeContri createReinfEvtInfoContriIdeContri() {
        return new Reinf.EvtInfoContri.IdeContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo createReinfEvtInfoContriInfoContriExclusaoIdePeriodo() {
        return new Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo createReinfEvtInfoContriInfoContriAlteracaoIdePeriodo() {
        return new Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade createReinfEvtInfoContriInfoContriAlteracaoNovaValidade() {
        return new Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato createReinfEvtInfoContriInfoContriAlteracaoInfoCadastroContato() {
        return new Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse createReinfEvtInfoContriInfoContriAlteracaoInfoCadastroSoftHouse() {
        return new Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR createReinfEvtInfoContriInfoContriAlteracaoInfoCadastroInfoEFR() {
        return new Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo createReinfEvtInfoContriInfoContriInclusaoIdePeriodo() {
        return new Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato createReinfEvtInfoContriInfoContriInclusaoInfoCadastroContato() {
        return new Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse createReinfEvtInfoContriInfoContriInclusaoInfoCadastroSoftHouse() {
        return new Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse();
    }

    /**
     * Create an instance of {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR }
     * 
     */
    public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR createReinfEvtInfoContriInfoContriInclusaoInfoCadastroInfoEFR() {
        return new Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR();
    }

}
