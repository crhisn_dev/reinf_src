//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.02.06 �s 11:06:30 AM BRST 
//


package br.com.tti.sefaz.reinf.eventos.tabprocesso;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.tti.sefaz.reinf.eventos.tabprocesso package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.tti.sefaz.reinf.eventos.tabprocesso
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso }
     * 
     */
    public Reinf.EvtTabProcesso createReinfEvtTabProcesso() {
        return new Reinf.EvtTabProcesso();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso createReinfEvtTabProcessoInfoProcesso() {
        return new Reinf.EvtTabProcesso.InfoProcesso();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Exclusao }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Exclusao createReinfEvtTabProcessoInfoProcessoExclusao() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Exclusao();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Alteracao createReinfEvtTabProcessoInfoProcessoAlteracao() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Alteracao();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso createReinfEvtTabProcessoInfoProcessoAlteracaoIdeProcesso() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Inclusao createReinfEvtTabProcessoInfoProcessoInclusao() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Inclusao();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso createReinfEvtTabProcessoInfoProcessoInclusaoIdeProcesso() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.IdeEvento }
     * 
     */
    public Reinf.EvtTabProcesso.IdeEvento createReinfEvtTabProcessoIdeEvento() {
        return new Reinf.EvtTabProcesso.IdeEvento();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.IdeContri }
     * 
     */
    public Reinf.EvtTabProcesso.IdeContri createReinfEvtTabProcessoIdeContri() {
        return new Reinf.EvtTabProcesso.IdeContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso createReinfEvtTabProcessoInfoProcessoExclusaoIdeProcesso() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade createReinfEvtTabProcessoInfoProcessoAlteracaoNovaValidade() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.InfoSusp }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.InfoSusp createReinfEvtTabProcessoInfoProcessoAlteracaoIdeProcessoInfoSusp() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.InfoSusp();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud createReinfEvtTabProcessoInfoProcessoAlteracaoIdeProcessoDadosProcJud() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.InfoSusp }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.InfoSusp createReinfEvtTabProcessoInfoProcessoInclusaoIdeProcessoInfoSusp() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.InfoSusp();
    }

    /**
     * Create an instance of {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud }
     * 
     */
    public Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud createReinfEvtTabProcessoInfoProcessoInclusaoIdeProcessoDadosProcJud() {
        return new Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud();
    }

}
