//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.02.06 �s 11:06:30 AM BRST 
//

package br.com.tti.sefaz.reinf.eventos.tabprocesso;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtTabProcesso">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="[1|2|3]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoProcesso">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;choice>
 *                               &lt;element name="inclusao" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ideProcesso">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="tpProc">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                         &lt;pattern value="[1|2]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="nrProc">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;minLength value="1"/>
 *                                                         &lt;maxLength value="21"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indAutoria">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                         &lt;pattern value="[1|2]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="infoSusp" maxOccurs="50">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="codSusp" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="[0-9]{0,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="indSusp">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="dtDecisao">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="indDeposito">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="[S|N]"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="dadosProcJud" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="ufVara">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;length value="2"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="codMunic">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="[0-9]{7}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="idVara">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;length value="2"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="alteracao" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ideProcesso">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="tpProc">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                         &lt;pattern value="[1|2]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="nrProc">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;minLength value="1"/>
 *                                                         &lt;maxLength value="21"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indAutoria">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                         &lt;pattern value="[1|2]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="infoSusp" maxOccurs="50">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="codSusp" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="[0-9]{0,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="indSusp">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="dtDecisao">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="indDeposito">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="[S|N]"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="dadosProcJud" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="ufVara">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;length value="2"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="codMunic">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="[0-9]{7}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="idVara">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;length value="2"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="novaValidade" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="exclusao" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ideProcesso">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="tpProc">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                         &lt;pattern value="[1|2]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="nrProc">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;minLength value="1"/>
 *                                                         &lt;maxLength value="21"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/choice>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtTabProcesso" })
@XmlRootElement(name = "Reinf")
public class Reinf {

	@XmlElement(required = true)
	protected Reinf.EvtTabProcesso evtTabProcesso;

	/**
	 * Obt�m o valor da propriedade evtTabProcesso.
	 * 
	 * @return possible object is {@link Reinf.EvtTabProcesso }
	 * 
	 */
	public Reinf.EvtTabProcesso getEvtTabProcesso() {
		return evtTabProcesso;
	}

	/**
	 * Define o valor da propriedade evtTabProcesso.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtTabProcesso }
	 * 
	 */
	public void setEvtTabProcesso(Reinf.EvtTabProcesso value) {
		this.evtTabProcesso = value;
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido
	 * dentro desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="[1|2|3]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="infoProcesso">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;choice>
	 *                     &lt;element name="inclusao" minOccurs="0">
	 *                       &lt;complexType>
	 *                         &lt;complexContent>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                             &lt;sequence>
	 *                               &lt;element name="ideProcesso">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="tpProc">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                               &lt;pattern value="[1|2]"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="nrProc">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;minLength value="1"/>
	 *                                               &lt;maxLength value="21"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indAutoria">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                               &lt;pattern value="[1|2]"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="infoSusp" maxOccurs="50">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="codSusp" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="[0-9]{0,14}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="indSusp">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="dtDecisao">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="indDeposito">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="[S|N]"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="dadosProcJud" minOccurs="0">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="ufVara">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;length value="2"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="codMunic">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="[0-9]{7}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="idVara">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;length value="2"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                             &lt;/sequence>
	 *                           &lt;/restriction>
	 *                         &lt;/complexContent>
	 *                       &lt;/complexType>
	 *                     &lt;/element>
	 *                     &lt;element name="alteracao" minOccurs="0">
	 *                       &lt;complexType>
	 *                         &lt;complexContent>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                             &lt;sequence>
	 *                               &lt;element name="ideProcesso">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="tpProc">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                               &lt;pattern value="[1|2]"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="nrProc">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;minLength value="1"/>
	 *                                               &lt;maxLength value="21"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="indAutoria">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                               &lt;pattern value="[1|2]"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="infoSusp" maxOccurs="50">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="codSusp" minOccurs="0">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="[0-9]{0,14}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="indSusp">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="dtDecisao">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="indDeposito">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="[S|N]"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="dadosProcJud" minOccurs="0">
	 *                                           &lt;complexType>
	 *                                             &lt;complexContent>
	 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                 &lt;sequence>
	 *                                                   &lt;element name="ufVara">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;length value="2"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="codMunic">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;pattern value="[0-9]{7}"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                   &lt;element name="idVara">
	 *                                                     &lt;simpleType>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                         &lt;length value="2"/>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/simpleType>
	 *                                                   &lt;/element>
	 *                                                 &lt;/sequence>
	 *                                               &lt;/restriction>
	 *                                             &lt;/complexContent>
	 *                                           &lt;/complexType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                               &lt;element name="novaValidade" minOccurs="0">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                             &lt;/sequence>
	 *                           &lt;/restriction>
	 *                         &lt;/complexContent>
	 *                       &lt;/complexType>
	 *                     &lt;/element>
	 *                     &lt;element name="exclusao" minOccurs="0">
	 *                       &lt;complexType>
	 *                         &lt;complexContent>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                             &lt;sequence>
	 *                               &lt;element name="ideProcesso">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="tpProc">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                               &lt;pattern value="[1|2]"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="nrProc">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                               &lt;minLength value="1"/>
	 *                                               &lt;maxLength value="21"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="iniValid">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                         &lt;element name="fimValid" minOccurs="0">
	 *                                           &lt;simpleType>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/simpleType>
	 *                                         &lt;/element>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                             &lt;/sequence>
	 *                           &lt;/restriction>
	 *                         &lt;/complexContent>
	 *                       &lt;/complexType>
	 *                     &lt;/element>
	 *                   &lt;/choice>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri", "infoProcesso" })
	@XmlRootElement(name = "evtTabProcesso")
	public static class EvtTabProcesso {

		@XmlElement(required = true)
		protected Reinf.EvtTabProcesso.IdeEvento ideEvento;
		@XmlElement(required = true)
		protected Reinf.EvtTabProcesso.IdeContri ideContri;
		@XmlElement(required = true)
		protected Reinf.EvtTabProcesso.InfoProcesso infoProcesso;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtTabProcesso.IdeEvento }
		 * 
		 */
		public Reinf.EvtTabProcesso.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtTabProcesso.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtTabProcesso.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtTabProcesso.IdeContri }
		 * 
		 */
		public Reinf.EvtTabProcesso.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtTabProcesso.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtTabProcesso.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade infoProcesso.
		 * 
		 * @return possible object is {@link Reinf.EvtTabProcesso.InfoProcesso }
		 * 
		 */
		public Reinf.EvtTabProcesso.InfoProcesso getInfoProcesso() {
			return infoProcesso;
		}

		/**
		 * Define o valor da propriedade infoProcesso.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link Reinf.EvtTabProcesso.InfoProcesso }
		 * 
		 */
		public void setInfoProcesso(Reinf.EvtTabProcesso.InfoProcesso value) {
			this.infoProcesso = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		public static class IdeContri {

			protected short tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			public short getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(short value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="[1|2|3]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpAmb", "procEmi", "verProc" })
		public static class IdeEvento {

			protected short tpAmb;
			protected short procEmi;
			@XmlElement(required = true)
			protected String verProc;

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			public short getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(short value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			public short getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(short value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;choice>
		 *           &lt;element name="inclusao" minOccurs="0">
		 *             &lt;complexType>
		 *               &lt;complexContent>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                   &lt;sequence>
		 *                     &lt;element name="ideProcesso">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="tpProc">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                     &lt;pattern value="[1|2]"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="nrProc">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;minLength value="1"/>
		 *                                     &lt;maxLength value="21"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indAutoria">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                     &lt;pattern value="[1|2]"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="infoSusp" maxOccurs="50">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="codSusp" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="[0-9]{0,14}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="indSusp">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="dtDecisao">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="indDeposito">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="[S|N]"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                               &lt;element name="dadosProcJud" minOccurs="0">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="ufVara">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;length value="2"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="codMunic">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="[0-9]{7}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="idVara">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;length value="2"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                   &lt;/sequence>
		 *                 &lt;/restriction>
		 *               &lt;/complexContent>
		 *             &lt;/complexType>
		 *           &lt;/element>
		 *           &lt;element name="alteracao" minOccurs="0">
		 *             &lt;complexType>
		 *               &lt;complexContent>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                   &lt;sequence>
		 *                     &lt;element name="ideProcesso">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="tpProc">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                     &lt;pattern value="[1|2]"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="nrProc">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;minLength value="1"/>
		 *                                     &lt;maxLength value="21"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="indAutoria">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                     &lt;pattern value="[1|2]"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="infoSusp" maxOccurs="50">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="codSusp" minOccurs="0">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="[0-9]{0,14}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="indSusp">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="dtDecisao">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="indDeposito">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="[S|N]"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                               &lt;element name="dadosProcJud" minOccurs="0">
		 *                                 &lt;complexType>
		 *                                   &lt;complexContent>
		 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                       &lt;sequence>
		 *                                         &lt;element name="ufVara">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;length value="2"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="codMunic">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;pattern value="[0-9]{7}"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                         &lt;element name="idVara">
		 *                                           &lt;simpleType>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                               &lt;length value="2"/>
		 *                                             &lt;/restriction>
		 *                                           &lt;/simpleType>
		 *                                         &lt;/element>
		 *                                       &lt;/sequence>
		 *                                     &lt;/restriction>
		 *                                   &lt;/complexContent>
		 *                                 &lt;/complexType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                     &lt;element name="novaValidade" minOccurs="0">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                   &lt;/sequence>
		 *                 &lt;/restriction>
		 *               &lt;/complexContent>
		 *             &lt;/complexType>
		 *           &lt;/element>
		 *           &lt;element name="exclusao" minOccurs="0">
		 *             &lt;complexType>
		 *               &lt;complexContent>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                   &lt;sequence>
		 *                     &lt;element name="ideProcesso">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="tpProc">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                     &lt;pattern value="[1|2]"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="nrProc">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                     &lt;minLength value="1"/>
		 *                                     &lt;maxLength value="21"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="iniValid">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                               &lt;element name="fimValid" minOccurs="0">
		 *                                 &lt;simpleType>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/simpleType>
		 *                               &lt;/element>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                   &lt;/sequence>
		 *                 &lt;/restriction>
		 *               &lt;/complexContent>
		 *             &lt;/complexType>
		 *           &lt;/element>
		 *         &lt;/choice>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "inclusao", "alteracao", "exclusao" })
		public static class InfoProcesso {

			protected Reinf.EvtTabProcesso.InfoProcesso.Inclusao inclusao;
			protected Reinf.EvtTabProcesso.InfoProcesso.Alteracao alteracao;
			protected Reinf.EvtTabProcesso.InfoProcesso.Exclusao exclusao;

			/**
			 * Obt�m o valor da propriedade inclusao.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao }
			 * 
			 */
			public Reinf.EvtTabProcesso.InfoProcesso.Inclusao getInclusao() {
				return inclusao;
			}

			/**
			 * Define o valor da propriedade inclusao.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao }
			 * 
			 */
			public void setInclusao(Reinf.EvtTabProcesso.InfoProcesso.Inclusao value) {
				this.inclusao = value;
			}

			/**
			 * Obt�m o valor da propriedade alteracao.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao }
			 * 
			 */
			public Reinf.EvtTabProcesso.InfoProcesso.Alteracao getAlteracao() {
				return alteracao;
			}

			/**
			 * Define o valor da propriedade alteracao.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao }
			 * 
			 */
			public void setAlteracao(Reinf.EvtTabProcesso.InfoProcesso.Alteracao value) {
				this.alteracao = value;
			}

			/**
			 * Obt�m o valor da propriedade exclusao.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Exclusao }
			 * 
			 */
			public Reinf.EvtTabProcesso.InfoProcesso.Exclusao getExclusao() {
				return exclusao;
			}

			/**
			 * Define o valor da propriedade exclusao.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Exclusao }
			 * 
			 */
			public void setExclusao(Reinf.EvtTabProcesso.InfoProcesso.Exclusao value) {
				this.exclusao = value;
			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado
			 * contido dentro desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="ideProcesso">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="tpProc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;pattern value="[1|2]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="nrProc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="1"/>
			 *                         &lt;maxLength value="21"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indAutoria">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;pattern value="[1|2]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoSusp" maxOccurs="50">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="codSusp" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="indSusp">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="dtDecisao">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="indDeposito">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[S|N]"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="dadosProcJud" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="ufVara">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;length value="2"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codMunic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{7}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="idVara">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;length value="2"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *         &lt;element name="novaValidade" minOccurs="0">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "ideProcesso", "novaValidade" })
			public static class Alteracao {

				@XmlElement(required = true)
				protected Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso ideProcesso;
				protected Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade novaValidade;

				/**
				 * Obt�m o valor da propriedade ideProcesso.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso }
				 * 
				 */
				public Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso getIdeProcesso() {
					return ideProcesso;
				}

				/**
				 * Define o valor da propriedade ideProcesso.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso }
				 * 
				 */
				public void setIdeProcesso(Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso value) {
					this.ideProcesso = value;
				}

				/**
				 * Obt�m o valor da propriedade novaValidade.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade }
				 * 
				 */
				public Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade getNovaValidade() {
					return novaValidade;
				}

				/**
				 * Define o valor da propriedade novaValidade.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade }
				 * 
				 */
				public void setNovaValidade(Reinf.EvtTabProcesso.InfoProcesso.Alteracao.NovaValidade value) {
					this.novaValidade = value;
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="tpProc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;pattern value="[1|2]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="nrProc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="1"/>
				 *               &lt;maxLength value="21"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indAutoria">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;pattern value="[1|2]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="infoSusp" maxOccurs="50">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="codSusp" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="indSusp">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="dtDecisao">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="indDeposito">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[S|N]"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="dadosProcJud" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="ufVara">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;length value="2"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codMunic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{7}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="idVara">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;length value="2"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "tpProc", "nrProc", "iniValid", "fimValid", "indAutoria", "infoSusp",
						"dadosProcJud" })
				public static class IdeProcesso {

					protected short tpProc;
					@XmlElement(required = true)
					protected String nrProc;
					@XmlElement(required = true)
					protected XMLGregorianCalendar iniValid;
					protected XMLGregorianCalendar fimValid;
					protected short indAutoria;
					@XmlElement(required = true)
					protected List<Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.InfoSusp> infoSusp;
					protected Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud dadosProcJud;

					/**
					 * Obt�m o valor da propriedade tpProc.
					 * 
					 */
					public short getTpProc() {
						return tpProc;
					}

					/**
					 * Define o valor da propriedade tpProc.
					 * 
					 */
					public void setTpProc(short value) {
						this.tpProc = value;
					}

					/**
					 * Obt�m o valor da propriedade nrProc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getNrProc() {
						return nrProc;
					}

					/**
					 * Define o valor da propriedade nrProc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setNrProc(String value) {
						this.nrProc = value;
					}

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setIniValid(XMLGregorianCalendar value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setFimValid(XMLGregorianCalendar value) {
						this.fimValid = value;
					}

					/**
					 * Obt�m o valor da propriedade indAutoria.
					 * 
					 */
					public short getIndAutoria() {
						return indAutoria;
					}

					/**
					 * Define o valor da propriedade indAutoria.
					 * 
					 */
					public void setIndAutoria(short value) {
						this.indAutoria = value;
					}

					/**
					 * Gets the value of the infoSusp property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the infoSusp property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoSusp().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.InfoSusp }
					 * 
					 * 
					 */
					public List<Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.InfoSusp> getInfoSusp() {
						if (infoSusp == null) {
							infoSusp = new ArrayList<Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.InfoSusp>();
						}
						return this.infoSusp;
					}

					/**
					 * Obt�m o valor da propriedade dadosProcJud.
					 * 
					 * @return possible object is
					 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud }
					 * 
					 */
					public Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud getDadosProcJud() {
						return dadosProcJud;
					}

					/**
					 * Define o valor da propriedade dadosProcJud.
					 * 
					 * @param value
					 *            allowed object is
					 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud }
					 * 
					 */
					public void setDadosProcJud(
							Reinf.EvtTabProcesso.InfoProcesso.Alteracao.IdeProcesso.DadosProcJud value) {
						this.dadosProcJud = value;
					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="ufVara">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;length value="2"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codMunic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{7}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="idVara">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;length value="2"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "ufVara", "codMunic", "idVara" })
					public static class DadosProcJud {

						@XmlElement(required = true)
						protected String ufVara;
						@XmlElement(required = true)
						protected String codMunic;
						@XmlElement(required = true)
						protected String idVara;

						/**
						 * Obt�m o valor da propriedade ufVara.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getUfVara() {
							return ufVara;
						}

						/**
						 * Define o valor da propriedade ufVara.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setUfVara(String value) {
							this.ufVara = value;
						}

						/**
						 * Obt�m o valor da propriedade codMunic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCodMunic() {
							return codMunic;
						}

						/**
						 * Define o valor da propriedade codMunic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodMunic(String value) {
							this.codMunic = value;
						}

						/**
						 * Obt�m o valor da propriedade idVara.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIdVara() {
							return idVara;
						}

						/**
						 * Define o valor da propriedade idVara.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIdVara(String value) {
							this.idVara = value;
						}

					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="codSusp" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="indSusp">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="dtDecisao">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="indDeposito">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[S|N]"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "codSusp", "indSusp", "dtDecisao", "indDeposito" })
					public static class InfoSusp {

						protected String codSusp;
						@XmlElement(required = true)
						protected String indSusp;
						@XmlElement(required = true)
						protected XMLGregorianCalendar dtDecisao;
						@XmlElement(required = true)
						protected String indDeposito;

						/**
						 * Obt�m o valor da propriedade codSusp.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCodSusp() {
							return codSusp;
						}

						/**
						 * Define o valor da propriedade codSusp.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSusp(String value) {
							this.codSusp = value;
						}

						/**
						 * Obt�m o valor da propriedade indSusp.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIndSusp() {
							return indSusp;
						}

						/**
						 * Define o valor da propriedade indSusp.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIndSusp(String value) {
							this.indSusp = value;
						}

						/**
						 * Obt�m o valor da propriedade dtDecisao.
						 * 
						 * @return possible object is
						 *         {@link XMLGregorianCalendar }
						 * 
						 */
						public XMLGregorianCalendar getDtDecisao() {
							return dtDecisao;
						}

						/**
						 * Define o valor da propriedade dtDecisao.
						 * 
						 * @param value
						 *            allowed object is
						 *            {@link XMLGregorianCalendar }
						 * 
						 */
						public void setDtDecisao(XMLGregorianCalendar value) {
							this.dtDecisao = value;
						}

						/**
						 * Obt�m o valor da propriedade indDeposito.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIndDeposito() {
							return indDeposito;
						}

						/**
						 * Define o valor da propriedade indDeposito.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIndDeposito(String value) {
							this.indDeposito = value;
						}

					}

				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "iniValid", "fimValid" })
				public static class NovaValidade {

					@XmlElement(required = true)
					protected XMLGregorianCalendar iniValid;
					protected XMLGregorianCalendar fimValid;

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setIniValid(XMLGregorianCalendar value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setFimValid(XMLGregorianCalendar value) {
						this.fimValid = value;
					}

				}

			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado
			 * contido dentro desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="ideProcesso">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="tpProc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;pattern value="[1|2]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="nrProc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="1"/>
			 *                         &lt;maxLength value="21"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "ideProcesso" })
			public static class Exclusao {

				@XmlElement(required = true)
				protected Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso ideProcesso;

				/**
				 * Obt�m o valor da propriedade ideProcesso.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso }
				 * 
				 */
				public Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso getIdeProcesso() {
					return ideProcesso;
				}

				/**
				 * Define o valor da propriedade ideProcesso.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso }
				 * 
				 */
				public void setIdeProcesso(Reinf.EvtTabProcesso.InfoProcesso.Exclusao.IdeProcesso value) {
					this.ideProcesso = value;
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="tpProc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;pattern value="[1|2]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="nrProc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="1"/>
				 *               &lt;maxLength value="21"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "tpProc", "nrProc", "iniValid", "fimValid" })
				public static class IdeProcesso {

					protected short tpProc;
					@XmlElement(required = true)
					protected String nrProc;
					@XmlElement(required = true)
					protected XMLGregorianCalendar iniValid;
					protected XMLGregorianCalendar fimValid;

					/**
					 * Obt�m o valor da propriedade tpProc.
					 * 
					 */
					public short getTpProc() {
						return tpProc;
					}

					/**
					 * Define o valor da propriedade tpProc.
					 * 
					 */
					public void setTpProc(short value) {
						this.tpProc = value;
					}

					/**
					 * Obt�m o valor da propriedade nrProc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getNrProc() {
						return nrProc;
					}

					/**
					 * Define o valor da propriedade nrProc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setNrProc(String value) {
						this.nrProc = value;
					}

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setIniValid(XMLGregorianCalendar value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setFimValid(XMLGregorianCalendar value) {
						this.fimValid = value;
					}

				}

			}

			/**
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado
			 * contido dentro desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="ideProcesso">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="tpProc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;pattern value="[1|2]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="nrProc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="1"/>
			 *                         &lt;maxLength value="21"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="iniValid">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="fimValid" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
			 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indAutoria">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;pattern value="[1|2]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoSusp" maxOccurs="50">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="codSusp" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="indSusp">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="dtDecisao">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="indDeposito">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[S|N]"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="dadosProcJud" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="ufVara">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;length value="2"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codMunic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{7}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="idVara">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;length value="2"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "ideProcesso" })
			public static class Inclusao {

				@XmlElement(required = true)
				protected Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso ideProcesso;

				/**
				 * Obt�m o valor da propriedade ideProcesso.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso }
				 * 
				 */
				public Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso getIdeProcesso() {
					return ideProcesso;
				}

				/**
				 * Define o valor da propriedade ideProcesso.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso }
				 * 
				 */
				public void setIdeProcesso(Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso value) {
					this.ideProcesso = value;
				}

				/**
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do
				 * esperado contido dentro desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="tpProc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;pattern value="[1|2]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="nrProc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="1"/>
				 *               &lt;maxLength value="21"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="iniValid">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="fimValid" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
				 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indAutoria">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;pattern value="[1|2]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="infoSusp" maxOccurs="50">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="codSusp" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="indSusp">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="dtDecisao">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="indDeposito">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[S|N]"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="dadosProcJud" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="ufVara">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;length value="2"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codMunic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{7}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="idVara">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;length value="2"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "tpProc", "nrProc", "iniValid", "fimValid", "indAutoria", "infoSusp",
						"dadosProcJud" })
				public static class IdeProcesso {

					protected short tpProc;
					@XmlElement(required = true)
					protected String nrProc;
					@XmlElement(required = true)
					protected XMLGregorianCalendar iniValid;
					protected XMLGregorianCalendar fimValid;
					protected short indAutoria;
					@XmlElement(required = true)
					protected List<Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.InfoSusp> infoSusp;
					protected Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud dadosProcJud;

					/**
					 * Obt�m o valor da propriedade tpProc.
					 * 
					 */
					public short getTpProc() {
						return tpProc;
					}

					/**
					 * Define o valor da propriedade tpProc.
					 * 
					 */
					public void setTpProc(short value) {
						this.tpProc = value;
					}

					/**
					 * Obt�m o valor da propriedade nrProc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getNrProc() {
						return nrProc;
					}

					/**
					 * Define o valor da propriedade nrProc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setNrProc(String value) {
						this.nrProc = value;
					}

					/**
					 * Obt�m o valor da propriedade iniValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getIniValid() {
						return iniValid;
					}

					/**
					 * Define o valor da propriedade iniValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setIniValid(XMLGregorianCalendar value) {
						this.iniValid = value;
					}

					/**
					 * Obt�m o valor da propriedade fimValid.
					 * 
					 * @return possible object is {@link XMLGregorianCalendar }
					 * 
					 */
					public XMLGregorianCalendar getFimValid() {
						return fimValid;
					}

					/**
					 * Define o valor da propriedade fimValid.
					 * 
					 * @param value
					 *            allowed object is {@link XMLGregorianCalendar
					 *            }
					 * 
					 */
					public void setFimValid(XMLGregorianCalendar value) {
						this.fimValid = value;
					}

					/**
					 * Obt�m o valor da propriedade indAutoria.
					 * 
					 */
					public short getIndAutoria() {
						return indAutoria;
					}

					/**
					 * Define o valor da propriedade indAutoria.
					 * 
					 */
					public void setIndAutoria(short value) {
						this.indAutoria = value;
					}

					/**
					 * Gets the value of the infoSusp property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the infoSusp property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoSusp().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.InfoSusp }
					 * 
					 * 
					 */
					public List<Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.InfoSusp> getInfoSusp() {
						if (infoSusp == null) {
							infoSusp = new ArrayList<Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.InfoSusp>();
						}
						return this.infoSusp;
					}

					/**
					 * Obt�m o valor da propriedade dadosProcJud.
					 * 
					 * @return possible object is
					 *         {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud }
					 * 
					 */
					public Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud getDadosProcJud() {
						return dadosProcJud;
					}

					/**
					 * Define o valor da propriedade dadosProcJud.
					 * 
					 * @param value
					 *            allowed object is
					 *            {@link Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud }
					 * 
					 */
					public void setDadosProcJud(
							Reinf.EvtTabProcesso.InfoProcesso.Inclusao.IdeProcesso.DadosProcJud value) {
						this.dadosProcJud = value;
					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="ufVara">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;length value="2"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codMunic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{7}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="idVara">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;length value="2"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "ufVara", "codMunic", "idVara" })
					public static class DadosProcJud {

						@XmlElement(required = true)
						protected String ufVara;
						@XmlElement(required = true)
						protected String codMunic;
						@XmlElement(required = true)
						protected String idVara;

						/**
						 * Obt�m o valor da propriedade ufVara.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getUfVara() {
							return ufVara;
						}

						/**
						 * Define o valor da propriedade ufVara.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setUfVara(String value) {
							this.ufVara = value;
						}

						/**
						 * Obt�m o valor da propriedade codMunic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCodMunic() {
							return codMunic;
						}

						/**
						 * Define o valor da propriedade codMunic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodMunic(String value) {
							this.codMunic = value;
						}

						/**
						 * Obt�m o valor da propriedade idVara.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIdVara() {
							return idVara;
						}

						/**
						 * Define o valor da propriedade idVara.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIdVara(String value) {
							this.idVara = value;
						}

					}

					/**
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do
					 * esperado contido dentro desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="codSusp" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="indSusp">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="0[1-5]|0[8-9]|10|1[1-3]|9[0|2]"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="dtDecisao">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="indDeposito">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[S|N]"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "codSusp", "indSusp", "dtDecisao", "indDeposito" })
					public static class InfoSusp {

						protected String codSusp;
						@XmlElement(required = true)
						protected String indSusp;
						@XmlElement(required = true)
						protected XMLGregorianCalendar dtDecisao;
						@XmlElement(required = true)
						protected String indDeposito;

						/**
						 * Obt�m o valor da propriedade codSusp.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getCodSusp() {
							return codSusp;
						}

						/**
						 * Define o valor da propriedade codSusp.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSusp(String value) {
							this.codSusp = value;
						}

						/**
						 * Obt�m o valor da propriedade indSusp.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIndSusp() {
							return indSusp;
						}

						/**
						 * Define o valor da propriedade indSusp.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIndSusp(String value) {
							this.indSusp = value;
						}

						/**
						 * Obt�m o valor da propriedade dtDecisao.
						 * 
						 * @return possible object is
						 *         {@link XMLGregorianCalendar }
						 * 
						 */
						public XMLGregorianCalendar getDtDecisao() {
							return dtDecisao;
						}

						/**
						 * Define o valor da propriedade dtDecisao.
						 * 
						 * @param value
						 *            allowed object is
						 *            {@link XMLGregorianCalendar }
						 * 
						 */
						public void setDtDecisao(XMLGregorianCalendar value) {
							this.dtDecisao = value;
						}

						/**
						 * Obt�m o valor da propriedade indDeposito.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						public String getIndDeposito() {
							return indDeposito;
						}

						/**
						 * Define o valor da propriedade indDeposito.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setIndDeposito(String value) {
							this.indDeposito = value;
						}

					}

				}

			}

		}

	}

}
