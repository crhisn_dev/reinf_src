package br.com.tti.sefaz.signer;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.tti.sefaz.signer.Main;

public class AssinadorPKCS11 extends Main {
	// private Provider p;

	// private Certificate certificate = null;
	// private PrivateKey privateKey = null;

	/*
	 * public AssinadorPKCS11(String password, String xml, boolean t) throws
	 * Exception {
	 * 
	 * 
	 * Provider p = null;// new ERACOMProvider(); Security.addProvider(p);
	 * 
	 * System.out.println(p.getName()); get the eracom keystore - access to the
	 * adapter
	 * 
	 * KeyStore keyStore = KeyStore.getInstance("CRYPTOKI", p.getName());
	 * 
	 * 
	 * Provider p = new SunPKCS11(PropriedadesMain.getConfig());
	 * Security.addProvider(p);
	 * 
	 * System.out.println(p.getName()); KeyStore keyStore =
	 * KeyStore.getInstance("PKCS11");
	 * 
	 * 
	 * LOAD the keystore from the adapter - presenting the password if required
	 * 
	 * if (password == null) { keyStore.load(null, null); } else {
	 * keyStore.load(null, password.toCharArray());
	 * 
	 * Enumeration<String> aliess = keyStore.aliases();
	 * 
	 * while (aliess.hasMoreElements()) { String a = aliess.nextElement();
	 * System.out.println("Alies: " + a); try { Key aa = keyStore.getKey(a,
	 * password.toCharArray()); RSAPrivateCrtKey c = (RSAPrivateCrtKey) aa;
	 * this.privateKey = c;
	 * 
	 * Entry aa = keyStore.getEntry(a, new KeyStore.PasswordProtection(password
	 * .toCharArray()));
	 * 
	 * System.out.println(keyStore.isKeyEntry(a));
	 * System.out.println(aa.getClass().toString()); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * this.certificate = keyStore.getCertificate("itambe24062009");
	 * 
	 * String ass = assina(xml, "infNFe"); System.out.println(ass); }
	 * 
	 * 
	 * Key key = keyStore.getKey("itambe24062009", password.toCharArray());
	 * 
	 * RSAPrivateCrtKey c = (RSAPrivKeyCrt) key; this.privateKey = c;
	 * 
	 * this.certificate = keyStore.getCertificate("itambe24062009"); } }
	 */

	public static Hashtable<String, Provider> alreadyprovider = new Hashtable<String, Provider>();

	// public static Hashtable<String, KeyStore> alreadykeystore = new
	// Hashtable<String, KeyStore>();
	public AssinadorPKCS11(String configname, String senha, String alias) {

		Provider p = alreadyprovider.get(configname);
		// o teste chama este
		if (p == null) {
			p = new sun.security.pkcs11.SunPKCS11(configname);

			Security.addProvider(p);
			alreadyprovider.put(configname, p);
			System.out.println("Provedor criado:" + p.getName());
		}
		char[] pin = senha.toCharArray();

		// KeyStore ks = this.alreadykeystore.get(configname);

		KeyStore ks = null;

		try {
			ks = KeyStore.getInstance("pkcs11", p);
		} catch (KeyStoreException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Nao conseguiu criar keystore com pkcs11", ex);
			System.exit(1);
		}

		try {
			ks.load(null, pin);
		} catch (IOException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "certificado ou senha invalidos", ex);
			System.exit(2);
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "algoritmo do certificado invalido", ex);
			System.exit(3);
		} catch (CertificateException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "certificado ou senha invalidos", ex);
			System.exit(4);
		}

		/*
		 * try { Enumeration<String> enumeration = ks.aliases(); String alias =
		 * enumeration.nextElement(); // System.out.println("alias:" + alias);
		 * PrivateKey privateKey = (PrivateKey) ks.getKey(alias, pin);
		 * Certificate[] certs = ks.getCertificateChain(alias); //
		 * System.out.println("eureka!! " + privateKey); } catch (Exception e) {
		 * e.printStackTrace(); }
		 */

		Enumeration<String> aliases = null;
		try {
			aliases = ks.aliases();
		} catch (KeyStoreException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "nenhuma entrada no keystore", ex);
			System.exit(5);
		}

		KeyStore.PrivateKeyEntry entry = null;

		/*
		 * while (aliases.hasMoreElements()) { String alias1 = (String)
		 * aliases.nextElement(); System.out.println("founded alias:" + alias1);
		 * }
		 */
		// System.out.println("Provider: " + ks.getProvider().toString());
		// System.out.println("Nome do provider: " +
		// ks.getProvider().getName());
		/*
		 * try { System.out.println("Numero de objetos no keystore: " +
		 * ks.size()); } catch (KeyStoreException e2) { // TODO Auto-generated
		 * catch block e2.printStackTrace(); }
		 */
		try {
			if (alias == null || alias.trim().isEmpty()) {
				while (aliases.hasMoreElements()) {
					String alias1 = (String) aliases.nextElement();

					if (ks.isKeyEntry(alias1)) {
						// System.out.println("new founded alias valid!!:" +
						// alias1);
						System.out.println("Configuracao iniciada corretamente:" + configname + " alias:" + alias1);
						entry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias1, new KeyStore.PasswordProtection(pin));

						privateKey = entry.getPrivateKey();
						certificate = entry.getCertificate();

						if (this.privateKey != null)
							System.out.println("Class key instance:" + this.privateKey.getClass().getCanonicalName());
						if (this.certificate != null)
							System.out.println("Class cert instance:" + this.certificate.getClass().getCanonicalName());
						break;
					}
				}
			} else {
				while (aliases.hasMoreElements()) {
					String alias1 = (String) aliases.nextElement();

					if (ks.isKeyEntry(alias1) && alias1.trim().equals(alias.trim())) {
						// System.out.println("new founded alias valid!!:" +
						// alias1);
						System.out.println("Configuracao iniciada corretamente:" + configname + " alias:" + alias);
						entry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias1, new KeyStore.PasswordProtection(pin));

						privateKey = entry.getPrivateKey();
						certificate = entry.getCertificate();

						if (this.privateKey != null)
							System.out.println("Class key instance:" + this.privateKey.getClass().getCanonicalName());
						if (this.certificate != null)
							System.out.println("Class cert instance:" + this.certificate.getClass().getCanonicalName());
						break;
					}
				}
			}
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableEntryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
