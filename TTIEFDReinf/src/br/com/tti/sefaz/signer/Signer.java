package br.com.tti.sefaz.signer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;

import sun.security.x509.X509CertImpl;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.remote.CertificatesConfig;
import br.com.tti.sefaz.security.PasswordManager;
import br.com.tti.sefaz.systemconfig.SystemParameters;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.SIGNER_TYPE;
import br.com.tti.sefaz.util.KeywordPassword;
import br.com.tti.sefaz.util.Locator;

public class Signer implements SignerInterface {

	private Hashtable<String, AssinadorInt> signers;

	private PasswordManager pass;
	private ManagerInterface manager;

	public Signer() {
		this.manager = Locator.getManagerReference();
		this.pass = new PasswordManager();
		this.signers = new Hashtable<String, AssinadorInt>();
		assert this.manager != null;
		if (SystemParameters.signer.equals(SIGNER_TYPE.PKCS12_SIGNER))
			this.initSignersPKCS12();
		if (SystemParameters.signer.equals(SIGNER_TYPE.PKCS11_SIGNER))
			this.initSignersPKCS11();

	}

	public void initSignersPKCS11() {
		try {
			Hashtable<String, CNPJData> cnpjs = this.manager.getCNPJ();
			for (String cnpj : cnpjs.keySet()) {
				String password = this.pass.getPassword("pkcs11_password");

				AssinadorInt signer = new Main(password);
				this.signers.put(cnpj, signer);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void initSignersPKCS12() {
		try {
			Vector<CertificatesConfig> certificates = this.manager.getCertificates();

			for (CertificatesConfig certificate : certificates) {
				String pfxFile = certificate.getPfxFile();
				String pass = this.pass.getPassword(pfxFile);
				AssinadorInt a = null;

				if (pass == null) {
					if (pfxFile.endsWith(".pfx")) {
						pass = KeywordPassword
								.typePassword("Digite a senha do arquivo PFX para assinatura " + pfxFile + ": ");
						a = new Main(pfxFile, pass);
					} else {
						pass = KeywordPassword.typePassword("Digite o PIN " + pfxFile + ": ");
						a = new AssinadorPKCS11(pfxFile, pass, null);
					}
					this.pass.savePassword(pfxFile, pass);
				} else {

					if (pfxFile.endsWith(".pfx")) {
						a = new Main(pfxFile, pass);
					} else {
						a = new AssinadorPKCS11(pfxFile, pass, null);
					}
				}

				Vector<String> cnpjs = certificate.getCnpjs();
				for (String cnpj : cnpjs) {
					this.signers.put(cnpj, a);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String signForCNPJ__Reinf(String cnpj, String xml, String idService) throws RemoteException {

		///////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////
		//////////////// raiz do xml deve ser <Reinf>!!!
		///////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////

		try {
			AssinadorInt signer = this.signers.get(cnpj.trim());

			// idService mantem a tag para assinar
			String tag1 = idService;

			if (signer instanceof Main) {
				Main singer_impl = (Main) signer;
				String xml_signed = singer_impl.myAssinaReinf(xml, tag1);
				return xml_signed;
			}
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		return null;

		/*
		 * try { String tag = ""; if
		 * (idService.equals(SystemProperties.ID_SERVICO_RECEPCAO)) {
		 * 
		 * if (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_NFE))
		 * { tag = "infNFe"; } else { tag = "infCTe"; }
		 * 
		 * } // infInut if
		 * (idService.equals(SystemProperties.ID_SERVICO_CANCELAMENTO)) { if
		 * (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_NFE)) {
		 * tag = "infCanc"; } else { tag = "infCanc"; } }
		 * 
		 * if (idService.equals(SystemProperties.ID_SERVICO_INUTILIZACAO)) { if
		 * (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_NFE)) {
		 * tag = "infInut"; } else { tag = "infInut"; } }
		 * 
		 * if (idService.equals(SystemProperties.ID_SERVICO_EVENTOS)) { if
		 * (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_NFE)) {
		 * tag = "infEvento"; } else { tag = "infEvento"; } }
		 * 
		 * String xmlSigned = this.signers.get(cnpj.trim()).assina(xml, tag);
		 * MyLogger.getLog().finest(xmlSigned); return xmlSigned; } catch
		 * (Exception e) { MyLogger.getLog().log(Level.INFO,
		 * e.getLocalizedMessage(), e); } return null;
		 */
	}

	@Override
	public Hashtable<String, Hashtable<String, Object>> readAllPFXProperties() {
		Hashtable<String, Hashtable<String, Object>> prop = new Hashtable<String, Hashtable<String, Object>>();
		Vector<CertificatesConfig> cnpjspfxs = new Vector<CertificatesConfig>();
		try {
			cnpjspfxs = this.manager.getCertificates();
		} catch (RemoteException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
		for (CertificatesConfig cert : cnpjspfxs) {
			String pfxfile = cert.getPfxFile();
			if (pfxfile.endsWith(".pfx")) {
				prop.put(pfxfile, this.readPFXProperties(pfxfile));
			}
		}

		return prop;
	}

	@Override
	public Hashtable<String, Object> readPFXProperties(String certificado) {

		String senha = this.pass.getPassword(certificado);

		KeyStore ks = null;
		try {
			ks = KeyStore.getInstance("pkcs12");
		} catch (KeyStoreException ex) {
			MyLogger.getLog().log(Level.INFO, ex.getLocalizedMessage(), ex);
		}

		try {
			new FileInputStream(certificado);
		} catch (FileNotFoundException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		try {
			ks.load(new FileInputStream(certificado), senha.toCharArray());
		} catch (IOException ex) {
			MyLogger.getLog().log(Level.INFO, ex.getLocalizedMessage(), ex);
		} catch (NoSuchAlgorithmException ex) {
			MyLogger.getLog().log(Level.INFO, ex.getLocalizedMessage(), ex);
		} catch (CertificateException ex) {
			MyLogger.getLog().log(Level.INFO, ex.getLocalizedMessage(), ex);
		}
		Enumeration<String> aliases = null;
		try {
			aliases = ks.aliases();
		} catch (KeyStoreException ex) {
			MyLogger.getLog().log(Level.INFO, ex.getLocalizedMessage(), ex);
		}

		KeyStore.PrivateKeyEntry entry = null;

		try {
			while (aliases.hasMoreElements()) {
				String alias1 = (String) aliases.nextElement();
				if (ks.isKeyEntry(alias1)) {

					entry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias1,
							new KeyStore.PasswordProtection(senha.toCharArray()));
					Certificate certificate = entry.getCertificate();

					X509CertImpl cc = (X509CertImpl) certificate;

					Hashtable<String, Object> props = new Hashtable<String, Object>();
					props.put("NA", cc.getNotAfter());
					props.put("NB", cc.getNotBefore());
					props.put("CN", cc.getSubjectX500Principal().toString());

					return props;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

}
