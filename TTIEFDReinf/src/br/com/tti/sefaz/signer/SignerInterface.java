package br.com.tti.sefaz.signer;

import java.rmi.RemoteException;
import java.util.Hashtable;

public interface SignerInterface {

	public String signForCNPJ__Reinf(String cnpj, String xml, String tag) throws RemoteException;

	public Hashtable<String, Object> readPFXProperties(String certificado) throws RemoteException;

	public Hashtable<String, Hashtable<String, Object>> readAllPFXProperties() throws RemoteException;

}
