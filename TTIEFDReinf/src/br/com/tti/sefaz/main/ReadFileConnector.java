package br.com.tti.sefaz.main;

import br.com.tti.sefaz.connector.ConnectorFactory;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.util.MainParameters;

public class ReadFileConnector {

	public static void main(String[] args) {
		MainParameters.processArguments(args);
		try {
			ConnectorFactory.createConnector("br.com.tti.sefaz.connector.ReadFileConnector");
		} catch (Exception e) {
			MyLogger.getLog().info(e.getLocalizedMessage());
			System.exit(100);
		}
		MyLogger.getLog().info("Connector Iniciado");
	}
}
