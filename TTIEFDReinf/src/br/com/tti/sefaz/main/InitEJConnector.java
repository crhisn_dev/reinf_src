package br.com.tti.sefaz.main;

import br.com.tti.sefaz.connector.ConnectorFactory;
import br.com.tti.sefaz.log.MyLogger;

public class InitEJConnector {

	public static void main(String[] args) {
		try {
			ConnectorFactory
					.createConnector("br.com.tti.sefaz.connector.EJConnector");
		} catch (Exception e) {
			MyLogger.getLog().info(e.getLocalizedMessage());
			System.exit(100);
		}
		MyLogger.getLog().info("Connector Iniciado");
	}
}
