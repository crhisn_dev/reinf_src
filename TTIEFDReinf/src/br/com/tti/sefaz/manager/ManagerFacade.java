package br.com.tti.sefaz.manager;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.Query;

import nfeimpressao.TaragonaInt;
import br.com.tti.ldap.authenticator.ADAuthenticator;
import br.com.tti.sefaz.buffer.DBAccess;
import br.com.tti.sefaz.buffer.local.DBAccessImpl;
import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.callback.CallBack;
import br.com.tti.sefaz.callback.CallBackInteface;
import br.com.tti.sefaz.cancelinut.CancelInut;
import br.com.tti.sefaz.cancelinut.CancelInutInterface;
import br.com.tti.sefaz.connector.Connector;
import br.com.tti.sefaz.connector.ConnectorNotifyThread;
import br.com.tti.sefaz.contingence.ContingeceInterface;
import br.com.tti.sefaz.contingence.Contingence;
import br.com.tti.sefaz.contingence.ModeOperation;
import br.com.tti.sefaz.event.EventManager;
import br.com.tti.sefaz.event.EventManagerController;
import br.com.tti.sefaz.event.EventManagerListener;
import br.com.tti.sefaz.externaldbaccess.LaunchExternalAccess;
import br.com.tti.sefaz.licence.CheckLicence;
import br.com.tti.sefaz.listeners.NotifyListenerThread;
import br.com.tti.sefaz.listeners.RemoveListenersThread;
import br.com.tti.sefaz.listeners.TTIEventListener;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.messenger.Messenger;
import br.com.tti.sefaz.messenger.MessengerInterface;
import br.com.tti.sefaz.persistence.EventData;
import br.com.tti.sefaz.persistence.EventData.TIPO_EVENTO;
import br.com.tti.sefaz.persistence.LogXML;
import br.com.tti.sefaz.persistence.SefazState;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.printer.PrintThread;
import br.com.tti.sefaz.printer.XMLPrinter;
import br.com.tti.sefaz.printer.impl.LocalPrinter;
import br.com.tti.sefaz.querier.Querier;
import br.com.tti.sefaz.querier.QuerierInterface;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.remote.CNPJSerialize;
import br.com.tti.sefaz.remote.CallBackConfig;
import br.com.tti.sefaz.remote.CertificatesConfig;
import br.com.tti.sefaz.remote.DBConfig;
import br.com.tti.sefaz.remote.MessengerConfig;
import br.com.tti.sefaz.remote.SchemaVersionConfig;
import br.com.tti.sefaz.remote.SenderConfig;
import br.com.tti.sefaz.remote.ServicesConfig;
import br.com.tti.sefaz.remote.events.ChangeModeOpEvent;
import br.com.tti.sefaz.remote.events.TTIEvent;
import br.com.tti.sefaz.sender.Sender;
import br.com.tti.sefaz.sender.SenderInterface;
import br.com.tti.sefaz.sender.email.EmailSender;
import br.com.tti.sefaz.signer.Signer;
import br.com.tti.sefaz.signer.SignerInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.XMLConfigSystem;
import br.com.tti.sefaz.tools.SendXml;
import br.com.tti.sefaz.tryagain.TryAgain;
import br.com.tti.sefaz.tryagain.impl.TryAgainImpl;
import br.com.tti.sefaz.users.Profile;
import br.com.tti.sefaz.users.User;
import br.com.tti.sefaz.users.UserManager;
import br.com.tti.sefaz.users.impl.UserManagerImpl;
import br.com.tti.sefaz.util.GCLauncher;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.util.ModoOpManager;
import br.com.tti.sefaz.xml.evento.TRetEvento;
import br.com.tti.tcteimpressao.remote.CTeImpressao;

public class ManagerFacade implements ManagerInterface {

	private XMLConfigSystem configSystem;
	private ModoOpManager modoOp;

	private Vector<SignerInterface> signers;
	private Vector<MessengerInterface> messengers;
	private Vector<CallBackInteface> callbacks;
	private Vector<SenderInterface> senders;
	private Vector<QuerierInterface> queriers;
	private Vector<CancelInutInterface> cancelInut;
	private Vector<TTIEventListener> listeners;
	private Vector<TTIEventListener> toRemoveListeners;

	private Vector<ContingeceInterface> contingence;
	private Vector<DBAccess> dbAccess;
	private Vector<UserManager> users;
	private Vector<TaragonaInt> ttiprinters;
	private Vector<CTeImpressao> ctettiprinters;
	private Vector<XMLPrinter> localprinter;
	private Vector<Connector> connectors;
	private Vector<Connector> toRemoveConnectors;
	private ADAuthenticator authenticator;

	private TryAgain tryAgain;

	public ManagerFacade() {

		/*
		 * CheckLicence l = new CheckLicence(MainParameters.getXml());
		 * l.check();
		 */

		this.configSystem = new XMLConfigSystem(MainParameters.getXml());

		this.signers = new Vector<SignerInterface>();
		this.messengers = new Vector<MessengerInterface>();
		this.callbacks = new Vector<CallBackInteface>();
		this.senders = new Vector<SenderInterface>();
		this.queriers = new Vector<QuerierInterface>();
		this.cancelInut = new Vector<CancelInutInterface>();

		this.listeners = new Vector<TTIEventListener>();
		this.toRemoveListeners = new Vector<TTIEventListener>();

		this.contingence = new Vector<ContingeceInterface>();
		this.dbAccess = new Vector<DBAccess>();
		this.users = new Vector<UserManager>();
		this.ttiprinters = new Vector<TaragonaInt>();
		this.ctettiprinters = new Vector<CTeImpressao>();
		this.localprinter = new Vector<XMLPrinter>();
		this.connectors = new Vector<Connector>();
		this.toRemoveConnectors = new Vector<Connector>();

		this.configSystem.make();
		if (MainParameters.isLocal()) {
			this.initLocalInstances();
		}

		this.tryAgain = new TryAgainImpl();
		this.modoOp = new ModoOpManager();

		// automatic process
		RemoveListenersThread.start(this.listeners, this.toRemoveListeners,
				this.connectors, this.toRemoveConnectors);

		// TryConsistencyThread.start();

		GCLauncher.start();

		EmailSender.getInstance();
		try {
			authenticator = new ADAuthenticator();
		} catch (Exception e) {
			MyLogger.getLog().info("AD nao configurado");
		}

	}

	private void initLocalInstances() {
		Locator.setManager(this);

		SignerInterface signer = new Signer();
		this.signers.add(signer);

		MessengerInterface msn = new Messenger();
		this.messengers.add(msn);

		CallBackInteface call = new CallBack();
		this.callbacks.add(call);

		SenderInterface sender = new Sender();
		this.senders.add(sender);

		QuerierInterface querier = new Querier();
		this.queriers.add(querier);

		CancelInutInterface cancelInut = new CancelInut();
		this.cancelInut.add(cancelInut);

		ContingeceInterface contingence = new Contingence();
		this.contingence.add(contingence);

		LocalPrinter print = new LocalPrinter();
		this.localprinter.add(print);

		if (MainParameters.isExternaldb()) {
			LaunchExternalAccess l = new LaunchExternalAccess();
			l.initExternalAccess();
		} else {
			DBAccess db = new DBAccessImpl();
			this.dbAccess.add(db);
		}

		UserManager user = new UserManagerImpl();
		this.users.add(user);

		try {
			this.eventmanager = new EventManagerController();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

	}

	@Override
	public String sendXMLMessage(String idServico, String header, String data,
			Hashtable prop) throws RemoteException {
		for (SenderInterface sender : this.senders) {
			return sender.sendXMLMessage(idServico, header, data, prop);
		}
		return null;
	}

	@Override
	public Hashtable<String, MessengerConfig> getMessengerConfig()
			throws RemoteException {
		return this.configSystem.getMsnConfigs();
	}

	@Override
	public Hashtable<String, CNPJData> getCNPJ() throws RemoteException {
		return this.configSystem.getCnpjsData();
	}

	@Override
	public Hashtable<String, CNPJSerialize> getCNPJsSerialize()
			throws RemoteException {
		return this.configSystem.getCnpjsSer();
	}

	@Override
	public DBConfig getDBConfig() throws RemoteException {
		return this.configSystem.getDbConfig();
	}

	@Override
	public Vector<CertificatesConfig> getCertificates() throws RemoteException {
		return this.configSystem.getCertificates();
	}

	@Override
	public SenderConfig getSenderConfig() throws RemoteException {
		return this.configSystem.getSenderConfig();
	}

	@Override
	public ServicesConfig getServiceConfig() throws RemoteException {
		return this.configSystem.getServiceConfig();
	}

	@Override
	public void checkXmlSet(String cnpj, String idLote, String nRecibo,
			Vector<String> notas, String uf, String ambient)
			throws RemoteException {
		for (CallBackInteface call : this.callbacks) {
			call.checkXmlSet(cnpj, idLote, nRecibo, notas, uf, ambient);
		}
	}

	@Override
	public void sendXml(String keyXml, String xml, String cnpjSender,
			String cnpjReceiver, String dateEmiss, String message, String uf,
			String ambient, boolean sign, boolean error,
			Hashtable<String, String> props) throws RemoteException {

		for (MessengerInterface msn : this.messengers) {
			msn.sendXml(keyXml, xml, cnpjSender, cnpjReceiver, dateEmiss,
					message, uf, ambient, sign, error, props);
		}
	}

	@Override
	public void sendXml(String xml, Hashtable<String, String> props)
			throws RemoteException {
		for (MessengerInterface msn : this.messengers) {
			msn.sendXml(xml, props);
		}
	}

	@Override
	public boolean isActive() throws RemoteException {
		for (MessengerInterface msn : this.messengers) {
			return msn.isActive();
		}
		return false;
	}

	@Override
	public TPEMISS getModo(String cnpj, String ambient) throws RemoteException {
		for (MessengerInterface msn : this.messengers) {
			return msn.getModo(cnpj, ambient);
		}
		return null;
	}

	@Override
	public void setMessengerParameters(String cnpj, String ambient,
			MessengerConfig msnConfig) throws RemoteException {
		for (MessengerInterface msn : this.messengers) {
			msn.setMessengerParameters(cnpj, ambient, msnConfig);
		}
	}

	@Override
	public Hashtable<String, CallBackConfig> getCallBackConfig()
			throws RemoteException {
		return this.configSystem.getCallsConfigs();
	}

	@Override
	public String signForCNPJ__Reinf(String cnpj, String xml, String tag)
			throws RemoteException {
		for (SignerInterface signer : this.signers) {
			return signer.signForCNPJ__Reinf(cnpj, xml, tag);
		}
		return null;
	}

	@Override
	public void setCallBackParameters(String cnpj, String uf,
			CallBackConfig config) throws RemoteException {
		for (CallBackInteface callback : this.callbacks) {
			callback.setCallBackParameters(cnpj, uf, config);
		}

	}

	@Override
	public Hashtable<Vector<String>, Vector<SchemaVersionConfig>> getSchemasVersion()
			throws RemoteException {
		return this.configSystem.getSchemasVersion();
	}

	@Override
	public String makeQueryXML(String uf, String ambient, String keyXml,
			boolean persist, boolean assyn) throws RemoteException {
		for (QuerierInterface querier : this.queriers) {
			return querier.makeQueryXML(uf, ambient, keyXml, persist, assyn);
		}
		return null;
	}

	@Override
	public String cancelXml(String cnpj, String uf, String ambient,
			String keyXml, String protocol, String just, boolean sync,
			Hashtable<String, String> props) throws RemoteException {
		for (CancelInutInterface cancel : this.cancelInut) {
			return cancel.cancelXml(cnpj, uf, ambient, keyXml, protocol, just,
					sync, props);
		}
		return null;
	}

	@Override
	public String inutXml(String cnpj, String uf, String ambient,
			String keyXml, String justl, boolean sync) throws RemoteException {
		for (CancelInutInterface cancel : this.cancelInut) {
			return cancel.inutXml(cnpj, uf, ambient, keyXml, justl, sync);
		}
		return null;
	}

	@Override
	public String inutXml(String nOp, String uf, String ambient, String ano,
			String cnpj, String mod, String serie, String ini, String fim,
			String just, boolean sync) throws RemoteException {
		for (CancelInutInterface cancel : this.cancelInut) {
			return cancel.inutXml(nOp, uf, ambient, ano, cnpj, mod, serie, ini,
					fim, just, sync);
		}
		return null;
	}

	// listener
	@Override
	public void processEvent(TTIEvent event) throws RemoteException {
		MyLogger.getLog().info(
				"notify: " + event.toString() + " class: " + event.getClass());
		this.notifyEvent(event);
		this.notifyDataConnector(event);
	}

	// notifier
	@Override
	public void addListener(TTIEventListener listener) throws RemoteException {
		MyLogger.getLog().info("Adding: " + listener.toString());
		this.listeners.add(listener);
	}

	@Override
	public void notifyEvent(TTIEvent event) throws RemoteException {

		for (TTIEventListener listener : this.listeners) {
			NotifyListenerThread notifytrhead = new NotifyListenerThread(
					listener, event, toRemoveListeners);

			Thread t = new Thread(notifytrhead);
			t.start();

			/*
			 * try { listener.processEvent(event); } catch (Exception e) {
			 * MyLogger.getLog().info( "Manager listener not found: " +
			 * e.getLocalizedMessage()); }
			 */
		}
	}

	@Override
	public void changeToState(String cnpj, String ambient, TPEMISS modo,
			Hashtable<String, Object> props) throws RemoteException {

		CNPJData cnpjdata = this.getCNPJ().get(cnpj);
		ambient = cnpjdata.getAmbiente();

		MyLogger.getLog().info("CNPJData ambient:" + ambient);

		if (props == null) {
			props = new Hashtable<String, Object>();
		}

		if (!props.containsKey("xjust")) {
			props.put("xjust", "");
		}

		if (!props.containsKey("dhcont")) {
			props.put("dhcont", "");
		}

		for (ModeOperation op : this.messengers) {
			op.changeToState(cnpj, ambient, modo, props);
		}

		for (ModeOperation op : this.callbacks) {
			op.changeToState(cnpj, ambient, modo, props);
		}

		for (ModeOperation op : this.cancelInut) {
			op.changeToState(cnpj, ambient, modo, props);
		}

		for (ModeOperation op : this.senders) {
			op.changeToState(cnpj, ambient, modo, props);
		}

		ChangeModeOpEvent s = new ChangeModeOpEvent();
		s.setCnpj(cnpj);
		s.setAmb(ambient);
		s.setModo(modo);

		this.notifyEvent(s);
	}

	@Override
	public Hashtable<String, String> getJust(String cnpj, String ambient,
			TPEMISS tpemiss) throws RemoteException {
		CNPJData cnpjdata = this.getCNPJ().get(cnpj);
		ambient = cnpjdata.getAmbiente();

		MyLogger.getLog().info("CNPJData ambient:" + ambient);

		for (MessengerInterface msn : this.messengers) {
			return msn.getJust(cnpj, ambient, tpemiss);
		}
		return null;
	}

	@Override
	public void enterContingence(String cnpj, String ambient, boolean async,
			TPEMISS type) throws RemoteException {
		for (ContingeceInterface cont : this.contingence) {
			cont.enterContingence(cnpj, ambient, async, type);
		}
	}

	@Override
	public void leaveContingence(String cnpj, String ambient, boolean async,
			TPEMISS type) throws RemoteException {
		this.contingence.get(0).leaveContingence(cnpj, ambient, async, type);
	}

	@Override
	public void processInContingence(String keyxml, String ambient, String xml)
			throws RemoteException {
		this.contingence.get(0).processInContingence(keyxml, ambient, xml);
	}

	@Override
	public Vector<XMLData> findXMLData(Date d1, Date d2, Vector<String> cnpjs,
			String ambient, Hashtable prop) throws RemoteException {
		for (DBAccess db : this.dbAccess) {
			return db.findXMLData(d1, d2, cnpjs, ambient, prop);
		}
		return null;
	}

	@Override
	public Vector<XMLData> findXMLData(String d1, String d2,
			Vector<String> cnpjs, String ambient, Hashtable prop)
			throws RemoteException {
		for (DBAccess db : this.dbAccess) {
			return db.findXMLData(d1, d2, cnpjs, ambient, prop);
		}
		return null;
	}

	@Override
	public Vector<TTIEvent> findXMLEvent(Date d1, Date d2,
			Vector<String> cnpjs, String ambient, Hashtable prop)
			throws RemoteException {
		for (DBAccess db : this.dbAccess) {
			return db.findXMLEvent(d1, d2, cnpjs, ambient, prop);
		}
		return null;
	}

	@Override
	public Vector<TTIEvent> findXMLEvent(String d1, String d2,
			Vector<String> cnpjs, String ambient, Hashtable prop)
			throws RemoteException {
		for (DBAccess db : this.dbAccess) {
			return db.findXMLEvent(d1, d2, cnpjs, ambient, prop);
		}
		return null;
	}

	@Override
	public void register(User user, Profile p) throws RemoteException {
		for (UserManager u : this.users) {
			u.register(user, p);
		}
	}

	/*
	 * @Override public MODO_OP getModoOp(String cnpj) throws RemoteException {
	 * return this.modoOp.getModo(cnpj); }
	 */

	@Override
	public void changePassword(String login, String newPassword)
			throws RemoteException {
		for (UserManager u : this.users) {
			u.changePassword(login, newPassword);
		}
	}

	@Override
	public User getUser(String login, String password) throws RemoteException {

		for (UserManager u : this.users) {
			return u.getUser(login, password);
		}
		return null;

		/*
		 * if (this.authenticator != null) {
		 * MyLogger.getLog().info("recover user from AD"); try { Map map =
		 * this.authenticator.authenticate(login, password); User user = new
		 * User();
		 * 
		 * user.setId(login); user.setSenha(password);
		 * 
		 * Profile perfil = new Profile();
		 * 
		 * ManagerInterface ger = Locator.getManagerReference();
		 * Hashtable<String, CNPJData> cnpjs = ger.getCNPJ(); String[] csss =
		 * new String[cnpjs.keySet().size()]; cnpjs.keySet().toArray(csss);
		 * perfil.setCnpjs(csss);
		 * 
		 * if (user.getPerfil() == null) { user.setPerfil(perfil); }
		 * 
		 * return user; } catch (Exception e) { e.printStackTrace(); }
		 * 
		 * }
		 */

		// return null;
	}

	public User createUser(String login, String name, String pass) {

		ManagerInterface ger = Locator.getManagerReference();
		try {
			User u = new User();
			Profile p = new Profile();

			u.setId(login);
			u.setNome(name);
			u.setSenha(pass);

			Hashtable<String, CNPJData> cnpjs = ger.getCNPJ();
			String[] csss = new String[cnpjs.keySet().size()];
			cnpjs.keySet().toArray(csss);
			p.setCnpjs(csss);

			u.setPerfil(p);

			return u;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	synchronized public void printXml(String keyXml, String xml, TPEMISS modo,
			Hashtable<String, String> prop) throws RemoteException {
		MyLogger.getLog().info("Reprint XML: " + keyXml);
		Vector<TaragonaInt> remove = new Vector<TaragonaInt>();
		for (TaragonaInt print : this.ttiprinters) {
			try {
				print.imprimeDanfe(keyXml, modo.toString(), xml, prop);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
				MyLogger.getLog().info(
						"Removing printer: " + print.toString() + " reason: "
								+ e.getLocalizedMessage());
				remove.add(print);
			}
		}

		for (TaragonaInt print : remove) {
			this.ttiprinters.remove(print);
		}

		Vector<CTeImpressao> removecteprinter = new Vector<CTeImpressao>();
		for (CTeImpressao cteprinter : this.ctettiprinters) {
			try {
				MyLogger.getLog().info("Init print thread:" + keyXml);
				PrintThread printt = new PrintThread(removecteprinter,
						cteprinter, keyXml, xml, prop);
				Thread t = new Thread(printt);
				t.start();

			} catch (Exception e) {
				MyLogger.getLog().info(
						"Removing printer: " + cteprinter.toString()
								+ " reason: " + e.getLocalizedMessage());
				removecteprinter.add(cteprinter);
			}
		}

		for (CTeImpressao print : removecteprinter) {
			try {
				this.ctettiprinters.remove(print);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);

			}
		}
	}

	@Override
	public void registrarImpressao(TaragonaInt imp) throws RemoteException {
		MyLogger.getLog().info("register printer:" + imp.toString());
		this.ttiprinters.add(imp);
	}

	@Override
	public void rePrintXml(String keyXml, TPEMISS modo) throws RemoteException {
		for (XMLPrinter print : this.localprinter) {
			print.rePrintXml(keyXml, modo);
		}
	}

	@Override
	public boolean checkPFXPassword(String pfx, String password)
			throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Vector<LogXML> findLog(String keyXml) throws RemoteException {
		for (DBAccess dba : this.dbAccess) {
			return dba.findLog(keyXml);
		}
		return null;
	}

	@Override
	public String findXML(String keyXml) throws RemoteException {
		for (DBAccess dba : this.dbAccess) {
			return dba.findXML(keyXml);
		}
		return null;
	}

	@Override
	public boolean checkXMLMessage(String idServico, Hashtable prop)
			throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addConnector(Connector connector) throws RemoteException {
		MyLogger.getLog().info("Connector registered: " + connector.toString());
		this.connectors.add(connector);
	}

	@Override
	public void notifyDataConnector(TTIEvent event) throws RemoteException {
		for (Connector connect : this.connectors) {
			try {
				ConnectorNotifyThread notifier = new ConnectorNotifyThread(
						connect, event, this.toRemoveConnectors);
				Thread t = new Thread(notifier);
				t.start();
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
		/*
		 * Vector<Connector> removeconnectors = new Vector<Connector>(); for
		 * (Connector connect : this.connectors) { try {
		 * connect.processEvent(event); } catch (Exception e) {
		 * MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		 * MyLogger.getLog().info( "Connector dont found: " +
		 * e.getLocalizedMessage()); removeconnectors.add(connect); } }
		 * 
		 * try { for (Connector connector : removeconnectors) {
		 * this.connectors.remove(connector); } } catch (Exception e) {
		 * MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		 * MyLogger.getLog().info( "this.connectors.remove(connector): " +
		 * e.getLocalizedMessage()); }
		 */

	}

	@Override
	public SefazState makeQueryState(String uf, String ambient, String keyXml,
			boolean persist, boolean assyn) throws RemoteException {
		for (QuerierInterface querier : this.queriers) {
			// sempre sincrono-- sempre false
			return querier.makeQueryState(uf, ambient, keyXml, persist, false);
		}
		return null;
	}

	@Override
	public void synchronizeXmlWithSefaz(String keyXml) throws RemoteException {
		this.tryAgain.synchronizeXmlWithSefaz(keyXml);
	}

	@Override
	public void synchronizeXmlWithSefaz(String cnpj, String ambient, String uf)
			throws RemoteException {
		this.tryAgain.synchronizeXmlWithSefaz(cnpj, ambient, uf);
	}

	@Override
	public byte[] findPDF(String id) throws RemoteException {
		for (XMLPrinter printer : this.localprinter) {
			return printer.findPDF(id);
		}
		return null;
	}

	@Override
	public void registrarCTeImpressao(CTeImpressao imp) throws RemoteException {
		MyLogger.getLog().info("CTe Impressao registrado:" + imp.toString());
		this.ctettiprinters.add(imp);
	}

	@Override
	public void sendXMLProtEmail(String keyxml, String emails,
			Hashtable<String, Object> props) throws RemoteException {
		if (props != null && props.containsKey("sendemail")) {
			try {
				EmailSender.getInstance().registerKey(keyxml, emails);
				EmailSender.getInstance().sendEmailFiles(keyxml,
						Boolean.parseBoolean(props.get("xml").toString()),
						Boolean.parseBoolean(props.get("pdf").toString()));
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		} else {
			try {
				XMLData xmldata = XMLDataCache.getInstance().findData(keyxml);
				EmailSender.getInstance().registerKey(keyxml,
						xmldata.getEmail());
				EmailSender.getInstance().sendEmailXMLProt(keyxml);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
	}

	@Override
	public Hashtable<String, Object> genericRemoteCall(
			Hashtable<String, Object> params) throws RemoteException {
		MyLogger.getLog().info("params:" + params.toString());

		if (params.containsKey("xmlcce")) {
			String id = (String) params.get("xmlcce");
			try {
				if (id.startsWith("CTe")) {
					id = id.replace("CTe", "");
				}
				List<EventData> events = this.obterEventosSequence(null, id,
						TIPO_EVENTO.CCE.getCodigo(), null);
				if (events != null && !events.isEmpty()) {
					EventData event = events.get(0);
					String xml = event.getXmlString();
					Hashtable<String, Object> response = new Hashtable<String, Object>();
					response.put("xmlcce", xml);
					return response;
				}

			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}

			return null;

		}

		if (params.containsKey("sendxml")) {
			String[] args = new String[] { "-xml", MainParameters.getOutros() };

			MainParameters.processArguments(args);
			SendXml a = new SendXml(MainParameters.getXml());

			a.enviar();
		}

		if (params.containsKey("findentities")) {
			MyLogger.getLog().info("params:" + params.toString());
			String sqlquery = params.get("findentities").toString();
			Class classs = (Class) params.get("classs");

			DAO dao = DaoFactory.createDAO(classs);
			Query q = dao.createQuery(sqlquery);

			Hashtable<String, Object> result = new Hashtable<String, Object>();
			result.put("results", q.getResultList());
			return result;

		}

		if (params.containsKey("filelocator")) {
			Hashtable<String, Object> result = new Hashtable<String, Object>();
			result.put("results", MainParameters.getCaminhoarquivos());
			return result;
		}

		return null;
	}

	@Override
	public Hashtable<String, Object> readPFXProperties(String certificado)
			throws RemoteException {
		for (SignerInterface signer : this.signers) {
			return signer.readPFXProperties(certificado);
		}
		return null;
	}

	@Override
	public Hashtable<String, Hashtable<String, Object>> readAllPFXProperties()
			throws RemoteException {
		for (SignerInterface signer : this.signers) {
			return signer.readAllPFXProperties();
		}
		return null;
	}

	// //////////////////////////////////////////////////////
	// //////////// EVENTMANAGER
	// /////////////////////////////////////////////////////

	private EventManager eventmanager;

	@Override
	public List<EventData> obterEventos(String keyxml,
			Hashtable<String, String> props) throws RemoteException {
		// TODO Auto-generated method stub
		return this.eventmanager.obterEventos(keyxml, props);
	}

	public List<EventData> obterEventosSequence(DAO<EventData> daoevent,
			String keyxml, String codetipoevent, Hashtable<String, String> props)
			throws Exception {
		return this.eventmanager.obterEventosSequence(daoevent, keyxml,
				codetipoevent, props);
	}

	@Override
	public TRetEvento sendEvent(String keyxml, TIPO_EVENTO evento,
			String datahora, String cnpjclient, Hashtable<String, Object> params)
			throws Exception {

		KeyXml key = new KeyXml(keyxml);
		CNPJData cnpjdata = this.getCNPJ().get(key.getCnpj());
		String ambient = cnpjdata.getAmbiente();

		MyLogger.getLog().info("CNPJData ambient:" + ambient);
		params.put("ambient", ambient);

		return this.eventmanager.sendEvent(keyxml, evento, datahora,
				cnpjclient, params);
	}

	@Override
	public void addListener(EventManagerListener listener) throws Exception {
		this.eventmanager.addListener(listener);

	}

	@Override
	public List<EventData> recoverEventData(String keyxml,
			TIPO_EVENTO tipoevento, Date data, Hashtable<String, Object> props)
			throws Exception {
		return this.eventmanager.recoverEventData(keyxml, tipoevento, data,
				props);
	}
}
