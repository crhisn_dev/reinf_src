package br.com.tti.sefaz.querier;

import java.util.Hashtable;
import java.util.ResourceBundle;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.sender.port.SenderPortController;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.XMLConfigSystem;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.xmlgenerate.XMLWrapperFactory;
import br.com.tti.sefaz.xmlgenerate.XMLWrapperQuery;

public class QuerierInstance {

	// private SenderDispatchController sender;
	private ResourceBundle bundle;
	private String ambient;
	private String xmlFile;
	private SenderPortController sender;

	public QuerierInstance() {
		this.bundle = ResourceBundle.getBundle("configure");
		this.ambient = this.bundle.getString("ambient");
		this.xmlFile = this.bundle.getString("fileconfig");

		String[] args = new String[3];
		args[0] = "-xml";
		args[1] = this.xmlFile;
		args[2] = "-localdb";

		MainParameters.processArguments(args);

		XMLConfigSystem configSystem = new XMLConfigSystem(MainParameters
				.getXml());
		configSystem.make();

		sender = new SenderPortController(configSystem.getSenderConfig(),
				configSystem.getServiceConfig());

		/*
		 * this.sender = new SenderDispatchController(configSystem
		 * .getSenderConfig(), configSystem.getServiceConfig());
		 */
	}

	public QuerierInstance(String xmlfile) {
		// this.bundle = ResourceBundle.getBundle("configure");
		this.ambient = "producao";
		this.xmlFile = xmlfile;

		String[] args = new String[3];
		args[0] = "-xml";
		args[1] = this.xmlFile;
		args[2] = "-localdb";

		MainParameters.processArguments(args);

		XMLConfigSystem configSystem = new XMLConfigSystem(MainParameters
				.getXml());
		configSystem.make();

		sender = new SenderPortController(configSystem.getSenderConfig(),
				configSystem.getServiceConfig());

		/*
		 * this.sender = new SenderDispatchController(configSystem
		 * .getSenderConfig(), configSystem.getServiceConfig());
		 */
	}

	public String convertAmbient(String ambient) {
		if (ambient.equals(SystemProperties.AMBIENT_HOMOLOGACAO))
			return "2";
		if (ambient.equals(SystemProperties.AMBIENT_PRODUCAO))
			return "1";
		return "3";
	}

	public String createQueryXmlMessage(String keyXml) {
		String version = this.bundle.getString("versao");
		String tpAmb = this.convertAmbient(this.ambient);
		String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><consSitNFe xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\""
				+ version
				+ "\"><tpAmb>"
				+ tpAmb
				+ "</tpAmb><xServ>CONSULTAR</xServ><chNFe>"
				+ keyXml
				+ "</chNFe></consSitNFe>";
		return xmlString;
	}

	public String makeQuery(String key) {

		KeyXml keyM = new KeyXml(key);

		String version = this.bundle.getString("versao");
		String cabecalho = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><cabecMsg xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"1.02\">"
				+ "<versaoDados>" + version + "</versaoDados>" + "</cabecMsg>";
		String dados = this.createQueryXmlMessage(key);

		Hashtable<String, String> prop = new Hashtable<String, String>();
		prop.put("AMBIENT", ambient);
		prop.put("UF", keyM.getUF());

		String result = "";
		try {
			MyLogger.getLog().info("Header:" + cabecalho);
			MyLogger.getLog().info("Body:" + dados);
			result = this.sender.sendXMLMessage(
					SystemProperties.ID_SERVICO_CONSULTA, cabecalho, dados,
					prop);
			MyLogger.getLog().info("Result:" + result);
			XMLWrapperQuery prot = XMLWrapperFactory
					.createReturnQueryWrapper(result);

			return prot.getXMotivo();
		} catch (Exception e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public XMLWrapperQuery makeQueryComplete(String key) {

		KeyXml keyM = new KeyXml(key);

		String version = this.bundle.getString("versao");
		String cabecalho = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><cabecMsg xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"1.02\">"
				+ "<versaoDados>" + version + "</versaoDados>" + "</cabecMsg>";
		String dados = this.createQueryXmlMessage(key);

		Hashtable<String, String> prop = new Hashtable<String, String>();
		prop.put("AMBIENT", ambient);
		prop.put("UF", keyM.getUF());

		String result = "";
		try {
			MyLogger.getLog().info("Header:" + cabecalho);
			MyLogger.getLog().info("Body:" + dados);
			result = this.sender.sendXMLMessage(
					SystemProperties.ID_SERVICO_CONSULTA, cabecalho, dados,
					prop);
			MyLogger.getLog().info("Result:" + result);
			XMLWrapperQuery prot = XMLWrapperFactory
					.createReturnQueryWrapper(result);

			return prot;
		} catch (Exception e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		// MainParameters.processArguments(args);
		QuerierInstance q = new QuerierInstance();
		System.out.println(q
				.makeQuery("29101214109664000106550010000432391911712928"));
	}

	/*
	 * KeyXmlManager keyM = new KeyXmlManager(key); String cabecalho =
	 * "<cteCabecMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteConsulta\">"
	 * + "<cUF>" + keyM.getUF() + "</cUF>" + "<versaoDados>1.01</versaoDados>" +
	 * "</cteCabecMsg>";
	 * 
	 * String dados =
	 * "<cteDadosMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteConsulta\"><consSitCTe xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\""
	 * + "1.01" + "\"><tpAmb>" + "2" + "</tpAmb><xServ>CONSULTAR</xServ><chCTe>"
	 * + key + "</chCTe></consSitCTe></cteDadosMsg>";
	 */
}
