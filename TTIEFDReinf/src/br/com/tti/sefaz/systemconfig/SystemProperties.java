package br.com.tti.sefaz.systemconfig;

public class SystemProperties {

	public static String SOAP_12 = "soap12";
	public static String SOAP_11 = "soap1";

	public static String AMBIENT_PRODUCAO = "producao";
	public static String AMBIENT_HOMOLOGACAO = "homologacao";

	public static String AMBIENT_SCAN = "scan";
	public static String AMBIENT_DPEC = "dpec";

	public static String ID_SERVICO_RECEPCAO = "Recepcao";
	public static String ID_SERVICO_RETRECEPCAO = "RetRecepcao";
	public static String ID_SERVICO_CONSULTA = "Consulta";
	public static String ID_SERVICO_CANCELAMENTO = "Cancelamento";
	public static String ID_SERVICO_INUTILIZACAO = "Inutilizacao";
	public static String ID_SERVICO_ESTADO_SERVICOS = "StatusServicos";
	public static String ID_SERVICO_EVENTOS = "RecepcaoEvento";

	public enum TPEMISS {
		EMPTY0, NORMAL, EMPTY2, EMPTY3, CONTINGENCE_EPEC, CONTINGENCE, EMPTY6, SVC_RS, SVC_SP, CONTINGENCE_SCAN,
	};

	public enum SYSTEM {
		TTI_NFE, TTI_CTE
	};

	public enum ARCHITECTURE {
		REMOTE, LOCAL
	};

	public enum REINF_EVENT {
		evtInfoContri("R-1000", "Informações do Contribuinte"),

		evtTabProcesso("R-1070", "Tabela de Processos Administrativos/Judiciais"),

		evtServTom("R-2010", "Retenção Contribuição Previdenciária – Serviços Tomados"),

		evtPrestadorServicos("R-2020", "Retenção Contribuição Previdenciária – Serviços Prestados"),

		evtRecursoRecebidoAssociacao("R-2030", "Recursos Recebidos por Associação Desportiva"),

		evtRecursoRepassadoAssociacao("R-2040", "Recursos Repassados para Associação Desportiva"),

		evtInfoProdRural("R-2050", "Comercialização da Produção por Produtor Rural PJ/Agroindústria"),

		evtInfoCPRB("R-2060", "Contribuição Previdenciária sobre a Receita Bruta - CPRB"),

		evtRetFonte_nn("R-2070", "Retenções na Fonte - IR, CSLL, Cofins, PIS/PASEP - Pagamentos diversos"),

		evtReabreEvPer("R-2098", "Reabertura dos Eventos Periódicos"),

		evtFechamento("R-2099", "Fechamento dos Eventos Periódicos"),

		evtEspDesportivo("R-3010", "Receita de Espetáculo Desportivo"),

		evtInfoTrib("R-5001", "Informações das bases e dos tributos consolidados por contribuinte"),

		evtExclusao("R-9000", "Exclusão de Eventos");

		private String code;
		private String description;

		private REINF_EVENT(String code, String description) {
			this.code = code;
			this.description = description;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

	}

	public enum XML_STATE {
		GERADA, // 0
		ERRO_SCHEMA_XML, // 1
		ERRO_SCHEMA_LOTE, // 2
		TENTANDO_ENVIO, // 3
		ERRO_ENVIO, // 4
		ERRO_COMUNICACAO_SEFAZ, // 5
		ERRO_COMUNICACAO_LOCAL, // 6
		ENVIADA, // 7
		AUTORIZADA, // 8
		REJEITADA, // 9
		DENEGADA, // 10
		CANCELADA, // 11
		INUTLIZADA, // 12
		ERRO_CANCELADA, // 13
		ERRO_INUT, // 14
		CANCELADA_CONTINGENCIA, // 15
		ERRO_VALIDACAO, // 16
		INUT_CONTINGENCIA, // 17
		EPEC_ENVIADO, // 18
		EPEC_NAO_ENVIADO // 19

	};

	public enum TYPE_PRINT {
		IMPRESSA_NORMAL, IMPRESSA_CONT, IMPRESSA_DPEC,
	};

	public static enum SET_STATE {
		ENVIADO, CONFIRMADO_ENVIO, ERRO_ENVIO, GERADO, PROCESSADO, TEMPO_PROCESSAMENTO_LIMITE, TENTANDO_ENVIO, ERRO_ESQUEMA
	};

	public static enum RECIBE_STATE {
		CHECADO, PENDENTE
	};

	public static enum SIGNER_TYPE {
		PKCS11_SIGNER, PKCS12_SIGNER
	};

	public static void main(String[] args) {

	}
}
