package br.com.tti.sefaz.tryagain.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.Query;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.persistence.CTeEvento;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.Locator;

public class TryConsistencyThread implements Runnable {

	private static long TIME_OUT = (3 * 60 * 1000L); // 6min
	private static long CHECK_TRYAGAIN_INTERVAL = (3 * 60 * 1000L); // 3min

	private DAO<XMLData> daoData;
	private DAO<CTeEvento> daoEvent;
	private Vector<String> alreadyprocessed;
	private SimpleDateFormat sdf;

	public TryConsistencyThread() {
		this.daoData = DaoFactory.createDAO(XMLData.class);
		this.daoEvent = DaoFactory.createDAO(CTeEvento.class);
		this.alreadyprocessed = new Vector<String>();
		this.sdf = new SimpleDateFormat("yyyy-MM-dd");
	}

	public void findCTeEvents() {
		this.daoEvent.clean();

		String currentdate = sdf.format(Calendar.getInstance().getTime());

		String sql = "SELECT * FROM ifsapp.CTE_EVENTO WHERE env_status = 'L' and tipo_evento = '1' and cte_dt_ev >= '"
				+ currentdate + "'";

		Query q = this.daoEvent.createNativeQuery(sql);

		MyLogger.getLog().info("SQL query: " + sql);

		List<CTeEvento> events = null;
		try {
			events = q.getResultList();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		if (events != null) {
			MyLogger.getLog().info("Set size for events:" + events.toString());

			for (CTeEvento event : events) {
				try {
					XMLData exitsdata = this.daoData.findEntity(event
							.getCte_key());
					if (exitsdata == null) {
						MyLogger.getLog().info(
								"Send to TryAgain:" + event.getCte_key());

						event.setEnv_status("A");
						this.daoEvent.updateEntity(event);
						this.daoEvent.flush();
					}
				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(),
							e);
				}
			}
		}
	}

	public void findXMLDataForConsistency() {

		this.daoData.clean();

		MyLogger.getLog().info("Find XMLData pending...");

		/*
		 * String sql = "select o.keyXml, o.lastDataStateUpdate from XMLData as
		 * o where o.state = :state1 or o.state = :state2 or o.state = :state3
		 * or o.state = :state4"; Query query = this.daoData.createQuery(sql);
		 * 
		 * query.setParameter("state1", XML_STATE.GERADA);
		 * query.setParameter("state2", XML_STATE.TENTANDO_ENVIO);
		 * query.setParameter("state3", XML_STATE.ENVIADA);
		 * query.setParameter("state4", XML_STATE.ERRO_COMUNICACAO_SEFAZ);
		 */

		String sql = "select o.keyXml, o.lastDataStateUpdate from XMLData as o where o.state = :state1";

		Query query = this.daoData.createQuery(sql);

		query.setParameter("state1", XML_STATE.ENVIADA);

		/*
		 * String sql = "select o.keyXml, o.lastDataStateUpdate from XMLData as
		 * o "; Query query = this.daoData.createQuery(sql);
		 */
		List<Object[]> xmls = null;
		try {
			xmls = query.getResultList();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		if (xmls != null) {
			MyLogger.getLog().info("Set size for TryAgain:" + xmls.toString());

			for (Object[] row : xmls) {
				try {
					String keyxml = (String) row[0];

					Date lastUpdateTime = (Date) row[1];
					long currenttime = Calendar.getInstance().getTimeInMillis();

					if (lastUpdateTime == null
							|| (lastUpdateTime != null && (currenttime - lastUpdateTime
									.getTime()) > TIME_OUT)) {
						if (!this.alreadyprocessed.contains(keyxml)) {
							this.alreadyprocessed.add(keyxml);
							// time to wait for send request
							MyLogger.getLog()
									.info("Send to TryAgain:" + keyxml);
							Locator.getManagerReference()
									.synchronizeXmlWithSefaz(keyxml);
						}

					}
				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(),
							e);

				}
			}
		}
	}

	@Override
	public void run() {
		while (true) {
			try {
				this.findXMLDataForConsistency();
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}

			try {
				this.findCTeEvents();
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}

			try {
				Thread.sleep(CHECK_TRYAGAIN_INTERVAL);
			} catch (InterruptedException e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}

		}
	}

	public static void start() {
		Thread t = new Thread(new TryConsistencyThread());
		t.start();
	}

	public static void main(String[] args) {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(sdf1.format(Calendar.getInstance().getTime()));
	}

}
