package br.com.tti.sefaz.tryagain.impl;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.Query;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.TTIState;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.systemconfig.States;
import br.com.tti.sefaz.systemconfig.SystemParameters;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.tryagain.TryAgain;
import br.com.tti.sefaz.util.Locator;

public class TryAgainImpl implements TryAgain {

	private ManagerInterface manager;

	private Vector<TTIState> dontSend;
	private Vector<TTIState> toQuery;

	private DAO<XMLData> daoData;

	private SimpleDateFormat sdf;

	public TryAgainImpl() {
		this.manager = Locator.getManagerReference();
		this.daoData = DaoFactory.createDAO(XMLData.class);
		this.dontSend = States.getINSTANCE().getDontReSend();
		this.toQuery = States.getINSTANCE().getToQuery();
		this.sdf = new SimpleDateFormat("yyyy-MM-dd");
	}

	private void processXML(XMLData xmlData) {
		TTIState state = new TTIState(xmlData.getState(), 1);
		if (this.toQuery.contains(state)) {
			try {
				// deveria ser synchronized para conseguir processar o XML
				// novamente
				this.manager.makeQueryState(xmlData.getUf(),
						xmlData.getAmbient(), xmlData.getKeyXml(), true, false);
			} catch (RemoteException e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}

		if (!this.dontSend.contains(state)) {
			try {
				this.manager.sendXml(xmlData.getKeyXml(),
						xmlData.getXmlString(), xmlData.getCnpjEmit(),
						xmlData.getCnpjDest(), xmlData.getDateEmiss(), null,
						xmlData.getUf(), xmlData.getAmbient(), false, false,
						null);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
	}

	@Override
	public void synchronizeXmlWithSefaz(String keyXml) throws RemoteException {
		MyLogger.getLog().info("Try Again for Key: " + keyXml);
		XMLData xmlData = null;
		try {
			xmlData = this.daoData.findEntity(keyXml);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			return;
		}

		if (xmlData != null) {
			this.manager.makeQueryState(xmlData.getUf(),
					xmlData.getAmbient(), keyXml, true, false);
			try {
				xmlData = this.daoData.findEntity(keyXml);
				if (xmlData != null)
					this.processXML(xmlData);

			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
	}

	@Override
	public void synchronizeXmlWithSefaz(String cnpj, String ambient, String uf)
			throws RemoteException {
		MyLogger.getLog().info("Try Again for CNPJ: " + cnpj);

		String sql = "select o from XMLData as o where o.cnpjEmit = '"
				+ cnpj
				+ "' and o.dateCreate >= '"
				+ this.sdf.format(Calendar.getInstance().getTime())
				+ "' and o.state = :state0 and o.state = :state1 and o.state = :state2";

		Query query = this.daoData.createQuery(sql);

		query.setParameter("state0", XML_STATE.GERADA);
		query.setParameter("state1", XML_STATE.TENTANDO_ENVIO);
		query.setParameter("state2", XML_STATE.ENVIADA);

		MyLogger.getLog().info("SQL quering for CNPJ consistency:" + sql);

		List<XMLData> xmls = null;
		try {
			xmls = query.getResultList();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		if (xmls != null) {
			for (XMLData xml : xmls) {
				this.processXML(xml);
			}
		}
	}

	public static void main(String[] args) {
		try {
			ManagerInterface manager1 = Locator.getManagerReference();

			Hashtable<String, CNPJData> cnpjs = manager1.getCNPJ();

			for (CNPJData cnpjdata : cnpjs.values()) {
				manager1.synchronizeXmlWithSefaz(cnpjdata.getCnpj(),
						SystemProperties.AMBIENT_HOMOLOGACAO, cnpjdata.getUf());
			}
		} catch (RemoteException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}
}
