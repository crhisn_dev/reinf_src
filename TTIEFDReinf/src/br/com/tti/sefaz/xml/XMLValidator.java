package br.com.tti.sefaz.xml;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBElement;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import br.com.tti.nfev200.TNFe;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.util.ReadFile;

public class XMLValidator {
	private SchemaFactory schemaFactory;
	private Schema schema;
	private Validator validator;
	private boolean lasterror;

	public XMLValidator(String xsdFile) {
		Source schemaSource = new StreamSource(new File(xsdFile));

		this.schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			this.schema = this.schemaFactory.newSchema(schemaSource);
			this.validator = this.schema.newValidator();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	synchronized public void validateXml(String xml) throws SAXException, IOException {
		StringReader sr = new StringReader(xml);
		StreamSource fileStream = new StreamSource(sr);
		this.validator.validate(fileStream);
	}

	public String validateMessage(String xml) {
		String message = null;
		lasterror = false;
		try {
			this.validateXml(xml);
		} catch (SAXException e) {
			e.printStackTrace();
			message = e.getMessage();
			lasterror = true;
		} catch (IOException e) {
			e.printStackTrace();
			message = e.getMessage();
			lasterror = true;
		} catch (Exception e) {
			e.printStackTrace();
			message = e.getMessage();
			lasterror = true;
		}
		return message;
	}

	public void validateXml(File file) throws SAXException, IOException {
		StreamSource fileStream = new StreamSource(file);
		this.validator.validate(fileStream);

	}

	public boolean isLastError() {
		return this.lasterror;
	}

	public static void main(String[] args) {
		MainParameters.processArguments(args);
		{
			XMLValidator x = new XMLValidator("C:\\TTIReinf\\documentos\\v104\\evtFechamento-v1_04_00.xsd");

			File f = new File("C:/Users/Administrador.WIN-SERVIDOR/Downloads/REINF_20101810251809193(1).XML");

			try {
				x.validateXml(f);
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		

		/*
		 * XMLGenerator gen = new XMLGenerator("br.com.tti.nfev200"); try { TNFe
		 * rr = ((JAXBElement<TNFe>) gen.toObject(ReadFile.readFile(f
		 * .getAbsolutePath()))).getValue();
		 * System.out.println(rr.getInfNFe().getIde().getNFref().size()); }
		 * catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

	}
}
