package br.com.tti.sefaz.callback;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.remote.CallBackConfig;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.ModoOpManager;

public class CallBackController {

	private Hashtable<String, SystemProperties.TPEMISS> modos;
	private ManagerInterface manager;
	private CheckThreadFactory checkFactory;
	private ModoOpManager managerModo;

	public CallBackController(ManagerInterface manager) {
		super();
		this.manager = manager;
		this.managerModo = new ModoOpManager();
		this.checkFactory = CheckThreadFactory.getFactory(this.manager);
		this.modos = new Hashtable<String, TPEMISS>();

		this.initModos();
	}

	private void initModos() {
		try {
			Hashtable<String, CNPJData> cnpjs = this.manager.getCNPJ();
			for (String cnpj : cnpjs.keySet()) {
				TPEMISS modo = this.managerModo.getModo(cnpj);
				if (modo == null) {
					modo = TPEMISS.NORMAL;
					this.managerModo.saveModo(cnpj, modo, "", "");
				}
				this.modos.put(cnpj, modo);
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void checkXmlSet(String cnpj, String nSet, String nRecibo,
			Vector<String> keyXml, String uf, String ambient)
			throws RemoteException {
		TPEMISS modo = this.modos.get(cnpj);
		if (modo.equals(SystemProperties.TPEMISS.NORMAL)
				|| modo.equals(SystemProperties.TPEMISS.SVC_RS)
				|| modo.equals(SystemProperties.TPEMISS.SVC_SP)) {
			SetCheckThread checker = this.checkFactory.createCheckThread(cnpj,
					nRecibo, nSet, keyXml, uf, ambient);
			Thread t = new Thread(checker);
			t.start();
		}
	}

	public void changeToModo(String cnpj, TPEMISS modo, String xjust, String  dhcont) {
		MyLogger.getLog()
				.info("Changing state: Callback" + " for: " + cnpj + " with: "
						+ modo);
		this.managerModo.saveModo(cnpj, modo, xjust, dhcont);
		this.modos.put(cnpj, modo);
	}

	public void setParameters(String cnpj, CallBackConfig config) {
		this.checkFactory.changeConfig(cnpj, config);
	}
}
