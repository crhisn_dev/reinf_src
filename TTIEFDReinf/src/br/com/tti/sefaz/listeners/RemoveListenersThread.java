package br.com.tti.sefaz.listeners;

import java.util.Vector;
import java.util.logging.Level;

import br.com.tti.sefaz.connector.Connector;
import br.com.tti.sefaz.log.MyLogger;

public class RemoveListenersThread implements Runnable {

	private Vector<TTIEventListener> listeners;
	private Vector<TTIEventListener> toRemoveListeners;

	private Vector<Connector> connectors;
	private Vector<Connector> toRemoveConnectors;

	public RemoveListenersThread(Vector<TTIEventListener> listeners,
			Vector<TTIEventListener> toRemoveListeners,
			Vector<Connector> connectors, Vector<Connector> toRemoveConnectors) {
		super();
		this.listeners = listeners;
		this.toRemoveListeners = toRemoveListeners;

		this.connectors = connectors;
		this.toRemoveConnectors = toRemoveConnectors;
	}

	@Override
	public void run() {
		while (true) {
			MyLogger.getLog().info("Removing listeners and connectors... ");
			try {
				for (TTIEventListener listener : this.toRemoveListeners) {
					this.listeners.remove(listener);
				}
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
				MyLogger.getLog().info(
						" ********************* " + e.getLocalizedMessage());
			}

			try {
				for (TTIEventListener listener : this.toRemoveConnectors) {
					this.connectors.remove(listener);
				}
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
				MyLogger.getLog().info(
						" ********************* " + e.getLocalizedMessage());
			}

			try {
				Thread.sleep(1 * 60 * 1000L);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
	}

	public static void start(Vector<TTIEventListener> listeners,
			Vector<TTIEventListener> toremovelisteners,
			Vector<Connector> connectors, Vector<Connector> toRemoveConnectors) {

		RemoveListenersThread threadremove = new RemoveListenersThread(
				listeners, toremovelisteners, connectors, toRemoveConnectors);

		Thread t = new Thread(threadremove);
		t.start();
	}

}
