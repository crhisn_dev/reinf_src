package br.com.tti.sefaz.listeners;

import java.util.Vector;
import java.util.logging.Level;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.remote.events.TTIEvent;

public class NotifyListenerThread implements Runnable {

	private TTIEventListener listener;

	private TTIEvent event;

	private Vector<TTIEventListener> toRemoveListeners;

	public NotifyListenerThread(TTIEventListener listener, TTIEvent event,
			Vector<TTIEventListener> toRemoveListeners) {
		super();
		this.listener = listener;
		this.event = event;
		this.toRemoveListeners = toRemoveListeners;
	}

	@Override
	public void run() {
		try {
			this.listener.processEvent(this.event);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			MyLogger.getLog().info(
					"Adding listener to remove:" + this.listener.toString());
			this.toRemoveListeners.add(this.listener);
		} catch (Throwable e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			MyLogger.getLog().info(
					"Adding listener to remove:" + this.listener.toString());
			this.toRemoveListeners.add(this.listener);
		}

	}

}
