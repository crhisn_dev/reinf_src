package br.com.tti.sefaz.cache;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.Query;
import com.sun.jmx.snmp.internal.SnmpModelLcd;

import br.com.tti.sefaz.connector.ConnectorProxy;
import br.com.tti.sefaz.listeners.DataListener;
import br.com.tti.sefaz.listeners.DataNotifier;
import br.com.tti.sefaz.listeners.impl.EventManagerListenerImpl;
import br.com.tti.sefaz.listeners.impl.LogDatabaseImpl;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.persistence.LogXML;
import br.com.tti.sefaz.persistence.SetData;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.persistence.dao.XMLDaoJPA;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.MainParameters;

public class XMLDataCache implements DataNotifier {

	private static XMLDataCache cache = null;

	public static XMLDataCache getInstance() {
		if (cache == null) {
			cache = new XMLDataCache();
			cache.addListener(new EventManagerListenerImpl());
			cache.addListener(new LogDatabaseImpl());
			if (MainParameters.isActiveconnector()) {
				cache.addListener(new ConnectorProxy());
			}
		}
		return cache;
	}

	private Vector<DataListener> listeners;
	private XMLDaoJPA<XMLData> daoxml;
	private DAO<LogXML> daolog;

	public XMLDataCache() {
		this.listeners = new Vector<DataListener>();
		this.daoxml = (XMLDaoJPA<XMLData>) DaoFactory.createDAO(XMLData.class);
		this.daolog = DaoFactory.createDAO(LogXML.class);
	}

	synchronized public XMLData findData(String keyXml) throws Exception {
		return this.daoxml.findEntity(keyXml);
	}

	synchronized public void simpleSaveState(XMLData date) throws Exception {
		XMLData dataExits = this.daoxml.findEntity(date.getKeyXml());
		if (dataExits == null) {
			this.daoxml.saveEntity(date);
			this.daoxml.flush();
			// this.notifyData(date);
		} else {
			// this.daoXml.updateEntity(date);
		}
	}

	private boolean checkWasSend(String keyxml) {
		Query q = this.daolog
				.createQuery("SELECT l FROM LogXML as l WHERE l.keyXml = :key and l.state = :status");
		q.setParameter("key", keyxml.startsWith("CTe") ? keyxml : "CTe"
				+ keyxml);
		q.setParameter("status", XML_STATE.ENVIADA);

		List<LogXML> result = q.getResultList();

		if (result != null) {
			return !result.isEmpty();
		}
		return false;
	}

	synchronized public void saveState(XMLData xmldata) throws Exception {

		/*KeyXml key = new KeyXml(xmldata.getKeyXml());
		String keyEpec = key.withTpEmissWithCTe(TPEMISS.CONTINGENCE_EPEC);
		String keyPapel = key.withTpEmissWithCTe(TPEMISS.CONTINGENCE);
		String keyNomal = key.withTpEmissWithCTe(TPEMISS.NORMAL);

		MyLogger.getLog().info("keyEpec:" + keyEpec);
		MyLogger.getLog().info("keyPapel:" + keyPapel);
		MyLogger.getLog().info("keyNomal:" + keyNomal);

		// TODO: Caso 1: CTe normal enviado sem retorno ==> chega O mesmo CTe em
		// alguma contingencia
		if (this.daoxml.existEntity(keyNomal)) {
			XMLData xdNormal = this.daoxml.findEntity(keyNomal);
			if (XML_STATE.ENVIADA.equals(xdNormal.getState())) {

				MyLogger.getLog().info("caso 1");
				if (!TPEMISS.NORMAL.equals(xmldata.getModo())) {
					MyLogger.getLog().info("caso 1a");

					// Caso 1: Nao fazer nada - Nao processar novo CTe
					throw new Exception("CTe enviado sem retorno da SEFAZ:"
							+ xdNormal.getKeyXml() + " state: "
							+ xdNormal.getState().toString());
				} else {
					MyLogger.getLog().info("caso 1b");
					// Novo CTe enviado em normal e nao teve retorno
					// reprocessar CTe?? por q pode ter autorizado
				}
			} else {
				MyLogger.getLog().info("caso 2");

				// TODO: Caso 2: CTe normal nao enviado ==> chega o mesmo
				// CTe em alguma contingencia

				if (!TPEMISS.NORMAL.equals(xmldata.getModo())) {
					try {
						MyLogger.getLog().info("caso 2a");
						// Caso 2: apagar cte em normal e processar nova
						this.daoxml.removeEntity(this.daoxml
								.findEntity(keyNomal));
						this.daoxml.flush();
					} catch (Exception e) {
						MyLogger.getLog().log(Level.INFO,
								e.getLocalizedMessage(), e);
					}
				} else {
					MyLogger.getLog().info("caso 2b");

				}
			}
		} else if (this.daoxml.existEntity(keyEpec)) {
			MyLogger.getLog().info("caso 3");

			// TODO: Caso 3: CTe com EPEC autorizado => chega CTe normal

			XMLData xdEpec = this.daoxml.findEntity(keyEpec);
			if (xdEpec.getState().equals(XML_STATE.EPEC_ENVIADO)) {
				MyLogger.getLog().info("caso 3a");
				// EPEC enviado e autorizado: nao processar novo CTe
				// --> permitir o RE-envio de EPECthrow new
				// Exception("EPEC do CTe ja autorizado na SEFAZ");
			} else {
				try {
					MyLogger.getLog().info("caso 3b");
					// EPEC nao autorizado: apagar CTe em EPEC e processar novo
					this.daoxml.removeEntity(xdEpec);
					this.daoxml.flush();
				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(),
							e);
				}
			}
		} else if (this.daoxml.existEntity(keyPapel)) {
			MyLogger.getLog().info("caso 4");

			// TODO: Caso 4: CTe com Papel Moeda impresso ==> chega CTe normal

			XMLData xdPapel = this.daoxml.findEntity(keyPapel);
			if (xdPapel.getState().equals(XML_STATE.GERADA)
					&& xdPapel.getModo().equals(TPEMISS.CONTINGENCE)) {
				MyLogger.getLog().info("caso 4a");
				// papel moeda impresso
			} else {
				try {
					MyLogger.getLog().info("caso 4b");
					// nao impresso ==> apagar CTe em papel
					this.daoxml.removeEntity(xdPapel);
					this.daoxml.flush();
				} catch (Exception e) {
					MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(),
							e);
				}
			}
		}*/

		MyLogger.getLog().info("caso default");
		MyLogger.getLog().info("modo:" + xmldata.getModo().toString());
		MyLogger.getLog().info("status:" + xmldata.getState().toString());

		if (this.daoxml.existEntity(xmldata.getKeyXml())) {
			MyLogger.getLog().info("exits");
			XMLData dataExits = this.daoxml.findEntity(xmldata.getKeyXml());
			if (dataExits.getModo() != null
					&& !dataExits.getModo().equals(TPEMISS.NORMAL)) {
				MyLogger.getLog().info("exits 1");
				xmldata.setModo(dataExits.getModo());
			}

			if (dataExits.getState() == null) {
				MyLogger.getLog().info("exits 2");
				this.daoxml.updateEntity(dataExits);
				this.daoxml.flush();
				return;
			}

			if (dataExits.getState().equals(XML_STATE.AUTORIZADA)
					&& dataExits.getAmbient().equals(
							SystemProperties.AMBIENT_HOMOLOGACAO)
					&& xmldata.getAmbient().equals(
							SystemProperties.AMBIENT_PRODUCAO)) {
				MyLogger.getLog().info("exits 3");
				this.daoxml.updateEntity(xmldata);
				this.daoxml.flush();
			}

			if (!dataExits.getState().equals(XML_STATE.AUTORIZADA)) {
				MyLogger.getLog().info("exits 4");
				this.daoxml.updateEntity(xmldata);
				this.daoxml.flush();
			}

			if (dataExits.getState().equals(XML_STATE.AUTORIZADA)
					&& dataExits.getAmbient().equals(
							SystemProperties.AMBIENT_HOMOLOGACAO)
					&& xmldata.getAmbient().equals(
							SystemProperties.AMBIENT_HOMOLOGACAO)) {
				throw new Exception("XML ja autorizado no ambiente: "
						+ dataExits.getAmbient());
			}

			if (dataExits.getState().equals(XML_STATE.AUTORIZADA)
					&& dataExits.getAmbient().equals(
							SystemProperties.AMBIENT_PRODUCAO)) {
				throw new Exception("XML ja autorizado no ambiente: "
						+ dataExits.getAmbient());
			}

		} else {
			MyLogger.getLog().info("non exits");
			this.daoxml.saveEntity(xmldata);
			this.daoxml.flush();
		}

		this.notifyData(xmldata);
	}

	synchronized public void updateState(String keyXml, XML_STATE stateV,
			Date date, String motivo, String xml, String autorizedProtocol) {
		XMLData state = this.daoxml.findEntity(keyXml);
		state.setState(stateV);
		state.setLastDataStateUpdate(date);
		state.setXMotivo(motivo);
		state.setAutorizedProtocol(xml);
		state.setNAutorizedProtocol(autorizedProtocol);
		
		this.daoxml.updateEntity(state);
		this.daoxml.flush();
		this.notifyData(state);
	}

	synchronized public void updateXMLData(XMLData data) {
		this.daoxml.updateEntity(data);
		this.daoxml.flush();
	}

	synchronized public void updateStateSefaz(String keyXml, XML_STATE stateV,
			Date date, String cStat, String xMotivo, String dhSefaz) {

		if (!keyXml.startsWith("CTe")) {
			keyXml = "CTe" + keyXml;
		}

		XMLData state = this.daoxml.findEntity(keyXml);

		if (state == null) {
			MyLogger.getLog().info("XML not found key:" + keyXml);
			return;
		}

		state.setState(stateV);
		state.setLastDataStateUpdate(date);
		state.setSefazCode(Integer.parseInt(cStat));
		state.setXMotivo(xMotivo);
		state.setDhSefaz(dhSefaz);

		this.daoxml.updateEntity(state);
		this.daoxml.flush();

		this.notifyData(state);
	}

	synchronized public void updateSendState(String keyXml, String cStat,
			String xMotivo, String dhSefaz) {
		if (!keyXml.startsWith("CTe")) {
			keyXml = "CTe" + keyXml;
		}

		XMLData state = this.daoxml.findEntity(keyXml);
		state.setLastDataStateUpdate(Calendar.getInstance().getTime());
		state.setDateSended((Calendar.getInstance().getTime()));
		state.setState(XML_STATE.ENVIADA);
		state.setSefazCode(Integer.parseInt(cStat));
		state.setXMotivo(xMotivo);
		state.setDhSefaz(dhSefaz);

		this.daoxml.updateEntity(state);
		this.daoxml.flush();

		this.notifyData(state);
	}

	synchronized public void updateAutorizeState(String keyXml,
			String nProtocol, String xmlProtocol, String cStat, String xMotivo,
			String dhSefaz) {

		if (!keyXml.startsWith("CTe")) {
			keyXml = "CTe" + keyXml;
		}

		XMLData state = this.daoxml.findEntity(keyXml);
		Date current = Calendar.getInstance().getTime();
		state.setState(XML_STATE.AUTORIZADA);
		state.setDateAutorized(current);
		state.setLastDataStateUpdate(current);
		state.setSefazCode(Integer.parseInt(cStat));
		state.setXMotivo(xMotivo);
		state.setNAutorizedProtocol(nProtocol);
		state.setAutorizedProtocol(xmlProtocol);
		state.setDhSefaz(dhSefaz);

		this.daoxml.updateEntity(state);
		this.daoxml.flush();

		this.notifyData(state);
	}

	synchronized public void updateCancelState(String keyXml, String nProtocol,
			String xmlProtocol, String cStat, String xMotivo, String dhSefaz,
			String just) {
		if (!keyXml.startsWith("CTe")) {
			keyXml = "CTe" + keyXml;
		}
		XMLData state = this.daoxml.findEntity(keyXml);
		Date current = Calendar.getInstance().getTime();

		state.setState(XML_STATE.CANCELADA);

		state.setLastDataStateUpdate(current);
		state.setSefazCode(Integer.parseInt(cStat));
		state.setXMotivo(xMotivo);

		String request = state.getCancelProtocol();

		if (request != null) {
			state.setCancelProtocol(request + xmlProtocol);
		} else {
			state.setCancelProtocol(xmlProtocol);
		}

		state.setNCancelProtocol(nProtocol);
		state.setDhSefaz(dhSefaz);
		// state.setDescription(just);

		this.daoxml.updateEntity(state);
		this.daoxml.flush();
		this.notifyData(state);
	}

	synchronized public void updateInutState(String keyXml, String nProtocol,
			String xmlProtocol, String cStat, String xMotivo, String dhSefaz,
			String just) {
		if (!keyXml.startsWith("CTe")) {
			keyXml = "CTe" + keyXml;
		}

		XMLData state = this.daoxml.findEntity(keyXml);
		Date current = Calendar.getInstance().getTime();

		state.setState(XML_STATE.INUTLIZADA);

		state.setLastDataStateUpdate(current);
		state.setSefazCode(Integer.parseInt(cStat));
		state.setXMotivo(xMotivo);

		String requestxml = state.getInutProtocol();

		if (requestxml == null) {
			state.setInutProtocol(requestxml + xmlProtocol);
		} else {
			state.setInutProtocol(xmlProtocol);
		}

		state.setNInutProtocol(nProtocol);
		state.setDhSefaz(dhSefaz);
		// state.setDescription(just);

		this.daoxml.updateEntity(state);
		this.daoxml.flush();
		this.notifyData(state);
	}

	synchronized public void forceFlushCache() {
		this.daoxml.flush();
	}

	@Override
	public void addListener(DataListener l) {
		this.listeners.add(l);
	}

	@Override
	public void notifyData(XMLData data) {
		for (DataListener l : this.listeners) {
			l.process(data);
		}
	}

	@Override
	public void notifyData(SetData data) throws RemoteException {
		for (DataListener l : this.listeners) {
			l.process(data);
		}
	}
}
