package br.com.tti.sefaz.messenger;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.UUID;
import java.util.logging.Level;

import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.contingence.StateManager;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.printer.XMLPrinter;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.remote.MessengerConfig;
import br.com.tti.sefaz.sender.email.EmailSender;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.REINF_EVENT;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.util.FileLocator;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.ModoOpManager;

public class MessengerController extends Thread {

	// cnpj, ambient, modo
	private Hashtable<String, Hashtable<String, TPEMISS>> modos;
	private XMLPrinter printer;
	private ManagerInterface manager;
	private BufferFactory factory;

	private ModoOpManager managerModo;

	private XMLDataCache cacheXml;

	// buffers: cnpj, ambiente, buffer
	private Hashtable<String, Hashtable<String, XMLBuffer>> buffers;

	public MessengerController(ManagerInterface manager) {
		super();
		this.manager = manager;
		this.factory = BufferFactory.getFactory(this.manager);
		this.modos = new Hashtable<String, Hashtable<String, TPEMISS>>();
		this.buffers = new Hashtable<String, Hashtable<String, XMLBuffer>>();
		this.cacheXml = XMLDataCache.getInstance();
		this.managerModo = new ModoOpManager();
		this.initModos();
	}

	private void initModos() {
		try {
			Hashtable<String, CNPJData> cnpjs = this.manager.getCNPJ();
			for (String cnpj : cnpjs.keySet()) {
				Hashtable<String, TPEMISS> mss = new Hashtable<String, TPEMISS>();

				TPEMISS modo = this.managerModo.getModo(cnpj);
				if (modo == null) {
					modo = TPEMISS.NORMAL;
					this.managerModo.saveModo(cnpj, modo, "", "");
				}
				mss.put(SystemProperties.AMBIENT_HOMOLOGACAO, modo);
				mss.put(SystemProperties.AMBIENT_PRODUCAO, modo);
				this.modos.put(cnpj, mss);

			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Hashtable<String, TPEMISS> mss = new Hashtable<String, TPEMISS>();
		mss.put(SystemProperties.AMBIENT_HOMOLOGACAO, TPEMISS.NORMAL);
		mss.put(SystemProperties.AMBIENT_PRODUCAO, TPEMISS.NORMAL);
		this.modos.put("00000000000000", mss);
	}

	private XMLBuffer getBufferReference(String cnpj, String ambient) {
		Hashtable<String, XMLBuffer> tableBuffer = this.buffers.get(cnpj);

		if (tableBuffer == null) {
			tableBuffer = new Hashtable<String, XMLBuffer>();
			this.buffers.put(cnpj, tableBuffer);
		}

		XMLBuffer buffer = tableBuffer.get(ambient);
		if (buffer == null) {
			buffer = this.factory.createBuffer(cnpj, ambient);
			tableBuffer.put(ambient, buffer);
		}

		return buffer;
	}

	private XMLData persistXml(String keyXml, String xml, String uf, String cnpjSender, String cnpjReceiver,
			String dateEmiss, String ambient, TPEMISS modo, String message, XML_STATE state,
			Hashtable<String, String> props) throws Exception {

		XMLData xmlData = new XMLData();
		xmlData.setKeyXml(keyXml);
		xmlData.setAmbient(ambient);
		xmlData.setCnpjEmit(cnpjSender);
		xmlData.setCnpjDest(cnpjReceiver);
		xmlData.setDateEmiss(dateEmiss);
		xmlData.setXmlString(xml);
		xmlData.setState(state);
		xmlData.setUf(uf);
		xmlData.setModo(modo);
		xmlData.setDateCreate(Calendar.getInstance().getTime());
		xmlData.setXMotivo(message);

		xmlData.setEvent_type(REINF_EVENT.values()[Integer.parseInt(props.get("reinf_event"))]);

		KeyXml key = new KeyXml(keyXml);

		try {
			xmlData.setNumero(Integer.parseInt(key.getNumeroNota()));
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		try {
			xmlData.setSerie(Integer.parseInt(key.getSerie()));
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		try {
			if (props != null) {
				if (props.containsKey("ID_PK")) {
					String id_pk = props.get("ID_PK");
					if (id_pk != null && !id_pk.isEmpty()) {
						xmlData.setId_pk_evento(Integer.parseInt(id_pk));
					}
				}

				if (props.containsKey("email")) {
					String email = props.get("email");
					if (email != null && !email.isEmpty()) {
						xmlData.setEmail(email);
					}
				}

				if (props.containsKey("xmlprint")) {
					String xmlprint = props.get("xmlprint");
					xmlData.setXmlStringprint(xmlprint);
				}

				if (props.containsKey("impressora")) {
					String impressora = props.get("impressora");
					xmlData.setImpressora(impressora);
				}
			}
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		MyLogger.getLog().info("setting xmotivo:" + xmlData.getXMotivo());
		this.cacheXml.saveState(xmlData);
		this.cacheXml.forceFlushCache();

		return xmlData;
	}

	public void changeToModo(String cnpj, String ambient, TPEMISS modo, String xjust, String dhcont) {
		MyLogger.getLog().info("Changing state: Messenger" + " for: " + cnpj + " with: " + modo);

		this.managerModo.saveModo(cnpj, modo, xjust, dhcont);
		this.modos.get(cnpj).put(ambient, modo);
		this.buffers = new Hashtable<String, Hashtable<String, XMLBuffer>>();
	}

	public void addXmlError(String keyXml, String xml, String uf, String cnpjSender, String cnpjReceiver,
			String dateEmiss, String ambient, String message, Hashtable<String, String> props) throws Exception {
		MyLogger.getLog().info("message:" + message);
		TPEMISS modo = this.modos.get(cnpjSender).get(ambient);
		this.persistXml(keyXml, xml, uf, cnpjSender, cnpjReceiver, dateEmiss, ambient, modo, message,
				XML_STATE.ERRO_VALIDACAO, props);
	}

	private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	public void addXml(String keyXml, String xml, String uf, String cnpjSender, String cnpjReceiver, String dateEmiss,
			String ambient, Hashtable<String, String> props) throws Exception {

		TPEMISS modo = this.modos.get(cnpjSender).get(ambient);
		MyLogger.getLog().info("modos CNPJ " + cnpjSender + " ambient: " + ambient + " modo: " + modo);

		boolean reenvioeopec = false;

		if (props.containsKey("epec")) {
			reenvioeopec = Boolean.parseBoolean(props.get("epec"));
			MyLogger.getLog().info("Chegou como EPEC:" + props.toString());
		}

		XMLData xmldata = null;
		if (!reenvioeopec) {
			xmldata = this.persistXml(keyXml, xml, uf, cnpjSender, cnpjReceiver, dateEmiss, ambient, modo, null,
					XML_STATE.GERADA, props);
		}

		if (props != null && props.containsKey("emails")) {
			String emails = props.get("emails");
			EmailSender.getInstance().registerKey(keyXml, emails);
		}

		if (modo.equals(SystemProperties.TPEMISS.CONTINGENCE)) {
			MyLogger.getLog().info("In contingence: " + keyXml);
			MyLogger.getLog().info("modo: " + modo);
			// this.printer.printXml(keyXml, xml, modo);

			Hashtable<String, String> prop = new Hashtable<String, String>();
			if (xmldata != null) {
				prop.put("DATAGERADA", this.df.format(xmldata.getDateCreate()));
				prop.put("IMPRESSORA", (xmldata.getImpressora() != null ? xmldata.getImpressora() : ""));

				prop.put("pdffile", FileLocator.getInstance().findPDF(xmldata.getCnpjEmit(), xmldata.getDateCreate(),
						xmldata.getKeyXml()));
				prop.put("psfile", FileLocator.getInstance().findPS(xmldata.getCnpjEmit(), xmldata.getDateCreate(),
						xmldata.getKeyXml()));
			} else {
				MyLogger.getLog().info("XMLData as null!! NFe:" + keyXml);
			}

			this.manager.printXml(keyXml, xml, modo, prop);
		}

		if (modo.equals(SystemProperties.TPEMISS.CONTINGENCE_EPEC)) {
			// this.printer.printXml(keyXml, xml, modo);
			this.manager.processInContingence(keyXml, ambient, xml);
		}

		if (modo.equals(SystemProperties.TPEMISS.NORMAL) || modo.equals(SystemProperties.TPEMISS.SVC_RS)
				|| modo.equals(SystemProperties.TPEMISS.SVC_SP)) {
			XMLBuffer buffer = this.getBufferReference(cnpjSender, ambient);
			buffer.addXmlToBuffer(keyXml, xml);
		}

	}

	private void checkTimeOuts() {
		for (String cnpj : this.buffers.keySet()) {
			Hashtable<String, XMLBuffer> ambientss = this.buffers.get(cnpj);
			for (String ambient : ambientss.keySet()) {
				TPEMISS modo = this.modos.get(cnpj).get(ambient);
				if (modo.equals(SystemProperties.TPEMISS.NORMAL) || modo.equals(SystemProperties.TPEMISS.SVC_RS)
						|| modo.equals(SystemProperties.TPEMISS.SVC_SP)) {
					Hashtable<String, XMLBuffer> table = this.buffers.get(cnpj);

					for (XMLBuffer buffer : table.values()) {
						long timeLapsed = Calendar.getInstance().getTimeInMillis() - buffer.getLastTime();
						if (timeLapsed > buffer.getTimeout() && !buffer.isBufferEmpty()) {
							buffer.sendSet();
						}
					}

				}
			}
		}
	}

	public void setParameters(String cnpj, String ambient, MessengerConfig msnConfig) throws RemoteException {
		XMLBuffer buffer = this.getBufferReference(cnpj, ambient);

		buffer.setSizeFile(msnConfig.getSizeFile());
		buffer.setSizeSet(msnConfig.getSizeFile());
		buffer.setTimeout(msnConfig.getTimeout());

		MyLogger.getLog().info("Update set size: " + buffer.getSizeSet());
		MyLogger.getLog().info("Update file size: " + buffer.getSizeFile());
		MyLogger.getLog().info("Update time-out: " + buffer.getTimeout());

	}

	public TPEMISS getModo(String cnpj, String ambient) {
		return this.managerModo.getModo(cnpj);
	}

	public Hashtable<String, String> getXJust(String cnpj) {
		return this.managerModo.getXJustDHCont(cnpj);
	}

	@Override
	public void run() {
		while (true) {
			this.checkTimeOuts();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
