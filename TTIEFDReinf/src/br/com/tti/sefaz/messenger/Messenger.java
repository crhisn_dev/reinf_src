package br.com.tti.sefaz.messenger;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import br.com.tti.sefaz.classes.recepcao.TCTe;
import br.com.tti.sefaz.classes.recepcao.TCTe.InfCte.Compl.ObsCont;
import br.com.tti.sefaz.contingence.ModeOperation;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.remote.MessengerConfig;
import br.com.tti.sefaz.sender.email.EmailSender;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.REINF_EVENT;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.Locator;

public class Messenger implements MessengerInterface, ModeOperation {

	private ManagerInterface manager;
	private MessengerController controller;
	// private PreProcess preproc;

	public Messenger() {
		this.manager = Locator.getManagerReference();
		assert this.manager != null;
		this.controller = new MessengerController(this.manager);
		// this.preproc = new PreProcess();

		Thread t = new Thread(this.controller);
		t.start();
	}

	@Override
	public void sendXml(String keyXml, String xml, String cnpjSender, String cnpjReceiver, String dateEmiss,
			String message, String uf, String ambient, boolean sign, boolean error, Hashtable<String, String> prop)
			throws RemoteException {

		CNPJData cnpjdata = this.manager.getCNPJ().get(cnpjSender);
		ambient = cnpjdata.getAmbiente();

		MyLogger.getLog().info("CNPJData ambient:" + ambient);

		if (prop == null) {
			prop = new Hashtable<String, String>();
		}

		prop.put("xmlprint", xml);

		try {
			xml = new String(xml.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

		// MyLogger.getLog().info("xml utf:" + xml);

		RequestThread request = new RequestThread(this.manager, this.controller, keyXml, xml, cnpjSender, cnpjReceiver,
				dateEmiss, message, uf, ambient, sign, error, prop);
		Thread t = new Thread(request);
		t.start();
	}

	@Override
	public void setMessengerParameters(String cnpj, String ambient, MessengerConfig msnConfig) throws RemoteException {
		this.controller.setParameters(cnpj, ambient, msnConfig);

	}

	@Override
	public void changeToState(String cnpj, String ambient, TPEMISS modo, Hashtable<String, Object> props)
			throws RemoteException {
		this.controller.changeToModo(cnpj, ambient, modo, props.get("xjust").toString(),
				props.get("dhcont").toString());
	}

	@Override
	public void sendXml(String xml, Hashtable<String, String> prop) throws RemoteException {
		try {
			ProcessEventXML p = new ProcessEventXML();
			p.processXML(xml);

			String cnpjReceiver = "";
			String dateEmiss = "";
			String keyXml = p.getId();

			{
				keyXml = keyXml.replace("ID", "");
				int fill_length = 44 - keyXml.length();
				if (fill_length > 0) {
					keyXml = keyXml + "00000000000000000000000000000000".substring(0, fill_length + 1);
				}
			}

			String cnpjSender = p.getCnpj();
			REINF_EVENT event_type = p.getEvent_type();
			if (prop == null) {
				prop = new Hashtable<>();
			}
			prop.put("reinf_event", event_type.ordinal() + "");

			String uf = "";
			String ambient = SystemProperties.AMBIENT_PRODUCAO;

			xml = p.getReinf_string();

			this.sendXml(keyXml, xml, cnpjSender, cnpjReceiver, dateEmiss, p.getMessage(), uf, ambient, true,
					p.isError(), prop);

		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}

	@Override
	public boolean isActive() throws RemoteException {
		return true;
	}

	@Override
	public TPEMISS getModo(String cnpj, String ambient) throws RemoteException {
		return this.controller.getModo(cnpj, ambient);
	}

	@Override
	public Hashtable<String, String> getJust(String cnpj, String ambient, TPEMISS tpemiss) throws RemoteException {
		return this.controller.getXJust(cnpj);
	}

}
