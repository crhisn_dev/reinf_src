package br.com.tti.sefaz.messenger;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.logging.Level;

import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.EventData.EVENTO_STATUS;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.remote.events.ChangeStateXmlEvent;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.REINF_EVENT;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.xml.XMLRepare;

public class RequestThread implements Runnable {

	private ManagerInterface manager;
	private MessengerController controller;
	private String keyXml;
	private String xml;
	private String cnpjSender;
	private String cnpjReceiver;
	private String dateEmiss;
	private String message;
	private String uf;
	private String ambient;
	private Hashtable<String, String> props;

	private boolean sign;
	private boolean error;

	public RequestThread(ManagerInterface manager, MessengerController controller, String keyXml, String xml,
			String cnpjSender, String cnpjReceiver, String dateEmiss, String message, String uf, String ambient,
			boolean sign, boolean error, Hashtable<String, String> props) {
		super();
		this.manager = manager;
		this.controller = controller;
		this.keyXml = keyXml;
		this.xml = xml;
		this.cnpjSender = cnpjSender;
		this.cnpjReceiver = cnpjReceiver;
		this.dateEmiss = dateEmiss;
		this.message = message;
		this.uf = uf;
		this.ambient = ambient;
		this.sign = sign;
		this.error = error;
		this.props = props;
	}

	public boolean canProcess() {
		try {
			XMLData xmlData = XMLDataCache.getInstance().findData(this.keyXml);

			if (xmlData == null) {
				return true;
			}

			if (xmlData != null && xmlData.getState().equals(XML_STATE.AUTORIZADA)) {
				MyLogger.getLog().info("XML ja autorizado:" + xmlData.getKeyXml());

				ChangeStateXmlEvent notifyData = ChangeStateXmlEvent.createEvent(xmlData);
				this.manager.notifyDataConnector(notifyData);
				this.manager.notifyEvent(notifyData);

				return false;
			}

		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, "XMLData dont process: " + this.keyXml, e);
			return false;
		}
		return true;
	}

	@Override
	public void run() {

		if (!this.canProcess())
			return;

		System.out.println(xml);

		xml = XMLRepare.repareXmlForSign(xml);
		if (this.sign) {
			try {
				System.out.println(this.props);
				REINF_EVENT type = REINF_EVENT.values()[Integer.parseInt(props.get("reinf_event").toString())];

				String tag = "";

				if (REINF_EVENT.evtInfoContri.equals(type)) {
					tag = "evtInfoContri";
				}

				if (REINF_EVENT.evtInfoCPRB.equals(type)) {
					tag = "evtCPRB";
				}
				
				if (REINF_EVENT.evtServTom.equals(type)) {
					tag = "evtServTom";
				}
				
				if(REINF_EVENT.evtFechamento.equals(type)){
					tag = "evtFechaEvPer";
				}
				
				if(REINF_EVENT.evtReabreEvPer.equals(type)){
					tag = "evtReabreEvPer";
				}

				System.out.println("tag to sing: " + tag);
				xml = this.manager.signForCNPJ__Reinf(cnpjSender, xml, tag);
			} catch (RemoteException e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
			xml = XMLRepare.repareXmlAfterSign(xml);
		}
		if (this.error) {
			try {
				MyLogger.getLog().info("message:" + message);
				this.controller.addXmlError(keyXml, xml, uf, cnpjSender, cnpjReceiver, dateEmiss, ambient, message,
						props);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		} else {
			try {
				this.controller.addXml(keyXml, xml, uf, cnpjSender, cnpjReceiver, dateEmiss, ambient, props);
			} catch (Exception e) {
				MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
			}
		}
	}
}
