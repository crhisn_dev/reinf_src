package br.com.tti.sefaz.messenger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import br.com.tti.sefaz.persistence.EventData.EVENTO_STATUS;
import br.com.tti.sefaz.reinf.eventos.cprb.Reinf.EvtCPRB;
import br.com.tti.sefaz.reinf.eventos.fechamento.Reinf.EvtFechaEvPer;
import br.com.tti.sefaz.reinf.eventos.infocont.Reinf.EvtInfoContri;
import br.com.tti.sefaz.reinf.eventos.reabertura.Reinf.EvtReabreEvPer;
import br.com.tti.sefaz.reinf.eventos.servtomados.Reinf.EvtServTom;
import br.com.tti.sefaz.reinf.eventos.tabprocesso.Reinf.EvtTabProcesso;
import br.com.tti.sefaz.systemconfig.SystemProperties.REINF_EVENT;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.util.ReadFile;
import br.com.tti.sefaz.xml.XMLGenerator;
import br.com.tti.sefaz.xml.XMLValidator;

public class ProcessEventXML {

	private XMLGenerator gen;
	private String id;
	private String cnpj;
	private REINF_EVENT event_type;
	private String reinf_string;
	private String message;
	private boolean error;

	public ProcessEventXML() {
		this.gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.infocont");
	}

	private String findSubXML(String xml, String tag) throws Exception {

		Reader reader = new InputStreamReader(new ByteArrayInputStream(xml.getBytes()));

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		Document doc = dbf.newDocumentBuilder().parse(new InputSource(reader));

		NodeList nprotocolo = doc.getElementsByTagName(tag);

		System.out.println("length:" + nprotocolo.getLength());

		if (nprotocolo.item(0) != null) {

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer trans = tf.newTransformer();
			trans.transform(new DOMSource(nprotocolo.item(0)), new StreamResult(os));

			String xmlnfe = os.toString();

			return xmlnfe;
		}

		return null;
	}

	private String findSubXMLFirst(String xml, String tag) throws Exception {

		Reader reader = new InputStreamReader(new ByteArrayInputStream(xml.getBytes()));

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		Document doc = dbf.newDocumentBuilder().parse(new InputSource(reader));

		NodeList nprotocolo = doc.getElementsByTagName(tag);

		if (nprotocolo.item(0) != null) {

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer trans = tf.newTransformer();
			trans.transform(new DOMSource(nprotocolo.item(0)), new StreamResult(os));

			String xmlnfe = os.toString();

			return xmlnfe;
		}

		return null;
	}

	private void createID() {

	}

	public void processXML(String xml) throws Exception {
		this.error = false;
		this.reinf_string = this.findSubXML(xml, "Reinf");

		System.out.println("reinf: " + this.reinf_string);

		this.reinf_string = this.reinf_string.replaceAll("Hjid=\"0\"", "");
		this.reinf_string = this.reinf_string.replace("id=\"0\"", "id=\"ID1154157520000002019061114094300022\"");

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		String event_xml = this.findSubXMLFirst(this.reinf_string, "evtInfoContri");
		if (event_xml != null) {
			try {
				XMLValidator validator = new XMLValidator(MainParameters.getXSDEvento());
				validator.validateXml(this.reinf_string);
			} catch (SAXException e) {
				e.printStackTrace();
				this.message = e.getMessage();
				this.error = true;
				// return;
			}

			XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.infocont");
			EvtInfoContri evento = (EvtInfoContri) gen.toObject(event_xml);

			if (evento.getId() == null) {
				evento.setId("ID1154157520000002019061114094300022");
			}
			this.id = evento.getId();

			// this.id = evento.getId() == null ?
			// "ID1163696110001412018102317021800002" : evento.getId();

			this.cnpj = evento.getIdeContri().getNrInsc();
			this.event_type = REINF_EVENT.valueOf("evtInfoContri");
		}

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		event_xml = this.findSubXML(this.reinf_string, "evtServTom");
		if (event_xml != null) {

			try {
				XMLValidator validator = new XMLValidator(MainParameters.getXsdevtservtom());
				validator.validateXml(this.reinf_string);
			} catch (SAXException e) {
				e.printStackTrace();
				this.message = e.getMessage();
				this.error = true;
				// return;
			}

			XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.servtomados");
			EvtServTom evento = (EvtServTom) gen.toObject(event_xml);

			if (evento.getId() == null) {
				evento.setId("ID1154157520000002019061114094300022");
			}
			this.id = evento.getId();
			// this.id = evento.getId() == null ?
			// "ID1163696110001412018102317021800002" : evento.getId();
			this.cnpj = evento.getIdeContri().getNrInsc();
			this.event_type = REINF_EVENT.valueOf("evtServTom");
		}

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		event_xml = this.findSubXML(this.reinf_string, "evtReabreEvPer");
		if (event_xml != null) {
			try {
				XMLValidator validator = new XMLValidator(MainParameters.getXsdevtreabertura());
				validator.validateXml(this.reinf_string);
			} catch (SAXException e) {
				e.printStackTrace();
				this.message = e.getMessage();
				this.error = true;
				// return;
			}

			XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.reabertura");
			EvtReabreEvPer evento = (EvtReabreEvPer) gen.toObject(event_xml);

			if (evento.getId() == null) {
				evento.setId("ID1154157520000002019061114094300022");
			}
			this.id = evento.getId();
			// this.id = evento.getId() == null ?
			// "ID1163696110001412018102317021800002" : evento.getId();
			this.cnpj = evento.getIdeContri().getNrInsc();
			this.event_type = REINF_EVENT.valueOf("evtReabreEvPer");
		}

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		event_xml = this.findSubXML(this.reinf_string, "evtFechaEvPer");
		if (event_xml != null) {
			try {
				XMLValidator validator = new XMLValidator(MainParameters.getXsdevtfechamento());
				validator.validateXml(this.reinf_string);
			} catch (SAXException e) {
				e.printStackTrace();
				this.message = e.getMessage();
				this.error = true;
				// return;
			}

			XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.fechamento");
			EvtFechaEvPer evento = (EvtFechaEvPer) gen.toObject(event_xml);

			this.id = evento.getId() == null ? "ID1163696110001412018102317021800002" : evento.getId();

			this.cnpj = evento.getIdeContri().getNrInsc();
			this.event_type = REINF_EVENT.valueOf("evtFechamento");
		} else {
			/////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////
			event_xml = this.findSubXML(xml, "evtCPRB");
			if (event_xml != null) {

				try {
					System.out.println(MainParameters.getXsdevtcprb());

					XMLValidator validator = new XMLValidator(MainParameters.getXsdevtcprb());
					validator.validateXml(xml);
				} catch (SAXException e) {
					e.printStackTrace();
					this.message = e.getMessage();
					this.error = true;
					// return;
				}

				XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.cprb");
				EvtCPRB evento = (EvtCPRB) gen.toObject(event_xml);

				this.id = evento.getId() == null ? "ID1163696110001412018102317021800002" : evento.getId();

				this.cnpj = evento.getIdeContri().getNrInsc();
				this.event_type = REINF_EVENT.valueOf("evtInfoCPRB");
			}

		}

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		event_xml = this.findSubXML(xml, "evtTabProcesso");
		if (event_xml != null) {
			try {
				XMLValidator validator = new XMLValidator(MainParameters.getXsdevtservtom());
				validator.validateXml(xml);
			} catch (SAXException e) {
				e.printStackTrace();
				this.message = e.getMessage();
				this.error = true;
				// return;
			}

			XMLGenerator gen = new XMLGenerator("br.com.tti.sefaz.reinf.eventos.tabprocesso");
			EvtTabProcesso evento = (EvtTabProcesso) gen.toObject(event_xml);

			this.id = evento.getId() == null ? "ID1163696110001412018102317021800002" : evento.getId();

			this.cnpj = evento.getIdeContri().getNrInsc();
			this.event_type = REINF_EVENT.valueOf("evtTabProcesso");
		}

	}

	public String getId() {
		return id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public REINF_EVENT getEvent_type() {
		return event_type;
	}

	public String getReinf_string() {
		return reinf_string;
	}

	public static void main(String[] args) {
		try {
			ProcessEventXML f = new ProcessEventXML();
			f.processXML(
					ReadFile.readFile("G:\\TTIDesktop\\sidney_environment\\myworkspace\\TTIEFDReinf\\src\\reinf.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

}
