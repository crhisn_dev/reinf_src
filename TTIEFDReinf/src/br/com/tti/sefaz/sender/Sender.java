package br.com.tti.sefaz.sender;

import java.rmi.RemoteException;
import java.util.Hashtable;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.remote.CNPJData;
import br.com.tti.sefaz.sender.dispatch.SenderDispatchController;
import br.com.tti.sefaz.systemconfig.SystemParameters;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.ModoOpManager;

public class Sender implements SenderInterface {

	private SenderGenericController senderController;
	private ManagerInterface manager;
	private ModoOpManager modomanager;
	private Hashtable<String, TPEMISS> modos;

	public Sender() {
		this.manager = Locator.getManagerReference();
		this.modomanager = new ModoOpManager();
		this.modos = new Hashtable<String, TPEMISS>();

		assert this.manager != null;

		if (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_NFE)) {
			this.senderController = new SenderDispatchController(this.manager);
		}
		// this.senderController = new SenderPortController(this.manager);

		if (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_CTE)) {
			this.senderController = new SenderDispatchController(this.manager);

			/*
			 * this.senderController = new SenderGenericControllerMock(
			 * this.manager);
			 */

		}

		this.initModos();

	}

	private void initModos() {
		try {
			Hashtable<String, CNPJData> cnpjs = this.manager.getCNPJ();
			for (String cnpj : cnpjs.keySet()) {
				TPEMISS modo = this.modomanager.getModo(cnpj);
				if (modo == null) {
					modo = TPEMISS.NORMAL;
					this.modomanager.saveModo(cnpj, modo, "", "");
				}
				this.modos.put(cnpj, modo);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String sendXMLMessage(String idServico, String header, String data,
			Hashtable prop) throws RemoteException {

		MyLogger.getLog().info("***** send message props:" + prop);

		String uf = prop.get("UF").toString();
		TPEMISS modo = this.modos.get(prop.get("CNPJ").toString());
		if (TPEMISS.SVC_RS.equals(modo)) {
			uf = "svcrs";
		}
		if (TPEMISS.SVC_SP.equals(modo)) {
			uf = "svcsp";
		}

		prop.put("UF", uf);

		return this.senderController.sendXMLMessage(idServico, header, data,
				prop);
	}

	@Override
	public boolean checkXMLMessage(String idServico, Hashtable prop)
			throws RemoteException {

		return false;
	}

	@Override
	public void changeToState(String cnpj, String ambient, TPEMISS modo,
			Hashtable<String, Object> props) throws RemoteException {
		MyLogger.getLog().info(
				"Changing state: Sender" + " for: " + cnpj + " with: " + modo);
		this.modomanager.saveModo(cnpj, modo, props.get("xjust").toString(),
				props.get("dhcont").toString());
		this.modos.put(cnpj, modo);

	}

	@Override
	public TPEMISS getModo(String cnpj, String ambient) throws RemoteException {

		return this.modos.get(cnpj);
	}

	@Override
	public Hashtable<String, String> getJust(String cnpj, String ambient,
			TPEMISS tpemiss) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
