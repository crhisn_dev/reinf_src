package br.com.tti.sefaz.sender.dispatch;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.List;

import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Dispatch;

import br.com.taragona.nfe.util.PropriedadesMain;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.reinf.eventos.retorno.Reinf;
import br.com.tti.sefaz.reinf.eventos.retorno.TArquivoReinf;
import br.com.tti.sefaz.remote.SenderConfig;
import br.com.tti.sefaz.remote.ServicesConfig;
import br.com.tti.sefaz.sender.SenderGenericController;
import br.com.tti.sefaz.sender.xml.XMLConfigSender;
import br.com.tti.sefaz.systemconfig.SystemConfigInterface;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.systemconfig.XMLConfigSystem;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.util.ReadFile;
import br.com.tti.sefaz.xml.XMLGenerator;

public class SenderDispatchController extends SenderGenericController {

	private DispatchFactory factory;

	private XMLConfigSender config;

	private InvokerDispatch invoker;

	// private SenderConfig conf;

	private ServicesConfig servicesConfig;

	public SenderDispatchController(SystemConfigInterface configurator) {
		super(configurator);

		try {
			// this.conf = configurator.getSenderConfig();
			this.servicesConfig = configurator.getServiceConfig();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// this.config = new XMLConfigSender(conf.getServicesFile());
		// this.config.parse();

		this.factory = new DispatchFactory(this.servicesConfig);
		this.invoker = new InvokerDispatch();
	}

	public SenderDispatchController(SenderConfig conf, ServicesConfig servicesConf) {
		super(conf);

		this.servicesConfig = servicesConf;

		// this.config = new XMLConfigSender(conf.getServicesFile());
		// this.config.parse();

		this.factory = new DispatchFactory(this.servicesConfig);
		this.invoker = new InvokerDispatch();
	}

	// @Override
	public String sendXMLMessage(String idServico, String header, String data, Hashtable prop) throws RemoteException {
		String ambient = (String) prop.get("AMBIENT");
		String uf = (String) prop.get("UF");

		MyLogger.getLog().info("uf:" + uf);
		MyLogger.getLog().info("amb:" + ambient);
		MyLogger.getLog().info("idServico:" + idServico);

		Dispatch<SOAPMessage> dispatch = this.factory.createDispath(uf, idServico, ambient);

		MyLogger.getLog().info("header:" + header);
		MyLogger.getLog().info("data:" + data);

		String response = null;
		try {
			response = this.invoker.invoke(dispatch, header, data);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RemoteException(e.getLocalizedMessage(), e.getCause());
		}

		MyLogger.getLog().info("response:" + response);

		

		return response;
	}

	public static void main(String[] args) {
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dumpTreshold", "9999");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "9999");

		MainParameters.processArguments(args);

		ajustarPropriedadesFwd("C:\\TTIRec\\certificados\\myroot.jks", "C:\\TTIReinf\\certificados\\comm.pfx",
				"crhisn", "61403800");
		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "true");

		// XMLConfigSystem xc = new XMLConfigSystem(
		// "E:\\TTICTe\\conf\\configuracao_cte.xml");

		XMLConfigSystem xc = new XMLConfigSystem("C:\\TTICTe\\conf\\configuracao_red.xml");
		xc.make();
		xc.getServiceConfig();

		// SystemConfigInterface conf = new
		SenderDispatchController sender = new SenderDispatchController(xc.getSenderConfig(), xc.getServiceConfig());

		String cabecalho = "<cteCabecMsg xmlns=\"http://www.portalfiscal.inf.br/cte/wsdl/CteStatusServico\">"
				+ "<cUF>35</cUF>" + "<versaoDados>1.01</versaoDados>" + "</cteCabecMsg>";

		String dados = "<consStatServCte xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\"1.01\">"
				+ "<tpAmb>2</tpAmb>" + "<xServ>STATUS</xServ>" + "</consStatServCte>";

		Hashtable<String, String> prop = new Hashtable<String, String>();
		prop.put("AMBIENT", SystemProperties.AMBIENT_HOMOLOGACAO);
		prop.put("UF", "35");

		try {
			String data1 = ReadFile
					.readFile("G:\\TTIDesktop\\sidney_environment\\myworkspace\\TTIEFDReinf\\src\\b.xml");

			System.out.println(sender.sendXMLMessage(SystemProperties.ID_SERVICO_RETRECEPCAO, cabecalho, data1, prop));
		} catch (Exception e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void ajustarPropriedadesFwd(String fileTrust, String fileKey, String passwordTrust,
			String passwordKey) {
		System.setProperty("javax.net.ssl.keyStore", fileKey);
		System.setProperty("javax.net.ssl.trustStore", fileTrust);
		System.setProperty("javax.net.ssl.keyStorePassword", passwordKey);
		System.setProperty("javax.net.ssl.trustStorePassword", passwordTrust);
		System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
	}

}
