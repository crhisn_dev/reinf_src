package br.com.tti.sefaz.sender.dispatch;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;

import br.com.taragona.nfe.sender.util.MyAuthenticator;
import br.com.tti.sefaz.exceptions.MySenderException;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.util.ReadFile;

public class InvokerDispatch {

	private MessageFactory factory;

	public InvokerDispatch() {
		try {
			factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
		} catch (SOAPException e) {
			e.printStackTrace();
		}
	}

	public SOAPMessage createSOAPMessage(String header, String data, String version) {
		String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
				+ "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
				+ "<soap12:Header>" + header + "</soap12:Header><soap12:Body>" + data
				+ "</soap12:Body></soap12:Envelope>";

		MyLogger.getLog().info(request);

		try {
			SOAPMessage message = this.factory.createMessage();
			message.getSOAPPart().setContent((Source) new StreamSource(new StringReader(request)));
			message.saveChanges();
			return message;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public SOAPMessage createSOAPMessage11(String header, String data, String version) {
		String request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
				+ " xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "<SOAP-ENV:Header />"
				+ "<s:Body xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
				+ data + "</s:Body></s:Envelope>";

		MyLogger.getLog().info(request);

		try {
			SOAPMessage message = this.factory.createMessage();
			message.getSOAPPart().setContent((Source) new StreamSource(new StringReader(request)));
			message.saveChanges();

			return message;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getSOAPMessageAsString(SOAPMessage msg) throws Exception {
		ByteArrayOutputStream baos = null;
		String s = null;
		msg.setProperty(javax.xml.soap.SOAPMessage.CHARACTER_SET_ENCODING, "ISO-8859-1");
		try {
			baos = new ByteArrayOutputStream();
			msg.writeTo(baos);
			s = baos.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	public String invoke(Dispatch<SOAPMessage> dispatch, String header, String data) throws Exception {

		if (MainParameters.isProxy()) {
			dispatch.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, MyAuthenticator.loginStatic);
			dispatch.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, MyAuthenticator.passStatic);
		}

		SOAPMessage soap = createSOAPMessage11(header, data, null);
		SOAPMessage response = null;
		try {
			response = dispatch.invoke(soap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new MySenderException(e, MySenderException.ERROR_CALL_SERVICE);
		}
		return getSOAPMessageAsString(response);
	}
}
