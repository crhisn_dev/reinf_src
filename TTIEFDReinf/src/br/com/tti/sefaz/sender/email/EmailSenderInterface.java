package br.com.tti.sefaz.sender.email;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Hashtable;

public interface EmailSenderInterface extends Remote {

	public void sendXMLProtEmail(String keyxml, String emails, Hashtable<String, Object> props)
			throws RemoteException;
}
