package br.com.tti.sefaz.sender.email;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import br.com.tti.sefaz.cache.XMLDataCache;
import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.manager.ManagerInterface;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.remote.SchemaVersionConfig;
import br.com.tti.sefaz.systemconfig.SystemParameters;
import br.com.tti.sefaz.systemconfig.SystemProperties;
import br.com.tti.sefaz.util.FileLocator;
import br.com.tti.sefaz.util.KeyXml;
import br.com.tti.sefaz.util.Locator;
import br.com.tti.sefaz.util.MainParameters;
import br.com.tti.sefaz.xmlgenerate.XMLMessageFactory;
import br.com.tti.sefaz.xmlgenerate.cte.XMLMessageFactoryCTe;
import br.com.tti.sefaz.xmlgenerate.nfe.XMLMessageFactoryNFe;

import com.sun.mail.smtp.SMTPAddressFailedException;
import com.sun.mail.smtp.SMTPAddressSucceededException;
import com.sun.mail.smtp.SMTPSendFailedException;
import com.sun.mail.smtp.SMTPTransport;

public class EmailSender {

	// private ResourceBundle bundlescnpjs;

	private ResourceBundle bundleconfig;
	private Hashtable<String, String> emails = new Hashtable<String, String>();
	private XMLMessageFactoryCTe factory;

	private boolean autenticar;
	private boolean ssl;

	private static EmailSender INSTANCE = null;

	public static EmailSender getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EmailSender();
		}

		return INSTANCE;
	}

	/* private static MailSender sender = null; */

	/*
	 * public static MailSender getInstance() { if (sender == null) { sender =
	 * new MailSender(); } return sender; }
	 */

	private EmailSender() {

		if (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_NFE)) {
			// this.factory = new XMLMessageFactoryNFe(null);
		}

		if (SystemParameters.system.equals(SystemProperties.SYSTEM.TTI_CTE)) {
			this.factory = new XMLMessageFactoryCTe(
					new Hashtable<Vector<String>, Vector<SchemaVersionConfig>>());
		}

		try {
			this.bundleconfig = ResourceBundle.getBundle("configuracao_email");
			this.autenticar = Boolean.parseBoolean(this.bundleconfig
					.getString("autenticar"));
			this.ssl = Boolean.parseBoolean(this.bundleconfig.getString("ssl"));

		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}
	}

	public void sendEmailFiles(String keyxml, boolean xml, boolean pdf) {
		try {
			String emails = this.emails.get(keyxml);
			Vector<String> filesv = new Vector<String>();

			if (xml) {
				File fileprot = this.factory.createXMLAutorizedProt(keyxml,
						null);

				/*
				 * File file = File.createTempFile(keyxml, "_prot.xml");
				 * 
				 * 
				 * OutputStreamWriter out = new OutputStreamWriter( new
				 * FileOutputStream(file.getAbsoluteFile()), "UTF-8");
				 * out.write(protxml); out.close();
				 */

				filesv.add(fileprot.getAbsolutePath());
			}

			if (pdf) {
				ManagerInterface manager = Locator.getManagerReference();
				byte[] pdfcontent = manager.findPDF(keyxml);

				File file = File.createTempFile(keyxml, ".pdf");
				FileOutputStream fos = new FileOutputStream(file);
				fos.write(pdfcontent);
				fos.close();

				filesv.add(file.getAbsolutePath());
			}

			MyLogger.getLog().info("send to:" + emails);
			MyLogger.getLog().info("files:" + filesv.toString());

			String[] files = new String[filesv.size()];
			filesv.toArray(files);

			/*
			 * FileWriter fw = new FileWriter(file); fw.write(protxml);
			 * fw.close();
			 */

			KeyXml key = new KeyXml(keyxml);

			this.sendRawEmail(key.getCnpj(), emails, files);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

	}

	public File findPDFFile(String keyxml) throws Exception {
		XMLData xmldata = XMLDataCache.getInstance().findData(keyxml);
		File pdffile = new File(FileLocator.getInstance().findPDF(
				xmldata.getCnpjEmit(), xmldata.getDateCreate(),
				xmldata.getKeyXml()));
		for (int i = 0; i < 15; i++) {
			if (pdffile.exists() && pdffile.length() > 0) {
				break;
			}
			MyLogger.getLog().info(
					"esperando gerar pdf:" + pdffile.getAbsolutePath());
			Thread.sleep(500);
		}

		if (pdffile.exists()) {
			return pdffile;
		}

		return null;
	}

	public void sendEmailCancel(String keyxml, String emailsdb, Date date) {

		try {

			if (!keyxml.startsWith("CTe")) {
				keyxml = "CTe" + keyxml;
			}

			String emails = this.emails.get(keyxml);

			if (emails == null) {
				emails = emailsdb;
			}

			KeyXml key = new KeyXml(keyxml);
		
			String cancelprotxml = this.factory.createCancelProt(keyxml, null);

			String filepath = FileLocator.getInstance().findXMLCancelProt(
					key.getCnpj(), date, keyxml);
			OutputStreamWriter out = new OutputStreamWriter(
					new FileOutputStream(filepath), "UTF-8");
			out.write(cancelprotxml);
			out.close();

			String[] files = new String[] { filepath };

			MyLogger.getLog().info("emails registrados:" + emails);
			for (String filess : files) {
				MyLogger.getLog().info("files:" + filess);
			}

			this.sendRawCancelEmail(key.getCnpj(), emails, files);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

	}
	
	public void sendRawCancelEmail(String cnpj, String tolist,
			String[] filenames) {

		if (cnpj == null)
			cnpj = "";

		String subj = null;
		String msg = null;
		String copias = null;

		try {
			copias = this.bundleconfig.getString("email_copias");
		} catch (Exception e) {
			// copias = bundleconfig.getString("email_copias");
		}

		/*
		 * try { to = this.emails.get(cnpj); } catch (Exception e) { to =
		 * bundlescnpjs.getString("default_email"); }
		 */

		if (tolist == null || tolist.isEmpty()) {
			tolist = bundleconfig.getString("default_email");
		}

		try {
			msg = this.bundleconfig.getString(cnpj + "_mensagem_cancelamento");
		} catch (Exception e) {
			msg = bundleconfig.getString("default_mensagem_cancelamento");
		}
		if (msg.isEmpty()) {
			msg = bundleconfig.getString("default_mensagem_cancelamento");
		}

		try {
			subj = this.bundleconfig.getString(cnpj + "_assunto_cancelamento");
		} catch (Exception e) {
			subj = bundleconfig.getString("default_assunto_cancelamento");
		}

		if (subj.isEmpty()) {
			subj = bundleconfig.getString("default_assunto_cancelamento");
		}

		if (copias != null) {
			tolist = tolist + "," + copias.trim();
		}

		String[] tos = tolist.split("\\,");

		MyLogger.getLog().info(
				"********************** Sending Email Cancel to:" + tolist);

		for (String file : filenames) {
			MyLogger.getLog().info(
					"********************** Sending Cancel Files:" + file);

		}

		try {
			myMail(tos, subj, msg, filenames);
		} catch (Exception e) {
			e.printStackTrace();
			tos = new String[1];
			tos[0] = bundleconfig.getString("default_email_erro");
			try {
				myMail(tos, subj, msg, filenames);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}


	
	public void sendEmailXMLProt(String keyxml) {
		try {
			if (!keyxml.startsWith("CTe")) {
				keyxml = "CTe" + keyxml;
			}

			String emails = this.emails.get(keyxml);

			File fileprot = this.factory.createXMLAutorizedProt(keyxml, null);

			/*
			 * File file = File.createTempFile(keyxml, "_prot.xml");
			 * OutputStreamWriter out = new OutputStreamWriter( new
			 * FileOutputStream(file.getAbsoluteFile()), "UTF-8");
			 * out.write(protxml); out.close();
			 */

			/*
			 * FileWriter fw = new FileWriter(file); fw.write(protxml);
			 * fw.close();
			 */
			String[] files = null;
			File pdffile = this.findPDFFile(keyxml);

			if (pdffile != null && MainParameters.isPdf()) {
				files = new String[] { fileprot.getAbsolutePath(),
						pdffile.getAbsolutePath() };
			} else {
				files = new String[] { fileprot.getAbsolutePath() };
			}

			MyLogger.getLog().info("emails registrados:" + emails);
			for (String filess : files) {
				MyLogger.getLog().info("files:" + filess);
			}

			KeyXml key = new KeyXml(keyxml);

			this.sendRawEmail(key.getCnpj(), emails, files);
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, e.getLocalizedMessage(), e);
		}

	}

	public void registerKey(String keyxml, String emails) {
		if (emails != null) {
			this.emails.put(keyxml, emails);
		} else {
			this.emails.put(keyxml, "");
		}
	}

	public void postMail(String[] recipients, String subject, String message,
			String from) throws MessagingException {

		boolean debug = false;

		// Set the host smtp address
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.jcom.net");

		// create some properties and get the default Session
		Session session = Session.getDefaultInstance(props, null);
		session.setDebug(debug);

		// create a message
		Message msg = new MimeMessage(session);

		// set the from and to address
		InternetAddress addressFrom = new InternetAddress(from);
		msg.setFrom(addressFrom);

		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}
		msg.setRecipients(Message.RecipientType.TO, addressTo);

		// Optional : You can also set your custom headers in the Email if you
		// Want
		msg.addHeader("MyHeaderName", "myHeaderValue");

		// Setting the Subject and Content Type
		msg.setSubject(subject);
		msg.setContent(message, "text/plain");
		Transport.send(msg);
	}

	public void myMail(String[] to, String subj, String mesagem,
			String[] filenames) throws Exception {
		/*
		 * if (!this.isConfig) { return; }
		 */

		String from = this.bundleconfig.getString("from").trim(); // "crhisnoriega.noriega@taragona.com.br";
		// // from address
		String subject = subj; // the subject line
		String message = mesagem; // the body of the message
		String mailhost = this.bundleconfig.getString("smtp_server").trim(); // "smtp.sao.terra.com.br";
		// //
		// SMTP
		// server
		final String user = this.bundleconfig.getString("usuario").trim(); // "taragona.crhistian";
		// // user ID
		final String password = this.bundleconfig.getString("senha").trim(); // "tarachr";
		// //
		// password
		// password
		Authenticator auth2 = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		};

		Properties props = System.getProperties();

		if (mailhost != null) {
			props.put("mail.smtp.host", mailhost);
			props.put("mail.smtps.host", mailhost);
		}
		if (this.autenticar) {
			props.put("mail.smtp.auth", "true");
		}

		if (this.ssl) {
			props.put("mail.transport.protocol", "smtps");
		} else {
			props.put("mail.transport.protocol", "smtp");
		}

		props.put("mail.smtp.port", this.bundleconfig.getString("port"));

		MyLogger.getLog().info("ssl:" + ssl);
		MyLogger.getLog().info("host:" + props.getProperty("mail.smtp.host"));
		MyLogger.getLog().info("port:" + props.getProperty("mail.smtp.port"));
		// Get a Session object
		javax.mail.Session session = javax.mail.Session.getInstance(props,
				auth2);

		// Construct the message
		javax.mail.Message msg = new MimeMessage(session);

		//String copia = this.bundleconfig.getString("copias");

		try {
			// Set message details
			InternetAddress[] addressTo = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++) {				
				addressTo[i] = new InternetAddress(to[i]);
			}

			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);

			/*
			 * if (copia != null && !copia.trim().isEmpty()) { InternetAddress[]
			 * addressCC = new InternetAddress[] { new InternetAddress( copia)
			 * }; msg.setRecipients(javax.mail.Message.RecipientType.CC,
			 * addressCC); }
			 */

			msg.setSubject(subject);
			msg.setSentDate(Calendar.getInstance().getTime() );
			msg.setText(message);

			Multipart mp = new MimeMultipart();

			MimeBodyPart mbp2 = new MimeBodyPart();
			mbp2.setText(message);
			mp.addBodyPart(mbp2);

			for (String filename : filenames) {

				if (filename != null) {
					MimeBodyPart mbp = new MimeBodyPart();
					FileDataSource fds = new FileDataSource(filename);
					mbp.setDataHandler(new DataHandler(fds));
					mbp.setFileName(fds.getName());
					mp.addBodyPart(mbp);
				}

				msg.setContent(mp);
			}
			// Send the thing off
			SMTPTransport t = (SMTPTransport) session
					.getTransport(ssl ? "smtps" : "smtp");
			try {
				if (this.autenticar) {
					t.connect(mailhost, user, password);
				} else {
					t.connect();
				}
				t.sendMessage(msg, msg.getAllRecipients());
			} finally {
				t.close();
			}
			MyLogger.getLog().info(
					"*********************** Mail was sent successfully to:"
							+ to[0]);
		} catch (Exception e) {
			if (e instanceof SendFailedException) {
				MessagingException sfe = (MessagingException) e;
				if (sfe instanceof SMTPSendFailedException) {
					SMTPSendFailedException ssfe = (SMTPSendFailedException) sfe;
					log("Smtp_Send_Failed:");
				}
				Exception ne;
				while ((ne = sfe.getNextException()) != null
						&& ne instanceof MessagingException) {
					sfe = (MessagingException) ne;
					if (sfe instanceof SMTPAddressFailedException) {
						SMTPAddressFailedException ssfe = (SMTPAddressFailedException) sfe;
						log("Address failed:");
						log(ssfe.toString());
						log("  Address: " + ssfe.getAddress());
						log("  Command: " + ssfe.getCommand());
						log("  Return Code: " + ssfe.getReturnCode());
						log("  Response: " + ssfe.getMessage());
					} else if (sfe instanceof SMTPAddressSucceededException) {
						log("Address succeeded:");
						SMTPAddressSucceededException ssfe = (SMTPAddressSucceededException) sfe;
					}
				}
			} else {
				log("Got Exception : " + e);
			}
			e.printStackTrace();
			throw e;
		}
	}

	public void myMailRaw(String[] to, String subj, String mesagem,
			String[] filenames) throws Exception {

		String from = "crhisnoriega@gmail.com"; // "crhisnoriega.noriega@taragona.com.br";
		// // from address
		String subject = subj; // the subject line
		String message = mesagem; // the body of the message
		String mailhost = "smtp.gmail.com"; // "smtp.sao.terra.com.br";
		// //
		// SMTP
		// server
		String user = "crhisnoriega"; // "taragona.crhistian";
		// // user ID
		String password = "@crhisn2572@"; // "tarachr"; //
		// password
		// password
		boolean auth = true;
		boolean ssl = true;
		Properties props = System.getProperties();

		if (mailhost != null) {
			props.put("mail.smtp.host", mailhost);
		}
		if (auth) {
			props.put("mail.smtp.auth", "true");
		}

		props.put("mail.smpt.port", "25");

		// Get a Session object
		javax.mail.Session session = javax.mail.Session
				.getInstance(props, null);

		// Construct the message
		javax.mail.Message msg = new MimeMessage(session);

		try {
			// Set message details
			InternetAddress[] addressTo = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
			}

			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			msg.setText(message);

			Multipart mp = new MimeMultipart();

			MimeBodyPart mbp2 = new MimeBodyPart();
			mbp2.setText(message);
			mp.addBodyPart(mbp2);

			for (String filename : filenames) {

				if (filename != null) {
					MimeBodyPart mbp = new MimeBodyPart();
					FileDataSource fds = new FileDataSource(filename);
					mbp.setDataHandler(new DataHandler(fds));
					mbp.setFileName(fds.getName());
					mp.addBodyPart(mbp);
				}

				msg.setContent(mp);
			}
			// Send the thing off
			SMTPTransport t = (SMTPTransport) session
					.getTransport(ssl ? "smtps" : "smtp");
			try {
				if (auth) {
					t.connect(mailhost, user, password);
				} else {
					t.connect();
				}
				t.sendMessage(msg, msg.getAllRecipients());
			} finally {
				t.close();
			}
			log("Mail was sent successfully.");
		} catch (Exception e) {
			if (e instanceof SendFailedException) {
				MessagingException sfe = (MessagingException) e;
				if (sfe instanceof SMTPSendFailedException) {
					SMTPSendFailedException ssfe = (SMTPSendFailedException) sfe;
					log("Smtp_Send_Failed:");
				}
				Exception ne;
				while ((ne = sfe.getNextException()) != null
						&& ne instanceof MessagingException) {
					sfe = (MessagingException) ne;
					if (sfe instanceof SMTPAddressFailedException) {
						SMTPAddressFailedException ssfe = (SMTPAddressFailedException) sfe;
						log("Address failed:");
						log(ssfe.toString());
						log("  Address: " + ssfe.getAddress());
						log("  Command: " + ssfe.getCommand());
						log("  Return Code: " + ssfe.getReturnCode());
						log("  Response: " + ssfe.getMessage());
					} else if (sfe instanceof SMTPAddressSucceededException) {
						log("Address succeeded:");
						SMTPAddressSucceededException ssfe = (SMTPAddressSucceededException) sfe;
					}
				}
			} else {
				log("Got Exception : " + e);
			}
			e.printStackTrace();
			throw e;
		}
	}

	public void log(String a) {
		MyLogger.getLog().info(a);
	}

	public void sendRawEmail(String cnpj, String tolist, String[] filenames) {

		if (cnpj == null)
			cnpj = "";

		String subj = null;
		String msg = null;
		String copias = null;

		try {
			copias = this.bundleconfig.getString("email_copias");
		} catch (Exception e) {
			// copias = bundleconfig.getString("email_copias");
		}

		/*
		 * try { to = this.emails.get(cnpj); } catch (Exception e) { to =
		 * bundlescnpjs.getString("default_email"); }
		 */

		if (tolist == null || tolist.isEmpty()) {
			tolist = bundleconfig.getString("default_email");
		}

		try {
			msg = this.bundleconfig.getString(cnpj + "_mensagem");
		} catch (Exception e) {
			msg = bundleconfig.getString("default_mensagem");
		}
		if (msg.isEmpty()) {
			msg = bundleconfig.getString("default_mensagem");
		}

		try {
			subj = this.bundleconfig.getString(cnpj + "_assunto");
		} catch (Exception e) {
			subj = bundleconfig.getString("default_assunto");
		}

		if (subj.isEmpty()) {
			subj = bundleconfig.getString("default_assunto");
		}

		if (copias != null && !copias.trim().isEmpty()) {
			tolist = tolist + "," + copias.trim();
		}

		String[] tos = tolist.split("\\,");

		MyLogger.getLog().info(
				"********************** Sending Email to:" + tolist);

		for (String file : filenames) {
			MyLogger.getLog().info(
					"********************** Sending Files:" + file);

		}

		try {
			myMail(tos, subj, msg, filenames);
		} catch (Exception e) {
			e.printStackTrace();
			tos = new String[1];
			tos[0] = bundleconfig.getString("default_email_erro");
			try {
				myMail(tos, subj, msg, filenames);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		EmailSender emailsender = new EmailSender();
		emailsender.sendRawEmail(null, "crhisnoriega@gmail.com",
				new String[] {});
	}
}