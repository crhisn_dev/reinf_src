package br.com.tti.sefaz.tools;

import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.persistence.CTeEvento;
import br.com.tti.sefaz.persistence.ModeOperationData;
import br.com.tti.sefaz.persistence.PFXFile;
import br.com.tti.sefaz.persistence.SetData;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.MyEntityManagerFactory;
import br.com.tti.sefaz.remote.DBConfig;
import br.com.tti.sefaz.systemconfig.XMLConfigSystem;
import br.com.tti.sefaz.systemconfig.SystemProperties.TPEMISS;
import br.com.tti.sefaz.util.MainParameters;

public class CreateDB {

	private XMLConfigSystem config;
	private DBConfig dbConfig;

	public CreateDB(String fileXml) {
		this.config = new XMLConfigSystem(fileXml);
		this.config.make();
		this.dbConfig = this.config.getDbConfig();
	}

	public CreateDB() {
		DBConfig db = new DBConfig();
		db.setDriver("com.mysql.jdbc.Driver");
		db.setPassword("crhisn@");
		db.setUrl("jdbc:mysql://localhost/tticte");
		db.setUser("root");
		this.dbConfig = db;
	}

	public void createDB() {

		dbConfig.setProp("create-tables");
		EntityManager em = MyEntityManagerFactory.createEntityManager(dbConfig);

		/*
		 * PFXFile f = new PFXFile(); f.setPfx("123"); f.setPassword("ds");
		 * 
		 * XMLData d = new XMLData(); d.setKeyXml("dfasf");
		 * 
		 * SetData sd = new SetData(); sd.setNumberSet("12");
		 * 
		 * ModeOperationData m = new ModeOperationData(); m.setCnpj("321");
		 * m.setModo(MODO_OP.NORMAL);
		 */

		CTeEvento e = new CTeEvento();
		e.setId_pk(11L);
		// e.setDacte(new byte[] {});

		EntityTransaction t = em.getTransaction();
		t.begin();
		// em.persist(sd);
		em.persist(e);
		// em.persist(d);
		// em.persist(m);
		t.commit();
		MyLogger.getLog().info("Tabelas criadas com sucesso!");
	}

	public static void main(String[] args) {
		try {
			MainParameters.processArguments(args);
			CreateDB dbc = new CreateDB(MainParameters.getXml());
			dbc.createDB();
		} catch (Exception e) {
			MyLogger.getLog().log(Level.INFO, "Problems in table creation!", e);
		}
	}
}
