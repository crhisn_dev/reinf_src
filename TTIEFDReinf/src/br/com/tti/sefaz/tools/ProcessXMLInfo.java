package br.com.tti.sefaz.tools;

import java.io.File;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.Query;

import br.com.tti.sefaz.log.MyLogger;
import br.com.tti.sefaz.persistence.XMLData;
import br.com.tti.sefaz.persistence.dao.DAO;
import br.com.tti.sefaz.persistence.dao.DaoFactory;
import br.com.tti.sefaz.remote.DBConfig;
import br.com.tti.sefaz.systemconfig.SystemProperties.XML_STATE;
import br.com.tti.sefaz.systemconfig.XMLConfigSystem;

public class ProcessXMLInfo {

	private DBConfig dbConfig;

	private DAO<XMLData> daodata;
	private Date d1;
	private Date d2;

	private String outputdir;

	public ProcessXMLInfo(Date d1, Date d2, String outputdir) {
		super();

		ResourceBundle db = ResourceBundle.getBundle("bancodados");
		this.dbConfig = new DBConfig();
		String driver = db.getString("driver");
		String user = db.getString("usuario");
		String password = db.getString("senha");
		String url = db.getString("url");

		this.dbConfig.setDriver(driver);
		this.dbConfig.setPassword(password);
		this.dbConfig.setUrl(url);
		this.dbConfig.setUser(user);

		this.d1 = d1;
		this.d2 = d2;
		this.outputdir = outputdir;

		this.daodata = DaoFactory.createDAO(XMLData.class, this.dbConfig);
	}

	private static String START_INFPROT = "<infProt";
	private static String END_INFPROT = "</infProt>";

	private String repareProtocol(String xml, String versao) {

		xml = xml.replace("TProtCte", "protCTe");
		xml = xml.replace("tProtCte", "protCTe");
		xml = xml.replace("xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\"",
				"");

		xml = xml.replace("xmlns=\"http://www.w3.org/2000/09/xmldsig#\"", "");
		xml = xml.replace("xmlns:ns2=\"http://www.portalfiscal.inf.br/cte\"",
				"");
		xml = xml.replace("ns2:", "");

		xml = xml
				.replace(
						"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>",
						"");

		int pos1 = xml.indexOf(START_INFPROT);
		int pos2 = xml.indexOf(END_INFPROT);
		if (pos1 != -1 && pos2 != -2) {
			xml = xml.substring(pos1, pos2 + END_INFPROT.length());
		}

		xml = "<protCTe versao=\"" + versao + "\">" + xml + "</protCTe>";
		return xml;
	}

	public String createXMLAutorizedProt(XMLData xmldata,
			Hashtable<String, String> prop2) {

		String xmlNota = xmldata.getXmlString();
		String xmlProtocol = xmldata.getAutorizedProtocol();

		if (xmlNota == null || xmlNota.isEmpty()) {
			return null;
		}

		String versao = "";
		if (xmlNota.contains("versao=\"1.04\">")) {
			versao = "1.04";
		} else {
			versao = "2.00";
		}

		if (xmlProtocol != null) {
			xmlProtocol = this.repareProtocol(xmlProtocol, versao);
		}

		String header = null;

		header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><cteProc xmlns=\"http://www.portalfiscal.inf.br/cte\" versao=\""
				+ versao + "\">";

		String xml = header + xmlNota + xmlProtocol + "</cteProc>";

		return xml;

	}

	public void process() {
		SimpleDateFormat sdff = new SimpleDateFormat("dd/MM/yyyy");
		MyLogger.getLog().info(
				"Processando data inicio: " + sdff.format(this.d1) + " ate: "
						+ sdff.format(this.d2));

		Query q = this.daodata
				.createQuery("select x from XMLData as x where x.dateCreate >= :d1 and x.dateCreate <= :d2");

		q.setParameter("d1", d1);
		q.setParameter("d2", d2);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + File.separator
				+ "MMM");

		List<XMLData> rr = q.getResultList();

		for (XMLData xmldata : rr) {
			if (!XML_STATE.AUTORIZADA.equals(xmldata.getState())) {
				continue;
			}

			MyLogger.getLog().info("Processando CTe: " + xmldata.getKeyXml());

			try {
				String xml = this.createXMLAutorizedProt(xmldata, null);
				String cnpjemit = xmldata.getCnpjEmit();
				String cnpjdest = xmldata.getCnpjDest();

				String dataff = sdf.format(xmldata.getDateCreate());

				String path = this.outputdir + File.separator + cnpjemit
						+ File.separator + cnpjdest + File.separator + dataff;

				File pathdir = new File(path);
				if (!pathdir.exists()) {
					pathdir.mkdirs();
				}

				File xmlfile = new File(pathdir.getAbsoluteFile()
						+ File.separator
						+ xmldata.getKeyXml().replace("CTe", "")
						+ "-procCTe.xml");
				FileWriter fw = new FileWriter(xmlfile);
				fw.write(xml);
				fw.close();
			} catch (Exception e) {
				MyLogger.getLog().warning(
						"Erro processando CTe: " + xmldata.getKeyXml()
								+ " erro: " + e.getLocalizedMessage());
			}
		}

	}

	public static void main(String[] args) throws Exception {
		MyLogger.getLog().info("Iniciando...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Date d11 = Calendar.getInstance().getTime();
		d11 = sdf.parse(args[1]);

		Date d22 = Calendar.getInstance().getTime();
		d22 = sdf.parse(args[2]);

		File fileoutput = new File(args[0]);
		if (!fileoutput.exists()) {
			fileoutput.mkdirs();
		}

		ProcessXMLInfo ii = new ProcessXMLInfo(d11, d22,
				fileoutput.getAbsolutePath());
		ii.process();
	}
}
