package br.com.tti.tcteimpressao.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Hashtable;

public interface CTeImpressao extends Remote {

	public void imprinirCTe(String id, String xml,
			Hashtable<String, String> prop) throws RemoteException;
}
