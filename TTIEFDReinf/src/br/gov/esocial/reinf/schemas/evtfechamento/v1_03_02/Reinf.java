//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.10 �s 09:36:18 AM BRT 
//

package br.gov.esocial.reinf.schemas.evtfechamento.v1_03_02;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
// import javax.xml.datatype.String;
// import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.StringAsDateTime;
// import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtFechaEvPer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="perApur">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                   &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]{1}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="8"/>
 *                                   &lt;maxLength value="14"/>
 *                                   &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideRespInf" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nmResp">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="70"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="cpfResp">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="11"/>
 *                                   &lt;pattern value="[0-9]{11}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="telefone" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="13"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="email" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="60"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoFech">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="evtServTm">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="1"/>
 *                                   &lt;enumeration value="S"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="evtServPr">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="1"/>
 *                                   &lt;enumeration value="S"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="evtAssDespRec">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="1"/>
 *                                   &lt;enumeration value="S"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="evtAssDespRep">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="1"/>
 *                                   &lt;enumeration value="S"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="evtComProd">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="1"/>
 *                                   &lt;enumeration value="S"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="evtCPRB">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="1"/>
 *                                   &lt;enumeration value="S"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="evtPgtos">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="1"/>
 *                                   &lt;enumeration value="S"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="compSemMovto" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                   &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtFechaEvPer" })
@XmlRootElement(name = "Reinf")
// @Entity(name = "Reinf")
@Table(name = "REINF__0")
@Inheritance(strategy = InheritanceType.JOINED)
public class Reinf implements Equals, HashCode {

	@XmlElement(required = true)
	protected Reinf.EvtFechaEvPer evtFechaEvPer;
	@XmlAttribute(name = "Hjid")
	@Transient
	protected Long hjid;

	/**
	 * Obt�m o valor da propriedade evtFechaEvPer.
	 * 
	 * @return possible object is {@link Reinf.EvtFechaEvPer }
	 * 
	 */
	@ManyToOne(targetEntity = Reinf.EvtFechaEvPer.class, cascade = { CascadeType.ALL })
	@JoinColumn(name = "EVT_FECHA_EV_PER_REINF___0_H_0")
	public Reinf.EvtFechaEvPer getEvtFechaEvPer() {
		return evtFechaEvPer;
	}

	/**
	 * Define o valor da propriedade evtFechaEvPer.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtFechaEvPer }
	 * 
	 */
	public void setEvtFechaEvPer(Reinf.EvtFechaEvPer value) {
		this.evtFechaEvPer = value;
	}

	/**
	 * Obt�m o valor da propriedade hjid.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	@Id
	@Column(name = "HJID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getHjid() {
		return hjid;
	}

	/**
	 * Define o valor da propriedade hjid.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setHjid(Long value) {
		this.hjid = value;
	}

	public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
			EqualsStrategy strategy) {
		if (!(object instanceof Reinf)) {
			return false;
		}
		if (this == object) {
			return true;
		}
		final Reinf that = ((Reinf) object);
		{
			Reinf.EvtFechaEvPer lhsEvtFechaEvPer;
			lhsEvtFechaEvPer = this.getEvtFechaEvPer();
			Reinf.EvtFechaEvPer rhsEvtFechaEvPer;
			rhsEvtFechaEvPer = that.getEvtFechaEvPer();
			if (!strategy.equals(LocatorUtils.property(thisLocator, "evtFechaEvPer", lhsEvtFechaEvPer),
					LocatorUtils.property(thatLocator, "evtFechaEvPer", rhsEvtFechaEvPer), lhsEvtFechaEvPer,
					rhsEvtFechaEvPer)) {
				return false;
			}
		}
		return true;
	}

	public boolean equals(Object object) {
		final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
		return equals(null, null, object, strategy);
	}

	public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
		int currentHashCode = 1;
		{
			Reinf.EvtFechaEvPer theEvtFechaEvPer;
			theEvtFechaEvPer = this.getEvtFechaEvPer();
			currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtFechaEvPer", theEvtFechaEvPer),
					currentHashCode, theEvtFechaEvPer);
		}
		return currentHashCode;
	}

	public int hashCode() {
		final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
		return this.hashCode(null, strategy);
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido
	 * dentro desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="perApur">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]{1}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="8"/>
	 *                         &lt;maxLength value="14"/>
	 *                         &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideRespInf" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="nmResp">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="70"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="cpfResp">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="11"/>
	 *                         &lt;pattern value="[0-9]{11}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="telefone" minOccurs="0">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;maxLength value="13"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="email" minOccurs="0">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;maxLength value="60"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="infoFech">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="evtServTm">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="1"/>
	 *                         &lt;enumeration value="S"/>
	 *                         &lt;enumeration value="N"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="evtServPr">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="1"/>
	 *                         &lt;enumeration value="S"/>
	 *                         &lt;enumeration value="N"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="evtAssDespRec">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="1"/>
	 *                         &lt;enumeration value="S"/>
	 *                         &lt;enumeration value="N"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="evtAssDespRep">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="1"/>
	 *                         &lt;enumeration value="S"/>
	 *                         &lt;enumeration value="N"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="evtComProd">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="1"/>
	 *                         &lt;enumeration value="S"/>
	 *                         &lt;enumeration value="N"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="evtCPRB">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="1"/>
	 *                         &lt;enumeration value="S"/>
	 *                         &lt;enumeration value="N"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="evtPgtos">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="1"/>
	 *                         &lt;enumeration value="S"/>
	 *                         &lt;enumeration value="N"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="compSemMovto" minOccurs="0">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri", "ideRespInf", "infoFech" })
	@Entity(name = "Reinf$EvtFechaEvPer")
	@Table(name = "EVT_FECHA_EV_PER")
	@Inheritance(strategy = InheritanceType.JOINED)
	@XmlRootElement(name="evtFechaEvPer")
	public static class EvtFechaEvPer implements Equals, HashCode {

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtFechaEvPer.IdeEvento ideEvento;

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtFechaEvPer.IdeContri ideContri;

		protected Reinf.EvtFechaEvPer.IdeRespInf ideRespInf;

		@XmlElement(required = true)
		protected Reinf.EvtFechaEvPer.InfoFech infoFech;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;
		@XmlAttribute(name = "Hjid")
		@Id
		@Column(name = "ID")
		protected Long hjid;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtFechaEvPer.IdeEvento }
		 * 
		 */
		// @ManyToOne(targetEntity =
		// br.gov.esocial.reinf.schemas.evtfechamento.v1_03_02.Reinf.EvtFechaEvPer.IdeEvento.class,
		// cascade = {
		// CascadeType.ALL })
		// @JoinColumn(name = "IDE_EVENTO_EVT_FECHA_EV_PER__0")
		public Reinf.EvtFechaEvPer.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtFechaEvPer.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtFechaEvPer.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtFechaEvPer.IdeContri }
		 * 
		 */
		// @ManyToOne(targetEntity =
		// br.gov.esocial.reinf.schemas.evtfechamento.v1_03_02.Reinf.EvtFechaEvPer.IdeContri.class,
		// cascade = {
		// CascadeType.ALL })
		// @JoinColumn(name = "IDE_CONTRI_EVT_FECHA_EV_PER__0")
		public Reinf.EvtFechaEvPer.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtFechaEvPer.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtFechaEvPer.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade ideRespInf.
		 * 
		 * @return possible object is {@link Reinf.EvtFechaEvPer.IdeRespInf }
		 * 
		 */
		// @ManyToOne(targetEntity =
		// br.gov.esocial.reinf.schemas.evtfechamento.v1_03_02.Reinf.EvtFechaEvPer.IdeRespInf.class,
		// cascade = {
		// CascadeType.ALL })
		// @JoinColumn(name = "IDE_RESP_INF_EVT_FECHA_EV_PE_0")
		public Reinf.EvtFechaEvPer.IdeRespInf getIdeRespInf() {
			return ideRespInf;
		}

		/**
		 * Define o valor da propriedade ideRespInf.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtFechaEvPer.IdeRespInf }
		 * 
		 */
		public void setIdeRespInf(Reinf.EvtFechaEvPer.IdeRespInf value) {
			this.ideRespInf = value;
		}

		/**
		 * Obt�m o valor da propriedade infoFech.
		 * 
		 * @return possible object is {@link Reinf.EvtFechaEvPer.InfoFech }
		 * 
		 */
		// @ManyToOne(targetEntity =
		// br.gov.esocial.reinf.schemas.evtfechamento.v1_03_02.Reinf.EvtFechaEvPer.InfoFech.class,
		// cascade = {
		// CascadeType.ALL })
		// @JoinColumn(name = "INFO_FECH_EVT_FECHA_EV_PER_H_0")
		public Reinf.EvtFechaEvPer.InfoFech getInfoFech() {
			return infoFech;
		}

		/**
		 * Define o valor da propriedade infoFech.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtFechaEvPer.InfoFech }
		 * 
		 */
		public void setInfoFech(Reinf.EvtFechaEvPer.InfoFech value) {
			this.infoFech = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		@Basic
		@Column(name = "ID", length = 36)
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * Obt�m o valor da propriedade hjid.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */

		@Column(name = "HJID")
		@GeneratedValue(strategy = GenerationType.AUTO)
		public Long getHjid() {
			return hjid;
		}

		/**
		 * Define o valor da propriedade hjid.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setHjid(Long value) {
			this.hjid = value;
		}

		public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
				EqualsStrategy strategy) {
			if (!(object instanceof Reinf.EvtFechaEvPer)) {
				return false;
			}
			if (this == object) {
				return true;
			}
			final Reinf.EvtFechaEvPer that = ((Reinf.EvtFechaEvPer) object);
			{
				Reinf.EvtFechaEvPer.IdeEvento lhsIdeEvento;
				lhsIdeEvento = this.getIdeEvento();
				Reinf.EvtFechaEvPer.IdeEvento rhsIdeEvento;
				rhsIdeEvento = that.getIdeEvento();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEvento", lhsIdeEvento),
						LocatorUtils.property(thatLocator, "ideEvento", rhsIdeEvento), lhsIdeEvento, rhsIdeEvento)) {
					return false;
				}
			}
			{
				Reinf.EvtFechaEvPer.IdeContri lhsIdeContri;
				lhsIdeContri = this.getIdeContri();
				Reinf.EvtFechaEvPer.IdeContri rhsIdeContri;
				rhsIdeContri = that.getIdeContri();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideContri", lhsIdeContri),
						LocatorUtils.property(thatLocator, "ideContri", rhsIdeContri), lhsIdeContri, rhsIdeContri)) {
					return false;
				}
			}
			{
				Reinf.EvtFechaEvPer.IdeRespInf lhsIdeRespInf;
				lhsIdeRespInf = this.getIdeRespInf();
				Reinf.EvtFechaEvPer.IdeRespInf rhsIdeRespInf;
				rhsIdeRespInf = that.getIdeRespInf();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideRespInf", lhsIdeRespInf),
						LocatorUtils.property(thatLocator, "ideRespInf", rhsIdeRespInf), lhsIdeRespInf,
						rhsIdeRespInf)) {
					return false;
				}
			}
			{
				Reinf.EvtFechaEvPer.InfoFech lhsInfoFech;
				lhsInfoFech = this.getInfoFech();
				Reinf.EvtFechaEvPer.InfoFech rhsInfoFech;
				rhsInfoFech = that.getInfoFech();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "infoFech", lhsInfoFech),
						LocatorUtils.property(thatLocator, "infoFech", rhsInfoFech), lhsInfoFech, rhsInfoFech)) {
					return false;
				}
			}
			{
				String lhsId;
				lhsId = this.getId();
				String rhsId;
				rhsId = that.getId();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId),
						LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
					return false;
				}
			}
			return true;
		}

		public boolean equals(Object object) {
			final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
			return equals(null, null, object, strategy);
		}

		public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
			int currentHashCode = 1;
			{
				Reinf.EvtFechaEvPer.IdeEvento theIdeEvento;
				theIdeEvento = this.getIdeEvento();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEvento", theIdeEvento),
						currentHashCode, theIdeEvento);
			}
			{
				Reinf.EvtFechaEvPer.IdeContri theIdeContri;
				theIdeContri = this.getIdeContri();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideContri", theIdeContri),
						currentHashCode, theIdeContri);
			}
			{
				Reinf.EvtFechaEvPer.IdeRespInf theIdeRespInf;
				theIdeRespInf = this.getIdeRespInf();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideRespInf", theIdeRespInf),
						currentHashCode, theIdeRespInf);
			}
			{
				Reinf.EvtFechaEvPer.InfoFech theInfoFech;
				theInfoFech = this.getInfoFech();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoFech", theInfoFech),
						currentHashCode, theInfoFech);
			}
			{
				String theId;
				theId = this.getId();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode,
						theId);
			}
			return currentHashCode;
		}

		public int hashCode() {
			final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
			return this.hashCode(null, strategy);
		}

		/**
		 * Informa��es de identifica��o do contribuinte
		 * 
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="8"/>
		 *               &lt;maxLength value="14"/>
		 *               &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		// @Entity(name = "Reinf$EvtFechaEvPer$IdeContri")
		@Table(name = "IDE_CONTRI__3")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeContri implements Equals, HashCode {

			protected short tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			@Basic
			@Column(name = "TP_INSC", scale = 0)
			public short getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(short value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_INSC", length = 14)
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			@Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtFechaEvPer.IdeContri)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtFechaEvPer.IdeContri that = ((Reinf.EvtFechaEvPer.IdeContri) object);
				{
					short lhsTpInsc;
					lhsTpInsc = this.getTpInsc();
					short rhsTpInsc;
					rhsTpInsc = that.getTpInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInsc", lhsTpInsc),
							LocatorUtils.property(thatLocator, "tpInsc", rhsTpInsc), lhsTpInsc, rhsTpInsc)) {
						return false;
					}
				}
				{
					String lhsNrInsc;
					lhsNrInsc = this.getNrInsc();
					String rhsNrInsc;
					rhsNrInsc = that.getNrInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInsc", lhsNrInsc),
							LocatorUtils.property(thatLocator, "nrInsc", rhsNrInsc), lhsNrInsc, rhsNrInsc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					short theTpInsc;
					theTpInsc = this.getTpInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInsc", theTpInsc),
							currentHashCode, theTpInsc);
				}
				{
					String theNrInsc;
					theNrInsc = this.getNrInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInsc", theNrInsc),
							currentHashCode, theNrInsc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="perApur">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]{1}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "perApur", "tpAmb", "procEmi", "verProc" })
		// @Entity(name = "Reinf$EvtFechaEvPer$IdeEvento")
		@Table(name = "IDE_EVENTO__3")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeEvento implements Equals, HashCode {

			@XmlElement(required = true)
			protected String perApur;
			protected long tpAmb;
			protected long procEmi;
			@XmlElement(required = true)
			protected String verProc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade perApur.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Transient
			public String getPerApur() {
				return perApur;
			}

			/**
			 * Define o valor da propriedade perApur.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPerApur(String value) {
				this.perApur = value;
			}

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			@Basic
			@Column(name = "TP_AMB", scale = 0)
			public long getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(long value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			@Basic
			@Column(name = "PROC_EMI", scale = 0)
			public long getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(long value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "VER_PROC", length = 20)
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			@Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			/*
			 * @Basic
			 * 
			 * @Column(name = "PER_APUR_ITEM")
			 * 
			 * @Temporal(TemporalType.TIMESTAMP) public Date getPerApurItem() {
			 * return XmlAdapterUtils.unmarshall(StringAsDateTime.class,
			 * this.getPerApur()); }
			 * 
			 * public void setPerApurItem(Date target) {
			 * setPerApur(XmlAdapterUtils.marshall(StringAsDateTime.class,
			 * target)); }
			 */
			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtFechaEvPer.IdeEvento)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtFechaEvPer.IdeEvento that = ((Reinf.EvtFechaEvPer.IdeEvento) object);
				{
					String lhsPerApur;
					lhsPerApur = this.getPerApur();
					String rhsPerApur;
					rhsPerApur = that.getPerApur();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "perApur", lhsPerApur),
							LocatorUtils.property(thatLocator, "perApur", rhsPerApur), lhsPerApur, rhsPerApur)) {
						return false;
					}
				}
				{
					long lhsTpAmb;
					lhsTpAmb = this.getTpAmb();
					long rhsTpAmb;
					rhsTpAmb = that.getTpAmb();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpAmb", lhsTpAmb),
							LocatorUtils.property(thatLocator, "tpAmb", rhsTpAmb), lhsTpAmb, rhsTpAmb)) {
						return false;
					}
				}
				{
					long lhsProcEmi;
					lhsProcEmi = this.getProcEmi();
					long rhsProcEmi;
					rhsProcEmi = that.getProcEmi();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "procEmi", lhsProcEmi),
							LocatorUtils.property(thatLocator, "procEmi", rhsProcEmi), lhsProcEmi, rhsProcEmi)) {
						return false;
					}
				}
				{
					String lhsVerProc;
					lhsVerProc = this.getVerProc();
					String rhsVerProc;
					rhsVerProc = that.getVerProc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "verProc", lhsVerProc),
							LocatorUtils.property(thatLocator, "verProc", rhsVerProc), lhsVerProc, rhsVerProc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					String thePerApur;
					thePerApur = this.getPerApur();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "perApur", thePerApur),
							currentHashCode, thePerApur);
				}
				{
					long theTpAmb;
					theTpAmb = this.getTpAmb();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpAmb", theTpAmb),
							currentHashCode, theTpAmb);
				}
				{
					long theProcEmi;
					theProcEmi = this.getProcEmi();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "procEmi", theProcEmi),
							currentHashCode, theProcEmi);
				}
				{
					String theVerProc;
					theVerProc = this.getVerProc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verProc", theVerProc),
							currentHashCode, theVerProc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * Respons�vel pelas informa��es
		 * 
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="nmResp">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="70"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="cpfResp">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="11"/>
		 *               &lt;pattern value="[0-9]{11}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="telefone" minOccurs="0">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;maxLength value="13"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="email" minOccurs="0">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;maxLength value="60"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "nmResp", "cpfResp", "telefone", "email" })
		// @Entity(name = "Reinf$EvtFechaEvPer$IdeRespInf")
		@Table(name = "IDE_RESP_INF")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeRespInf implements Equals, HashCode {

			@XmlElement(required = true)
			protected String nmResp;
			@XmlElement(required = true)
			protected String cpfResp;
			protected String telefone;
			protected String email;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade nmResp.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NM_RESP", length = 70)
			public String getNmResp() {
				return nmResp;
			}

			/**
			 * Define o valor da propriedade nmResp.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNmResp(String value) {
				this.nmResp = value;
			}

			/**
			 * Obt�m o valor da propriedade cpfResp.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "CPF_RESP", length = 11)
			public String getCpfResp() {
				return cpfResp;
			}

			/**
			 * Define o valor da propriedade cpfResp.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCpfResp(String value) {
				this.cpfResp = value;
			}

			/**
			 * Obt�m o valor da propriedade telefone.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "TELEFONE", length = 13)
			public String getTelefone() {
				return telefone;
			}

			/**
			 * Define o valor da propriedade telefone.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setTelefone(String value) {
				this.telefone = value;
			}

			/**
			 * Obt�m o valor da propriedade email.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EMAIL", length = 60)
			public String getEmail() {
				return email;
			}

			/**
			 * Define o valor da propriedade email.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEmail(String value) {
				this.email = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			@Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtFechaEvPer.IdeRespInf)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtFechaEvPer.IdeRespInf that = ((Reinf.EvtFechaEvPer.IdeRespInf) object);
				{
					String lhsNmResp;
					lhsNmResp = this.getNmResp();
					String rhsNmResp;
					rhsNmResp = that.getNmResp();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nmResp", lhsNmResp),
							LocatorUtils.property(thatLocator, "nmResp", rhsNmResp), lhsNmResp, rhsNmResp)) {
						return false;
					}
				}
				{
					String lhsCpfResp;
					lhsCpfResp = this.getCpfResp();
					String rhsCpfResp;
					rhsCpfResp = that.getCpfResp();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "cpfResp", lhsCpfResp),
							LocatorUtils.property(thatLocator, "cpfResp", rhsCpfResp), lhsCpfResp, rhsCpfResp)) {
						return false;
					}
				}
				{
					String lhsTelefone;
					lhsTelefone = this.getTelefone();
					String rhsTelefone;
					rhsTelefone = that.getTelefone();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "telefone", lhsTelefone),
							LocatorUtils.property(thatLocator, "telefone", rhsTelefone), lhsTelefone, rhsTelefone)) {
						return false;
					}
				}
				{
					String lhsEmail;
					lhsEmail = this.getEmail();
					String rhsEmail;
					rhsEmail = that.getEmail();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "email", lhsEmail),
							LocatorUtils.property(thatLocator, "email", rhsEmail), lhsEmail, rhsEmail)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					String theNmResp;
					theNmResp = this.getNmResp();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nmResp", theNmResp),
							currentHashCode, theNmResp);
				}
				{
					String theCpfResp;
					theCpfResp = this.getCpfResp();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cpfResp", theCpfResp),
							currentHashCode, theCpfResp);
				}
				{
					String theTelefone;
					theTelefone = this.getTelefone();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "telefone", theTelefone),
							currentHashCode, theTelefone);
				}
				{
					String theEmail;
					theEmail = this.getEmail();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "email", theEmail),
							currentHashCode, theEmail);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * Respons�vel pelas informa��es
		 * 
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado
		 * contido dentro desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="evtServTm">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="1"/>
		 *               &lt;enumeration value="S"/>
		 *               &lt;enumeration value="N"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="evtServPr">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="1"/>
		 *               &lt;enumeration value="S"/>
		 *               &lt;enumeration value="N"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="evtAssDespRec">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="1"/>
		 *               &lt;enumeration value="S"/>
		 *               &lt;enumeration value="N"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="evtAssDespRep">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="1"/>
		 *               &lt;enumeration value="S"/>
		 *               &lt;enumeration value="N"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="evtComProd">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="1"/>
		 *               &lt;enumeration value="S"/>
		 *               &lt;enumeration value="N"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="evtCPRB">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="1"/>
		 *               &lt;enumeration value="S"/>
		 *               &lt;enumeration value="N"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="evtPgtos">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="1"/>
		 *               &lt;enumeration value="S"/>
		 *               &lt;enumeration value="N"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="compSemMovto" minOccurs="0">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "evtServTm", "evtServPr", "evtAssDespRec", "evtAssDespRep", "evtComProd",
				"evtCPRB", "evtPgtos", "compSemMovto" })
		// @Entity(name = "Reinf$EvtFechaEvPer$InfoFech")
		@Table(name = "INFO_FECH")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class InfoFech implements Equals, HashCode {

			@XmlElement(required = true)
			protected String evtServTm;
			@XmlElement(required = true)
			protected String evtServPr;
			@XmlElement(required = true)
			protected String evtAssDespRec;
			@XmlElement(required = true)
			protected String evtAssDespRep;
			@XmlElement(required = true)
			protected String evtComProd;
			@XmlElement(required = true)
			protected String evtCPRB;
			@XmlElement(required = true)
			protected String evtPgtos;
			protected String compSemMovto;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade evtServTm.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EVT_SERV_TM", length = 1)
			public String getEvtServTm() {
				return evtServTm;
			}

			/**
			 * Define o valor da propriedade evtServTm.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEvtServTm(String value) {
				this.evtServTm = value;
			}

			/**
			 * Obt�m o valor da propriedade evtServPr.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EVT_SERV_PR", length = 1)
			public String getEvtServPr() {
				return evtServPr;
			}

			/**
			 * Define o valor da propriedade evtServPr.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEvtServPr(String value) {
				this.evtServPr = value;
			}

			/**
			 * Obt�m o valor da propriedade evtAssDespRec.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EVT_ASS_DESP_REC", length = 1)
			public String getEvtAssDespRec() {
				return evtAssDespRec;
			}

			/**
			 * Define o valor da propriedade evtAssDespRec.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEvtAssDespRec(String value) {
				this.evtAssDespRec = value;
			}

			/**
			 * Obt�m o valor da propriedade evtAssDespRep.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EVT_ASS_DESP_REP", length = 1)
			public String getEvtAssDespRep() {
				return evtAssDespRep;
			}

			/**
			 * Define o valor da propriedade evtAssDespRep.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEvtAssDespRep(String value) {
				this.evtAssDespRep = value;
			}

			/**
			 * Obt�m o valor da propriedade evtComProd.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EVT_COM_PROD", length = 1)
			public String getEvtComProd() {
				return evtComProd;
			}

			/**
			 * Define o valor da propriedade evtComProd.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEvtComProd(String value) {
				this.evtComProd = value;
			}

			/**
			 * Obt�m o valor da propriedade evtCPRB.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EVT_CPRB", length = 1)
			public String getEvtCPRB() {
				return evtCPRB;
			}

			/**
			 * Define o valor da propriedade evtCPRB.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEvtCPRB(String value) {
				this.evtCPRB = value;
			}

			/**
			 * Obt�m o valor da propriedade evtPgtos.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "EVT_PGTOS", length = 1)
			public String getEvtPgtos() {
				return evtPgtos;
			}

			/**
			 * Define o valor da propriedade evtPgtos.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEvtPgtos(String value) {
				this.evtPgtos = value;
			}

			/**
			 * Obt�m o valor da propriedade compSemMovto.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Transient
			public String getCompSemMovto() {
				return compSemMovto;
			}

			/**
			 * Define o valor da propriedade compSemMovto.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCompSemMovto(String value) {
				this.compSemMovto = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			@Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			/*
			 * @Basic
			 * 
			 * @Column(name = "COMP_SEM_MOVTO_ITEM")
			 * 
			 * @Temporal(TemporalType.TIMESTAMP) public Date
			 * getCompSemMovtoItem() { return
			 * XmlAdapterUtils.unmarshall(StringAsDateTime.class,
			 * this.getCompSemMovto()); }
			 * 
			 * public void setCompSemMovtoItem(Date target) {
			 * setCompSemMovto(XmlAdapterUtils.marshall(StringAsDateTime.class,
			 * target)); }
			 */

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtFechaEvPer.InfoFech)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtFechaEvPer.InfoFech that = ((Reinf.EvtFechaEvPer.InfoFech) object);
				{
					String lhsEvtServTm;
					lhsEvtServTm = this.getEvtServTm();
					String rhsEvtServTm;
					rhsEvtServTm = that.getEvtServTm();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "evtServTm", lhsEvtServTm),
							LocatorUtils.property(thatLocator, "evtServTm", rhsEvtServTm), lhsEvtServTm,
							rhsEvtServTm)) {
						return false;
					}
				}
				{
					String lhsEvtServPr;
					lhsEvtServPr = this.getEvtServPr();
					String rhsEvtServPr;
					rhsEvtServPr = that.getEvtServPr();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "evtServPr", lhsEvtServPr),
							LocatorUtils.property(thatLocator, "evtServPr", rhsEvtServPr), lhsEvtServPr,
							rhsEvtServPr)) {
						return false;
					}
				}
				{
					String lhsEvtAssDespRec;
					lhsEvtAssDespRec = this.getEvtAssDespRec();
					String rhsEvtAssDespRec;
					rhsEvtAssDespRec = that.getEvtAssDespRec();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "evtAssDespRec", lhsEvtAssDespRec),
							LocatorUtils.property(thatLocator, "evtAssDespRec", rhsEvtAssDespRec), lhsEvtAssDespRec,
							rhsEvtAssDespRec)) {
						return false;
					}
				}
				{
					String lhsEvtAssDespRep;
					lhsEvtAssDespRep = this.getEvtAssDespRep();
					String rhsEvtAssDespRep;
					rhsEvtAssDespRep = that.getEvtAssDespRep();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "evtAssDespRep", lhsEvtAssDespRep),
							LocatorUtils.property(thatLocator, "evtAssDespRep", rhsEvtAssDespRep), lhsEvtAssDespRep,
							rhsEvtAssDespRep)) {
						return false;
					}
				}
				{
					String lhsEvtComProd;
					lhsEvtComProd = this.getEvtComProd();
					String rhsEvtComProd;
					rhsEvtComProd = that.getEvtComProd();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "evtComProd", lhsEvtComProd),
							LocatorUtils.property(thatLocator, "evtComProd", rhsEvtComProd), lhsEvtComProd,
							rhsEvtComProd)) {
						return false;
					}
				}
				{
					String lhsEvtCPRB;
					lhsEvtCPRB = this.getEvtCPRB();
					String rhsEvtCPRB;
					rhsEvtCPRB = that.getEvtCPRB();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "evtCPRB", lhsEvtCPRB),
							LocatorUtils.property(thatLocator, "evtCPRB", rhsEvtCPRB), lhsEvtCPRB, rhsEvtCPRB)) {
						return false;
					}
				}
				{
					String lhsEvtPgtos;
					lhsEvtPgtos = this.getEvtPgtos();
					String rhsEvtPgtos;
					rhsEvtPgtos = that.getEvtPgtos();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "evtPgtos", lhsEvtPgtos),
							LocatorUtils.property(thatLocator, "evtPgtos", rhsEvtPgtos), lhsEvtPgtos, rhsEvtPgtos)) {
						return false;
					}
				}
				{
					String lhsCompSemMovto;
					lhsCompSemMovto = this.getCompSemMovto();
					String rhsCompSemMovto;
					rhsCompSemMovto = that.getCompSemMovto();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "compSemMovto", lhsCompSemMovto),
							LocatorUtils.property(thatLocator, "compSemMovto", rhsCompSemMovto), lhsCompSemMovto,
							rhsCompSemMovto)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					String theEvtServTm;
					theEvtServTm = this.getEvtServTm();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtServTm", theEvtServTm),
							currentHashCode, theEvtServTm);
				}
				{
					String theEvtServPr;
					theEvtServPr = this.getEvtServPr();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtServPr", theEvtServPr),
							currentHashCode, theEvtServPr);
				}
				{
					String theEvtAssDespRec;
					theEvtAssDespRec = this.getEvtAssDespRec();
					currentHashCode = strategy.hashCode(
							LocatorUtils.property(locator, "evtAssDespRec", theEvtAssDespRec), currentHashCode,
							theEvtAssDespRec);
				}
				{
					String theEvtAssDespRep;
					theEvtAssDespRep = this.getEvtAssDespRep();
					currentHashCode = strategy.hashCode(
							LocatorUtils.property(locator, "evtAssDespRep", theEvtAssDespRep), currentHashCode,
							theEvtAssDespRep);
				}
				{
					String theEvtComProd;
					theEvtComProd = this.getEvtComProd();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtComProd", theEvtComProd),
							currentHashCode, theEvtComProd);
				}
				{
					String theEvtCPRB;
					theEvtCPRB = this.getEvtCPRB();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtCPRB", theEvtCPRB),
							currentHashCode, theEvtCPRB);
				}
				{
					String theEvtPgtos;
					theEvtPgtos = this.getEvtPgtos();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtPgtos", theEvtPgtos),
							currentHashCode, theEvtPgtos);
				}
				{
					String theCompSemMovto;
					theCompSemMovto = this.getCompSemMovto();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "compSemMovto", theCompSemMovto),
							currentHashCode, theCompSemMovto);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

	}

}
