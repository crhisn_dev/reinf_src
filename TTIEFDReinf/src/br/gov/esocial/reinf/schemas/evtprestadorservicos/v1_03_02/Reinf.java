//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.10 �s 09:36:18 AM BRT 
//

package br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
//import javax.xml.datatype.String;
//import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.StringAsDateTime;
// import org.jvnet.hyperjaxb3.xml.bind.annotation.adapters.XmlAdapterUtils;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtServPrest">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="indRetif">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrRecibo" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="16"/>
 *                                   &lt;maxLength value="52"/>
 *                                   &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="perApur">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                   &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoServPrest">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ideEstabPrest">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="tpInscEstabPrest">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                             &lt;pattern value="[1]"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="nrInscEstabPrest">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;pattern value="[0-9]{14}"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="ideTomador">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="tpInscTomador">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                       &lt;pattern value="[1|4]"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="nrInscTomador">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;pattern value="[0-9]{1,14}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="indObra">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                       &lt;pattern value="[0|1|2]"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalBruto">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalBaseRet">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalRetPrinc">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalRetAdic" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="vlrTotalNRetAdic" minOccurs="0">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;minLength value="4"/>
 *                                                       &lt;maxLength value="17"/>
 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="nfs" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="serie">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="5"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="numDocto">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="15"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="dtEmissaoNF">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="vlrBruto">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="obs" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;maxLength value="250"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="infoTpServ" maxOccurs="9">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="tpServico">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;pattern value="[0-9]{1,9}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrBaseRet">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrRetencao">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrRetSub" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrNRetPrinc" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos15" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos20" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrServicos25" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrAdicional" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="vlrNRetAdic" minOccurs="0">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;minLength value="4"/>
 *                                                                           &lt;maxLength value="17"/>
 *                                                                           &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="tpProcRetPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                                 &lt;pattern value="[1|2]"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="nrProcRetPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="21"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="codSuspPrinc" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;pattern value="[0-9]{0,14}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="valorPrinc">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="tpProcRetAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                                                 &lt;pattern value="[1|2]"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="nrProcRetAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="1"/>
 *                                                                 &lt;maxLength value="21"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="codSuspAdic" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;pattern value="[0-9]{0,14}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="valorAdic">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;minLength value="4"/>
 *                                                                 &lt;maxLength value="17"/>
 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtServPrest" })
@XmlRootElement(name = "Reinf")
// @Entity(name = "Reinf")
@Table(name = "REINF__2")
@Inheritance(strategy = InheritanceType.JOINED)
public class Reinf implements Equals, HashCode {

	@XmlElement(required = true)
	protected Reinf.EvtServPrest evtServPrest;
	@XmlAttribute(name = "Hjid")
	@Transient
	protected Long hjid;

	/**
	 * Obt�m o valor da propriedade evtServPrest.
	 * 
	 * @return possible object is {@link Reinf.EvtServPrest }
	 * 
	 */
	@ManyToOne(targetEntity = Reinf.EvtServPrest.class, cascade = { CascadeType.ALL })
	@JoinColumn(name = "EVT_SERV_PREST_REINF___2_HJID")
	public Reinf.EvtServPrest getEvtServPrest() {
		return evtServPrest;
	}

	/**
	 * Define o valor da propriedade evtServPrest.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtServPrest }
	 * 
	 */
	public void setEvtServPrest(Reinf.EvtServPrest value) {
		this.evtServPrest = value;
	}

	/**
	 * Obt�m o valor da propriedade hjid.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	// @Id
	@Column(name = "HJID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getHjid() {
		return hjid;
	}

	/**
	 * Define o valor da propriedade hjid.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setHjid(Long value) {
		this.hjid = value;
	}

	public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
			EqualsStrategy strategy) {
		if (!(object instanceof Reinf)) {
			return false;
		}
		if (this == object) {
			return true;
		}
		final Reinf that = ((Reinf) object);
		{
			Reinf.EvtServPrest lhsEvtServPrest;
			lhsEvtServPrest = this.getEvtServPrest();
			Reinf.EvtServPrest rhsEvtServPrest;
			rhsEvtServPrest = that.getEvtServPrest();
			if (!strategy.equals(LocatorUtils.property(thisLocator, "evtServPrest", lhsEvtServPrest),
					LocatorUtils.property(thatLocator, "evtServPrest", rhsEvtServPrest), lhsEvtServPrest,
					rhsEvtServPrest)) {
				return false;
			}
		}
		return true;
	}

	public boolean equals(Object object) {
		final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
		return equals(null, null, object, strategy);
	}

	public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
		int currentHashCode = 1;
		{
			Reinf.EvtServPrest theEvtServPrest;
			theEvtServPrest = this.getEvtServPrest();
			currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtServPrest", theEvtServPrest),
					currentHashCode, theEvtServPrest);
		}
		return currentHashCode;
	}

	public int hashCode() {
		final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
		return this.hashCode(null, strategy);
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
	 * desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="indRetif">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrRecibo" minOccurs="0">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="16"/>
	 *                         &lt;maxLength value="52"/>
	 *                         &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="perApur">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="infoServPrest">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="ideEstabPrest">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="tpInscEstabPrest">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                                   &lt;pattern value="[1]"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="nrInscEstabPrest">
	 *                               &lt;simpleType>
	 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                   &lt;pattern value="[0-9]{14}"/>
	 *                                 &lt;/restriction>
	 *                               &lt;/simpleType>
	 *                             &lt;/element>
	 *                             &lt;element name="ideTomador">
	 *                               &lt;complexType>
	 *                                 &lt;complexContent>
	 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                     &lt;sequence>
	 *                                       &lt;element name="tpInscTomador">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                             &lt;pattern value="[1|4]"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="nrInscTomador">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;pattern value="[0-9]{1,14}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="indObra">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                             &lt;pattern value="[0|1|2]"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalBruto">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalBaseRet">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalRetPrinc">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalRetAdic" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="vlrTotalNRetAdic" minOccurs="0">
	 *                                         &lt;simpleType>
	 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                             &lt;minLength value="4"/>
	 *                                             &lt;maxLength value="17"/>
	 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                           &lt;/restriction>
	 *                                         &lt;/simpleType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="nfs" maxOccurs="unbounded">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="serie">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="5"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="numDocto">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="15"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="dtEmissaoNF">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="vlrBruto">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="obs" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;maxLength value="250"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="infoTpServ" maxOccurs="9">
	 *                                                   &lt;complexType>
	 *                                                     &lt;complexContent>
	 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                                         &lt;sequence>
	 *                                                           &lt;element name="tpServico">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;pattern value="[0-9]{1,9}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrBaseRet">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrRetencao">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrRetSub" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrNRetPrinc" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos15" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos20" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrServicos25" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrAdicional" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                           &lt;element name="vlrNRetAdic" minOccurs="0">
	 *                                                             &lt;simpleType>
	 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                                 &lt;minLength value="4"/>
	 *                                                                 &lt;maxLength value="17"/>
	 *                                                                 &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                               &lt;/restriction>
	 *                                                             &lt;/simpleType>
	 *                                                           &lt;/element>
	 *                                                         &lt;/sequence>
	 *                                                       &lt;/restriction>
	 *                                                     &lt;/complexContent>
	 *                                                   &lt;/complexType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="tpProcRetPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                                       &lt;pattern value="[1|2]"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="nrProcRetPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="21"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="codSuspPrinc" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;pattern value="[0-9]{0,14}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="valorPrinc">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                       &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;sequence>
	 *                                                 &lt;element name="tpProcRetAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                                                       &lt;pattern value="[1|2]"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="nrProcRetAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="1"/>
	 *                                                       &lt;maxLength value="21"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="codSuspAdic" minOccurs="0">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;pattern value="[0-9]{0,14}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                                 &lt;element name="valorAdic">
	 *                                                   &lt;simpleType>
	 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                                       &lt;minLength value="4"/>
	 *                                                       &lt;maxLength value="17"/>
	 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
	 *                                                     &lt;/restriction>
	 *                                                   &lt;/simpleType>
	 *                                                 &lt;/element>
	 *                                               &lt;/sequence>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                     &lt;/sequence>
	 *                                   &lt;/restriction>
	 *                                 &lt;/complexContent>
	 *                               &lt;/complexType>
	 *                             &lt;/element>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri", "infoServPrest" })
	@Entity(name = "Reinf$EvtServPrest")
	@Table(name = "EVT_SERV_PREST")
	@Inheritance(strategy = InheritanceType.JOINED)

	public static class EvtServPrest implements Equals, HashCode {

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtServPrest.IdeEvento ideEvento;
		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtServPrest.IdeContri ideContri;
		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtServPrest.InfoServPrest infoServPrest;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;
		@XmlAttribute(name = "Hjid")
		@Id
		@Column(name = "ID")		
		protected Long hjid;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtServPrest.IdeEvento }
		 * 
		 */
		// @ManyToOne(targetEntity =
		// br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.IdeEvento.class,
		// cascade = {
		// CascadeType.ALL })
		// @JoinColumn(name = "IDE_EVENTO_EVT_SERV_PREST_HJ_0")
		public Reinf.EvtServPrest.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServPrest.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtServPrest.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtServPrest.IdeContri }
		 * 
		 */
		// @ManyToOne(targetEntity =
		// br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.IdeContri.class,
		// cascade = {
		// CascadeType.ALL })
		// @JoinColumn(name = "IDE_CONTRI_EVT_SERV_PREST_HJ_0")
		public Reinf.EvtServPrest.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServPrest.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtServPrest.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade infoServPrest.
		 * 
		 * @return possible object is {@link Reinf.EvtServPrest.InfoServPrest }
		 * 
		 */
		@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.InfoServPrest.class, cascade = {
				CascadeType.ALL })
		@JoinColumn(name = "INFO_SERV_PREST_EVT_SERV_PRE_0")
		public Reinf.EvtServPrest.InfoServPrest getInfoServPrest() {
			return infoServPrest;
		}

		/**
		 * Define o valor da propriedade infoServPrest.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtServPrest.InfoServPrest }
		 * 
		 */
		public void setInfoServPrest(Reinf.EvtServPrest.InfoServPrest value) {
			this.infoServPrest = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		@Basic
		@Column(name = "ID", length = 36)
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * Obt�m o valor da propriedade hjid.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		// @Id
		@Column(name = "HJID")
		@GeneratedValue(strategy = GenerationType.AUTO)
		public Long getHjid() {
			return hjid;
		}

		/**
		 * Define o valor da propriedade hjid.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setHjid(Long value) {
			this.hjid = value;
		}

		public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
				EqualsStrategy strategy) {
			if (!(object instanceof Reinf.EvtServPrest)) {
				return false;
			}
			if (this == object) {
				return true;
			}
			final Reinf.EvtServPrest that = ((Reinf.EvtServPrest) object);
			{
				Reinf.EvtServPrest.IdeEvento lhsIdeEvento;
				lhsIdeEvento = this.getIdeEvento();
				Reinf.EvtServPrest.IdeEvento rhsIdeEvento;
				rhsIdeEvento = that.getIdeEvento();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEvento", lhsIdeEvento),
						LocatorUtils.property(thatLocator, "ideEvento", rhsIdeEvento), lhsIdeEvento, rhsIdeEvento)) {
					return false;
				}
			}
			{
				Reinf.EvtServPrest.IdeContri lhsIdeContri;
				lhsIdeContri = this.getIdeContri();
				Reinf.EvtServPrest.IdeContri rhsIdeContri;
				rhsIdeContri = that.getIdeContri();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideContri", lhsIdeContri),
						LocatorUtils.property(thatLocator, "ideContri", rhsIdeContri), lhsIdeContri, rhsIdeContri)) {
					return false;
				}
			}
			{
				Reinf.EvtServPrest.InfoServPrest lhsInfoServPrest;
				lhsInfoServPrest = this.getInfoServPrest();
				Reinf.EvtServPrest.InfoServPrest rhsInfoServPrest;
				rhsInfoServPrest = that.getInfoServPrest();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "infoServPrest", lhsInfoServPrest),
						LocatorUtils.property(thatLocator, "infoServPrest", rhsInfoServPrest), lhsInfoServPrest,
						rhsInfoServPrest)) {
					return false;
				}
			}
			{
				String lhsId;
				lhsId = this.getId();
				String rhsId;
				rhsId = that.getId();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId),
						LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
					return false;
				}
			}
			return true;
		}

		public boolean equals(Object object) {
			final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
			return equals(null, null, object, strategy);
		}

		public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
			int currentHashCode = 1;
			{
				Reinf.EvtServPrest.IdeEvento theIdeEvento;
				theIdeEvento = this.getIdeEvento();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEvento", theIdeEvento),
						currentHashCode, theIdeEvento);
			}
			{
				Reinf.EvtServPrest.IdeContri theIdeContri;
				theIdeContri = this.getIdeContri();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideContri", theIdeContri),
						currentHashCode, theIdeContri);
			}
			{
				Reinf.EvtServPrest.InfoServPrest theInfoServPrest;
				theInfoServPrest = this.getInfoServPrest();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoServPrest", theInfoServPrest),
						currentHashCode, theInfoServPrest);
			}
			{
				String theId;
				theId = this.getId();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode,
						theId);
			}
			return currentHashCode;
		}

		public int hashCode() {
			final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
			return this.hashCode(null, strategy);
		}

		/**
		 * Informa\ufffd\ufffdes de identifica\ufffd\ufffdo do contribuinte
		 * 
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		// @Entity(name = "Reinf$EvtServPrest$IdeContri")
		@Table(name = "IDE_CONTRI__1")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeContri implements Equals, HashCode {

			protected short tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			@Basic
			@Column(name = "TP_INSC", scale = 0)
			public short getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(short value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_INSC")
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtServPrest.IdeContri)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtServPrest.IdeContri that = ((Reinf.EvtServPrest.IdeContri) object);
				{
					short lhsTpInsc;
					lhsTpInsc = this.getTpInsc();
					short rhsTpInsc;
					rhsTpInsc = that.getTpInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInsc", lhsTpInsc),
							LocatorUtils.property(thatLocator, "tpInsc", rhsTpInsc), lhsTpInsc, rhsTpInsc)) {
						return false;
					}
				}
				{
					String lhsNrInsc;
					lhsNrInsc = this.getNrInsc();
					String rhsNrInsc;
					rhsNrInsc = that.getNrInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInsc", lhsNrInsc),
							LocatorUtils.property(thatLocator, "nrInsc", rhsNrInsc), lhsNrInsc, rhsNrInsc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					short theTpInsc;
					theTpInsc = this.getTpInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInsc", theTpInsc),
							currentHashCode, theTpInsc);
				}
				{
					String theNrInsc;
					theNrInsc = this.getNrInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInsc", theNrInsc),
							currentHashCode, theNrInsc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="indRetif">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrRecibo" minOccurs="0">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="16"/>
		 *               &lt;maxLength value="52"/>
		 *               &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1,18}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="perApur">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "indRetif", "nrRecibo", "perApur", "tpAmb", "procEmi", "verProc" })
		// @Entity(name = "Reinf$EvtServPrest$IdeEvento")
		@Table(name = "IDE_EVENTO__1")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeEvento implements Equals, HashCode {

			protected short indRetif;
			protected String nrRecibo;
			@XmlElement(required = true)
			protected String perApur;
			protected long tpAmb;
			protected long procEmi;
			@XmlElement(required = true)
			protected String verProc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade indRetif.
			 * 
			 */
			@Basic
			@Column(name = "IND_RETIF", scale = 0)
			public short getIndRetif() {
				return indRetif;
			}

			/**
			 * Define o valor da propriedade indRetif.
			 * 
			 */
			public void setIndRetif(short value) {
				this.indRetif = value;
			}

			/**
			 * Obt�m o valor da propriedade nrRecibo.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_RECIBO", length = 52)
			public String getNrRecibo() {
				return nrRecibo;
			}

			/**
			 * Define o valor da propriedade nrRecibo.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrRecibo(String value) {
				this.nrRecibo = value;
			}

			/**
			 * Obt�m o valor da propriedade perApur.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Transient
			public String getPerApur() {
				return perApur;
			}

			/**
			 * Define o valor da propriedade perApur.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPerApur(String value) {
				this.perApur = value;
			}

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			@Basic
			@Column(name = "TP_AMB", scale = 0)
			public long getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(long value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			@Basic
			@Column(name = "PROC_EMI", scale = 0)
			public long getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(long value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "VER_PROC", length = 20)
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			/*
			 * @Basic
			 * 
			 * @Column(name = "PER_APUR_ITEM")
			 * 
			 * @Temporal(TemporalType.TIMESTAMP) public Date getPerApurItem() { return
			 * XmlAdapterUtils.unmarshall(StringAsDateTime.class, this.getPerApur()); }
			 * 
			 * public void setPerApurItem(Date target) {
			 * setPerApur(XmlAdapterUtils.marshall(StringAsDateTime.class, target)); }
			 */

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtServPrest.IdeEvento)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtServPrest.IdeEvento that = ((Reinf.EvtServPrest.IdeEvento) object);
				{
					short lhsIndRetif;
					lhsIndRetif = this.getIndRetif();
					short rhsIndRetif;
					rhsIndRetif = that.getIndRetif();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "indRetif", lhsIndRetif),
							LocatorUtils.property(thatLocator, "indRetif", rhsIndRetif), lhsIndRetif, rhsIndRetif)) {
						return false;
					}
				}
				{
					String lhsNrRecibo;
					lhsNrRecibo = this.getNrRecibo();
					String rhsNrRecibo;
					rhsNrRecibo = that.getNrRecibo();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrRecibo", lhsNrRecibo),
							LocatorUtils.property(thatLocator, "nrRecibo", rhsNrRecibo), lhsNrRecibo, rhsNrRecibo)) {
						return false;
					}
				}
				{
					String lhsPerApur;
					lhsPerApur = this.getPerApur();
					String rhsPerApur;
					rhsPerApur = that.getPerApur();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "perApur", lhsPerApur),
							LocatorUtils.property(thatLocator, "perApur", rhsPerApur), lhsPerApur, rhsPerApur)) {
						return false;
					}
				}
				{
					long lhsTpAmb;
					lhsTpAmb = this.getTpAmb();
					long rhsTpAmb;
					rhsTpAmb = that.getTpAmb();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpAmb", lhsTpAmb),
							LocatorUtils.property(thatLocator, "tpAmb", rhsTpAmb), lhsTpAmb, rhsTpAmb)) {
						return false;
					}
				}
				{
					long lhsProcEmi;
					lhsProcEmi = this.getProcEmi();
					long rhsProcEmi;
					rhsProcEmi = that.getProcEmi();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "procEmi", lhsProcEmi),
							LocatorUtils.property(thatLocator, "procEmi", rhsProcEmi), lhsProcEmi, rhsProcEmi)) {
						return false;
					}
				}
				{
					String lhsVerProc;
					lhsVerProc = this.getVerProc();
					String rhsVerProc;
					rhsVerProc = that.getVerProc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "verProc", lhsVerProc),
							LocatorUtils.property(thatLocator, "verProc", rhsVerProc), lhsVerProc, rhsVerProc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					short theIndRetif;
					theIndRetif = this.getIndRetif();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indRetif", theIndRetif),
							currentHashCode, theIndRetif);
				}
				{
					String theNrRecibo;
					theNrRecibo = this.getNrRecibo();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrRecibo", theNrRecibo),
							currentHashCode, theNrRecibo);
				}
				{
					String thePerApur;
					thePerApur = this.getPerApur();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "perApur", thePerApur),
							currentHashCode, thePerApur);
				}
				{
					long theTpAmb;
					theTpAmb = this.getTpAmb();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpAmb", theTpAmb),
							currentHashCode, theTpAmb);
				}
				{
					long theProcEmi;
					theProcEmi = this.getProcEmi();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "procEmi", theProcEmi),
							currentHashCode, theProcEmi);
				}
				{
					String theVerProc;
					theVerProc = this.getVerProc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verProc", theVerProc),
							currentHashCode, theVerProc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * Informa\ufffd\ufffdo dos Servi\ufffdos Prestados (Cess\ufffdo de M\ufffdo de
		 * Obra ou Empreitada)
		 * 
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="ideEstabPrest">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="tpInscEstabPrest">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *                         &lt;pattern value="[1]"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="nrInscEstabPrest">
		 *                     &lt;simpleType>
		 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                         &lt;pattern value="[0-9]{14}"/>
		 *                       &lt;/restriction>
		 *                     &lt;/simpleType>
		 *                   &lt;/element>
		 *                   &lt;element name="ideTomador">
		 *                     &lt;complexType>
		 *                       &lt;complexContent>
		 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                           &lt;sequence>
		 *                             &lt;element name="tpInscTomador">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                   &lt;pattern value="[1|4]"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="nrInscTomador">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;pattern value="[0-9]{1,14}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="indObra">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                   &lt;pattern value="[0|1|2]"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalBruto">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalBaseRet">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalRetPrinc">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalRetAdic" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="vlrTotalNRetAdic" minOccurs="0">
		 *                               &lt;simpleType>
		 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                   &lt;minLength value="4"/>
		 *                                   &lt;maxLength value="17"/>
		 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                 &lt;/restriction>
		 *                               &lt;/simpleType>
		 *                             &lt;/element>
		 *                             &lt;element name="nfs" maxOccurs="unbounded">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="serie">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="5"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="numDocto">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="15"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="dtEmissaoNF">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="vlrBruto">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="obs" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;maxLength value="250"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="infoTpServ" maxOccurs="9">
		 *                                         &lt;complexType>
		 *                                           &lt;complexContent>
		 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                               &lt;sequence>
		 *                                                 &lt;element name="tpServico">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;pattern value="[0-9]{1,9}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrBaseRet">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrRetencao">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrRetSub" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrNRetPrinc" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos15" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos20" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrServicos25" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrAdicional" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                                 &lt;element name="vlrNRetAdic" minOccurs="0">
		 *                                                   &lt;simpleType>
		 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                                       &lt;minLength value="4"/>
		 *                                                       &lt;maxLength value="17"/>
		 *                                                       &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                                     &lt;/restriction>
		 *                                                   &lt;/simpleType>
		 *                                                 &lt;/element>
		 *                                               &lt;/sequence>
		 *                                             &lt;/restriction>
		 *                                           &lt;/complexContent>
		 *                                         &lt;/complexType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                             &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="tpProcRetPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                             &lt;pattern value="[1|2]"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="nrProcRetPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="21"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="codSuspPrinc" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;pattern value="[0-9]{0,14}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="valorPrinc">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                             &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;sequence>
		 *                                       &lt;element name="tpProcRetAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *                                             &lt;pattern value="[1|2]"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="nrProcRetAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="1"/>
		 *                                             &lt;maxLength value="21"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="codSuspAdic" minOccurs="0">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;pattern value="[0-9]{0,14}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                       &lt;element name="valorAdic">
		 *                                         &lt;simpleType>
		 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                                             &lt;minLength value="4"/>
		 *                                             &lt;maxLength value="17"/>
		 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
		 *                                           &lt;/restriction>
		 *                                         &lt;/simpleType>
		 *                                       &lt;/element>
		 *                                     &lt;/sequence>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                           &lt;/sequence>
		 *                         &lt;/restriction>
		 *                       &lt;/complexContent>
		 *                     &lt;/complexType>
		 *                   &lt;/element>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "ideEstabPrest" })
		// @Entity(name = "Reinf$EvtServPrest$InfoServPrest")
		@Table(name = "INFO_SERV_PREST")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class InfoServPrest implements Equals, HashCode {

			@XmlElement(required = true)
			@Embedded
			protected Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest ideEstabPrest;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade ideEstabPrest.
			 * 
			 * @return possible object is
			 *         {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest }
			 * 
			 */
			@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.class, cascade = {
					CascadeType.ALL })
			@JoinColumn(name = "IDE_ESTAB_PREST_INFO_SERV_PR_0")
			public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest getIdeEstabPrest() {
				return ideEstabPrest;
			}

			/**
			 * Define o valor da propriedade ideEstabPrest.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest }
			 * 
			 */
			public void setIdeEstabPrest(Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest value) {
				this.ideEstabPrest = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtServPrest.InfoServPrest)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtServPrest.InfoServPrest that = ((Reinf.EvtServPrest.InfoServPrest) object);
				{
					Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest lhsIdeEstabPrest;
					lhsIdeEstabPrest = this.getIdeEstabPrest();
					Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest rhsIdeEstabPrest;
					rhsIdeEstabPrest = that.getIdeEstabPrest();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEstabPrest", lhsIdeEstabPrest),
							LocatorUtils.property(thatLocator, "ideEstabPrest", rhsIdeEstabPrest), lhsIdeEstabPrest,
							rhsIdeEstabPrest)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest theIdeEstabPrest;
					theIdeEstabPrest = this.getIdeEstabPrest();
					currentHashCode = strategy.hashCode(
							LocatorUtils.property(locator, "ideEstabPrest", theIdeEstabPrest), currentHashCode,
							theIdeEstabPrest);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

			/**
			 * 
			 * Registro que identifica o estabelecimento "prestador" de servi\ufffdos
			 * mediante cess\ufffdo de m\ufffdo de obra.
			 * 
			 * 
			 * <p>
			 * Classe Java de anonymous complex type.
			 * 
			 * <p>
			 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
			 * desta classe.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="tpInscEstabPrest">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
			 *               &lt;pattern value="[1]"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="nrInscEstabPrest">
			 *           &lt;simpleType>
			 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *               &lt;pattern value="[0-9]{14}"/>
			 *             &lt;/restriction>
			 *           &lt;/simpleType>
			 *         &lt;/element>
			 *         &lt;element name="ideTomador">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="tpInscTomador">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;pattern value="[1|4]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="nrInscTomador">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;pattern value="[0-9]{1,14}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="indObra">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                         &lt;pattern value="[0|1|2]"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalBruto">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalBaseRet">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalRetPrinc">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalRetAdic" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="vlrTotalNRetAdic" minOccurs="0">
			 *                     &lt;simpleType>
			 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                         &lt;minLength value="4"/>
			 *                         &lt;maxLength value="17"/>
			 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                       &lt;/restriction>
			 *                     &lt;/simpleType>
			 *                   &lt;/element>
			 *                   &lt;element name="nfs" maxOccurs="unbounded">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="serie">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="5"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="numDocto">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="15"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="dtEmissaoNF">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="vlrBruto">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="obs" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;maxLength value="250"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="infoTpServ" maxOccurs="9">
			 *                               &lt;complexType>
			 *                                 &lt;complexContent>
			 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                                     &lt;sequence>
			 *                                       &lt;element name="tpServico">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;pattern value="[0-9]{1,9}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrBaseRet">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrRetencao">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrRetSub" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrNRetPrinc" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos15" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos20" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrServicos25" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrAdicional" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                       &lt;element name="vlrNRetAdic" minOccurs="0">
			 *                                         &lt;simpleType>
			 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                             &lt;minLength value="4"/>
			 *                                             &lt;maxLength value="17"/>
			 *                                             &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                           &lt;/restriction>
			 *                                         &lt;/simpleType>
			 *                                       &lt;/element>
			 *                                     &lt;/sequence>
			 *                                   &lt;/restriction>
			 *                                 &lt;/complexContent>
			 *                               &lt;/complexType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="tpProcRetPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                                   &lt;pattern value="[1|2]"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nrProcRetPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="21"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codSuspPrinc" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="valorPrinc">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                   &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;sequence>
			 *                             &lt;element name="tpProcRetAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
			 *                                   &lt;pattern value="[1|2]"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="nrProcRetAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="1"/>
			 *                                   &lt;maxLength value="21"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="codSuspAdic" minOccurs="0">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;pattern value="[0-9]{0,14}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                             &lt;element name="valorAdic">
			 *                               &lt;simpleType>
			 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *                                   &lt;minLength value="4"/>
			 *                                   &lt;maxLength value="17"/>
			 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
			 *                                 &lt;/restriction>
			 *                               &lt;/simpleType>
			 *                             &lt;/element>
			 *                           &lt;/sequence>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "tpInscEstabPrest", "nrInscEstabPrest", "ideTomador" })
			// @Entity(name = "Reinf$EvtServPrest$InfoServPrest$IdeEstabPrest")
			@Table(name = "IDE_ESTAB_PREST")
			@Inheritance(strategy = InheritanceType.JOINED)
			@Embeddable
			public static class IdeEstabPrest implements Equals, HashCode {

				protected long tpInscEstabPrest;
				@XmlElement(required = true)
				protected String nrInscEstabPrest;
				@XmlElement(required = true)
				@Embedded
				protected Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador ideTomador;
				@XmlAttribute(name = "Hjid")
				@Transient
				protected Long hjid;

				/**
				 * Obt�m o valor da propriedade tpInscEstabPrest.
				 * 
				 */
				@Basic
				@Column(name = "TP_INSC_ESTAB_PREST", scale = 0)
				public long getTpInscEstabPrest() {
					return tpInscEstabPrest;
				}

				/**
				 * Define o valor da propriedade tpInscEstabPrest.
				 * 
				 */
				public void setTpInscEstabPrest(long value) {
					this.tpInscEstabPrest = value;
				}

				/**
				 * Obt�m o valor da propriedade nrInscEstabPrest.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				@Basic
				@Column(name = "NR_INSC_ESTAB_PREST")
				public String getNrInscEstabPrest() {
					return nrInscEstabPrest;
				}

				/**
				 * Define o valor da propriedade nrInscEstabPrest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setNrInscEstabPrest(String value) {
					this.nrInscEstabPrest = value;
				}

				/**
				 * Obt�m o valor da propriedade ideTomador.
				 * 
				 * @return possible object is
				 *         {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador }
				 * 
				 */
				@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.class, cascade = {
						CascadeType.ALL })
				@JoinColumn(name = "IDE_TOMADOR_IDE_ESTAB_PREST__0")
				public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador getIdeTomador() {
					return ideTomador;
				}

				/**
				 * Define o valor da propriedade ideTomador.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador }
				 * 
				 */
				public void setIdeTomador(Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador value) {
					this.ideTomador = value;
				}

				/**
				 * Obt�m o valor da propriedade hjid.
				 * 
				 * @return possible object is {@link Long }
				 * 
				 */
				// @Id
				@Column(name = "HJID")
				@GeneratedValue(strategy = GenerationType.AUTO)
				public Long getHjid() {
					return hjid;
				}

				/**
				 * Define o valor da propriedade hjid.
				 * 
				 * @param value
				 *            allowed object is {@link Long }
				 * 
				 */
				public void setHjid(Long value) {
					this.hjid = value;
				}

				public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
						EqualsStrategy strategy) {
					if (!(object instanceof Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest)) {
						return false;
					}
					if (this == object) {
						return true;
					}
					final Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest that = ((Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest) object);
					{
						long lhsTpInscEstabPrest;
						lhsTpInscEstabPrest = this.getTpInscEstabPrest();
						long rhsTpInscEstabPrest;
						rhsTpInscEstabPrest = that.getTpInscEstabPrest();
						if (!strategy.equals(
								LocatorUtils.property(thisLocator, "tpInscEstabPrest", lhsTpInscEstabPrest),
								LocatorUtils.property(thatLocator, "tpInscEstabPrest", rhsTpInscEstabPrest),
								lhsTpInscEstabPrest, rhsTpInscEstabPrest)) {
							return false;
						}
					}
					{
						String lhsNrInscEstabPrest;
						lhsNrInscEstabPrest = this.getNrInscEstabPrest();
						String rhsNrInscEstabPrest;
						rhsNrInscEstabPrest = that.getNrInscEstabPrest();
						if (!strategy.equals(
								LocatorUtils.property(thisLocator, "nrInscEstabPrest", lhsNrInscEstabPrest),
								LocatorUtils.property(thatLocator, "nrInscEstabPrest", rhsNrInscEstabPrest),
								lhsNrInscEstabPrest, rhsNrInscEstabPrest)) {
							return false;
						}
					}
					{
						Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador lhsIdeTomador;
						lhsIdeTomador = this.getIdeTomador();
						Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador rhsIdeTomador;
						rhsIdeTomador = that.getIdeTomador();
						if (!strategy.equals(LocatorUtils.property(thisLocator, "ideTomador", lhsIdeTomador),
								LocatorUtils.property(thatLocator, "ideTomador", rhsIdeTomador), lhsIdeTomador,
								rhsIdeTomador)) {
							return false;
						}
					}
					return true;
				}

				public boolean equals(Object object) {
					final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
					return equals(null, null, object, strategy);
				}

				public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
					int currentHashCode = 1;
					{
						long theTpInscEstabPrest;
						theTpInscEstabPrest = this.getTpInscEstabPrest();
						currentHashCode = strategy.hashCode(
								LocatorUtils.property(locator, "tpInscEstabPrest", theTpInscEstabPrest),
								currentHashCode, theTpInscEstabPrest);
					}
					{
						String theNrInscEstabPrest;
						theNrInscEstabPrest = this.getNrInscEstabPrest();
						currentHashCode = strategy.hashCode(
								LocatorUtils.property(locator, "nrInscEstabPrest", theNrInscEstabPrest),
								currentHashCode, theNrInscEstabPrest);
					}
					{
						Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador theIdeTomador;
						theIdeTomador = this.getIdeTomador();
						currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideTomador", theIdeTomador),
								currentHashCode, theIdeTomador);
					}
					return currentHashCode;
				}

				public int hashCode() {
					final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
					return this.hashCode(null, strategy);
				}

				/**
				 * Identifica\ufffd\ufffdo dos tomadores dos servi\ufffdos
				 * 
				 * <p>
				 * Classe Java de anonymous complex type.
				 * 
				 * <p>
				 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
				 * desta classe.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="tpInscTomador">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;pattern value="[1|4]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="nrInscTomador">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;pattern value="[0-9]{1,14}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="indObra">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *               &lt;pattern value="[0|1|2]"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalBruto">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalBaseRet">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalRetPrinc">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalRetAdic" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalNRetPrinc" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="vlrTotalNRetAdic" minOccurs="0">
				 *           &lt;simpleType>
				 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *               &lt;minLength value="4"/>
				 *               &lt;maxLength value="17"/>
				 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *             &lt;/restriction>
				 *           &lt;/simpleType>
				 *         &lt;/element>
				 *         &lt;element name="nfs" maxOccurs="unbounded">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="serie">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="5"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="numDocto">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="15"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="dtEmissaoNF">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="vlrBruto">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="obs" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;maxLength value="250"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="infoTpServ" maxOccurs="9">
				 *                     &lt;complexType>
				 *                       &lt;complexContent>
				 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                           &lt;sequence>
				 *                             &lt;element name="tpServico">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;pattern value="[0-9]{1,9}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrBaseRet">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrRetencao">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrRetSub" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrNRetPrinc" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos15" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos20" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrServicos25" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrAdicional" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                             &lt;element name="vlrNRetAdic" minOccurs="0">
				 *                               &lt;simpleType>
				 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                                   &lt;minLength value="4"/>
				 *                                   &lt;maxLength value="17"/>
				 *                                   &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                                 &lt;/restriction>
				 *                               &lt;/simpleType>
				 *                             &lt;/element>
				 *                           &lt;/sequence>
				 *                         &lt;/restriction>
				 *                       &lt;/complexContent>
				 *                     &lt;/complexType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoProcRetPr" maxOccurs="50" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="tpProcRetPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *                         &lt;pattern value="[1|2]"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nrProcRetPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="21"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codSuspPrinc" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="valorPrinc">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *         &lt;element name="infoProcRetAd" maxOccurs="50" minOccurs="0">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;sequence>
				 *                   &lt;element name="tpProcRetAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
				 *                         &lt;pattern value="[1|2]"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="nrProcRetAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="1"/>
				 *                         &lt;maxLength value="21"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="codSuspAdic" minOccurs="0">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;pattern value="[0-9]{0,14}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                   &lt;element name="valorAdic">
				 *                     &lt;simpleType>
				 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
				 *                         &lt;minLength value="4"/>
				 *                         &lt;maxLength value="17"/>
				 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
				 *                       &lt;/restriction>
				 *                     &lt;/simpleType>
				 *                   &lt;/element>
				 *                 &lt;/sequence>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "tpInscTomador", "nrInscTomador", "indObra", "vlrTotalBruto",
						"vlrTotalBaseRet", "vlrTotalRetPrinc", "vlrTotalRetAdic", "vlrTotalNRetPrinc",
						"vlrTotalNRetAdic", "nfs", "infoProcRetPr", "infoProcRetAd" })
				// @Entity(name = "Reinf$EvtServPrest$InfoServPrest$IdeEstabPrest$IdeTomador")
				@Table(name = "IDE_TOMADOR")
				@Inheritance(strategy = InheritanceType.JOINED)
				@Embeddable
				public static class IdeTomador implements Equals, HashCode {

					protected short tpInscTomador;
					@XmlElement(required = true)
					protected String nrInscTomador;
					protected short indObra;
					@XmlElement(required = true)
					protected String vlrTotalBruto;
					@XmlElement(required = true)
					protected String vlrTotalBaseRet;
					@XmlElement(required = true)
					protected String vlrTotalRetPrinc;
					protected String vlrTotalRetAdic;
					protected String vlrTotalNRetPrinc;
					protected String vlrTotalNRetAdic;
					@XmlElement(required = true)
					@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "NFS_IDE_TOMADOR_HJID")
					protected List<br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> nfs;

					
					
					@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "INFO_PROC_RET_PR_IDE_TOMADOR_0")					
					protected List<br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetPr> infoProcRetPr;

					
					
					
					@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "INFO_PROC_RET_AD_IDE_TOMADOR_0")					
					protected List<br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.InfoProcRetAd> infoProcRetAd;
					@XmlAttribute(name = "Hjid")
					@Transient
					protected Long hjid;

					/**
					 * Obt�m o valor da propriedade tpInscTomador.
					 * 
					 */
					@Basic
					@Column(name = "TP_INSC_TOMADOR", scale = 0)
					public short getTpInscTomador() {
						return tpInscTomador;
					}

					/**
					 * Define o valor da propriedade tpInscTomador.
					 * 
					 */
					public void setTpInscTomador(short value) {
						this.tpInscTomador = value;
					}

					/**
					 * Obt�m o valor da propriedade nrInscTomador.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "NR_INSC_TOMADOR")
					public String getNrInscTomador() {
						return nrInscTomador;
					}

					/**
					 * Define o valor da propriedade nrInscTomador.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setNrInscTomador(String value) {
						this.nrInscTomador = value;
					}

					/**
					 * Obt�m o valor da propriedade indObra.
					 * 
					 */
					@Basic
					@Column(name = "IND_OBRA", scale = 0)
					public short getIndObra() {
						return indObra;
					}

					/**
					 * Define o valor da propriedade indObra.
					 * 
					 */
					public void setIndObra(short value) {
						this.indObra = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalBruto.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_BRUTO", length = 17)
					public String getVlrTotalBruto() {
						return vlrTotalBruto;
					}

					/**
					 * Define o valor da propriedade vlrTotalBruto.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalBruto(String value) {
						this.vlrTotalBruto = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalBaseRet.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_BASE_RET", length = 17)
					public String getVlrTotalBaseRet() {
						return vlrTotalBaseRet;
					}

					/**
					 * Define o valor da propriedade vlrTotalBaseRet.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalBaseRet(String value) {
						this.vlrTotalBaseRet = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalRetPrinc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_RET_PRINC", length = 17)
					public String getVlrTotalRetPrinc() {
						return vlrTotalRetPrinc;
					}

					/**
					 * Define o valor da propriedade vlrTotalRetPrinc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalRetPrinc(String value) {
						this.vlrTotalRetPrinc = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalRetAdic.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_RET_ADIC", length = 17)
					public String getVlrTotalRetAdic() {
						return vlrTotalRetAdic;
					}

					/**
					 * Define o valor da propriedade vlrTotalRetAdic.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalRetAdic(String value) {
						this.vlrTotalRetAdic = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalNRetPrinc.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_NRET_PRINC", length = 17)
					public String getVlrTotalNRetPrinc() {
						return vlrTotalNRetPrinc;
					}

					/**
					 * Define o valor da propriedade vlrTotalNRetPrinc.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalNRetPrinc(String value) {
						this.vlrTotalNRetPrinc = value;
					}

					/**
					 * Obt�m o valor da propriedade vlrTotalNRetAdic.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					@Basic
					@Column(name = "VLR_TOTAL_NRET_ADIC", length = 17)
					public String getVlrTotalNRetAdic() {
						return vlrTotalNRetAdic;
					}

					/**
					 * Define o valor da propriedade vlrTotalNRetAdic.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setVlrTotalNRetAdic(String value) {
						this.vlrTotalNRetAdic = value;
					}

					/**
					 * Gets the value of the nfs property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live list, not a snapshot.
					 * Therefore any modification you make to the returned list will be present
					 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
					 * for the nfs property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getNfs().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs }
					 * 
					 * 
					 */
					@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "NFS_IDE_TOMADOR_HJID")
					public List<br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs> getNfs() {
						if (nfs == null) {
							nfs = new ArrayList<br.gov.esocial.reinf.schemas.evttomadorservicos.v1_03_02.Reinf.EvtServTom.InfoServTom.IdeEstabObra.IdePrestServ.Nfs>();
						}
						return this.nfs;
					}

					/**
					 * 
					 * 
					 */
					/*
					 * public void
					 * setNfs(List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs>
					 * nfs) { this.nfs = nfs; }
					 */

					/**
					 * Gets the value of the infoProcRetPr property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live list, not a snapshot.
					 * Therefore any modification you make to the returned list will be present
					 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
					 * for the infoProcRetPr property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoProcRetPr().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr }
					 * 
					 * 
					 */
					/*public List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr> getInfoProcRetPr() {
						if (infoProcRetPr == null) {
							infoProcRetPr = new ArrayList<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr>();
						}
						return this.infoProcRetPr;
					}*/

					/**
					 * 
					 * 
					 */
					/*public void setInfoProcRetPr(
							List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr> infoProcRetPr) {
						this.infoProcRetPr = infoProcRetPr;
					}*/

					/**
					 * Gets the value of the infoProcRetAd property.
					 * 
					 * <p>
					 * This accessor method returns a reference to the live list, not a snapshot.
					 * Therefore any modification you make to the returned list will be present
					 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
					 * for the infoProcRetAd property.
					 * 
					 * <p>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getInfoProcRetAd().add(newItem);
					 * </pre>
					 * 
					 * 
					 * <p>
					 * Objects of the following type(s) are allowed in the list
					 * {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd }
					 * 
					 * 
					 */
					/*@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd.class, cascade = {
							CascadeType.ALL })
					@JoinColumn(name = "INFO_PROC_RET_AD_IDE_TOMADOR_0")
					public List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd> getInfoProcRetAd() {
						if (infoProcRetAd == null) {
							infoProcRetAd = new ArrayList<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd>();
						}
						return this.infoProcRetAd;
					}*/

					/**
					 * 
					 * 
					 */
					/*public void setInfoProcRetAd(
							List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd> infoProcRetAd) {
						this.infoProcRetAd = infoProcRetAd;
					}*/

					/**
					 * Obt�m o valor da propriedade hjid.
					 * 
					 * @return possible object is {@link Long }
					 * 
					 */
					// @Id
					@Column(name = "HJID")
					@GeneratedValue(strategy = GenerationType.AUTO)
					public Long getHjid() {
						return hjid;
					}

					/**
					 * Define o valor da propriedade hjid.
					 * 
					 * @param value
					 *            allowed object is {@link Long }
					 * 
					 */
					public void setHjid(Long value) {
						this.hjid = value;
					}

					public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
							EqualsStrategy strategy) {
						if (!(object instanceof Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador)) {
							return false;
						}
						if (this == object) {
							return true;
						}
						final Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador that = ((Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador) object);
						{
							short lhsTpInscTomador;
							lhsTpInscTomador = this.getTpInscTomador();
							short rhsTpInscTomador;
							rhsTpInscTomador = that.getTpInscTomador();
							if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInscTomador", lhsTpInscTomador),
									LocatorUtils.property(thatLocator, "tpInscTomador", rhsTpInscTomador),
									lhsTpInscTomador, rhsTpInscTomador)) {
								return false;
							}
						}
						{
							String lhsNrInscTomador;
							lhsNrInscTomador = this.getNrInscTomador();
							String rhsNrInscTomador;
							rhsNrInscTomador = that.getNrInscTomador();
							if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInscTomador", lhsNrInscTomador),
									LocatorUtils.property(thatLocator, "nrInscTomador", rhsNrInscTomador),
									lhsNrInscTomador, rhsNrInscTomador)) {
								return false;
							}
						}
						{
							short lhsIndObra;
							lhsIndObra = this.getIndObra();
							short rhsIndObra;
							rhsIndObra = that.getIndObra();
							if (!strategy.equals(LocatorUtils.property(thisLocator, "indObra", lhsIndObra),
									LocatorUtils.property(thatLocator, "indObra", rhsIndObra), lhsIndObra,
									rhsIndObra)) {
								return false;
							}
						}
						{
							String lhsVlrTotalBruto;
							lhsVlrTotalBruto = this.getVlrTotalBruto();
							String rhsVlrTotalBruto;
							rhsVlrTotalBruto = that.getVlrTotalBruto();
							if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrTotalBruto", lhsVlrTotalBruto),
									LocatorUtils.property(thatLocator, "vlrTotalBruto", rhsVlrTotalBruto),
									lhsVlrTotalBruto, rhsVlrTotalBruto)) {
								return false;
							}
						}
						{
							String lhsVlrTotalBaseRet;
							lhsVlrTotalBaseRet = this.getVlrTotalBaseRet();
							String rhsVlrTotalBaseRet;
							rhsVlrTotalBaseRet = that.getVlrTotalBaseRet();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalBaseRet", lhsVlrTotalBaseRet),
									LocatorUtils.property(thatLocator, "vlrTotalBaseRet", rhsVlrTotalBaseRet),
									lhsVlrTotalBaseRet, rhsVlrTotalBaseRet)) {
								return false;
							}
						}
						{
							String lhsVlrTotalRetPrinc;
							lhsVlrTotalRetPrinc = this.getVlrTotalRetPrinc();
							String rhsVlrTotalRetPrinc;
							rhsVlrTotalRetPrinc = that.getVlrTotalRetPrinc();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalRetPrinc", lhsVlrTotalRetPrinc),
									LocatorUtils.property(thatLocator, "vlrTotalRetPrinc", rhsVlrTotalRetPrinc),
									lhsVlrTotalRetPrinc, rhsVlrTotalRetPrinc)) {
								return false;
							}
						}
						{
							String lhsVlrTotalRetAdic;
							lhsVlrTotalRetAdic = this.getVlrTotalRetAdic();
							String rhsVlrTotalRetAdic;
							rhsVlrTotalRetAdic = that.getVlrTotalRetAdic();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalRetAdic", lhsVlrTotalRetAdic),
									LocatorUtils.property(thatLocator, "vlrTotalRetAdic", rhsVlrTotalRetAdic),
									lhsVlrTotalRetAdic, rhsVlrTotalRetAdic)) {
								return false;
							}
						}
						{
							String lhsVlrTotalNRetPrinc;
							lhsVlrTotalNRetPrinc = this.getVlrTotalNRetPrinc();
							String rhsVlrTotalNRetPrinc;
							rhsVlrTotalNRetPrinc = that.getVlrTotalNRetPrinc();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalNRetPrinc", lhsVlrTotalNRetPrinc),
									LocatorUtils.property(thatLocator, "vlrTotalNRetPrinc", rhsVlrTotalNRetPrinc),
									lhsVlrTotalNRetPrinc, rhsVlrTotalNRetPrinc)) {
								return false;
							}
						}
						{
							String lhsVlrTotalNRetAdic;
							lhsVlrTotalNRetAdic = this.getVlrTotalNRetAdic();
							String rhsVlrTotalNRetAdic;
							rhsVlrTotalNRetAdic = that.getVlrTotalNRetAdic();
							if (!strategy.equals(
									LocatorUtils.property(thisLocator, "vlrTotalNRetAdic", lhsVlrTotalNRetAdic),
									LocatorUtils.property(thatLocator, "vlrTotalNRetAdic", rhsVlrTotalNRetAdic),
									lhsVlrTotalNRetAdic, rhsVlrTotalNRetAdic)) {
								return false;
							}
						}
						{
						}
						{}
						{}
						return true;
					}

					public boolean equals(Object object) {
						final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
						return equals(null, null, object, strategy);
					}

					public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
						int currentHashCode = 1;
						{
							short theTpInscTomador;
							theTpInscTomador = this.getTpInscTomador();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "tpInscTomador", theTpInscTomador), currentHashCode,
									theTpInscTomador);
						}
						{
							String theNrInscTomador;
							theNrInscTomador = this.getNrInscTomador();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "nrInscTomador", theNrInscTomador), currentHashCode,
									theNrInscTomador);
						}
						{
							short theIndObra;
							theIndObra = this.getIndObra();
							currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indObra", theIndObra),
									currentHashCode, theIndObra);
						}
						{
							String theVlrTotalBruto;
							theVlrTotalBruto = this.getVlrTotalBruto();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalBruto", theVlrTotalBruto), currentHashCode,
									theVlrTotalBruto);
						}
						{
							String theVlrTotalBaseRet;
							theVlrTotalBaseRet = this.getVlrTotalBaseRet();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalBaseRet", theVlrTotalBaseRet),
									currentHashCode, theVlrTotalBaseRet);
						}
						{
							String theVlrTotalRetPrinc;
							theVlrTotalRetPrinc = this.getVlrTotalRetPrinc();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalRetPrinc", theVlrTotalRetPrinc),
									currentHashCode, theVlrTotalRetPrinc);
						}
						{
							String theVlrTotalRetAdic;
							theVlrTotalRetAdic = this.getVlrTotalRetAdic();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalRetAdic", theVlrTotalRetAdic),
									currentHashCode, theVlrTotalRetAdic);
						}
						{
							String theVlrTotalNRetPrinc;
							theVlrTotalNRetPrinc = this.getVlrTotalNRetPrinc();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalNRetPrinc", theVlrTotalNRetPrinc),
									currentHashCode, theVlrTotalNRetPrinc);
						}
						{
							String theVlrTotalNRetAdic;
							theVlrTotalNRetAdic = this.getVlrTotalNRetAdic();
							currentHashCode = strategy.hashCode(
									LocatorUtils.property(locator, "vlrTotalNRetAdic", theVlrTotalNRetAdic),
									currentHashCode, theVlrTotalNRetAdic);
						}
						{
						}
						{}
						{}
						return currentHashCode;
					}

					public int hashCode() {
						final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
						return this.hashCode(null, strategy);
					}

					/**
					 * 
					 * Informa\ufffd\ufffdes de processos relacionados a n\ufffdo reten\ufffd\ufffdo
					 * de contribui\ufffd\ufffdo previdenci\ufffdria adicional Valida\ufffd\ufffdo:
					 * A soma dos valores informados no campo {valorAdic} deste grupo deve ser igual
					 * a {vlrTotalNRetAdic}.
					 * 
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
					 * desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="tpProcRetAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
					 *               &lt;pattern value="[1|2]"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nrProcRetAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="21"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codSuspAdic" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="valorAdic">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "tpProcRetAdic", "nrProcRetAdic", "codSuspAdic", "valorAdic" })
					// @Entity(name =
					// "Reinf$EvtServPrest$InfoServPrest$IdeEstabPrest$IdeTomador$InfoProcRetAd")
					@Table(name = "INFO_PROC_RET_AD__0")
					@Inheritance(strategy = InheritanceType.JOINED)
					public static class InfoProcRetAd implements Equals, HashCode {

						protected short tpProcRetAdic;
						@XmlElement(required = true)
						protected String nrProcRetAdic;
						protected String codSuspAdic;
						@XmlElement(required = true)
						protected String valorAdic;
						@XmlAttribute(name = "Hjid")
						@Transient
						protected Long hjid;

						/**
						 * Obt�m o valor da propriedade tpProcRetAdic.
						 * 
						 */
						@Basic
						@Column(name = "TP_PROC_RET_ADIC", scale = 0)
						public short getTpProcRetAdic() {
							return tpProcRetAdic;
						}

						/**
						 * Define o valor da propriedade tpProcRetAdic.
						 * 
						 */
						public void setTpProcRetAdic(short value) {
							this.tpProcRetAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade nrProcRetAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "NR_PROC_RET_ADIC", length = 21)
						public String getNrProcRetAdic() {
							return nrProcRetAdic;
						}

						/**
						 * Define o valor da propriedade nrProcRetAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNrProcRetAdic(String value) {
							this.nrProcRetAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade codSuspAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "COD_SUSP_ADIC")
						public String getCodSuspAdic() {
							return codSuspAdic;
						}

						/**
						 * Define o valor da propriedade codSuspAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSuspAdic(String value) {
							this.codSuspAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade valorAdic.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "VALOR_ADIC", length = 17)
						public String getValorAdic() {
							return valorAdic;
						}

						/**
						 * Define o valor da propriedade valorAdic.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setValorAdic(String value) {
							this.valorAdic = value;
						}

						/**
						 * Obt�m o valor da propriedade hjid.
						 * 
						 * @return possible object is {@link Long }
						 * 
						 */
						// @Id
						@Column(name = "HJID")
						@GeneratedValue(strategy = GenerationType.AUTO)
						public Long getHjid() {
							return hjid;
						}

						/**
						 * Define o valor da propriedade hjid.
						 * 
						 * @param value
						 *            allowed object is {@link Long }
						 * 
						 */
						public void setHjid(Long value) {
							this.hjid = value;
						}

						public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
								EqualsStrategy strategy) {
							if (!(object instanceof Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd)) {
								return false;
							}
							if (this == object) {
								return true;
							}
							final Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd that = ((Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd) object);
							{
								short lhsTpProcRetAdic;
								lhsTpProcRetAdic = this.getTpProcRetAdic();
								short rhsTpProcRetAdic;
								rhsTpProcRetAdic = that.getTpProcRetAdic();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "tpProcRetAdic", lhsTpProcRetAdic),
										LocatorUtils.property(thatLocator, "tpProcRetAdic", rhsTpProcRetAdic),
										lhsTpProcRetAdic, rhsTpProcRetAdic)) {
									return false;
								}
							}
							{
								String lhsNrProcRetAdic;
								lhsNrProcRetAdic = this.getNrProcRetAdic();
								String rhsNrProcRetAdic;
								rhsNrProcRetAdic = that.getNrProcRetAdic();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "nrProcRetAdic", lhsNrProcRetAdic),
										LocatorUtils.property(thatLocator, "nrProcRetAdic", rhsNrProcRetAdic),
										lhsNrProcRetAdic, rhsNrProcRetAdic)) {
									return false;
								}
							}
							{
								String lhsCodSuspAdic;
								lhsCodSuspAdic = this.getCodSuspAdic();
								String rhsCodSuspAdic;
								rhsCodSuspAdic = that.getCodSuspAdic();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "codSuspAdic", lhsCodSuspAdic),
										LocatorUtils.property(thatLocator, "codSuspAdic", rhsCodSuspAdic),
										lhsCodSuspAdic, rhsCodSuspAdic)) {
									return false;
								}
							}
							{
								String lhsValorAdic;
								lhsValorAdic = this.getValorAdic();
								String rhsValorAdic;
								rhsValorAdic = that.getValorAdic();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "valorAdic", lhsValorAdic),
										LocatorUtils.property(thatLocator, "valorAdic", rhsValorAdic), lhsValorAdic,
										rhsValorAdic)) {
									return false;
								}
							}
							return true;
						}

						public boolean equals(Object object) {
							final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
							return equals(null, null, object, strategy);
						}

						public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
							int currentHashCode = 1;
							{
								short theTpProcRetAdic;
								theTpProcRetAdic = this.getTpProcRetAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "tpProcRetAdic", theTpProcRetAdic),
										currentHashCode, theTpProcRetAdic);
							}
							{
								String theNrProcRetAdic;
								theNrProcRetAdic = this.getNrProcRetAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "nrProcRetAdic", theNrProcRetAdic),
										currentHashCode, theNrProcRetAdic);
							}
							{
								String theCodSuspAdic;
								theCodSuspAdic = this.getCodSuspAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "codSuspAdic", theCodSuspAdic), currentHashCode,
										theCodSuspAdic);
							}
							{
								String theValorAdic;
								theValorAdic = this.getValorAdic();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "valorAdic", theValorAdic), currentHashCode,
										theValorAdic);
							}
							return currentHashCode;
						}

						public int hashCode() {
							final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
							return this.hashCode(null, strategy);
						}

					}

					/**
					 * Informa\ufffd\ufffdes de processos relacionados a n\ufffdo reten\ufffd\ufffdo
					 * de contribui\ufffd\ufffdo previdenci\ufffdria
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
					 * desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="tpProcRetPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
					 *               &lt;pattern value="[1|2]"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="nrProcRetPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="21"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="codSuspPrinc" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;pattern value="[0-9]{0,14}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="valorPrinc">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "tpProcRetPrinc", "nrProcRetPrinc", "codSuspPrinc",
							"valorPrinc" })
					// @Entity(name =
					// "Reinf$EvtServPrest$InfoServPrest$IdeEstabPrest$IdeTomador$InfoProcRetPr")
					@Table(name = "INFO_PROC_RET_PR__0")
					@Inheritance(strategy = InheritanceType.JOINED)
					public static class InfoProcRetPr implements Equals, HashCode {

						protected short tpProcRetPrinc;
						@XmlElement(required = true)
						protected String nrProcRetPrinc;
						protected String codSuspPrinc;
						@XmlElement(required = true)
						protected String valorPrinc;
						@XmlAttribute(name = "Hjid")
						@Transient
						protected Long hjid;

						/**
						 * Obt�m o valor da propriedade tpProcRetPrinc.
						 * 
						 */
						@Basic
						@Column(name = "TP_PROC_RET_PRINC", scale = 0)
						public short getTpProcRetPrinc() {
							return tpProcRetPrinc;
						}

						/**
						 * Define o valor da propriedade tpProcRetPrinc.
						 * 
						 */
						public void setTpProcRetPrinc(short value) {
							this.tpProcRetPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade nrProcRetPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "NR_PROC_RET_PRINC", length = 21)
						public String getNrProcRetPrinc() {
							return nrProcRetPrinc;
						}

						/**
						 * Define o valor da propriedade nrProcRetPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNrProcRetPrinc(String value) {
							this.nrProcRetPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade codSuspPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "COD_SUSP_PRINC")
						public String getCodSuspPrinc() {
							return codSuspPrinc;
						}

						/**
						 * Define o valor da propriedade codSuspPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setCodSuspPrinc(String value) {
							this.codSuspPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade valorPrinc.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "VALOR_PRINC", length = 17)
						public String getValorPrinc() {
							return valorPrinc;
						}

						/**
						 * Define o valor da propriedade valorPrinc.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setValorPrinc(String value) {
							this.valorPrinc = value;
						}

						/**
						 * Obt�m o valor da propriedade hjid.
						 * 
						 * @return possible object is {@link Long }
						 * 
						 */
						// @Id
						@Column(name = "HJID")
						@GeneratedValue(strategy = GenerationType.AUTO)
						public Long getHjid() {
							return hjid;
						}

						/**
						 * Define o valor da propriedade hjid.
						 * 
						 * @param value
						 *            allowed object is {@link Long }
						 * 
						 */
						public void setHjid(Long value) {
							this.hjid = value;
						}

						public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
								EqualsStrategy strategy) {
							if (!(object instanceof Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr)) {
								return false;
							}
							if (this == object) {
								return true;
							}
							final Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr that = ((Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr) object);
							{
								short lhsTpProcRetPrinc;
								lhsTpProcRetPrinc = this.getTpProcRetPrinc();
								short rhsTpProcRetPrinc;
								rhsTpProcRetPrinc = that.getTpProcRetPrinc();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "tpProcRetPrinc", lhsTpProcRetPrinc),
										LocatorUtils.property(thatLocator, "tpProcRetPrinc", rhsTpProcRetPrinc),
										lhsTpProcRetPrinc, rhsTpProcRetPrinc)) {
									return false;
								}
							}
							{
								String lhsNrProcRetPrinc;
								lhsNrProcRetPrinc = this.getNrProcRetPrinc();
								String rhsNrProcRetPrinc;
								rhsNrProcRetPrinc = that.getNrProcRetPrinc();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "nrProcRetPrinc", lhsNrProcRetPrinc),
										LocatorUtils.property(thatLocator, "nrProcRetPrinc", rhsNrProcRetPrinc),
										lhsNrProcRetPrinc, rhsNrProcRetPrinc)) {
									return false;
								}
							}
							{
								String lhsCodSuspPrinc;
								lhsCodSuspPrinc = this.getCodSuspPrinc();
								String rhsCodSuspPrinc;
								rhsCodSuspPrinc = that.getCodSuspPrinc();
								if (!strategy.equals(
										LocatorUtils.property(thisLocator, "codSuspPrinc", lhsCodSuspPrinc),
										LocatorUtils.property(thatLocator, "codSuspPrinc", rhsCodSuspPrinc),
										lhsCodSuspPrinc, rhsCodSuspPrinc)) {
									return false;
								}
							}
							{
								String lhsValorPrinc;
								lhsValorPrinc = this.getValorPrinc();
								String rhsValorPrinc;
								rhsValorPrinc = that.getValorPrinc();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "valorPrinc", lhsValorPrinc),
										LocatorUtils.property(thatLocator, "valorPrinc", rhsValorPrinc), lhsValorPrinc,
										rhsValorPrinc)) {
									return false;
								}
							}
							return true;
						}

						public boolean equals(Object object) {
							final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
							return equals(null, null, object, strategy);
						}

						public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
							int currentHashCode = 1;
							{
								short theTpProcRetPrinc;
								theTpProcRetPrinc = this.getTpProcRetPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "tpProcRetPrinc", theTpProcRetPrinc),
										currentHashCode, theTpProcRetPrinc);
							}
							{
								String theNrProcRetPrinc;
								theNrProcRetPrinc = this.getNrProcRetPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "nrProcRetPrinc", theNrProcRetPrinc),
										currentHashCode, theNrProcRetPrinc);
							}
							{
								String theCodSuspPrinc;
								theCodSuspPrinc = this.getCodSuspPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "codSuspPrinc", theCodSuspPrinc),
										currentHashCode, theCodSuspPrinc);
							}
							{
								String theValorPrinc;
								theValorPrinc = this.getValorPrinc();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "valorPrinc", theValorPrinc), currentHashCode,
										theValorPrinc);
							}
							return currentHashCode;
						}

						public int hashCode() {
							final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
							return this.hashCode(null, strategy);
						}

					}

					/**
					 * Notas Fiscais do Prestador de Servi\ufffdos
					 * 
					 * <p>
					 * Classe Java de anonymous complex type.
					 * 
					 * <p>
					 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
					 * desta classe.
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;sequence>
					 *         &lt;element name="serie">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="5"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="numDocto">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="1"/>
					 *               &lt;maxLength value="15"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="dtEmissaoNF">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="vlrBruto">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;minLength value="4"/>
					 *               &lt;maxLength value="17"/>
					 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="obs" minOccurs="0">
					 *           &lt;simpleType>
					 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *               &lt;maxLength value="250"/>
					 *             &lt;/restriction>
					 *           &lt;/simpleType>
					 *         &lt;/element>
					 *         &lt;element name="infoTpServ" maxOccurs="9">
					 *           &lt;complexType>
					 *             &lt;complexContent>
					 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *                 &lt;sequence>
					 *                   &lt;element name="tpServico">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;pattern value="[0-9]{1,9}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrBaseRet">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrRetencao">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrRetSub" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrNRetPrinc" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos15" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos20" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrServicos25" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrAdicional" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                   &lt;element name="vlrNRetAdic" minOccurs="0">
					 *                     &lt;simpleType>
					 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
					 *                         &lt;minLength value="4"/>
					 *                         &lt;maxLength value="17"/>
					 *                         &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
					 *                       &lt;/restriction>
					 *                     &lt;/simpleType>
					 *                   &lt;/element>
					 *                 &lt;/sequence>
					 *               &lt;/restriction>
					 *             &lt;/complexContent>
					 *           &lt;/complexType>
					 *         &lt;/element>
					 *       &lt;/sequence>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 * 
					 * 
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "serie", "numDocto", "dtEmissaoNF", "vlrBruto", "obs",
							"infoTpServ" })
					// @Entity(name =
					// "Reinf$EvtServPrest$InfoServPrest$IdeEstabPrest$IdeTomador$Nfs")
					@Table(name = "NFS__0")
					@Inheritance(strategy = InheritanceType.JOINED)
					public static class Nfs implements Equals, HashCode {

						@XmlElement(required = true)
						protected String serie;
						@XmlElement(required = true)
						protected String numDocto;
						@XmlElement(required = true)
						protected String dtEmissaoNF;
						@XmlElement(required = true)
						protected String vlrBruto;
						protected String obs;
						@XmlElement(required = true)
						protected List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ> infoTpServ;
						@XmlAttribute(name = "Hjid")
						@Transient
						protected Long hjid;

						/**
						 * Obt�m o valor da propriedade serie.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "SERIE", length = 5)
						public String getSerie() {
							return serie;
						}

						/**
						 * Define o valor da propriedade serie.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setSerie(String value) {
							this.serie = value;
						}

						/**
						 * Obt�m o valor da propriedade numDocto.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "NUM_DOCTO", length = 15)
						public String getNumDocto() {
							return numDocto;
						}

						/**
						 * Define o valor da propriedade numDocto.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setNumDocto(String value) {
							this.numDocto = value;
						}

						/**
						 * Obt�m o valor da propriedade dtEmissaoNF.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Transient
						public String getDtEmissaoNF() {
							return dtEmissaoNF;
						}

						/**
						 * Define o valor da propriedade dtEmissaoNF.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setDtEmissaoNF(String value) {
							this.dtEmissaoNF = value;
						}

						/**
						 * Obt�m o valor da propriedade vlrBruto.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "VLR_BRUTO", length = 17)
						public String getVlrBruto() {
							return vlrBruto;
						}

						/**
						 * Define o valor da propriedade vlrBruto.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setVlrBruto(String value) {
							this.vlrBruto = value;
						}

						/**
						 * Obt�m o valor da propriedade obs.
						 * 
						 * @return possible object is {@link String }
						 * 
						 */
						@Basic
						@Column(name = "OBS", length = 250)
						public String getObs() {
							return obs;
						}

						/**
						 * Define o valor da propriedade obs.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 * 
						 */
						public void setObs(String value) {
							this.obs = value;
						}

						/**
						 * Gets the value of the infoTpServ property.
						 * 
						 * <p>
						 * This accessor method returns a reference to the live list, not a snapshot.
						 * Therefore any modification you make to the returned list will be present
						 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
						 * for the infoTpServ property.
						 * 
						 * <p>
						 * For example, to add a new item, do as follows:
						 * 
						 * <pre>
						 * getInfoTpServ().add(newItem);
						 * </pre>
						 * 
						 * 
						 * <p>
						 * Objects of the following type(s) are allowed in the list
						 * {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ }
						 * 
						 * 
						 */
						@OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evtprestadorservicos.v1_03_02.Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ.class, cascade = {
								CascadeType.ALL })
						@JoinColumn(name = "INFO_TP_SERV_NFS___0_HJID")
						public List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ> getInfoTpServ() {
							if (infoTpServ == null) {
								infoTpServ = new ArrayList<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ>();
							}
							return this.infoTpServ;
						}

						/**
						 * 
						 * 
						 */
						public void setInfoTpServ(
								List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ> infoTpServ) {
							this.infoTpServ = infoTpServ;
						}

						/**
						 * Obt�m o valor da propriedade hjid.
						 * 
						 * @return possible object is {@link Long }
						 * 
						 */
						// @Id
						@Column(name = "HJID")
						@GeneratedValue(strategy = GenerationType.AUTO)
						public Long getHjid() {
							return hjid;
						}

						/**
						 * Define o valor da propriedade hjid.
						 * 
						 * @param value
						 *            allowed object is {@link Long }
						 * 
						 */
						public void setHjid(Long value) {
							this.hjid = value;
						}

						/*
						 * @Basic
						 * 
						 * @Column(name = "DT_EMISSAO_NFITEM")
						 * 
						 * @Temporal(TemporalType.TIMESTAMP) public Date getDtEmissaoNFItem() { return
						 * XmlAdapterUtils.unmarshall(StringAsDateTime.class, this.getDtEmissaoNF()); }
						 * 
						 * public void setDtEmissaoNFItem(Date target) {
						 * setDtEmissaoNF(XmlAdapterUtils.marshall(StringAsDateTime.class, target)); }
						 */

						public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
								EqualsStrategy strategy) {
							if (!(object instanceof Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs)) {
								return false;
							}
							if (this == object) {
								return true;
							}
							final Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs that = ((Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs) object);
							{
								String lhsSerie;
								lhsSerie = this.getSerie();
								String rhsSerie;
								rhsSerie = that.getSerie();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "serie", lhsSerie),
										LocatorUtils.property(thatLocator, "serie", rhsSerie), lhsSerie, rhsSerie)) {
									return false;
								}
							}
							{
								String lhsNumDocto;
								lhsNumDocto = this.getNumDocto();
								String rhsNumDocto;
								rhsNumDocto = that.getNumDocto();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "numDocto", lhsNumDocto),
										LocatorUtils.property(thatLocator, "numDocto", rhsNumDocto), lhsNumDocto,
										rhsNumDocto)) {
									return false;
								}
							}
							{
								String lhsDtEmissaoNF;
								lhsDtEmissaoNF = this.getDtEmissaoNF();
								String rhsDtEmissaoNF;
								rhsDtEmissaoNF = that.getDtEmissaoNF();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "dtEmissaoNF", lhsDtEmissaoNF),
										LocatorUtils.property(thatLocator, "dtEmissaoNF", rhsDtEmissaoNF),
										lhsDtEmissaoNF, rhsDtEmissaoNF)) {
									return false;
								}
							}
							{
								String lhsVlrBruto;
								lhsVlrBruto = this.getVlrBruto();
								String rhsVlrBruto;
								rhsVlrBruto = that.getVlrBruto();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrBruto", lhsVlrBruto),
										LocatorUtils.property(thatLocator, "vlrBruto", rhsVlrBruto), lhsVlrBruto,
										rhsVlrBruto)) {
									return false;
								}
							}
							{
								String lhsObs;
								lhsObs = this.getObs();
								String rhsObs;
								rhsObs = that.getObs();
								if (!strategy.equals(LocatorUtils.property(thisLocator, "obs", lhsObs),
										LocatorUtils.property(thatLocator, "obs", rhsObs), lhsObs, rhsObs)) {
									return false;
								}
							}
							{
								List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ> lhsInfoTpServ;
								lhsInfoTpServ = (((this.infoTpServ != null) && (!this.infoTpServ.isEmpty()))
										? this.getInfoTpServ()
										: null);
								List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ> rhsInfoTpServ;
								rhsInfoTpServ = (((that.infoTpServ != null) && (!that.infoTpServ.isEmpty()))
										? that.getInfoTpServ()
										: null);
								if (!strategy.equals(LocatorUtils.property(thisLocator, "infoTpServ", lhsInfoTpServ),
										LocatorUtils.property(thatLocator, "infoTpServ", rhsInfoTpServ), lhsInfoTpServ,
										rhsInfoTpServ)) {
									return false;
								}
							}
							return true;
						}

						public boolean equals(Object object) {
							final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
							return equals(null, null, object, strategy);
						}

						public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
							int currentHashCode = 1;
							{
								String theSerie;
								theSerie = this.getSerie();
								currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "serie", theSerie),
										currentHashCode, theSerie);
							}
							{
								String theNumDocto;
								theNumDocto = this.getNumDocto();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "numDocto", theNumDocto), currentHashCode,
										theNumDocto);
							}
							{
								String theDtEmissaoNF;
								theDtEmissaoNF = this.getDtEmissaoNF();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "dtEmissaoNF", theDtEmissaoNF), currentHashCode,
										theDtEmissaoNF);
							}
							{
								String theVlrBruto;
								theVlrBruto = this.getVlrBruto();
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "vlrBruto", theVlrBruto), currentHashCode,
										theVlrBruto);
							}
							{
								String theObs;
								theObs = this.getObs();
								currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "obs", theObs),
										currentHashCode, theObs);
							}
							{
								List<Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ> theInfoTpServ;
								theInfoTpServ = (((this.infoTpServ != null) && (!this.infoTpServ.isEmpty()))
										? this.getInfoTpServ()
										: null);
								currentHashCode = strategy.hashCode(
										LocatorUtils.property(locator, "infoTpServ", theInfoTpServ), currentHashCode,
										theInfoTpServ);
							}
							return currentHashCode;
						}

						public int hashCode() {
							final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
							return this.hashCode(null, strategy);
						}

						/**
						 * Informa\ufffd\ufffdes sobre os tipos de Servi\ufffdos constantes da Nota
						 * Fiscal
						 * 
						 * <p>
						 * Classe Java de anonymous complex type.
						 * 
						 * <p>
						 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
						 * desta classe.
						 * 
						 * <pre>
						 * &lt;complexType>
						 *   &lt;complexContent>
						 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
						 *       &lt;sequence>
						 *         &lt;element name="tpServico">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;pattern value="[0-9]{1,9}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrBaseRet">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrRetencao">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrRetSub" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrNRetPrinc" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos15" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos20" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrServicos25" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrAdicional" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *         &lt;element name="vlrNRetAdic" minOccurs="0">
						 *           &lt;simpleType>
						 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
						 *               &lt;minLength value="4"/>
						 *               &lt;maxLength value="17"/>
						 *               &lt;pattern value="[0-9]{1,14}[,][0-9]{2}"/>
						 *             &lt;/restriction>
						 *           &lt;/simpleType>
						 *         &lt;/element>
						 *       &lt;/sequence>
						 *     &lt;/restriction>
						 *   &lt;/complexContent>
						 * &lt;/complexType>
						 * </pre>
						 * 
						 * 
						 */
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "tpServico", "vlrBaseRet", "vlrRetencao", "vlrRetSub",
								"vlrNRetPrinc", "vlrServicos15", "vlrServicos20", "vlrServicos25", "vlrAdicional",
								"vlrNRetAdic" })
						// @Entity(name =
						// "Reinf$EvtServPrest$InfoServPrest$IdeEstabPrest$IdeTomador$Nfs$InfoTpServ")
						@Table(name = "INFO_TP_SERV__0")
						@Inheritance(strategy = InheritanceType.JOINED)
						public static class InfoTpServ implements Equals, HashCode {

							@XmlElement(required = true)
							protected String tpServico;
							@XmlElement(required = true)
							protected String vlrBaseRet;
							@XmlElement(required = true)
							protected String vlrRetencao;
							protected String vlrRetSub;
							protected String vlrNRetPrinc;
							protected String vlrServicos15;
							protected String vlrServicos20;
							protected String vlrServicos25;
							protected String vlrAdicional;
							protected String vlrNRetAdic;
							@XmlAttribute(name = "Hjid")
							@Transient
							protected Long hjid;

							/**
							 * Obt�m o valor da propriedade tpServico.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "TP_SERVICO")
							public String getTpServico() {
								return tpServico;
							}

							/**
							 * Define o valor da propriedade tpServico.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setTpServico(String value) {
								this.tpServico = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrBaseRet.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_BASE_RET", length = 17)
							public String getVlrBaseRet() {
								return vlrBaseRet;
							}

							/**
							 * Define o valor da propriedade vlrBaseRet.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrBaseRet(String value) {
								this.vlrBaseRet = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrRetencao.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_RETENCAO", length = 17)
							public String getVlrRetencao() {
								return vlrRetencao;
							}

							/**
							 * Define o valor da propriedade vlrRetencao.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrRetencao(String value) {
								this.vlrRetencao = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrRetSub.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_RET_SUB", length = 17)
							public String getVlrRetSub() {
								return vlrRetSub;
							}

							/**
							 * Define o valor da propriedade vlrRetSub.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrRetSub(String value) {
								this.vlrRetSub = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrNRetPrinc.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_NRET_PRINC", length = 17)
							public String getVlrNRetPrinc() {
								return vlrNRetPrinc;
							}

							/**
							 * Define o valor da propriedade vlrNRetPrinc.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrNRetPrinc(String value) {
								this.vlrNRetPrinc = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos15.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_SERVICOS_15", length = 17)
							public String getVlrServicos15() {
								return vlrServicos15;
							}

							/**
							 * Define o valor da propriedade vlrServicos15.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos15(String value) {
								this.vlrServicos15 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos20.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_SERVICOS_20", length = 17)
							public String getVlrServicos20() {
								return vlrServicos20;
							}

							/**
							 * Define o valor da propriedade vlrServicos20.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos20(String value) {
								this.vlrServicos20 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrServicos25.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_SERVICOS_25", length = 17)
							public String getVlrServicos25() {
								return vlrServicos25;
							}

							/**
							 * Define o valor da propriedade vlrServicos25.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrServicos25(String value) {
								this.vlrServicos25 = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrAdicional.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_ADICIONAL", length = 17)
							public String getVlrAdicional() {
								return vlrAdicional;
							}

							/**
							 * Define o valor da propriedade vlrAdicional.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrAdicional(String value) {
								this.vlrAdicional = value;
							}

							/**
							 * Obt�m o valor da propriedade vlrNRetAdic.
							 * 
							 * @return possible object is {@link String }
							 * 
							 */
							@Basic
							@Column(name = "VLR_NRET_ADIC", length = 17)
							public String getVlrNRetAdic() {
								return vlrNRetAdic;
							}

							/**
							 * Define o valor da propriedade vlrNRetAdic.
							 * 
							 * @param value
							 *            allowed object is {@link String }
							 * 
							 */
							public void setVlrNRetAdic(String value) {
								this.vlrNRetAdic = value;
							}

							/**
							 * Obt�m o valor da propriedade hjid.
							 * 
							 * @return possible object is {@link Long }
							 * 
							 */
							// @Id
							@Column(name = "HJID")
							@GeneratedValue(strategy = GenerationType.AUTO)
							public Long getHjid() {
								return hjid;
							}

							/**
							 * Define o valor da propriedade hjid.
							 * 
							 * @param value
							 *            allowed object is {@link Long }
							 * 
							 */
							public void setHjid(Long value) {
								this.hjid = value;
							}

							public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
									EqualsStrategy strategy) {
								if (!(object instanceof Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ)) {
									return false;
								}
								if (this == object) {
									return true;
								}
								final Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ that = ((Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ) object);
								{
									String lhsTpServico;
									lhsTpServico = this.getTpServico();
									String rhsTpServico;
									rhsTpServico = that.getTpServico();
									if (!strategy.equals(LocatorUtils.property(thisLocator, "tpServico", lhsTpServico),
											LocatorUtils.property(thatLocator, "tpServico", rhsTpServico), lhsTpServico,
											rhsTpServico)) {
										return false;
									}
								}
								{
									String lhsVlrBaseRet;
									lhsVlrBaseRet = this.getVlrBaseRet();
									String rhsVlrBaseRet;
									rhsVlrBaseRet = that.getVlrBaseRet();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrBaseRet", lhsVlrBaseRet),
											LocatorUtils.property(thatLocator, "vlrBaseRet", rhsVlrBaseRet),
											lhsVlrBaseRet, rhsVlrBaseRet)) {
										return false;
									}
								}
								{
									String lhsVlrRetencao;
									lhsVlrRetencao = this.getVlrRetencao();
									String rhsVlrRetencao;
									rhsVlrRetencao = that.getVlrRetencao();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrRetencao", lhsVlrRetencao),
											LocatorUtils.property(thatLocator, "vlrRetencao", rhsVlrRetencao),
											lhsVlrRetencao, rhsVlrRetencao)) {
										return false;
									}
								}
								{
									String lhsVlrRetSub;
									lhsVlrRetSub = this.getVlrRetSub();
									String rhsVlrRetSub;
									rhsVlrRetSub = that.getVlrRetSub();
									if (!strategy.equals(LocatorUtils.property(thisLocator, "vlrRetSub", lhsVlrRetSub),
											LocatorUtils.property(thatLocator, "vlrRetSub", rhsVlrRetSub), lhsVlrRetSub,
											rhsVlrRetSub)) {
										return false;
									}
								}
								{
									String lhsVlrNRetPrinc;
									lhsVlrNRetPrinc = this.getVlrNRetPrinc();
									String rhsVlrNRetPrinc;
									rhsVlrNRetPrinc = that.getVlrNRetPrinc();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrNRetPrinc", lhsVlrNRetPrinc),
											LocatorUtils.property(thatLocator, "vlrNRetPrinc", rhsVlrNRetPrinc),
											lhsVlrNRetPrinc, rhsVlrNRetPrinc)) {
										return false;
									}
								}
								{
									String lhsVlrServicos15;
									lhsVlrServicos15 = this.getVlrServicos15();
									String rhsVlrServicos15;
									rhsVlrServicos15 = that.getVlrServicos15();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrServicos15", lhsVlrServicos15),
											LocatorUtils.property(thatLocator, "vlrServicos15", rhsVlrServicos15),
											lhsVlrServicos15, rhsVlrServicos15)) {
										return false;
									}
								}
								{
									String lhsVlrServicos20;
									lhsVlrServicos20 = this.getVlrServicos20();
									String rhsVlrServicos20;
									rhsVlrServicos20 = that.getVlrServicos20();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrServicos20", lhsVlrServicos20),
											LocatorUtils.property(thatLocator, "vlrServicos20", rhsVlrServicos20),
											lhsVlrServicos20, rhsVlrServicos20)) {
										return false;
									}
								}
								{
									String lhsVlrServicos25;
									lhsVlrServicos25 = this.getVlrServicos25();
									String rhsVlrServicos25;
									rhsVlrServicos25 = that.getVlrServicos25();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrServicos25", lhsVlrServicos25),
											LocatorUtils.property(thatLocator, "vlrServicos25", rhsVlrServicos25),
											lhsVlrServicos25, rhsVlrServicos25)) {
										return false;
									}
								}
								{
									String lhsVlrAdicional;
									lhsVlrAdicional = this.getVlrAdicional();
									String rhsVlrAdicional;
									rhsVlrAdicional = that.getVlrAdicional();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrAdicional", lhsVlrAdicional),
											LocatorUtils.property(thatLocator, "vlrAdicional", rhsVlrAdicional),
											lhsVlrAdicional, rhsVlrAdicional)) {
										return false;
									}
								}
								{
									String lhsVlrNRetAdic;
									lhsVlrNRetAdic = this.getVlrNRetAdic();
									String rhsVlrNRetAdic;
									rhsVlrNRetAdic = that.getVlrNRetAdic();
									if (!strategy.equals(
											LocatorUtils.property(thisLocator, "vlrNRetAdic", lhsVlrNRetAdic),
											LocatorUtils.property(thatLocator, "vlrNRetAdic", rhsVlrNRetAdic),
											lhsVlrNRetAdic, rhsVlrNRetAdic)) {
										return false;
									}
								}
								return true;
							}

							public boolean equals(Object object) {
								final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
								return equals(null, null, object, strategy);
							}

							public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
								int currentHashCode = 1;
								{
									String theTpServico;
									theTpServico = this.getTpServico();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "tpServico", theTpServico), currentHashCode,
											theTpServico);
								}
								{
									String theVlrBaseRet;
									theVlrBaseRet = this.getVlrBaseRet();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrBaseRet", theVlrBaseRet),
											currentHashCode, theVlrBaseRet);
								}
								{
									String theVlrRetencao;
									theVlrRetencao = this.getVlrRetencao();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrRetencao", theVlrRetencao),
											currentHashCode, theVlrRetencao);
								}
								{
									String theVlrRetSub;
									theVlrRetSub = this.getVlrRetSub();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrRetSub", theVlrRetSub), currentHashCode,
											theVlrRetSub);
								}
								{
									String theVlrNRetPrinc;
									theVlrNRetPrinc = this.getVlrNRetPrinc();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrNRetPrinc", theVlrNRetPrinc),
											currentHashCode, theVlrNRetPrinc);
								}
								{
									String theVlrServicos15;
									theVlrServicos15 = this.getVlrServicos15();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrServicos15", theVlrServicos15),
											currentHashCode, theVlrServicos15);
								}
								{
									String theVlrServicos20;
									theVlrServicos20 = this.getVlrServicos20();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrServicos20", theVlrServicos20),
											currentHashCode, theVlrServicos20);
								}
								{
									String theVlrServicos25;
									theVlrServicos25 = this.getVlrServicos25();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrServicos25", theVlrServicos25),
											currentHashCode, theVlrServicos25);
								}
								{
									String theVlrAdicional;
									theVlrAdicional = this.getVlrAdicional();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrAdicional", theVlrAdicional),
											currentHashCode, theVlrAdicional);
								}
								{
									String theVlrNRetAdic;
									theVlrNRetAdic = this.getVlrNRetAdic();
									currentHashCode = strategy.hashCode(
											LocatorUtils.property(locator, "vlrNRetAdic", theVlrNRetAdic),
											currentHashCode, theVlrNRetAdic);
								}
								return currentHashCode;
							}

							public int hashCode() {
								final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
								return this.hashCode(null, strategy);
							}

						}

					}

				}

			}

		}

	}

}
