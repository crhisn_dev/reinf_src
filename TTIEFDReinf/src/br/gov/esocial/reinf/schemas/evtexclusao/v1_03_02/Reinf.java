//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.10 �s 09:36:18 AM BRT 
//

package br.gov.esocial.reinf.schemas.evtexclusao.v1_03_02;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtExclusao">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="8"/>
 *                                   &lt;maxLength value="14"/>
 *                                   &lt;pattern value="\d{8}|\d{11}|\d{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoExclusao">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpEvento">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="6"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrRecEvt">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="16"/>
 *                                   &lt;maxLength value="52"/>
 *                                   &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4,6}[-][0-9]{1,18}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="perApur">
 *                               &lt;simpleType>
 *                                 &lt;union>
 *                                   &lt;simpleType>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
 *                                       &lt;pattern value="2{1}[0-9]{3}-{1}[0-1]{1}[0-9]{1}"/>
 *                                     &lt;/restriction>
 *                                   &lt;/simpleType>
 *                                   &lt;simpleType>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *                                       &lt;pattern value="2{1}[0-9]{3}-{1}[0-1]{1}[0-9]{1}-{1}[0-3]{1}[0-9]{1}"/>
 *                                     &lt;/restriction>
 *                                   &lt;/simpleType>
 *                                 &lt;/union>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtExclusao" })
@XmlRootElement(name = "Reinf")
// @Entity(name = "Reinf")
@Table(name = "REINF")
@Inheritance(strategy = InheritanceType.JOINED)
public class Reinf implements Equals, HashCode {

	@XmlElement(required = true)
	protected Reinf.EvtExclusao evtExclusao;
	@XmlAttribute(name = "Hjid")
	@Transient
	protected Long hjid;

	/**
	 * Obt�m o valor da propriedade evtExclusao.
	 * 
	 * @return possible object is {@link Reinf.EvtExclusao }
	 * 
	 */
	@ManyToOne(targetEntity = Reinf.EvtExclusao.class, cascade = { CascadeType.ALL })
	@JoinColumn(name = "EVT_EXCLUSAO_REINF_HJID")
	public Reinf.EvtExclusao getEvtExclusao() {
		return evtExclusao;
	}

	/**
	 * Define o valor da propriedade evtExclusao.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtExclusao }
	 * 
	 */
	public void setEvtExclusao(Reinf.EvtExclusao value) {
		this.evtExclusao = value;
	}

	/**
	 * Obt�m o valor da propriedade hjid.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	// @Id
	@Column(name = "HJID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getHjid() {
		return hjid;
	}

	/**
	 * Define o valor da propriedade hjid.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setHjid(Long value) {
		this.hjid = value;
	}

	public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
			EqualsStrategy strategy) {
		if (!(object instanceof Reinf)) {
			return false;
		}
		if (this == object) {
			return true;
		}
		final Reinf that = ((Reinf) object);
		{
			Reinf.EvtExclusao lhsEvtExclusao;
			lhsEvtExclusao = this.getEvtExclusao();
			Reinf.EvtExclusao rhsEvtExclusao;
			rhsEvtExclusao = that.getEvtExclusao();
			if (!strategy.equals(LocatorUtils.property(thisLocator, "evtExclusao", lhsEvtExclusao),
					LocatorUtils.property(thatLocator, "evtExclusao", rhsEvtExclusao), lhsEvtExclusao,
					rhsEvtExclusao)) {
				return false;
			}
		}
		return true;
	}

	public boolean equals(Object object) {
		final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
		return equals(null, null, object, strategy);
	}

	public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
		int currentHashCode = 1;
		{
			Reinf.EvtExclusao theEvtExclusao;
			theEvtExclusao = this.getEvtExclusao();
			currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtExclusao", theEvtExclusao),
					currentHashCode, theEvtExclusao);
		}
		return currentHashCode;
	}

	public int hashCode() {
		final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
		return this.hashCode(null, strategy);
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
	 * desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="8"/>
	 *                         &lt;maxLength value="14"/>
	 *                         &lt;pattern value="\d{8}|\d{11}|\d{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="infoExclusao">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpEvento">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="6"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrRecEvt">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="16"/>
	 *                         &lt;maxLength value="52"/>
	 *                         &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4,6}[-][0-9]{1,18}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="perApur">
	 *                     &lt;simpleType>
	 *                       &lt;union>
	 *                         &lt;simpleType>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
	 *                             &lt;pattern value="2{1}[0-9]{3}-{1}[0-1]{1}[0-9]{1}"/>
	 *                           &lt;/restriction>
	 *                         &lt;/simpleType>
	 *                         &lt;simpleType>
	 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
	 *                             &lt;pattern value="2{1}[0-9]{3}-{1}[0-1]{1}[0-9]{1}-{1}[0-3]{1}[0-9]{1}"/>
	 *                           &lt;/restriction>
	 *                         &lt;/simpleType>
	 *                       &lt;/union>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri", "infoExclusao" })
	@Entity(name = "Reinf$EvtExclusao")
	@Table(name = "EVT_EXCLUSAO")
	@Inheritance(strategy = InheritanceType.JOINED)
	public static class EvtExclusao implements Equals, HashCode {

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtExclusao.IdeEvento ideEvento;

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtExclusao.IdeContri ideContri;

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtExclusao.InfoExclusao infoExclusao;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;
		@XmlAttribute(name = "Hjid")
		@Id
		@Column(name = "ID")		
		protected Long hjid;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtExclusao.IdeEvento }
		 * 
		 */
		@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtexclusao.v1_03_02.Reinf.EvtExclusao.IdeEvento.class, cascade = {
				CascadeType.ALL })
		@JoinColumn(name = "IDE_EVENTO_EVT_EXCLUSAO_HJID")
		public Reinf.EvtExclusao.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtExclusao.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtExclusao.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtExclusao.IdeContri }
		 * 
		 */
		@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtexclusao.v1_03_02.Reinf.EvtExclusao.IdeContri.class, cascade = {
				CascadeType.ALL })
		@JoinColumn(name = "IDE_CONTRI_EVT_EXCLUSAO_HJID")
		public Reinf.EvtExclusao.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtExclusao.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtExclusao.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade infoExclusao.
		 * 
		 * @return possible object is {@link Reinf.EvtExclusao.InfoExclusao }
		 * 
		 */
		@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtexclusao.v1_03_02.Reinf.EvtExclusao.InfoExclusao.class, cascade = {
				CascadeType.ALL })
		@JoinColumn(name = "INFO_EXCLUSAO_EVT_EXCLUSAO_H_0")
		public Reinf.EvtExclusao.InfoExclusao getInfoExclusao() {
			return infoExclusao;
		}

		/**
		 * Define o valor da propriedade infoExclusao.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtExclusao.InfoExclusao }
		 * 
		 */
		public void setInfoExclusao(Reinf.EvtExclusao.InfoExclusao value) {
			this.infoExclusao = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		@Basic
		@Column(name = "ID", length = 36)
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * Obt�m o valor da propriedade hjid.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		// @Id
		@Column(name = "HJID")
		@GeneratedValue(strategy = GenerationType.AUTO)
		public Long getHjid() {
			return hjid;
		}

		/**
		 * Define o valor da propriedade hjid.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setHjid(Long value) {
			this.hjid = value;
		}

		public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
				EqualsStrategy strategy) {
			if (!(object instanceof Reinf.EvtExclusao)) {
				return false;
			}
			if (this == object) {
				return true;
			}
			final Reinf.EvtExclusao that = ((Reinf.EvtExclusao) object);
			{
				Reinf.EvtExclusao.IdeEvento lhsIdeEvento;
				lhsIdeEvento = this.getIdeEvento();
				Reinf.EvtExclusao.IdeEvento rhsIdeEvento;
				rhsIdeEvento = that.getIdeEvento();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEvento", lhsIdeEvento),
						LocatorUtils.property(thatLocator, "ideEvento", rhsIdeEvento), lhsIdeEvento, rhsIdeEvento)) {
					return false;
				}
			}
			{
				Reinf.EvtExclusao.IdeContri lhsIdeContri;
				lhsIdeContri = this.getIdeContri();
				Reinf.EvtExclusao.IdeContri rhsIdeContri;
				rhsIdeContri = that.getIdeContri();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideContri", lhsIdeContri),
						LocatorUtils.property(thatLocator, "ideContri", rhsIdeContri), lhsIdeContri, rhsIdeContri)) {
					return false;
				}
			}
			{
				Reinf.EvtExclusao.InfoExclusao lhsInfoExclusao;
				lhsInfoExclusao = this.getInfoExclusao();
				Reinf.EvtExclusao.InfoExclusao rhsInfoExclusao;
				rhsInfoExclusao = that.getInfoExclusao();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "infoExclusao", lhsInfoExclusao),
						LocatorUtils.property(thatLocator, "infoExclusao", rhsInfoExclusao), lhsInfoExclusao,
						rhsInfoExclusao)) {
					return false;
				}
			}
			{
				String lhsId;
				lhsId = this.getId();
				String rhsId;
				rhsId = that.getId();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId),
						LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
					return false;
				}
			}
			return true;
		}

		public boolean equals(Object object) {
			final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
			return equals(null, null, object, strategy);
		}

		public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
			int currentHashCode = 1;
			{
				Reinf.EvtExclusao.IdeEvento theIdeEvento;
				theIdeEvento = this.getIdeEvento();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEvento", theIdeEvento),
						currentHashCode, theIdeEvento);
			}
			{
				Reinf.EvtExclusao.IdeContri theIdeContri;
				theIdeContri = this.getIdeContri();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideContri", theIdeContri),
						currentHashCode, theIdeContri);
			}
			{
				Reinf.EvtExclusao.InfoExclusao theInfoExclusao;
				theInfoExclusao = this.getInfoExclusao();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoExclusao", theInfoExclusao),
						currentHashCode, theInfoExclusao);
			}
			{
				String theId;
				theId = this.getId();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode,
						theId);
			}
			return currentHashCode;
		}

		public int hashCode() {
			final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
			return this.hashCode(null, strategy);
		}

		/**
		 * Informa��es de identifica��o do contribuinte
		 * 
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="8"/>
		 *               &lt;maxLength value="14"/>
		 *               &lt;pattern value="\d{8}|\d{11}|\d{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		// @Entity(name = "Reinf$EvtExclusao$IdeContri")
		@Table(name = "IDE_CONTRI__4")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeContri implements Equals, HashCode {

			protected short tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			@Basic
			@Column(name = "TP_INSC", scale = 0)
			public short getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(short value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_INSC", length = 14)
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtExclusao.IdeContri)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtExclusao.IdeContri that = ((Reinf.EvtExclusao.IdeContri) object);
				{
					short lhsTpInsc;
					lhsTpInsc = this.getTpInsc();
					short rhsTpInsc;
					rhsTpInsc = that.getTpInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInsc", lhsTpInsc),
							LocatorUtils.property(thatLocator, "tpInsc", rhsTpInsc), lhsTpInsc, rhsTpInsc)) {
						return false;
					}
				}
				{
					String lhsNrInsc;
					lhsNrInsc = this.getNrInsc();
					String rhsNrInsc;
					rhsNrInsc = that.getNrInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInsc", lhsNrInsc),
							LocatorUtils.property(thatLocator, "nrInsc", rhsNrInsc), lhsNrInsc, rhsNrInsc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					short theTpInsc;
					theTpInsc = this.getTpInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInsc", theTpInsc),
							currentHashCode, theTpInsc);
				}
				{
					String theNrInsc;
					theNrInsc = this.getNrInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInsc", theNrInsc),
							currentHashCode, theNrInsc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpAmb", "procEmi", "verProc" })
		// @Entity(name = "Reinf$EvtExclusao$IdeEvento")
		@Table(name = "IDE_EVENTO__4")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class IdeEvento implements Equals, HashCode {

			protected long tpAmb;
			protected short procEmi;
			@XmlElement(required = true)
			protected String verProc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			@Basic
			@Column(name = "TP_AMB", scale = 0)
			public long getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(long value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			@Basic
			@Column(name = "PROC_EMI", scale = 0)
			public short getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(short value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "VER_PROC", length = 20)
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtExclusao.IdeEvento)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtExclusao.IdeEvento that = ((Reinf.EvtExclusao.IdeEvento) object);
				{
					long lhsTpAmb;
					lhsTpAmb = this.getTpAmb();
					long rhsTpAmb;
					rhsTpAmb = that.getTpAmb();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpAmb", lhsTpAmb),
							LocatorUtils.property(thatLocator, "tpAmb", rhsTpAmb), lhsTpAmb, rhsTpAmb)) {
						return false;
					}
				}
				{
					short lhsProcEmi;
					lhsProcEmi = this.getProcEmi();
					short rhsProcEmi;
					rhsProcEmi = that.getProcEmi();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "procEmi", lhsProcEmi),
							LocatorUtils.property(thatLocator, "procEmi", rhsProcEmi), lhsProcEmi, rhsProcEmi)) {
						return false;
					}
				}
				{
					String lhsVerProc;
					lhsVerProc = this.getVerProc();
					String rhsVerProc;
					rhsVerProc = that.getVerProc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "verProc", lhsVerProc),
							LocatorUtils.property(thatLocator, "verProc", rhsVerProc), lhsVerProc, rhsVerProc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					long theTpAmb;
					theTpAmb = this.getTpAmb();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpAmb", theTpAmb),
							currentHashCode, theTpAmb);
				}
				{
					short theProcEmi;
					theProcEmi = this.getProcEmi();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "procEmi", theProcEmi),
							currentHashCode, theProcEmi);
				}
				{
					String theVerProc;
					theVerProc = this.getVerProc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verProc", theVerProc),
							currentHashCode, theVerProc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * Registro que identifica o evento objeto da exclus�o
		 * 
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpEvento">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="6"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrRecEvt">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="16"/>
		 *               &lt;maxLength value="52"/>
		 *               &lt;pattern value="[0-9]{1,18}[-][0-9]{2}[-][0-9]{4}[-][0-9]{4,6}[-][0-9]{1,18}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="perApur">
		 *           &lt;simpleType>
		 *             &lt;union>
		 *               &lt;simpleType>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gYearMonth">
		 *                   &lt;pattern value="2{1}[0-9]{3}-{1}[0-1]{1}[0-9]{1}"/>
		 *                 &lt;/restriction>
		 *               &lt;/simpleType>
		 *               &lt;simpleType>
		 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
		 *                   &lt;pattern value="2{1}[0-9]{3}-{1}[0-1]{1}[0-9]{1}-{1}[0-3]{1}[0-9]{1}"/>
		 *                 &lt;/restriction>
		 *               &lt;/simpleType>
		 *             &lt;/union>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpEvento", "nrRecEvt", "perApur" })
		// @Entity(name = "Reinf$EvtExclusao$InfoExclusao")
		@Table(name = "INFO_EXCLUSAO")
		@Inheritance(strategy = InheritanceType.JOINED)
		@Embeddable
		public static class InfoExclusao implements Equals, HashCode {

			@XmlElement(required = true)
			protected String tpEvento;
			@XmlElement(required = true)
			protected String nrRecEvt;
			@XmlElement(required = true)
			protected String perApur;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade tpEvento.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "TP_EVENTO", length = 6)
			public String getTpEvento() {
				return tpEvento;
			}

			/**
			 * Define o valor da propriedade tpEvento.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setTpEvento(String value) {
				this.tpEvento = value;
			}

			/**
			 * Obt�m o valor da propriedade nrRecEvt.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_REC_EVT", length = 52)
			public String getNrRecEvt() {
				return nrRecEvt;
			}

			/**
			 * Define o valor da propriedade nrRecEvt.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrRecEvt(String value) {
				this.nrRecEvt = value;
			}

			/**
			 * Obt�m o valor da propriedade perApur.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "PER_APUR")
			public String getPerApur() {
				return perApur;
			}

			/**
			 * Define o valor da propriedade perApur.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPerApur(String value) {
				this.perApur = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtExclusao.InfoExclusao)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtExclusao.InfoExclusao that = ((Reinf.EvtExclusao.InfoExclusao) object);
				{
					String lhsTpEvento;
					lhsTpEvento = this.getTpEvento();
					String rhsTpEvento;
					rhsTpEvento = that.getTpEvento();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpEvento", lhsTpEvento),
							LocatorUtils.property(thatLocator, "tpEvento", rhsTpEvento), lhsTpEvento, rhsTpEvento)) {
						return false;
					}
				}
				{
					String lhsNrRecEvt;
					lhsNrRecEvt = this.getNrRecEvt();
					String rhsNrRecEvt;
					rhsNrRecEvt = that.getNrRecEvt();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrRecEvt", lhsNrRecEvt),
							LocatorUtils.property(thatLocator, "nrRecEvt", rhsNrRecEvt), lhsNrRecEvt, rhsNrRecEvt)) {
						return false;
					}
				}
				{
					String lhsPerApur;
					lhsPerApur = this.getPerApur();
					String rhsPerApur;
					rhsPerApur = that.getPerApur();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "perApur", lhsPerApur),
							LocatorUtils.property(thatLocator, "perApur", rhsPerApur), lhsPerApur, rhsPerApur)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					String theTpEvento;
					theTpEvento = this.getTpEvento();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpEvento", theTpEvento),
							currentHashCode, theTpEvento);
				}
				{
					String theNrRecEvt;
					theNrRecEvt = this.getNrRecEvt();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrRecEvt", theNrRecEvt),
							currentHashCode, theNrRecEvt);
				}
				{
					String thePerApur;
					thePerApur = this.getPerApur();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "perApur", thePerApur),
							currentHashCode, thePerApur);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

	}

}
