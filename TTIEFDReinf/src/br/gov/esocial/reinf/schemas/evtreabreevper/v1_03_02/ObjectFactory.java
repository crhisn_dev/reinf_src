//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.10 �s 09:36:18 AM BRT 
//


package br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link Reinf.EvtReabreEvPer }
     * 
     */
    public Reinf.EvtReabreEvPer createReinfEvtReabreEvPer() {
        return new Reinf.EvtReabreEvPer();
    }

    /**
     * Create an instance of {@link Reinf.EvtReabreEvPer.IdeEvento }
     * 
     */
    public Reinf.EvtReabreEvPer.IdeEvento createReinfEvtReabreEvPerIdeEvento() {
        return new Reinf.EvtReabreEvPer.IdeEvento();
    }

    /**
     * Create an instance of {@link Reinf.EvtReabreEvPer.IdeContri }
     * 
     */
    public Reinf.EvtReabreEvPer.IdeContri createReinfEvtReabreEvPerIdeContri() {
        return new Reinf.EvtReabreEvPer.IdeContri();
    }

}
