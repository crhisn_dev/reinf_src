//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.10 �s 09:36:18 AM BRT 
//

package br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

/**
 * <p>
 * Classe Java de anonymous complex type.
 * 
 * <p>
 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
 * desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtReabreEvPer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="perApur">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="7"/>
 *                                   &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]{1}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="8"/>
 *                                   &lt;maxLength value="14"/>
 *                                   &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "evtReabreEvPer" })
@XmlRootElement(name = "Reinf")
// @Entity(name = "Reinf")
@Table(name = "REINF__3")
@Inheritance(strategy = InheritanceType.JOINED)
public class Reinf implements Equals, HashCode {

	@XmlElement(required = true)
	protected Reinf.EvtReabreEvPer evtReabreEvPer;
	@XmlAttribute(name = "Hjid")
	@Transient
	protected Long hjid;

	/**
	 * Obt�m o valor da propriedade evtReabreEvPer.
	 * 
	 * @return possible object is {@link Reinf.EvtReabreEvPer }
	 * 
	 */
	@ManyToOne(targetEntity = Reinf.EvtReabreEvPer.class, cascade = { CascadeType.ALL })
	@JoinColumn(name = "EVT_REABRE_EV_PER_REINF___3__0")
	public Reinf.EvtReabreEvPer getEvtReabreEvPer() {
		return evtReabreEvPer;
	}

	/**
	 * Define o valor da propriedade evtReabreEvPer.
	 * 
	 * @param value
	 *            allowed object is {@link Reinf.EvtReabreEvPer }
	 * 
	 */
	public void setEvtReabreEvPer(Reinf.EvtReabreEvPer value) {
		this.evtReabreEvPer = value;
	}

	/**
	 * Obt�m o valor da propriedade hjid.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	// @Id
	@Column(name = "HJID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getHjid() {
		return hjid;
	}

	/**
	 * Define o valor da propriedade hjid.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setHjid(Long value) {
		this.hjid = value;
	}

	public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
			EqualsStrategy strategy) {
		if (!(object instanceof Reinf)) {
			return false;
		}
		if (this == object) {
			return true;
		}
		final Reinf that = ((Reinf) object);
		{
			Reinf.EvtReabreEvPer lhsEvtReabreEvPer;
			lhsEvtReabreEvPer = this.getEvtReabreEvPer();
			Reinf.EvtReabreEvPer rhsEvtReabreEvPer;
			rhsEvtReabreEvPer = that.getEvtReabreEvPer();
			if (!strategy.equals(LocatorUtils.property(thisLocator, "evtReabreEvPer", lhsEvtReabreEvPer),
					LocatorUtils.property(thatLocator, "evtReabreEvPer", rhsEvtReabreEvPer), lhsEvtReabreEvPer,
					rhsEvtReabreEvPer)) {
				return false;
			}
		}
		return true;
	}

	public boolean equals(Object object) {
		final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
		return equals(null, null, object, strategy);
	}

	public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
		int currentHashCode = 1;
		{
			Reinf.EvtReabreEvPer theEvtReabreEvPer;
			theEvtReabreEvPer = this.getEvtReabreEvPer();
			currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtReabreEvPer", theEvtReabreEvPer),
					currentHashCode, theEvtReabreEvPer);
		}
		return currentHashCode;
	}

	public int hashCode() {
		final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
		return this.hashCode(null, strategy);
	}

	/**
	 * <p>
	 * Classe Java de anonymous complex type.
	 * 
	 * <p>
	 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
	 * desta classe.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="ideEvento">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="perApur">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;length value="7"/>
	 *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="tpAmb">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="procEmi">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
	 *                         &lt;pattern value="[1|2]{1}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="verProc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="1"/>
	 *                         &lt;maxLength value="20"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ideContri">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="tpInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
	 *                         &lt;pattern value="1|2"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                   &lt;element name="nrInsc">
	 *                     &lt;simpleType>
	 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                         &lt;minLength value="8"/>
	 *                         &lt;maxLength value="14"/>
	 *                         &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
	 *                       &lt;/restriction>
	 *                     &lt;/simpleType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="id" use="required">
	 *         &lt;simpleType>
	 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
	 *             &lt;length value="36"/>
	 *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
	 *           &lt;/restriction>
	 *         &lt;/simpleType>
	 *       &lt;/attribute>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "ideEvento", "ideContri" })
	@Entity(name = "Reinf$EvtReabreEvPer")
	@Table(name = "EVT_REABRE_EV_PER")
	@Inheritance(strategy = InheritanceType.JOINED)
	public static class EvtReabreEvPer implements Equals, HashCode {

		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtReabreEvPer.IdeEvento ideEvento;
		
		@XmlElement(required = true)
		@Embedded
		protected Reinf.EvtReabreEvPer.IdeContri ideContri;
		@XmlAttribute(name = "id", required = true)
		@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
		@XmlID
		protected String id;
		@XmlAttribute(name = "Hjid")
		@Id
		@Column(name="ID")
		protected Long hjid;

		/**
		 * Obt�m o valor da propriedade ideEvento.
		 * 
		 * @return possible object is {@link Reinf.EvtReabreEvPer.IdeEvento }
		 * 
		 */
		//@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02.Reinf.EvtReabreEvPer.IdeEvento.class, cascade = {
		//		CascadeType.ALL })
		//@JoinColumn(name = "IDE_EVENTO_EVT_REABRE_EV_PER_0")
		public Reinf.EvtReabreEvPer.IdeEvento getIdeEvento() {
			return ideEvento;
		}

		/**
		 * Define o valor da propriedade ideEvento.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtReabreEvPer.IdeEvento }
		 * 
		 */
		public void setIdeEvento(Reinf.EvtReabreEvPer.IdeEvento value) {
			this.ideEvento = value;
		}

		/**
		 * Obt�m o valor da propriedade ideContri.
		 * 
		 * @return possible object is {@link Reinf.EvtReabreEvPer.IdeContri }
		 * 
		 */
		//@ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtreabreevper.v1_03_02.Reinf.EvtReabreEvPer.IdeContri.class, cascade = {
		//		CascadeType.ALL })
		//@JoinColumn(name = "IDE_CONTRI_EVT_REABRE_EV_PER_0")
		public Reinf.EvtReabreEvPer.IdeContri getIdeContri() {
			return ideContri;
		}

		/**
		 * Define o valor da propriedade ideContri.
		 * 
		 * @param value
		 *            allowed object is {@link Reinf.EvtReabreEvPer.IdeContri }
		 * 
		 */
		public void setIdeContri(Reinf.EvtReabreEvPer.IdeContri value) {
			this.ideContri = value;
		}

		/**
		 * Obt�m o valor da propriedade id.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		@Basic
		@Column(name = "ID", length = 36)
		public String getId() {
			return id;
		}

		/**
		 * Define o valor da propriedade id.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value) {
			this.id = value;
		}

		/**
		 * Obt�m o valor da propriedade hjid.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		// @Id
		@Column(name = "HJID")
		@GeneratedValue(strategy = GenerationType.AUTO)
		public Long getHjid() {
			return hjid;
		}

		/**
		 * Define o valor da propriedade hjid.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setHjid(Long value) {
			this.hjid = value;
		}

		public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
				EqualsStrategy strategy) {
			if (!(object instanceof Reinf.EvtReabreEvPer)) {
				return false;
			}
			if (this == object) {
				return true;
			}
			final Reinf.EvtReabreEvPer that = ((Reinf.EvtReabreEvPer) object);
			{
				Reinf.EvtReabreEvPer.IdeEvento lhsIdeEvento;
				lhsIdeEvento = this.getIdeEvento();
				Reinf.EvtReabreEvPer.IdeEvento rhsIdeEvento;
				rhsIdeEvento = that.getIdeEvento();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEvento", lhsIdeEvento),
						LocatorUtils.property(thatLocator, "ideEvento", rhsIdeEvento), lhsIdeEvento, rhsIdeEvento)) {
					return false;
				}
			}
			{
				Reinf.EvtReabreEvPer.IdeContri lhsIdeContri;
				lhsIdeContri = this.getIdeContri();
				Reinf.EvtReabreEvPer.IdeContri rhsIdeContri;
				rhsIdeContri = that.getIdeContri();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "ideContri", lhsIdeContri),
						LocatorUtils.property(thatLocator, "ideContri", rhsIdeContri), lhsIdeContri, rhsIdeContri)) {
					return false;
				}
			}
			{
				String lhsId;
				lhsId = this.getId();
				String rhsId;
				rhsId = that.getId();
				if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId),
						LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
					return false;
				}
			}
			return true;
		}

		public boolean equals(Object object) {
			final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
			return equals(null, null, object, strategy);
		}

		public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
			int currentHashCode = 1;
			{
				Reinf.EvtReabreEvPer.IdeEvento theIdeEvento;
				theIdeEvento = this.getIdeEvento();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEvento", theIdeEvento),
						currentHashCode, theIdeEvento);
			}
			{
				Reinf.EvtReabreEvPer.IdeContri theIdeContri;
				theIdeContri = this.getIdeContri();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideContri", theIdeContri),
						currentHashCode, theIdeContri);
			}
			{
				String theId;
				theId = this.getId();
				currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode,
						theId);
			}
			return currentHashCode;
		}

		public int hashCode() {
			final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
			return this.hashCode(null, strategy);
		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="tpInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
		 *               &lt;pattern value="1|2"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="nrInsc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="8"/>
		 *               &lt;maxLength value="14"/>
		 *               &lt;pattern value="[0-9]{11}|[0-9]{8}|[0-9]{14}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "tpInsc", "nrInsc" })
		// @Entity(name = "Reinf$EvtReabreEvPer$IdeContri")
		@Table(name = "IDE_CONTRI__0")
		@Inheritance(strategy = InheritanceType.JOINED)
		public static class IdeContri implements Equals, HashCode {

			protected short tpInsc;
			@XmlElement(required = true)
			protected String nrInsc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade tpInsc.
			 * 
			 */
			@Basic
			@Column(name = "TP_INSC", scale = 0)
			public short getTpInsc() {
				return tpInsc;
			}

			/**
			 * Define o valor da propriedade tpInsc.
			 * 
			 */
			public void setTpInsc(short value) {
				this.tpInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade nrInsc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "NR_INSC", length = 14)
			public String getNrInsc() {
				return nrInsc;
			}

			/**
			 * Define o valor da propriedade nrInsc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNrInsc(String value) {
				this.nrInsc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtReabreEvPer.IdeContri)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtReabreEvPer.IdeContri that = ((Reinf.EvtReabreEvPer.IdeContri) object);
				{
					short lhsTpInsc;
					lhsTpInsc = this.getTpInsc();
					short rhsTpInsc;
					rhsTpInsc = that.getTpInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInsc", lhsTpInsc),
							LocatorUtils.property(thatLocator, "tpInsc", rhsTpInsc), lhsTpInsc, rhsTpInsc)) {
						return false;
					}
				}
				{
					String lhsNrInsc;
					lhsNrInsc = this.getNrInsc();
					String rhsNrInsc;
					rhsNrInsc = that.getNrInsc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInsc", lhsNrInsc),
							LocatorUtils.property(thatLocator, "nrInsc", rhsNrInsc), lhsNrInsc, rhsNrInsc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					short theTpInsc;
					theTpInsc = this.getTpInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInsc", theTpInsc),
							currentHashCode, theTpInsc);
				}
				{
					String theNrInsc;
					theNrInsc = this.getNrInsc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInsc", theNrInsc),
							currentHashCode, theNrInsc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

		/**
		 * <p>
		 * Classe Java de anonymous complex type.
		 * 
		 * <p>
		 * O seguinte fragmento do esquema especifica o conte�do esperado contido dentro
		 * desta classe.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="perApur">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;length value="7"/>
		 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="tpAmb">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="procEmi">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
		 *               &lt;pattern value="[1|2]{1}"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *         &lt;element name="verProc">
		 *           &lt;simpleType>
		 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *               &lt;minLength value="1"/>
		 *               &lt;maxLength value="20"/>
		 *             &lt;/restriction>
		 *           &lt;/simpleType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "perApur", "tpAmb", "procEmi", "verProc" })
		// @Entity(name = "Reinf$EvtReabreEvPer$IdeEvento")
		@Table(name = "IDE_EVENTO__0")
		@Inheritance(strategy = InheritanceType.JOINED)
		public static class IdeEvento implements Equals, HashCode {

			@XmlElement(required = true)
			protected String perApur;
			protected long tpAmb;
			protected long procEmi;
			@XmlElement(required = true)
			protected String verProc;
			@XmlAttribute(name = "Hjid")
			@Transient
			protected Long hjid;

			/**
			 * Obt�m o valor da propriedade perApur.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "PER_APUR", length = 7)
			public String getPerApur() {
				return perApur;
			}

			/**
			 * Define o valor da propriedade perApur.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPerApur(String value) {
				this.perApur = value;
			}

			/**
			 * Obt�m o valor da propriedade tpAmb.
			 * 
			 */
			@Basic
			@Column(name = "TP_AMB", scale = 0)
			public long getTpAmb() {
				return tpAmb;
			}

			/**
			 * Define o valor da propriedade tpAmb.
			 * 
			 */
			public void setTpAmb(long value) {
				this.tpAmb = value;
			}

			/**
			 * Obt�m o valor da propriedade procEmi.
			 * 
			 */
			@Basic
			@Column(name = "PROC_EMI", scale = 0)
			public long getProcEmi() {
				return procEmi;
			}

			/**
			 * Define o valor da propriedade procEmi.
			 * 
			 */
			public void setProcEmi(long value) {
				this.procEmi = value;
			}

			/**
			 * Obt�m o valor da propriedade verProc.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			@Basic
			@Column(name = "VER_PROC", length = 20)
			public String getVerProc() {
				return verProc;
			}

			/**
			 * Define o valor da propriedade verProc.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setVerProc(String value) {
				this.verProc = value;
			}

			/**
			 * Obt�m o valor da propriedade hjid.
			 * 
			 * @return possible object is {@link Long }
			 * 
			 */
			// @Id
			@Column(name = "HJID")
			@GeneratedValue(strategy = GenerationType.AUTO)
			public Long getHjid() {
				return hjid;
			}

			/**
			 * Define o valor da propriedade hjid.
			 * 
			 * @param value
			 *            allowed object is {@link Long }
			 * 
			 */
			public void setHjid(Long value) {
				this.hjid = value;
			}

			public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object,
					EqualsStrategy strategy) {
				if (!(object instanceof Reinf.EvtReabreEvPer.IdeEvento)) {
					return false;
				}
				if (this == object) {
					return true;
				}
				final Reinf.EvtReabreEvPer.IdeEvento that = ((Reinf.EvtReabreEvPer.IdeEvento) object);
				{
					String lhsPerApur;
					lhsPerApur = this.getPerApur();
					String rhsPerApur;
					rhsPerApur = that.getPerApur();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "perApur", lhsPerApur),
							LocatorUtils.property(thatLocator, "perApur", rhsPerApur), lhsPerApur, rhsPerApur)) {
						return false;
					}
				}
				{
					long lhsTpAmb;
					lhsTpAmb = this.getTpAmb();
					long rhsTpAmb;
					rhsTpAmb = that.getTpAmb();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "tpAmb", lhsTpAmb),
							LocatorUtils.property(thatLocator, "tpAmb", rhsTpAmb), lhsTpAmb, rhsTpAmb)) {
						return false;
					}
				}
				{
					long lhsProcEmi;
					lhsProcEmi = this.getProcEmi();
					long rhsProcEmi;
					rhsProcEmi = that.getProcEmi();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "procEmi", lhsProcEmi),
							LocatorUtils.property(thatLocator, "procEmi", rhsProcEmi), lhsProcEmi, rhsProcEmi)) {
						return false;
					}
				}
				{
					String lhsVerProc;
					lhsVerProc = this.getVerProc();
					String rhsVerProc;
					rhsVerProc = that.getVerProc();
					if (!strategy.equals(LocatorUtils.property(thisLocator, "verProc", lhsVerProc),
							LocatorUtils.property(thatLocator, "verProc", rhsVerProc), lhsVerProc, rhsVerProc)) {
						return false;
					}
				}
				return true;
			}

			public boolean equals(Object object) {
				final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
				return equals(null, null, object, strategy);
			}

			public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
				int currentHashCode = 1;
				{
					String thePerApur;
					thePerApur = this.getPerApur();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "perApur", thePerApur),
							currentHashCode, thePerApur);
				}
				{
					long theTpAmb;
					theTpAmb = this.getTpAmb();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpAmb", theTpAmb),
							currentHashCode, theTpAmb);
				}
				{
					long theProcEmi;
					theProcEmi = this.getProcEmi();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "procEmi", theProcEmi),
							currentHashCode, theProcEmi);
				}
				{
					String theVerProc;
					theVerProc = this.getVerProc();
					currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verProc", theVerProc),
							currentHashCode, theVerProc);
				}
				return currentHashCode;
			}

			public int hashCode() {
				final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
				return this.hashCode(null, strategy);
			}

		}

	}

}
