
package br.gov.fazenda.sped;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.gov.fazenda.sped package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.gov.fazenda.sped
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReceberLoteEventos }
     * 
     */
    public ReceberLoteEventos createReceberLoteEventos() {
        return new ReceberLoteEventos();
    }

    /**
     * Create an instance of {@link ReceberLoteEventosResponse }
     * 
     */
    public ReceberLoteEventosResponse createReceberLoteEventosResponse() {
        return new ReceberLoteEventosResponse();
    }

    /**
     * Create an instance of {@link ReceberLoteEventos.LoteEventos }
     * 
     */
    public ReceberLoteEventos.LoteEventos createReceberLoteEventosLoteEventos() {
        return new ReceberLoteEventos.LoteEventos();
    }

    /**
     * Create an instance of {@link ReceberLoteEventosResponse.ReceberLoteEventosResult }
     * 
     */
    public ReceberLoteEventosResponse.ReceberLoteEventosResult createReceberLoteEventosResponseReceberLoteEventosResult() {
        return new ReceberLoteEventosResponse.ReceberLoteEventosResult();
    }

}
