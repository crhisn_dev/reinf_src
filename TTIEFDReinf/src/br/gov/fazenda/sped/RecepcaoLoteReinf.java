
package br.gov.fazenda.sped;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "RecepcaoLoteReinf", targetNamespace = "http://sped.fazenda.gov.br/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface RecepcaoLoteReinf {


    /**
     * 
     * @param loteEventos
     * @return
     *     returns br.gov.fazenda.sped.ReceberLoteEventosResponse.ReceberLoteEventosResult
     */
    @WebMethod(operationName = "ReceberLoteEventos", action = "http://sped.fazenda.gov.br/RecepcaoLoteReinf/ReceberLoteEventos")
    @WebResult(name = "ReceberLoteEventosResult", targetNamespace = "http://sped.fazenda.gov.br/")
    @RequestWrapper(localName = "ReceberLoteEventos", targetNamespace = "http://sped.fazenda.gov.br/", className = "br.gov.fazenda.sped.ReceberLoteEventos")
    @ResponseWrapper(localName = "ReceberLoteEventosResponse", targetNamespace = "http://sped.fazenda.gov.br/", className = "br.gov.fazenda.sped.ReceberLoteEventosResponse")
    public br.gov.fazenda.sped.ReceberLoteEventosResponse.ReceberLoteEventosResult receberLoteEventos(
        @WebParam(name = "loteEventos", targetNamespace = "http://sped.fazenda.gov.br/")
        br.gov.fazenda.sped.ReceberLoteEventos.LoteEventos loteEventos);

}
