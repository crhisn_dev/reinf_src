//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.09.24 �s 09:45:57 AM BRT 
//


package br.com.tti.sefaz.reinf.eventos.evtservprest;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.tti.sefaz.reinf.eventos.evtservprest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.tti.sefaz.reinf.eventos.evtservprest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest }
     * 
     */
    public Reinf.EvtServPrest createReinfEvtServPrest() {
        return new Reinf.EvtServPrest();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.InfoServPrest }
     * 
     */
    public Reinf.EvtServPrest.InfoServPrest createReinfEvtServPrestInfoServPrest() {
        return new Reinf.EvtServPrest.InfoServPrest();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest }
     * 
     */
    public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest createReinfEvtServPrestInfoServPrestIdeEstabPrest() {
        return new Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador }
     * 
     */
    public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador createReinfEvtServPrestInfoServPrestIdeEstabPrestIdeTomador() {
        return new Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs }
     * 
     */
    public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs createReinfEvtServPrestInfoServPrestIdeEstabPrestIdeTomadorNfs() {
        return new Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.IdeEvento }
     * 
     */
    public Reinf.EvtServPrest.IdeEvento createReinfEvtServPrestIdeEvento() {
        return new Reinf.EvtServPrest.IdeEvento();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.IdeContri }
     * 
     */
    public Reinf.EvtServPrest.IdeContri createReinfEvtServPrestIdeContri() {
        return new Reinf.EvtServPrest.IdeContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr }
     * 
     */
    public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr createReinfEvtServPrestInfoServPrestIdeEstabPrestIdeTomadorInfoProcRetPr() {
        return new Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetPr();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd }
     * 
     */
    public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd createReinfEvtServPrestInfoServPrestIdeEstabPrestIdeTomadorInfoProcRetAd() {
        return new Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.InfoProcRetAd();
    }

    /**
     * Create an instance of {@link Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ }
     * 
     */
    public Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ createReinfEvtServPrestInfoServPrestIdeEstabPrestIdeTomadorNfsInfoTpServ() {
        return new Reinf.EvtServPrest.InfoServPrest.IdeEstabPrest.IdeTomador.Nfs.InfoTpServ();
    }

}
