//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.09.24 �s 01:35:49 PM BRT 
//


package br.com.tti.sefaz.reinf.eventos.retorno.total;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Define uma ocorrencia encontrada no processamento de um arquivo.
 * 
 * <p>Classe Java de TRegistroOcorrencias complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TRegistroOcorrencias">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tpOcorr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *               &lt;pattern value="[1|2]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="localErroAviso">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="200"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codResp">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dscResp">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRegistroOcorrencias", propOrder = {
    "tpOcorr",
    "localErroAviso",
    "codResp",
    "dscResp"
})
public class TRegistroOcorrencias {

    protected short tpOcorr;
    @XmlElement(required = true)
    protected String localErroAviso;
    @XmlElement(required = true)
    protected String codResp;
    @XmlElement(required = true)
    protected String dscResp;

    /**
     * Obt�m o valor da propriedade tpOcorr.
     * 
     */
    public short getTpOcorr() {
        return tpOcorr;
    }

    /**
     * Define o valor da propriedade tpOcorr.
     * 
     */
    public void setTpOcorr(short value) {
        this.tpOcorr = value;
    }

    /**
     * Obt�m o valor da propriedade localErroAviso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalErroAviso() {
        return localErroAviso;
    }

    /**
     * Define o valor da propriedade localErroAviso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalErroAviso(String value) {
        this.localErroAviso = value;
    }

    /**
     * Obt�m o valor da propriedade codResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResp() {
        return codResp;
    }

    /**
     * Define o valor da propriedade codResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResp(String value) {
        this.codResp = value;
    }

    /**
     * Obt�m o valor da propriedade dscResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDscResp() {
        return dscResp;
    }

    /**
     * Define o valor da propriedade dscResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDscResp(String value) {
        this.dscResp = value;
    }

}
