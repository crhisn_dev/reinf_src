//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.10.03 �s 10:03:40 AM BRT 
//


package br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evtInfoContri">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ideEvento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpAmb">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="[1|2]"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="procEmi">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="verProc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="20"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ideContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tpInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                                   &lt;pattern value="1|2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="nrInsc">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="[0-9]{8}|[0-9]{11}|[0-9]{14}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infoContri">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;choice>
 *                               &lt;element name="inclusao">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="idePeriodo">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="infoCadastro">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="classTrib">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="2"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indEscrituracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indDesoneracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indAcordoIsenMulta">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indSitPJ" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="[0-4]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="contato">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="nmCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cpfCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="11"/>
 *                                                                   &lt;pattern value="[0-9]{3,11}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneFixo" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneCel" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="cnpjSoftHouse">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{3,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmRazao">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="115"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmCont">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="telefone" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="infoEFR" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="ideEFR">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="S|N"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cnpjEFR" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="8"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="alteracao">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="idePeriodo">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="infoCadastro">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="classTrib">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="2"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indEscrituracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indDesoneracao">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indAcordoIsenMulta">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="0|1"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="indSitPJ" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
 *                                                         &lt;pattern value="[0-4]"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="contato">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="nmCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cpfCtt">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="11"/>
 *                                                                   &lt;pattern value="[0-9]{3,11}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneFixo" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="foneCel" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="cnpjSoftHouse">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="3"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{3,14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmRazao">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="115"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="nmCont">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="70"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="telefone" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="13"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="email" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="1"/>
 *                                                                   &lt;maxLength value="60"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="infoEFR" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="ideEFR">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;pattern value="S|N"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                             &lt;element name="cnpjEFR" minOccurs="0">
 *                                                               &lt;simpleType>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                   &lt;minLength value="8"/>
 *                                                                   &lt;maxLength value="14"/>
 *                                                                   &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/simpleType>
 *                                                             &lt;/element>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="novaValidade" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="exclusao">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="idePeriodo">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="iniValid">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="fimValid" minOccurs="0">
 *                                                     &lt;simpleType>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                         &lt;length value="7"/>
 *                                                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/simpleType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/choice>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
 *                       &lt;length value="36"/>
 *                       &lt;pattern value="I{1}D{1}[0-9]{34}"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "evtInfoContri"
})
@XmlRootElement(name = "Reinf")
@Entity(name = "Reinf")
@Table(name = "REINF")
@Inheritance(strategy = InheritanceType.JOINED)
public class Reinf
    implements Equals, HashCode
{

    @XmlElement(required = true)
    protected Reinf.EvtInfoContri evtInfoContri;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Obt�m o valor da propriedade evtInfoContri.
     * 
     * @return
     *     possible object is
     *     {@link Reinf.EvtInfoContri }
     *     
     */
    @ManyToOne(targetEntity = Reinf.EvtInfoContri.class, cascade = {
        CascadeType.ALL
    })
    @JoinColumn(name = "EVT_INFO_CONTRI_REINF_HJID")
    public Reinf.EvtInfoContri getEvtInfoContri() {
        return evtInfoContri;
    }

    /**
     * Define o valor da propriedade evtInfoContri.
     * 
     * @param value
     *     allowed object is
     *     {@link Reinf.EvtInfoContri }
     *     
     */
    public void setEvtInfoContri(Reinf.EvtInfoContri value) {
        this.evtInfoContri = value;
    }

    /**
     * Obt�m o valor da propriedade hjid.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Define o valor da propriedade hjid.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Reinf)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Reinf that = ((Reinf) object);
        {
            Reinf.EvtInfoContri lhsEvtInfoContri;
            lhsEvtInfoContri = this.getEvtInfoContri();
            Reinf.EvtInfoContri rhsEvtInfoContri;
            rhsEvtInfoContri = that.getEvtInfoContri();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "evtInfoContri", lhsEvtInfoContri), LocatorUtils.property(thatLocator, "evtInfoContri", rhsEvtInfoContri), lhsEvtInfoContri, rhsEvtInfoContri)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Reinf.EvtInfoContri theEvtInfoContri;
            theEvtInfoContri = this.getEvtInfoContri();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evtInfoContri", theEvtInfoContri), currentHashCode, theEvtInfoContri);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ideEvento">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tpAmb">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                         &lt;pattern value="[1|2]"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="procEmi">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                         &lt;pattern value="1|2"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="verProc">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;minLength value="1"/>
     *                         &lt;maxLength value="20"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ideContri">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tpInsc">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
     *                         &lt;pattern value="1|2"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="nrInsc">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;pattern value="[0-9]{8}|[0-9]{11}|[0-9]{14}"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="infoContri">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;choice>
     *                     &lt;element name="inclusao">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;element name="idePeriodo">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="iniValid">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="fimValid" minOccurs="0">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="infoCadastro">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="classTrib">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="2"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indEscrituracao">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="0|1"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indDesoneracao">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="0|1"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indAcordoIsenMulta">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="0|1"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indSitPJ" minOccurs="0">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="[0-4]"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="contato">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="nmCtt">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="70"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="cpfCtt">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="3"/>
     *                                                         &lt;maxLength value="11"/>
     *                                                         &lt;pattern value="[0-9]{3,11}"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="foneFixo" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="13"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="foneCel" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="13"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="email" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="60"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="cnpjSoftHouse">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="3"/>
     *                                                         &lt;maxLength value="14"/>
     *                                                         &lt;pattern value="[0-9]{3,14}"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="nmRazao">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="115"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="nmCont">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="70"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="telefone" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="13"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="email" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="60"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="infoEFR" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="ideEFR">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;pattern value="S|N"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="cnpjEFR" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="8"/>
     *                                                         &lt;maxLength value="14"/>
     *                                                         &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                     &lt;element name="alteracao">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;element name="idePeriodo">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="iniValid">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="fimValid" minOccurs="0">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="infoCadastro">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="classTrib">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="2"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indEscrituracao">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="0|1"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indDesoneracao">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="0|1"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indAcordoIsenMulta">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="0|1"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="indSitPJ" minOccurs="0">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
     *                                               &lt;pattern value="[0-4]"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="contato">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="nmCtt">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="70"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="cpfCtt">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="3"/>
     *                                                         &lt;maxLength value="11"/>
     *                                                         &lt;pattern value="[0-9]{3,11}"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="foneFixo" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;maxLength value="13"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="foneCel" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;maxLength value="13"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="email" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="60"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="cnpjSoftHouse">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="3"/>
     *                                                         &lt;maxLength value="14"/>
     *                                                         &lt;pattern value="[0-9]{3,14}"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="nmRazao">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="115"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="nmCont">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="70"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="telefone" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="13"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="email" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="1"/>
     *                                                         &lt;maxLength value="60"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="infoEFR" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="ideEFR">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;pattern value="S|N"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="cnpjEFR" minOccurs="0">
     *                                                     &lt;simpleType>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                         &lt;minLength value="8"/>
     *                                                         &lt;maxLength value="14"/>
     *                                                         &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
     *                                                       &lt;/restriction>
     *                                                     &lt;/simpleType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="novaValidade" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="iniValid">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="fimValid" minOccurs="0">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                     &lt;element name="exclusao">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;element name="idePeriodo">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="iniValid">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                         &lt;element name="fimValid" minOccurs="0">
     *                                           &lt;simpleType>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                               &lt;length value="7"/>
     *                                               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
     *                                             &lt;/restriction>
     *                                           &lt;/simpleType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                   &lt;/choice>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="id" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}ID">
     *             &lt;length value="36"/>
     *             &lt;pattern value="I{1}D{1}[0-9]{34}"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ideEvento",
        "ideContri",
        "infoContri"
    })
    @Entity(name = "Reinf$EvtInfoContri")
    @Table(name = "EVT_INFO_CONTRI")
    @Inheritance(strategy = InheritanceType.JOINED)
    public static class EvtInfoContri
        implements Equals, HashCode
    {

        @XmlElement(required = true)
        protected Reinf.EvtInfoContri.IdeEvento ideEvento;
        @XmlElement(required = true)
        protected Reinf.EvtInfoContri.IdeContri ideContri;
        @XmlElement(required = true)
        protected Reinf.EvtInfoContri.InfoContri infoContri;
        @XmlAttribute(name = "id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String id;
        @XmlAttribute(name = "Hjid")
        protected Long hjid;

        /**
         * Obt�m o valor da propriedade ideEvento.
         * 
         * @return
         *     possible object is
         *     {@link Reinf.EvtInfoContri.IdeEvento }
         *     
         */
        @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.IdeEvento.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "IDE_EVENTO_EVT_INFO_CONTRI_H_0")
        public Reinf.EvtInfoContri.IdeEvento getIdeEvento() {
            return ideEvento;
        }

        /**
         * Define o valor da propriedade ideEvento.
         * 
         * @param value
         *     allowed object is
         *     {@link Reinf.EvtInfoContri.IdeEvento }
         *     
         */
        public void setIdeEvento(Reinf.EvtInfoContri.IdeEvento value) {
            this.ideEvento = value;
        }

        /**
         * Obt�m o valor da propriedade ideContri.
         * 
         * @return
         *     possible object is
         *     {@link Reinf.EvtInfoContri.IdeContri }
         *     
         */
        @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.IdeContri.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "IDE_CONTRI_EVT_INFO_CONTRI_H_0")
        public Reinf.EvtInfoContri.IdeContri getIdeContri() {
            return ideContri;
        }

        /**
         * Define o valor da propriedade ideContri.
         * 
         * @param value
         *     allowed object is
         *     {@link Reinf.EvtInfoContri.IdeContri }
         *     
         */
        public void setIdeContri(Reinf.EvtInfoContri.IdeContri value) {
            this.ideContri = value;
        }

        /**
         * Obt�m o valor da propriedade infoContri.
         * 
         * @return
         *     possible object is
         *     {@link Reinf.EvtInfoContri.InfoContri }
         *     
         */
        @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.class, cascade = {
            CascadeType.ALL
        })
        @JoinColumn(name = "INFO_CONTRI_EVT_INFO_CONTRI__0")
        public Reinf.EvtInfoContri.InfoContri getInfoContri() {
            return infoContri;
        }

        /**
         * Define o valor da propriedade infoContri.
         * 
         * @param value
         *     allowed object is
         *     {@link Reinf.EvtInfoContri.InfoContri }
         *     
         */
        public void setInfoContri(Reinf.EvtInfoContri.InfoContri value) {
            this.infoContri = value;
        }

        /**
         * Obt�m o valor da propriedade id.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        @Basic
        @Column(name = "ID", length = 36)
        public String getId() {
            return id;
        }

        /**
         * Define o valor da propriedade id.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Obt�m o valor da propriedade hjid.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        @Id
        @Column(name = "HJID")
        @GeneratedValue(strategy = GenerationType.AUTO)
        public Long getHjid() {
            return hjid;
        }

        /**
         * Define o valor da propriedade hjid.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setHjid(Long value) {
            this.hjid = value;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof Reinf.EvtInfoContri)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final Reinf.EvtInfoContri that = ((Reinf.EvtInfoContri) object);
            {
                Reinf.EvtInfoContri.IdeEvento lhsIdeEvento;
                lhsIdeEvento = this.getIdeEvento();
                Reinf.EvtInfoContri.IdeEvento rhsIdeEvento;
                rhsIdeEvento = that.getIdeEvento();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEvento", lhsIdeEvento), LocatorUtils.property(thatLocator, "ideEvento", rhsIdeEvento), lhsIdeEvento, rhsIdeEvento)) {
                    return false;
                }
            }
            {
                Reinf.EvtInfoContri.IdeContri lhsIdeContri;
                lhsIdeContri = this.getIdeContri();
                Reinf.EvtInfoContri.IdeContri rhsIdeContri;
                rhsIdeContri = that.getIdeContri();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "ideContri", lhsIdeContri), LocatorUtils.property(thatLocator, "ideContri", rhsIdeContri), lhsIdeContri, rhsIdeContri)) {
                    return false;
                }
            }
            {
                Reinf.EvtInfoContri.InfoContri lhsInfoContri;
                lhsInfoContri = this.getInfoContri();
                Reinf.EvtInfoContri.InfoContri rhsInfoContri;
                rhsInfoContri = that.getInfoContri();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "infoContri", lhsInfoContri), LocatorUtils.property(thatLocator, "infoContri", rhsInfoContri), lhsInfoContri, rhsInfoContri)) {
                    return false;
                }
            }
            {
                String lhsId;
                lhsId = this.getId();
                String rhsId;
                rhsId = that.getId();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
            int currentHashCode = 1;
            {
                Reinf.EvtInfoContri.IdeEvento theIdeEvento;
                theIdeEvento = this.getIdeEvento();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEvento", theIdeEvento), currentHashCode, theIdeEvento);
            }
            {
                Reinf.EvtInfoContri.IdeContri theIdeContri;
                theIdeContri = this.getIdeContri();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideContri", theIdeContri), currentHashCode, theIdeContri);
            }
            {
                Reinf.EvtInfoContri.InfoContri theInfoContri;
                theInfoContri = this.getInfoContri();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoContri", theInfoContri), currentHashCode, theInfoContri);
            }
            {
                String theId;
                theId = this.getId();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
            }
            return currentHashCode;
        }

        public int hashCode() {
            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
            return this.hashCode(null, strategy);
        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tpInsc">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
         *               &lt;pattern value="1|2"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="nrInsc">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;pattern value="[0-9]{8}|[0-9]{11}|[0-9]{14}"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tpInsc",
            "nrInsc"
        })
        @Entity(name = "Reinf$EvtInfoContri$IdeContri")
        @Table(name = "IDE_CONTRI")
        @Inheritance(strategy = InheritanceType.JOINED)
        public static class IdeContri
            implements Equals, HashCode
        {

            protected short tpInsc;
            @XmlElement(required = true)
            protected String nrInsc;
            @XmlAttribute(name = "Hjid")
            protected Long hjid;

            /**
             * Obt�m o valor da propriedade tpInsc.
             * 
             */
            @Basic
            @Column(name = "TP_INSC", scale = 0)
            public short getTpInsc() {
                return tpInsc;
            }

            /**
             * Define o valor da propriedade tpInsc.
             * 
             */
            public void setTpInsc(short value) {
                this.tpInsc = value;
            }

            /**
             * Obt�m o valor da propriedade nrInsc.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            @Basic
            @Column(name = "NR_INSC")
            public String getNrInsc() {
                return nrInsc;
            }

            /**
             * Define o valor da propriedade nrInsc.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNrInsc(String value) {
                this.nrInsc = value;
            }

            /**
             * Obt�m o valor da propriedade hjid.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            @Id
            @Column(name = "HJID")
            @GeneratedValue(strategy = GenerationType.AUTO)
            public Long getHjid() {
                return hjid;
            }

            /**
             * Define o valor da propriedade hjid.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setHjid(Long value) {
                this.hjid = value;
            }

            public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                if (!(object instanceof Reinf.EvtInfoContri.IdeContri)) {
                    return false;
                }
                if (this == object) {
                    return true;
                }
                final Reinf.EvtInfoContri.IdeContri that = ((Reinf.EvtInfoContri.IdeContri) object);
                {
                    short lhsTpInsc;
                    lhsTpInsc = this.getTpInsc();
                    short rhsTpInsc;
                    rhsTpInsc = that.getTpInsc();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "tpInsc", lhsTpInsc), LocatorUtils.property(thatLocator, "tpInsc", rhsTpInsc), lhsTpInsc, rhsTpInsc)) {
                        return false;
                    }
                }
                {
                    String lhsNrInsc;
                    lhsNrInsc = this.getNrInsc();
                    String rhsNrInsc;
                    rhsNrInsc = that.getNrInsc();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "nrInsc", lhsNrInsc), LocatorUtils.property(thatLocator, "nrInsc", rhsNrInsc), lhsNrInsc, rhsNrInsc)) {
                        return false;
                    }
                }
                return true;
            }

            public boolean equals(Object object) {
                final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                return equals(null, null, object, strategy);
            }

            public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                int currentHashCode = 1;
                {
                    short theTpInsc;
                    theTpInsc = this.getTpInsc();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpInsc", theTpInsc), currentHashCode, theTpInsc);
                }
                {
                    String theNrInsc;
                    theNrInsc = this.getNrInsc();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nrInsc", theNrInsc), currentHashCode, theNrInsc);
                }
                return currentHashCode;
            }

            public int hashCode() {
                final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                return this.hashCode(null, strategy);
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tpAmb">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *               &lt;pattern value="[1|2]"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="procEmi">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *               &lt;pattern value="1|2"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="verProc">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;minLength value="1"/>
         *               &lt;maxLength value="20"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tpAmb",
            "procEmi",
            "verProc"
        })
        @Entity(name = "Reinf$EvtInfoContri$IdeEvento")
        @Table(name = "IDE_EVENTO")
        @Inheritance(strategy = InheritanceType.JOINED)
        public static class IdeEvento
            implements Equals, HashCode
        {

            protected long tpAmb;
            protected long procEmi;
            @XmlElement(required = true)
            protected String verProc;
            @XmlAttribute(name = "Hjid")
            protected Long hjid;

            /**
             * Obt�m o valor da propriedade tpAmb.
             * 
             */
            @Basic
            @Column(name = "TP_AMB", scale = 0)
            public long getTpAmb() {
                return tpAmb;
            }

            /**
             * Define o valor da propriedade tpAmb.
             * 
             */
            public void setTpAmb(long value) {
                this.tpAmb = value;
            }

            /**
             * Obt�m o valor da propriedade procEmi.
             * 
             */
            @Basic
            @Column(name = "PROC_EMI", scale = 0)
            public long getProcEmi() {
                return procEmi;
            }

            /**
             * Define o valor da propriedade procEmi.
             * 
             */
            public void setProcEmi(long value) {
                this.procEmi = value;
            }

            /**
             * Obt�m o valor da propriedade verProc.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            @Basic
            @Column(name = "VER_PROC", length = 20)
            public String getVerProc() {
                return verProc;
            }

            /**
             * Define o valor da propriedade verProc.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVerProc(String value) {
                this.verProc = value;
            }

            /**
             * Obt�m o valor da propriedade hjid.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            @Id
            @Column(name = "HJID")
            @GeneratedValue(strategy = GenerationType.AUTO)
            public Long getHjid() {
                return hjid;
            }

            /**
             * Define o valor da propriedade hjid.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setHjid(Long value) {
                this.hjid = value;
            }

            public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                if (!(object instanceof Reinf.EvtInfoContri.IdeEvento)) {
                    return false;
                }
                if (this == object) {
                    return true;
                }
                final Reinf.EvtInfoContri.IdeEvento that = ((Reinf.EvtInfoContri.IdeEvento) object);
                {
                    long lhsTpAmb;
                    lhsTpAmb = this.getTpAmb();
                    long rhsTpAmb;
                    rhsTpAmb = that.getTpAmb();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "tpAmb", lhsTpAmb), LocatorUtils.property(thatLocator, "tpAmb", rhsTpAmb), lhsTpAmb, rhsTpAmb)) {
                        return false;
                    }
                }
                {
                    long lhsProcEmi;
                    lhsProcEmi = this.getProcEmi();
                    long rhsProcEmi;
                    rhsProcEmi = that.getProcEmi();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "procEmi", lhsProcEmi), LocatorUtils.property(thatLocator, "procEmi", rhsProcEmi), lhsProcEmi, rhsProcEmi)) {
                        return false;
                    }
                }
                {
                    String lhsVerProc;
                    lhsVerProc = this.getVerProc();
                    String rhsVerProc;
                    rhsVerProc = that.getVerProc();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "verProc", lhsVerProc), LocatorUtils.property(thatLocator, "verProc", rhsVerProc), lhsVerProc, rhsVerProc)) {
                        return false;
                    }
                }
                return true;
            }

            public boolean equals(Object object) {
                final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                return equals(null, null, object, strategy);
            }

            public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                int currentHashCode = 1;
                {
                    long theTpAmb;
                    theTpAmb = this.getTpAmb();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tpAmb", theTpAmb), currentHashCode, theTpAmb);
                }
                {
                    long theProcEmi;
                    theProcEmi = this.getProcEmi();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "procEmi", theProcEmi), currentHashCode, theProcEmi);
                }
                {
                    String theVerProc;
                    theVerProc = this.getVerProc();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verProc", theVerProc), currentHashCode, theVerProc);
                }
                return currentHashCode;
            }

            public int hashCode() {
                final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                return this.hashCode(null, strategy);
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;choice>
         *           &lt;element name="inclusao">
         *             &lt;complexType>
         *               &lt;complexContent>
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                   &lt;sequence>
         *                     &lt;element name="idePeriodo">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="iniValid">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="fimValid" minOccurs="0">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="infoCadastro">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="classTrib">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="2"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indEscrituracao">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="0|1"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indDesoneracao">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="0|1"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indAcordoIsenMulta">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="0|1"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indSitPJ" minOccurs="0">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="[0-4]"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="contato">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="nmCtt">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="70"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="cpfCtt">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="3"/>
         *                                               &lt;maxLength value="11"/>
         *                                               &lt;pattern value="[0-9]{3,11}"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="foneFixo" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="13"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="foneCel" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="13"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="email" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="60"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="cnpjSoftHouse">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="3"/>
         *                                               &lt;maxLength value="14"/>
         *                                               &lt;pattern value="[0-9]{3,14}"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="nmRazao">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="115"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="nmCont">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="70"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="telefone" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="13"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="email" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="60"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="infoEFR" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="ideEFR">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;pattern value="S|N"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="cnpjEFR" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="8"/>
         *                                               &lt;maxLength value="14"/>
         *                                               &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/sequence>
         *                 &lt;/restriction>
         *               &lt;/complexContent>
         *             &lt;/complexType>
         *           &lt;/element>
         *           &lt;element name="alteracao">
         *             &lt;complexType>
         *               &lt;complexContent>
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                   &lt;sequence>
         *                     &lt;element name="idePeriodo">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="iniValid">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="fimValid" minOccurs="0">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="infoCadastro">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="classTrib">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="2"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indEscrituracao">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="0|1"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indDesoneracao">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="0|1"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indAcordoIsenMulta">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="0|1"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="indSitPJ" minOccurs="0">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
         *                                     &lt;pattern value="[0-4]"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="contato">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="nmCtt">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="70"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="cpfCtt">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="3"/>
         *                                               &lt;maxLength value="11"/>
         *                                               &lt;pattern value="[0-9]{3,11}"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="foneFixo" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;maxLength value="13"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="foneCel" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;maxLength value="13"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="email" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="60"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="cnpjSoftHouse">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="3"/>
         *                                               &lt;maxLength value="14"/>
         *                                               &lt;pattern value="[0-9]{3,14}"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="nmRazao">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="115"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="nmCont">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="70"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="telefone" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="13"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="email" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="1"/>
         *                                               &lt;maxLength value="60"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="infoEFR" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="ideEFR">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;pattern value="S|N"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                         &lt;element name="cnpjEFR" minOccurs="0">
         *                                           &lt;simpleType>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                               &lt;minLength value="8"/>
         *                                               &lt;maxLength value="14"/>
         *                                               &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
         *                                             &lt;/restriction>
         *                                           &lt;/simpleType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="novaValidade" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="iniValid">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="fimValid" minOccurs="0">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/sequence>
         *                 &lt;/restriction>
         *               &lt;/complexContent>
         *             &lt;/complexType>
         *           &lt;/element>
         *           &lt;element name="exclusao">
         *             &lt;complexType>
         *               &lt;complexContent>
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                   &lt;sequence>
         *                     &lt;element name="idePeriodo">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="iniValid">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                               &lt;element name="fimValid" minOccurs="0">
         *                                 &lt;simpleType>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                     &lt;length value="7"/>
         *                                     &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
         *                                   &lt;/restriction>
         *                                 &lt;/simpleType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/sequence>
         *                 &lt;/restriction>
         *               &lt;/complexContent>
         *             &lt;/complexType>
         *           &lt;/element>
         *         &lt;/choice>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "inclusao",
            "alteracao",
            "exclusao"
        })
        @Entity(name = "Reinf$EvtInfoContri$InfoContri")
        @Table(name = "INFO_CONTRI")
        @Inheritance(strategy = InheritanceType.JOINED)
        public static class InfoContri
            implements Equals, HashCode
        {

            protected Reinf.EvtInfoContri.InfoContri.Inclusao inclusao;
            protected Reinf.EvtInfoContri.InfoContri.Alteracao alteracao;
            protected Reinf.EvtInfoContri.InfoContri.Exclusao exclusao;
            @XmlAttribute(name = "Hjid")
            protected Long hjid;

            /**
             * Obt�m o valor da propriedade inclusao.
             * 
             * @return
             *     possible object is
             *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao }
             *     
             */
            @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Inclusao.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "INCLUSAO_INFO_CONTRI_HJID")
            public Reinf.EvtInfoContri.InfoContri.Inclusao getInclusao() {
                return inclusao;
            }

            /**
             * Define o valor da propriedade inclusao.
             * 
             * @param value
             *     allowed object is
             *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao }
             *     
             */
            public void setInclusao(Reinf.EvtInfoContri.InfoContri.Inclusao value) {
                this.inclusao = value;
            }

            /**
             * Obt�m o valor da propriedade alteracao.
             * 
             * @return
             *     possible object is
             *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao }
             *     
             */
            @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Alteracao.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "ALTERACAO_INFO_CONTRI_HJID")
            public Reinf.EvtInfoContri.InfoContri.Alteracao getAlteracao() {
                return alteracao;
            }

            /**
             * Define o valor da propriedade alteracao.
             * 
             * @param value
             *     allowed object is
             *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao }
             *     
             */
            public void setAlteracao(Reinf.EvtInfoContri.InfoContri.Alteracao value) {
                this.alteracao = value;
            }

            /**
             * Obt�m o valor da propriedade exclusao.
             * 
             * @return
             *     possible object is
             *     {@link Reinf.EvtInfoContri.InfoContri.Exclusao }
             *     
             */
            @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Exclusao.class, cascade = {
                CascadeType.ALL
            })
            @JoinColumn(name = "EXCLUSAO_INFO_CONTRI_HJID")
            public Reinf.EvtInfoContri.InfoContri.Exclusao getExclusao() {
                return exclusao;
            }

            /**
             * Define o valor da propriedade exclusao.
             * 
             * @param value
             *     allowed object is
             *     {@link Reinf.EvtInfoContri.InfoContri.Exclusao }
             *     
             */
            public void setExclusao(Reinf.EvtInfoContri.InfoContri.Exclusao value) {
                this.exclusao = value;
            }

            /**
             * Obt�m o valor da propriedade hjid.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            @Id
            @Column(name = "HJID")
            @GeneratedValue(strategy = GenerationType.AUTO)
            public Long getHjid() {
                return hjid;
            }

            /**
             * Define o valor da propriedade hjid.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setHjid(Long value) {
                this.hjid = value;
            }

            public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                if (!(object instanceof Reinf.EvtInfoContri.InfoContri)) {
                    return false;
                }
                if (this == object) {
                    return true;
                }
                final Reinf.EvtInfoContri.InfoContri that = ((Reinf.EvtInfoContri.InfoContri) object);
                {
                    Reinf.EvtInfoContri.InfoContri.Inclusao lhsInclusao;
                    lhsInclusao = this.getInclusao();
                    Reinf.EvtInfoContri.InfoContri.Inclusao rhsInclusao;
                    rhsInclusao = that.getInclusao();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "inclusao", lhsInclusao), LocatorUtils.property(thatLocator, "inclusao", rhsInclusao), lhsInclusao, rhsInclusao)) {
                        return false;
                    }
                }
                {
                    Reinf.EvtInfoContri.InfoContri.Alteracao lhsAlteracao;
                    lhsAlteracao = this.getAlteracao();
                    Reinf.EvtInfoContri.InfoContri.Alteracao rhsAlteracao;
                    rhsAlteracao = that.getAlteracao();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "alteracao", lhsAlteracao), LocatorUtils.property(thatLocator, "alteracao", rhsAlteracao), lhsAlteracao, rhsAlteracao)) {
                        return false;
                    }
                }
                {
                    Reinf.EvtInfoContri.InfoContri.Exclusao lhsExclusao;
                    lhsExclusao = this.getExclusao();
                    Reinf.EvtInfoContri.InfoContri.Exclusao rhsExclusao;
                    rhsExclusao = that.getExclusao();
                    if (!strategy.equals(LocatorUtils.property(thisLocator, "exclusao", lhsExclusao), LocatorUtils.property(thatLocator, "exclusao", rhsExclusao), lhsExclusao, rhsExclusao)) {
                        return false;
                    }
                }
                return true;
            }

            public boolean equals(Object object) {
                final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                return equals(null, null, object, strategy);
            }

            public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                int currentHashCode = 1;
                {
                    Reinf.EvtInfoContri.InfoContri.Inclusao theInclusao;
                    theInclusao = this.getInclusao();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "inclusao", theInclusao), currentHashCode, theInclusao);
                }
                {
                    Reinf.EvtInfoContri.InfoContri.Alteracao theAlteracao;
                    theAlteracao = this.getAlteracao();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "alteracao", theAlteracao), currentHashCode, theAlteracao);
                }
                {
                    Reinf.EvtInfoContri.InfoContri.Exclusao theExclusao;
                    theExclusao = this.getExclusao();
                    currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "exclusao", theExclusao), currentHashCode, theExclusao);
                }
                return currentHashCode;
            }

            public int hashCode() {
                final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                return this.hashCode(null, strategy);
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="idePeriodo">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="iniValid">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="fimValid" minOccurs="0">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="infoCadastro">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="classTrib">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="2"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indEscrituracao">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="0|1"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indDesoneracao">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="0|1"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indAcordoIsenMulta">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="0|1"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indSitPJ" minOccurs="0">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="[0-4]"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="contato">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="nmCtt">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="70"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="cpfCtt">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="3"/>
             *                                   &lt;maxLength value="11"/>
             *                                   &lt;pattern value="[0-9]{3,11}"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="foneFixo" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;maxLength value="13"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="foneCel" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;maxLength value="13"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="email" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="60"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="cnpjSoftHouse">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="3"/>
             *                                   &lt;maxLength value="14"/>
             *                                   &lt;pattern value="[0-9]{3,14}"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="nmRazao">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="115"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="nmCont">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="70"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="telefone" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="13"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="email" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="60"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="infoEFR" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ideEFR">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;pattern value="S|N"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="cnpjEFR" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="8"/>
             *                                   &lt;maxLength value="14"/>
             *                                   &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="novaValidade" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="iniValid">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="fimValid" minOccurs="0">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idePeriodo",
                "infoCadastro",
                "novaValidade"
            })
            @Entity(name = "Reinf$EvtInfoContri$InfoContri$Alteracao")
            @Table(name = "ALTERACAO")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class Alteracao
                implements Equals, HashCode
            {

                @XmlElement(required = true)
                protected Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo idePeriodo;
                @XmlElement(required = true)
                protected Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro infoCadastro;
                protected Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade novaValidade;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade idePeriodo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo }
                 *     
                 */
                @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo.class, cascade = {
                    CascadeType.ALL
                })
                @JoinColumn(name = "IDE_PERIODO_ALTERACAO_HJID")
                public Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo getIdePeriodo() {
                    return idePeriodo;
                }

                /**
                 * Define o valor da propriedade idePeriodo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo }
                 *     
                 */
                public void setIdePeriodo(Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo value) {
                    this.idePeriodo = value;
                }

                /**
                 * Obt�m o valor da propriedade infoCadastro.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro }
                 *     
                 */
                @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.class, cascade = {
                    CascadeType.ALL
                })
                @JoinColumn(name = "INFO_CADASTRO_ALTERACAO_HJID")
                public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro getInfoCadastro() {
                    return infoCadastro;
                }

                /**
                 * Define o valor da propriedade infoCadastro.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro }
                 *     
                 */
                public void setInfoCadastro(Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro value) {
                    this.infoCadastro = value;
                }

                /**
                 * Obt�m o valor da propriedade novaValidade.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade }
                 *     
                 */
                @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade.class, cascade = {
                    CascadeType.ALL
                })
                @JoinColumn(name = "NOVA_VALIDADE_ALTERACAO_HJID")
                public Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade getNovaValidade() {
                    return novaValidade;
                }

                /**
                 * Define o valor da propriedade novaValidade.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade }
                 *     
                 */
                public void setNovaValidade(Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade value) {
                    this.novaValidade = value;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Alteracao)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtInfoContri.InfoContri.Alteracao that = ((Reinf.EvtInfoContri.InfoContri.Alteracao) object);
                    {
                        Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo lhsIdePeriodo;
                        lhsIdePeriodo = this.getIdePeriodo();
                        Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo rhsIdePeriodo;
                        rhsIdePeriodo = that.getIdePeriodo();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "idePeriodo", lhsIdePeriodo), LocatorUtils.property(thatLocator, "idePeriodo", rhsIdePeriodo), lhsIdePeriodo, rhsIdePeriodo)) {
                            return false;
                        }
                    }
                    {
                        Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro lhsInfoCadastro;
                        lhsInfoCadastro = this.getInfoCadastro();
                        Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro rhsInfoCadastro;
                        rhsInfoCadastro = that.getInfoCadastro();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "infoCadastro", lhsInfoCadastro), LocatorUtils.property(thatLocator, "infoCadastro", rhsInfoCadastro), lhsInfoCadastro, rhsInfoCadastro)) {
                            return false;
                        }
                    }
                    {
                        Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade lhsNovaValidade;
                        lhsNovaValidade = this.getNovaValidade();
                        Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade rhsNovaValidade;
                        rhsNovaValidade = that.getNovaValidade();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "novaValidade", lhsNovaValidade), LocatorUtils.property(thatLocator, "novaValidade", rhsNovaValidade), lhsNovaValidade, rhsNovaValidade)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo theIdePeriodo;
                        theIdePeriodo = this.getIdePeriodo();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idePeriodo", theIdePeriodo), currentHashCode, theIdePeriodo);
                    }
                    {
                        Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro theInfoCadastro;
                        theInfoCadastro = this.getInfoCadastro();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoCadastro", theInfoCadastro), currentHashCode, theInfoCadastro);
                    }
                    {
                        Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade theNovaValidade;
                        theNovaValidade = this.getNovaValidade();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "novaValidade", theNovaValidade), currentHashCode, theNovaValidade);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }


                /**
                 * <p>Classe Java de anonymous complex type.
                 * 
                 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="iniValid">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="fimValid" minOccurs="0">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "iniValid",
                    "fimValid"
                })
                @Entity(name = "Reinf$EvtInfoContri$InfoContri$Alteracao$IdePeriodo")
                @Table(name = "IDE_PERIODO__0")
                @Inheritance(strategy = InheritanceType.JOINED)
                public static class IdePeriodo
                    implements Equals, HashCode
                {

                    @XmlElement(required = true)
                    protected String iniValid;
                    protected String fimValid;
                    @XmlAttribute(name = "Hjid")
                    protected Long hjid;

                    /**
                     * Obt�m o valor da propriedade iniValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "INI_VALID", length = 7)
                    public String getIniValid() {
                        return iniValid;
                    }

                    /**
                     * Define o valor da propriedade iniValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIniValid(String value) {
                        this.iniValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade fimValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "FIM_VALID", length = 7)
                    public String getFimValid() {
                        return fimValid;
                    }

                    /**
                     * Define o valor da propriedade fimValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFimValid(String value) {
                        this.fimValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade hjid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Id
                    @Column(name = "HJID")
                    @GeneratedValue(strategy = GenerationType.AUTO)
                    public Long getHjid() {
                        return hjid;
                    }

                    /**
                     * Define o valor da propriedade hjid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setHjid(Long value) {
                        this.hjid = value;
                    }

                    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                        if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo)) {
                            return false;
                        }
                        if (this == object) {
                            return true;
                        }
                        final Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo that = ((Reinf.EvtInfoContri.InfoContri.Alteracao.IdePeriodo) object);
                        {
                            String lhsIniValid;
                            lhsIniValid = this.getIniValid();
                            String rhsIniValid;
                            rhsIniValid = that.getIniValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "iniValid", lhsIniValid), LocatorUtils.property(thatLocator, "iniValid", rhsIniValid), lhsIniValid, rhsIniValid)) {
                                return false;
                            }
                        }
                        {
                            String lhsFimValid;
                            lhsFimValid = this.getFimValid();
                            String rhsFimValid;
                            rhsFimValid = that.getFimValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "fimValid", lhsFimValid), LocatorUtils.property(thatLocator, "fimValid", rhsFimValid), lhsFimValid, rhsFimValid)) {
                                return false;
                            }
                        }
                        return true;
                    }

                    public boolean equals(Object object) {
                        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                        return equals(null, null, object, strategy);
                    }

                    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                        int currentHashCode = 1;
                        {
                            String theIniValid;
                            theIniValid = this.getIniValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "iniValid", theIniValid), currentHashCode, theIniValid);
                        }
                        {
                            String theFimValid;
                            theFimValid = this.getFimValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fimValid", theFimValid), currentHashCode, theFimValid);
                        }
                        return currentHashCode;
                    }

                    public int hashCode() {
                        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                        return this.hashCode(null, strategy);
                    }

                }


                /**
                 * <p>Classe Java de anonymous complex type.
                 * 
                 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="classTrib">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="2"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indEscrituracao">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="0|1"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indDesoneracao">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="0|1"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indAcordoIsenMulta">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="0|1"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indSitPJ" minOccurs="0">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="[0-4]"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="contato">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="nmCtt">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="70"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="cpfCtt">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="3"/>
                 *                         &lt;maxLength value="11"/>
                 *                         &lt;pattern value="[0-9]{3,11}"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="foneFixo" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;maxLength value="13"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="foneCel" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;maxLength value="13"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="email" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="60"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="cnpjSoftHouse">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="3"/>
                 *                         &lt;maxLength value="14"/>
                 *                         &lt;pattern value="[0-9]{3,14}"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="nmRazao">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="115"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="nmCont">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="70"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="telefone" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="13"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="email" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="60"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="infoEFR" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ideEFR">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;pattern value="S|N"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="cnpjEFR" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="8"/>
                 *                         &lt;maxLength value="14"/>
                 *                         &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "classTrib",
                    "indEscrituracao",
                    "indDesoneracao",
                    "indAcordoIsenMulta",
                    "indSitPJ",
                    "contato",
                    "softHouse",
                    "infoEFR"
                })
                @Entity(name = "Reinf$EvtInfoContri$InfoContri$Alteracao$InfoCadastro")
                @Table(name = "INFO_CADASTRO")
                @Inheritance(strategy = InheritanceType.JOINED)
                public static class InfoCadastro
                    implements Equals, HashCode
                {

                    @XmlElement(required = true)
                    protected String classTrib;
                    protected long indEscrituracao;
                    protected long indDesoneracao;
                    protected long indAcordoIsenMulta;
                    protected Long indSitPJ;
                    @XmlElement(required = true)
                    protected Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato contato;
                    protected List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> softHouse;
                    protected Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR infoEFR;
                    @XmlAttribute(name = "Hjid")
                    protected Long hjid;

                    /**
                     * Obt�m o valor da propriedade classTrib.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "CLASS_TRIB", length = 2)
                    public String getClassTrib() {
                        return classTrib;
                    }

                    /**
                     * Define o valor da propriedade classTrib.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClassTrib(String value) {
                        this.classTrib = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indEscrituracao.
                     * 
                     */
                    @Basic
                    @Column(name = "IND_ESCRITURACAO", scale = 0)
                    public long getIndEscrituracao() {
                        return indEscrituracao;
                    }

                    /**
                     * Define o valor da propriedade indEscrituracao.
                     * 
                     */
                    public void setIndEscrituracao(long value) {
                        this.indEscrituracao = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indDesoneracao.
                     * 
                     */
                    @Basic
                    @Column(name = "IND_DESONERACAO", scale = 0)
                    public long getIndDesoneracao() {
                        return indDesoneracao;
                    }

                    /**
                     * Define o valor da propriedade indDesoneracao.
                     * 
                     */
                    public void setIndDesoneracao(long value) {
                        this.indDesoneracao = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indAcordoIsenMulta.
                     * 
                     */
                    @Basic
                    @Column(name = "IND_ACORDO_ISEN_MULTA", scale = 0)
                    public long getIndAcordoIsenMulta() {
                        return indAcordoIsenMulta;
                    }

                    /**
                     * Define o valor da propriedade indAcordoIsenMulta.
                     * 
                     */
                    public void setIndAcordoIsenMulta(long value) {
                        this.indAcordoIsenMulta = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indSitPJ.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Basic
                    @Column(name = "IND_SIT_PJ", scale = 0)
                    public Long getIndSitPJ() {
                        return indSitPJ;
                    }

                    /**
                     * Define o valor da propriedade indSitPJ.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setIndSitPJ(Long value) {
                        this.indSitPJ = value;
                    }

                    /**
                     * Obt�m o valor da propriedade contato.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato }
                     *     
                     */
                    @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato.class, cascade = {
                        CascadeType.ALL
                    })
                    @JoinColumn(name = "CONTATO_INFO_CADASTRO_HJID")
                    public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato getContato() {
                        return contato;
                    }

                    /**
                     * Define o valor da propriedade contato.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato }
                     *     
                     */
                    public void setContato(Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato value) {
                        this.contato = value;
                    }

                    /**
                     * Gets the value of the softHouse property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the softHouse property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getSoftHouse().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse }
                     * 
                     * 
                     */
                    @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse.class, cascade = {
                        CascadeType.ALL
                    })
                    @JoinColumn(name = "SOFT_HOUSE_INFO_CADASTRO_HJID")
                    public List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> getSoftHouse() {
                        if (softHouse == null) {
                            softHouse = new ArrayList<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse>();
                        }
                        return this.softHouse;
                    }

                    /**
                     * 
                     * 
                     */
                    public void setSoftHouse(List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> softHouse) {
                        this.softHouse = softHouse;
                    }

                    /**
                     * Obt�m o valor da propriedade infoEFR.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR }
                     *     
                     */
                    @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR.class, cascade = {
                        CascadeType.ALL
                    })
                    @JoinColumn(name = "INFO_EFR_INFO_CADASTRO_HJID")
                    public Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR getInfoEFR() {
                        return infoEFR;
                    }

                    /**
                     * Define o valor da propriedade infoEFR.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR }
                     *     
                     */
                    public void setInfoEFR(Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR value) {
                        this.infoEFR = value;
                    }

                    /**
                     * Obt�m o valor da propriedade hjid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Id
                    @Column(name = "HJID")
                    @GeneratedValue(strategy = GenerationType.AUTO)
                    public Long getHjid() {
                        return hjid;
                    }

                    /**
                     * Define o valor da propriedade hjid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setHjid(Long value) {
                        this.hjid = value;
                    }

                    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                        if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro)) {
                            return false;
                        }
                        if (this == object) {
                            return true;
                        }
                        final Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro that = ((Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro) object);
                        {
                            String lhsClassTrib;
                            lhsClassTrib = this.getClassTrib();
                            String rhsClassTrib;
                            rhsClassTrib = that.getClassTrib();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "classTrib", lhsClassTrib), LocatorUtils.property(thatLocator, "classTrib", rhsClassTrib), lhsClassTrib, rhsClassTrib)) {
                                return false;
                            }
                        }
                        {
                            long lhsIndEscrituracao;
                            lhsIndEscrituracao = this.getIndEscrituracao();
                            long rhsIndEscrituracao;
                            rhsIndEscrituracao = that.getIndEscrituracao();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indEscrituracao", lhsIndEscrituracao), LocatorUtils.property(thatLocator, "indEscrituracao", rhsIndEscrituracao), lhsIndEscrituracao, rhsIndEscrituracao)) {
                                return false;
                            }
                        }
                        {
                            long lhsIndDesoneracao;
                            lhsIndDesoneracao = this.getIndDesoneracao();
                            long rhsIndDesoneracao;
                            rhsIndDesoneracao = that.getIndDesoneracao();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indDesoneracao", lhsIndDesoneracao), LocatorUtils.property(thatLocator, "indDesoneracao", rhsIndDesoneracao), lhsIndDesoneracao, rhsIndDesoneracao)) {
                                return false;
                            }
                        }
                        {
                            long lhsIndAcordoIsenMulta;
                            lhsIndAcordoIsenMulta = this.getIndAcordoIsenMulta();
                            long rhsIndAcordoIsenMulta;
                            rhsIndAcordoIsenMulta = that.getIndAcordoIsenMulta();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indAcordoIsenMulta", lhsIndAcordoIsenMulta), LocatorUtils.property(thatLocator, "indAcordoIsenMulta", rhsIndAcordoIsenMulta), lhsIndAcordoIsenMulta, rhsIndAcordoIsenMulta)) {
                                return false;
                            }
                        }
                        {
                            Long lhsIndSitPJ;
                            lhsIndSitPJ = this.getIndSitPJ();
                            Long rhsIndSitPJ;
                            rhsIndSitPJ = that.getIndSitPJ();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indSitPJ", lhsIndSitPJ), LocatorUtils.property(thatLocator, "indSitPJ", rhsIndSitPJ), lhsIndSitPJ, rhsIndSitPJ)) {
                                return false;
                            }
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato lhsContato;
                            lhsContato = this.getContato();
                            Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato rhsContato;
                            rhsContato = that.getContato();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "contato", lhsContato), LocatorUtils.property(thatLocator, "contato", rhsContato), lhsContato, rhsContato)) {
                                return false;
                            }
                        }
                        {
                            List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> lhsSoftHouse;
                            lhsSoftHouse = (((this.softHouse!= null)&&(!this.softHouse.isEmpty()))?this.getSoftHouse():null);
                            List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> rhsSoftHouse;
                            rhsSoftHouse = (((that.softHouse!= null)&&(!that.softHouse.isEmpty()))?that.getSoftHouse():null);
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "softHouse", lhsSoftHouse), LocatorUtils.property(thatLocator, "softHouse", rhsSoftHouse), lhsSoftHouse, rhsSoftHouse)) {
                                return false;
                            }
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR lhsInfoEFR;
                            lhsInfoEFR = this.getInfoEFR();
                            Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR rhsInfoEFR;
                            rhsInfoEFR = that.getInfoEFR();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "infoEFR", lhsInfoEFR), LocatorUtils.property(thatLocator, "infoEFR", rhsInfoEFR), lhsInfoEFR, rhsInfoEFR)) {
                                return false;
                            }
                        }
                        return true;
                    }

                    public boolean equals(Object object) {
                        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                        return equals(null, null, object, strategy);
                    }

                    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                        int currentHashCode = 1;
                        {
                            String theClassTrib;
                            theClassTrib = this.getClassTrib();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "classTrib", theClassTrib), currentHashCode, theClassTrib);
                        }
                        {
                            long theIndEscrituracao;
                            theIndEscrituracao = this.getIndEscrituracao();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indEscrituracao", theIndEscrituracao), currentHashCode, theIndEscrituracao);
                        }
                        {
                            long theIndDesoneracao;
                            theIndDesoneracao = this.getIndDesoneracao();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indDesoneracao", theIndDesoneracao), currentHashCode, theIndDesoneracao);
                        }
                        {
                            long theIndAcordoIsenMulta;
                            theIndAcordoIsenMulta = this.getIndAcordoIsenMulta();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indAcordoIsenMulta", theIndAcordoIsenMulta), currentHashCode, theIndAcordoIsenMulta);
                        }
                        {
                            Long theIndSitPJ;
                            theIndSitPJ = this.getIndSitPJ();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indSitPJ", theIndSitPJ), currentHashCode, theIndSitPJ);
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato theContato;
                            theContato = this.getContato();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "contato", theContato), currentHashCode, theContato);
                        }
                        {
                            List<Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse> theSoftHouse;
                            theSoftHouse = (((this.softHouse!= null)&&(!this.softHouse.isEmpty()))?this.getSoftHouse():null);
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "softHouse", theSoftHouse), currentHashCode, theSoftHouse);
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR theInfoEFR;
                            theInfoEFR = this.getInfoEFR();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoEFR", theInfoEFR), currentHashCode, theInfoEFR);
                        }
                        return currentHashCode;
                    }

                    public int hashCode() {
                        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                        return this.hashCode(null, strategy);
                    }


                    /**
                     * <p>Classe Java de anonymous complex type.
                     * 
                     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="nmCtt">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="70"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="cpfCtt">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="3"/>
                     *               &lt;maxLength value="11"/>
                     *               &lt;pattern value="[0-9]{3,11}"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="foneFixo" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;maxLength value="13"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="foneCel" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;maxLength value="13"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="email" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="60"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "nmCtt",
                        "cpfCtt",
                        "foneFixo",
                        "foneCel",
                        "email"
                    })
                    @Entity(name = "Reinf$EvtInfoContri$InfoContri$Alteracao$InfoCadastro$Contato")
                    @Table(name = "CONTATO")
                    @Inheritance(strategy = InheritanceType.JOINED)
                    public static class Contato
                        implements Equals, HashCode
                    {

                        @XmlElement(required = true)
                        protected String nmCtt;
                        @XmlElement(required = true)
                        protected String cpfCtt;
                        protected String foneFixo;
                        protected String foneCel;
                        protected String email;
                        @XmlAttribute(name = "Hjid")
                        protected Long hjid;

                        /**
                         * Obt�m o valor da propriedade nmCtt.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "NM_CTT", length = 70)
                        public String getNmCtt() {
                            return nmCtt;
                        }

                        /**
                         * Define o valor da propriedade nmCtt.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNmCtt(String value) {
                            this.nmCtt = value;
                        }

                        /**
                         * Obt�m o valor da propriedade cpfCtt.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "CPF_CTT", length = 11)
                        public String getCpfCtt() {
                            return cpfCtt;
                        }

                        /**
                         * Define o valor da propriedade cpfCtt.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCpfCtt(String value) {
                            this.cpfCtt = value;
                        }

                        /**
                         * Obt�m o valor da propriedade foneFixo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "FONE_FIXO", length = 13)
                        public String getFoneFixo() {
                            return foneFixo;
                        }

                        /**
                         * Define o valor da propriedade foneFixo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setFoneFixo(String value) {
                            this.foneFixo = value;
                        }

                        /**
                         * Obt�m o valor da propriedade foneCel.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "FONE_CEL", length = 13)
                        public String getFoneCel() {
                            return foneCel;
                        }

                        /**
                         * Define o valor da propriedade foneCel.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setFoneCel(String value) {
                            this.foneCel = value;
                        }

                        /**
                         * Obt�m o valor da propriedade email.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "EMAIL", length = 60)
                        public String getEmail() {
                            return email;
                        }

                        /**
                         * Define o valor da propriedade email.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEmail(String value) {
                            this.email = value;
                        }

                        /**
                         * Obt�m o valor da propriedade hjid.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Long }
                         *     
                         */
                        @Id
                        @Column(name = "HJID")
                        @GeneratedValue(strategy = GenerationType.AUTO)
                        public Long getHjid() {
                            return hjid;
                        }

                        /**
                         * Define o valor da propriedade hjid.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Long }
                         *     
                         */
                        public void setHjid(Long value) {
                            this.hjid = value;
                        }

                        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                            if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato)) {
                                return false;
                            }
                            if (this == object) {
                                return true;
                            }
                            final Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato that = ((Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.Contato) object);
                            {
                                String lhsNmCtt;
                                lhsNmCtt = this.getNmCtt();
                                String rhsNmCtt;
                                rhsNmCtt = that.getNmCtt();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "nmCtt", lhsNmCtt), LocatorUtils.property(thatLocator, "nmCtt", rhsNmCtt), lhsNmCtt, rhsNmCtt)) {
                                    return false;
                                }
                            }
                            {
                                String lhsCpfCtt;
                                lhsCpfCtt = this.getCpfCtt();
                                String rhsCpfCtt;
                                rhsCpfCtt = that.getCpfCtt();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "cpfCtt", lhsCpfCtt), LocatorUtils.property(thatLocator, "cpfCtt", rhsCpfCtt), lhsCpfCtt, rhsCpfCtt)) {
                                    return false;
                                }
                            }
                            {
                                String lhsFoneFixo;
                                lhsFoneFixo = this.getFoneFixo();
                                String rhsFoneFixo;
                                rhsFoneFixo = that.getFoneFixo();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "foneFixo", lhsFoneFixo), LocatorUtils.property(thatLocator, "foneFixo", rhsFoneFixo), lhsFoneFixo, rhsFoneFixo)) {
                                    return false;
                                }
                            }
                            {
                                String lhsFoneCel;
                                lhsFoneCel = this.getFoneCel();
                                String rhsFoneCel;
                                rhsFoneCel = that.getFoneCel();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "foneCel", lhsFoneCel), LocatorUtils.property(thatLocator, "foneCel", rhsFoneCel), lhsFoneCel, rhsFoneCel)) {
                                    return false;
                                }
                            }
                            {
                                String lhsEmail;
                                lhsEmail = this.getEmail();
                                String rhsEmail;
                                rhsEmail = that.getEmail();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "email", lhsEmail), LocatorUtils.property(thatLocator, "email", rhsEmail), lhsEmail, rhsEmail)) {
                                    return false;
                                }
                            }
                            return true;
                        }

                        public boolean equals(Object object) {
                            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                            return equals(null, null, object, strategy);
                        }

                        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                            int currentHashCode = 1;
                            {
                                String theNmCtt;
                                theNmCtt = this.getNmCtt();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nmCtt", theNmCtt), currentHashCode, theNmCtt);
                            }
                            {
                                String theCpfCtt;
                                theCpfCtt = this.getCpfCtt();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cpfCtt", theCpfCtt), currentHashCode, theCpfCtt);
                            }
                            {
                                String theFoneFixo;
                                theFoneFixo = this.getFoneFixo();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foneFixo", theFoneFixo), currentHashCode, theFoneFixo);
                            }
                            {
                                String theFoneCel;
                                theFoneCel = this.getFoneCel();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foneCel", theFoneCel), currentHashCode, theFoneCel);
                            }
                            {
                                String theEmail;
                                theEmail = this.getEmail();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "email", theEmail), currentHashCode, theEmail);
                            }
                            return currentHashCode;
                        }

                        public int hashCode() {
                            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                            return this.hashCode(null, strategy);
                        }

                    }


                    /**
                     * <p>Classe Java de anonymous complex type.
                     * 
                     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ideEFR">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;pattern value="S|N"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="cnpjEFR" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="8"/>
                     *               &lt;maxLength value="14"/>
                     *               &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ideEFR",
                        "cnpjEFR"
                    })
                    @Entity(name = "Reinf$EvtInfoContri$InfoContri$Alteracao$InfoCadastro$InfoEFR")
                    @Table(name = "INFO_EFR")
                    @Inheritance(strategy = InheritanceType.JOINED)
                    public static class InfoEFR
                        implements Equals, HashCode
                    {

                        @XmlElement(required = true)
                        protected String ideEFR;
                        protected String cnpjEFR;
                        @XmlAttribute(name = "Hjid")
                        protected Long hjid;

                        /**
                         * Obt�m o valor da propriedade ideEFR.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "IDE_EFR")
                        public String getIdeEFR() {
                            return ideEFR;
                        }

                        /**
                         * Define o valor da propriedade ideEFR.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setIdeEFR(String value) {
                            this.ideEFR = value;
                        }

                        /**
                         * Obt�m o valor da propriedade cnpjEFR.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "CNPJ_EFR", length = 14)
                        public String getCnpjEFR() {
                            return cnpjEFR;
                        }

                        /**
                         * Define o valor da propriedade cnpjEFR.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCnpjEFR(String value) {
                            this.cnpjEFR = value;
                        }

                        /**
                         * Obt�m o valor da propriedade hjid.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Long }
                         *     
                         */
                        @Id
                        @Column(name = "HJID")
                        @GeneratedValue(strategy = GenerationType.AUTO)
                        public Long getHjid() {
                            return hjid;
                        }

                        /**
                         * Define o valor da propriedade hjid.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Long }
                         *     
                         */
                        public void setHjid(Long value) {
                            this.hjid = value;
                        }

                        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                            if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR)) {
                                return false;
                            }
                            if (this == object) {
                                return true;
                            }
                            final Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR that = ((Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.InfoEFR) object);
                            {
                                String lhsIdeEFR;
                                lhsIdeEFR = this.getIdeEFR();
                                String rhsIdeEFR;
                                rhsIdeEFR = that.getIdeEFR();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEFR", lhsIdeEFR), LocatorUtils.property(thatLocator, "ideEFR", rhsIdeEFR), lhsIdeEFR, rhsIdeEFR)) {
                                    return false;
                                }
                            }
                            {
                                String lhsCnpjEFR;
                                lhsCnpjEFR = this.getCnpjEFR();
                                String rhsCnpjEFR;
                                rhsCnpjEFR = that.getCnpjEFR();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "cnpjEFR", lhsCnpjEFR), LocatorUtils.property(thatLocator, "cnpjEFR", rhsCnpjEFR), lhsCnpjEFR, rhsCnpjEFR)) {
                                    return false;
                                }
                            }
                            return true;
                        }

                        public boolean equals(Object object) {
                            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                            return equals(null, null, object, strategy);
                        }

                        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                            int currentHashCode = 1;
                            {
                                String theIdeEFR;
                                theIdeEFR = this.getIdeEFR();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEFR", theIdeEFR), currentHashCode, theIdeEFR);
                            }
                            {
                                String theCnpjEFR;
                                theCnpjEFR = this.getCnpjEFR();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cnpjEFR", theCnpjEFR), currentHashCode, theCnpjEFR);
                            }
                            return currentHashCode;
                        }

                        public int hashCode() {
                            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                            return this.hashCode(null, strategy);
                        }

                    }


                    /**
                     * <p>Classe Java de anonymous complex type.
                     * 
                     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="cnpjSoftHouse">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="3"/>
                     *               &lt;maxLength value="14"/>
                     *               &lt;pattern value="[0-9]{3,14}"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="nmRazao">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="115"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="nmCont">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="70"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="telefone" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="13"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="email" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="60"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "cnpjSoftHouse",
                        "nmRazao",
                        "nmCont",
                        "telefone",
                        "email"
                    })
                    @Entity(name = "Reinf$EvtInfoContri$InfoContri$Alteracao$InfoCadastro$SoftHouse")
                    @Table(name = "SOFT_HOUSE")
                    @Inheritance(strategy = InheritanceType.JOINED)
                    public static class SoftHouse
                        implements Equals, HashCode
                    {

                        @XmlElement(required = true)
                        protected String cnpjSoftHouse;
                        @XmlElement(required = true)
                        protected String nmRazao;
                        @XmlElement(required = true)
                        protected String nmCont;
                        protected String telefone;
                        protected String email;
                        @XmlAttribute(name = "Hjid")
                        protected Long hjid;

                        /**
                         * Obt�m o valor da propriedade cnpjSoftHouse.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "CNPJ_SOFT_HOUSE", length = 14)
                        public String getCnpjSoftHouse() {
                            return cnpjSoftHouse;
                        }

                        /**
                         * Define o valor da propriedade cnpjSoftHouse.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCnpjSoftHouse(String value) {
                            this.cnpjSoftHouse = value;
                        }

                        /**
                         * Obt�m o valor da propriedade nmRazao.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "NM_RAZAO", length = 115)
                        public String getNmRazao() {
                            return nmRazao;
                        }

                        /**
                         * Define o valor da propriedade nmRazao.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNmRazao(String value) {
                            this.nmRazao = value;
                        }

                        /**
                         * Obt�m o valor da propriedade nmCont.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "NM_CONT", length = 70)
                        public String getNmCont() {
                            return nmCont;
                        }

                        /**
                         * Define o valor da propriedade nmCont.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNmCont(String value) {
                            this.nmCont = value;
                        }

                        /**
                         * Obt�m o valor da propriedade telefone.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "TELEFONE", length = 13)
                        public String getTelefone() {
                            return telefone;
                        }

                        /**
                         * Define o valor da propriedade telefone.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setTelefone(String value) {
                            this.telefone = value;
                        }

                        /**
                         * Obt�m o valor da propriedade email.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "EMAIL", length = 60)
                        public String getEmail() {
                            return email;
                        }

                        /**
                         * Define o valor da propriedade email.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEmail(String value) {
                            this.email = value;
                        }

                        /**
                         * Obt�m o valor da propriedade hjid.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Long }
                         *     
                         */
                        @Id
                        @Column(name = "HJID")
                        @GeneratedValue(strategy = GenerationType.AUTO)
                        public Long getHjid() {
                            return hjid;
                        }

                        /**
                         * Define o valor da propriedade hjid.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Long }
                         *     
                         */
                        public void setHjid(Long value) {
                            this.hjid = value;
                        }

                        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                            if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse)) {
                                return false;
                            }
                            if (this == object) {
                                return true;
                            }
                            final Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse that = ((Reinf.EvtInfoContri.InfoContri.Alteracao.InfoCadastro.SoftHouse) object);
                            {
                                String lhsCnpjSoftHouse;
                                lhsCnpjSoftHouse = this.getCnpjSoftHouse();
                                String rhsCnpjSoftHouse;
                                rhsCnpjSoftHouse = that.getCnpjSoftHouse();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "cnpjSoftHouse", lhsCnpjSoftHouse), LocatorUtils.property(thatLocator, "cnpjSoftHouse", rhsCnpjSoftHouse), lhsCnpjSoftHouse, rhsCnpjSoftHouse)) {
                                    return false;
                                }
                            }
                            {
                                String lhsNmRazao;
                                lhsNmRazao = this.getNmRazao();
                                String rhsNmRazao;
                                rhsNmRazao = that.getNmRazao();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "nmRazao", lhsNmRazao), LocatorUtils.property(thatLocator, "nmRazao", rhsNmRazao), lhsNmRazao, rhsNmRazao)) {
                                    return false;
                                }
                            }
                            {
                                String lhsNmCont;
                                lhsNmCont = this.getNmCont();
                                String rhsNmCont;
                                rhsNmCont = that.getNmCont();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "nmCont", lhsNmCont), LocatorUtils.property(thatLocator, "nmCont", rhsNmCont), lhsNmCont, rhsNmCont)) {
                                    return false;
                                }
                            }
                            {
                                String lhsTelefone;
                                lhsTelefone = this.getTelefone();
                                String rhsTelefone;
                                rhsTelefone = that.getTelefone();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "telefone", lhsTelefone), LocatorUtils.property(thatLocator, "telefone", rhsTelefone), lhsTelefone, rhsTelefone)) {
                                    return false;
                                }
                            }
                            {
                                String lhsEmail;
                                lhsEmail = this.getEmail();
                                String rhsEmail;
                                rhsEmail = that.getEmail();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "email", lhsEmail), LocatorUtils.property(thatLocator, "email", rhsEmail), lhsEmail, rhsEmail)) {
                                    return false;
                                }
                            }
                            return true;
                        }

                        public boolean equals(Object object) {
                            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                            return equals(null, null, object, strategy);
                        }

                        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                            int currentHashCode = 1;
                            {
                                String theCnpjSoftHouse;
                                theCnpjSoftHouse = this.getCnpjSoftHouse();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cnpjSoftHouse", theCnpjSoftHouse), currentHashCode, theCnpjSoftHouse);
                            }
                            {
                                String theNmRazao;
                                theNmRazao = this.getNmRazao();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nmRazao", theNmRazao), currentHashCode, theNmRazao);
                            }
                            {
                                String theNmCont;
                                theNmCont = this.getNmCont();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nmCont", theNmCont), currentHashCode, theNmCont);
                            }
                            {
                                String theTelefone;
                                theTelefone = this.getTelefone();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "telefone", theTelefone), currentHashCode, theTelefone);
                            }
                            {
                                String theEmail;
                                theEmail = this.getEmail();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "email", theEmail), currentHashCode, theEmail);
                            }
                            return currentHashCode;
                        }

                        public int hashCode() {
                            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                            return this.hashCode(null, strategy);
                        }

                    }

                }


                /**
                 * <p>Classe Java de anonymous complex type.
                 * 
                 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="iniValid">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="fimValid" minOccurs="0">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "iniValid",
                    "fimValid"
                })
                @Entity(name = "Reinf$EvtInfoContri$InfoContri$Alteracao$NovaValidade")
                @Table(name = "NOVA_VALIDADE")
                @Inheritance(strategy = InheritanceType.JOINED)
                public static class NovaValidade
                    implements Equals, HashCode
                {

                    @XmlElement(required = true)
                    protected String iniValid;
                    protected String fimValid;
                    @XmlAttribute(name = "Hjid")
                    protected Long hjid;

                    /**
                     * Obt�m o valor da propriedade iniValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "INI_VALID", length = 7)
                    public String getIniValid() {
                        return iniValid;
                    }

                    /**
                     * Define o valor da propriedade iniValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIniValid(String value) {
                        this.iniValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade fimValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "FIM_VALID", length = 7)
                    public String getFimValid() {
                        return fimValid;
                    }

                    /**
                     * Define o valor da propriedade fimValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFimValid(String value) {
                        this.fimValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade hjid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Id
                    @Column(name = "HJID")
                    @GeneratedValue(strategy = GenerationType.AUTO)
                    public Long getHjid() {
                        return hjid;
                    }

                    /**
                     * Define o valor da propriedade hjid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setHjid(Long value) {
                        this.hjid = value;
                    }

                    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                        if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade)) {
                            return false;
                        }
                        if (this == object) {
                            return true;
                        }
                        final Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade that = ((Reinf.EvtInfoContri.InfoContri.Alteracao.NovaValidade) object);
                        {
                            String lhsIniValid;
                            lhsIniValid = this.getIniValid();
                            String rhsIniValid;
                            rhsIniValid = that.getIniValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "iniValid", lhsIniValid), LocatorUtils.property(thatLocator, "iniValid", rhsIniValid), lhsIniValid, rhsIniValid)) {
                                return false;
                            }
                        }
                        {
                            String lhsFimValid;
                            lhsFimValid = this.getFimValid();
                            String rhsFimValid;
                            rhsFimValid = that.getFimValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "fimValid", lhsFimValid), LocatorUtils.property(thatLocator, "fimValid", rhsFimValid), lhsFimValid, rhsFimValid)) {
                                return false;
                            }
                        }
                        return true;
                    }

                    public boolean equals(Object object) {
                        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                        return equals(null, null, object, strategy);
                    }

                    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                        int currentHashCode = 1;
                        {
                            String theIniValid;
                            theIniValid = this.getIniValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "iniValid", theIniValid), currentHashCode, theIniValid);
                        }
                        {
                            String theFimValid;
                            theFimValid = this.getFimValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fimValid", theFimValid), currentHashCode, theFimValid);
                        }
                        return currentHashCode;
                    }

                    public int hashCode() {
                        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                        return this.hashCode(null, strategy);
                    }

                }

            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="idePeriodo">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="iniValid">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="fimValid" minOccurs="0">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idePeriodo"
            })
            @Entity(name = "Reinf$EvtInfoContri$InfoContri$Exclusao")
            @Table(name = "EXCLUSAO")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class Exclusao
                implements Equals, HashCode
            {

                @XmlElement(required = true)
                protected Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo idePeriodo;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade idePeriodo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo }
                 *     
                 */
                @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo.class, cascade = {
                    CascadeType.ALL
                })
                @JoinColumn(name = "IDE_PERIODO_EXCLUSAO_HJID")
                public Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo getIdePeriodo() {
                    return idePeriodo;
                }

                /**
                 * Define o valor da propriedade idePeriodo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo }
                 *     
                 */
                public void setIdePeriodo(Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo value) {
                    this.idePeriodo = value;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Exclusao)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtInfoContri.InfoContri.Exclusao that = ((Reinf.EvtInfoContri.InfoContri.Exclusao) object);
                    {
                        Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo lhsIdePeriodo;
                        lhsIdePeriodo = this.getIdePeriodo();
                        Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo rhsIdePeriodo;
                        rhsIdePeriodo = that.getIdePeriodo();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "idePeriodo", lhsIdePeriodo), LocatorUtils.property(thatLocator, "idePeriodo", rhsIdePeriodo), lhsIdePeriodo, rhsIdePeriodo)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo theIdePeriodo;
                        theIdePeriodo = this.getIdePeriodo();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idePeriodo", theIdePeriodo), currentHashCode, theIdePeriodo);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }


                /**
                 * <p>Classe Java de anonymous complex type.
                 * 
                 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="iniValid">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="fimValid" minOccurs="0">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "iniValid",
                    "fimValid"
                })
                @Entity(name = "Reinf$EvtInfoContri$InfoContri$Exclusao$IdePeriodo")
                @Table(name = "IDE_PERIODO")
                @Inheritance(strategy = InheritanceType.JOINED)
                public static class IdePeriodo
                    implements Equals, HashCode
                {

                    @XmlElement(required = true)
                    protected String iniValid;
                    protected String fimValid;
                    @XmlAttribute(name = "Hjid")
                    protected Long hjid;

                    /**
                     * Obt�m o valor da propriedade iniValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "INI_VALID", length = 7)
                    public String getIniValid() {
                        return iniValid;
                    }

                    /**
                     * Define o valor da propriedade iniValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIniValid(String value) {
                        this.iniValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade fimValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "FIM_VALID", length = 7)
                    public String getFimValid() {
                        return fimValid;
                    }

                    /**
                     * Define o valor da propriedade fimValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFimValid(String value) {
                        this.fimValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade hjid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Id
                    @Column(name = "HJID")
                    @GeneratedValue(strategy = GenerationType.AUTO)
                    public Long getHjid() {
                        return hjid;
                    }

                    /**
                     * Define o valor da propriedade hjid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setHjid(Long value) {
                        this.hjid = value;
                    }

                    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                        if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo)) {
                            return false;
                        }
                        if (this == object) {
                            return true;
                        }
                        final Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo that = ((Reinf.EvtInfoContri.InfoContri.Exclusao.IdePeriodo) object);
                        {
                            String lhsIniValid;
                            lhsIniValid = this.getIniValid();
                            String rhsIniValid;
                            rhsIniValid = that.getIniValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "iniValid", lhsIniValid), LocatorUtils.property(thatLocator, "iniValid", rhsIniValid), lhsIniValid, rhsIniValid)) {
                                return false;
                            }
                        }
                        {
                            String lhsFimValid;
                            lhsFimValid = this.getFimValid();
                            String rhsFimValid;
                            rhsFimValid = that.getFimValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "fimValid", lhsFimValid), LocatorUtils.property(thatLocator, "fimValid", rhsFimValid), lhsFimValid, rhsFimValid)) {
                                return false;
                            }
                        }
                        return true;
                    }

                    public boolean equals(Object object) {
                        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                        return equals(null, null, object, strategy);
                    }

                    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                        int currentHashCode = 1;
                        {
                            String theIniValid;
                            theIniValid = this.getIniValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "iniValid", theIniValid), currentHashCode, theIniValid);
                        }
                        {
                            String theFimValid;
                            theFimValid = this.getFimValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fimValid", theFimValid), currentHashCode, theFimValid);
                        }
                        return currentHashCode;
                    }

                    public int hashCode() {
                        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                        return this.hashCode(null, strategy);
                    }

                }

            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="idePeriodo">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="iniValid">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="fimValid" minOccurs="0">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="7"/>
             *                         &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="infoCadastro">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="classTrib">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;length value="2"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indEscrituracao">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="0|1"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indDesoneracao">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="0|1"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indAcordoIsenMulta">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="0|1"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="indSitPJ" minOccurs="0">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
             *                         &lt;pattern value="[0-4]"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="contato">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="nmCtt">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="70"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="cpfCtt">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="3"/>
             *                                   &lt;maxLength value="11"/>
             *                                   &lt;pattern value="[0-9]{3,11}"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="foneFixo" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="13"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="foneCel" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="13"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="email" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="60"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="cnpjSoftHouse">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="3"/>
             *                                   &lt;maxLength value="14"/>
             *                                   &lt;pattern value="[0-9]{3,14}"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="nmRazao">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="115"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="nmCont">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="70"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="telefone" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="13"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="email" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="1"/>
             *                                   &lt;maxLength value="60"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="infoEFR" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ideEFR">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;pattern value="S|N"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="cnpjEFR" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;minLength value="8"/>
             *                                   &lt;maxLength value="14"/>
             *                                   &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idePeriodo",
                "infoCadastro"
            })
            @Entity(name = "Reinf$EvtInfoContri$InfoContri$Inclusao")
            @Table(name = "INCLUSAO")
            @Inheritance(strategy = InheritanceType.JOINED)
            public static class Inclusao
                implements Equals, HashCode
            {

                @XmlElement(required = true)
                protected Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo idePeriodo;
                @XmlElement(required = true)
                protected Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro infoCadastro;
                @XmlAttribute(name = "Hjid")
                protected Long hjid;

                /**
                 * Obt�m o valor da propriedade idePeriodo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo }
                 *     
                 */
                @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo.class, cascade = {
                    CascadeType.ALL
                })
                @JoinColumn(name = "IDE_PERIODO_INCLUSAO_HJID")
                public Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo getIdePeriodo() {
                    return idePeriodo;
                }

                /**
                 * Define o valor da propriedade idePeriodo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo }
                 *     
                 */
                public void setIdePeriodo(Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo value) {
                    this.idePeriodo = value;
                }

                /**
                 * Obt�m o valor da propriedade infoCadastro.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro }
                 *     
                 */
                @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.class, cascade = {
                    CascadeType.ALL
                })
                @JoinColumn(name = "INFO_CADASTRO_INCLUSAO_HJID")
                public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro getInfoCadastro() {
                    return infoCadastro;
                }

                /**
                 * Define o valor da propriedade infoCadastro.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro }
                 *     
                 */
                public void setInfoCadastro(Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro value) {
                    this.infoCadastro = value;
                }

                /**
                 * Obt�m o valor da propriedade hjid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *     
                 */
                @Id
                @Column(name = "HJID")
                @GeneratedValue(strategy = GenerationType.AUTO)
                public Long getHjid() {
                    return hjid;
                }

                /**
                 * Define o valor da propriedade hjid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *     
                 */
                public void setHjid(Long value) {
                    this.hjid = value;
                }

                public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                    if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Inclusao)) {
                        return false;
                    }
                    if (this == object) {
                        return true;
                    }
                    final Reinf.EvtInfoContri.InfoContri.Inclusao that = ((Reinf.EvtInfoContri.InfoContri.Inclusao) object);
                    {
                        Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo lhsIdePeriodo;
                        lhsIdePeriodo = this.getIdePeriodo();
                        Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo rhsIdePeriodo;
                        rhsIdePeriodo = that.getIdePeriodo();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "idePeriodo", lhsIdePeriodo), LocatorUtils.property(thatLocator, "idePeriodo", rhsIdePeriodo), lhsIdePeriodo, rhsIdePeriodo)) {
                            return false;
                        }
                    }
                    {
                        Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro lhsInfoCadastro;
                        lhsInfoCadastro = this.getInfoCadastro();
                        Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro rhsInfoCadastro;
                        rhsInfoCadastro = that.getInfoCadastro();
                        if (!strategy.equals(LocatorUtils.property(thisLocator, "infoCadastro", lhsInfoCadastro), LocatorUtils.property(thatLocator, "infoCadastro", rhsInfoCadastro), lhsInfoCadastro, rhsInfoCadastro)) {
                            return false;
                        }
                    }
                    return true;
                }

                public boolean equals(Object object) {
                    final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                    return equals(null, null, object, strategy);
                }

                public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                    int currentHashCode = 1;
                    {
                        Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo theIdePeriodo;
                        theIdePeriodo = this.getIdePeriodo();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idePeriodo", theIdePeriodo), currentHashCode, theIdePeriodo);
                    }
                    {
                        Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro theInfoCadastro;
                        theInfoCadastro = this.getInfoCadastro();
                        currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoCadastro", theInfoCadastro), currentHashCode, theInfoCadastro);
                    }
                    return currentHashCode;
                }

                public int hashCode() {
                    final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                    return this.hashCode(null, strategy);
                }


                /**
                 * <p>Classe Java de anonymous complex type.
                 * 
                 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="iniValid">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="fimValid" minOccurs="0">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="7"/>
                 *               &lt;pattern value="20([0-9][0-9])-(0[1-9]|1[0-2])"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "iniValid",
                    "fimValid"
                })
                @Entity(name = "Reinf$EvtInfoContri$InfoContri$Inclusao$IdePeriodo")
                @Table(name = "IDE_PERIODO__1")
                @Inheritance(strategy = InheritanceType.JOINED)
                public static class IdePeriodo
                    implements Equals, HashCode
                {

                    @XmlElement(required = true)
                    protected String iniValid;
                    protected String fimValid;
                    @XmlAttribute(name = "Hjid")
                    protected Long hjid;

                    /**
                     * Obt�m o valor da propriedade iniValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "INI_VALID", length = 7)
                    public String getIniValid() {
                        return iniValid;
                    }

                    /**
                     * Define o valor da propriedade iniValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIniValid(String value) {
                        this.iniValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade fimValid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "FIM_VALID", length = 7)
                    public String getFimValid() {
                        return fimValid;
                    }

                    /**
                     * Define o valor da propriedade fimValid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFimValid(String value) {
                        this.fimValid = value;
                    }

                    /**
                     * Obt�m o valor da propriedade hjid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Id
                    @Column(name = "HJID")
                    @GeneratedValue(strategy = GenerationType.AUTO)
                    public Long getHjid() {
                        return hjid;
                    }

                    /**
                     * Define o valor da propriedade hjid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setHjid(Long value) {
                        this.hjid = value;
                    }

                    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                        if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo)) {
                            return false;
                        }
                        if (this == object) {
                            return true;
                        }
                        final Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo that = ((Reinf.EvtInfoContri.InfoContri.Inclusao.IdePeriodo) object);
                        {
                            String lhsIniValid;
                            lhsIniValid = this.getIniValid();
                            String rhsIniValid;
                            rhsIniValid = that.getIniValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "iniValid", lhsIniValid), LocatorUtils.property(thatLocator, "iniValid", rhsIniValid), lhsIniValid, rhsIniValid)) {
                                return false;
                            }
                        }
                        {
                            String lhsFimValid;
                            lhsFimValid = this.getFimValid();
                            String rhsFimValid;
                            rhsFimValid = that.getFimValid();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "fimValid", lhsFimValid), LocatorUtils.property(thatLocator, "fimValid", rhsFimValid), lhsFimValid, rhsFimValid)) {
                                return false;
                            }
                        }
                        return true;
                    }

                    public boolean equals(Object object) {
                        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                        return equals(null, null, object, strategy);
                    }

                    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                        int currentHashCode = 1;
                        {
                            String theIniValid;
                            theIniValid = this.getIniValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "iniValid", theIniValid), currentHashCode, theIniValid);
                        }
                        {
                            String theFimValid;
                            theFimValid = this.getFimValid();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fimValid", theFimValid), currentHashCode, theFimValid);
                        }
                        return currentHashCode;
                    }

                    public int hashCode() {
                        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                        return this.hashCode(null, strategy);
                    }

                }


                /**
                 * <p>Classe Java de anonymous complex type.
                 * 
                 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="classTrib">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;length value="2"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indEscrituracao">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="0|1"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indDesoneracao">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="0|1"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indAcordoIsenMulta">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="0|1"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="indSitPJ" minOccurs="0">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt">
                 *               &lt;pattern value="[0-4]"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="contato">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="nmCtt">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="70"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="cpfCtt">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="3"/>
                 *                         &lt;maxLength value="11"/>
                 *                         &lt;pattern value="[0-9]{3,11}"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="foneFixo" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="13"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="foneCel" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="13"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="email" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="60"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="softHouse" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="cnpjSoftHouse">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="3"/>
                 *                         &lt;maxLength value="14"/>
                 *                         &lt;pattern value="[0-9]{3,14}"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="nmRazao">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="115"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="nmCont">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="70"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="telefone" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="13"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="email" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="1"/>
                 *                         &lt;maxLength value="60"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="infoEFR" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ideEFR">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;pattern value="S|N"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="cnpjEFR" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;minLength value="8"/>
                 *                         &lt;maxLength value="14"/>
                 *                         &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "classTrib",
                    "indEscrituracao",
                    "indDesoneracao",
                    "indAcordoIsenMulta",
                    "indSitPJ",
                    "contato",
                    "softHouse",
                    "infoEFR"
                })
                @Entity(name = "Reinf$EvtInfoContri$InfoContri$Inclusao$InfoCadastro")
                @Table(name = "INFO_CADASTRO__0")
                @Inheritance(strategy = InheritanceType.JOINED)
                public static class InfoCadastro
                    implements Equals, HashCode
                {

                    @XmlElement(required = true)
                    protected String classTrib;
                    protected long indEscrituracao;
                    protected long indDesoneracao;
                    protected long indAcordoIsenMulta;
                    protected Long indSitPJ;
                    @XmlElement(required = true)
                    protected Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato contato;
                    protected List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> softHouse;
                    protected Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR infoEFR;
                    @XmlAttribute(name = "Hjid")
                    protected Long hjid;

                    /**
                     * Obt�m o valor da propriedade classTrib.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    @Basic
                    @Column(name = "CLASS_TRIB", length = 2)
                    public String getClassTrib() {
                        return classTrib;
                    }

                    /**
                     * Define o valor da propriedade classTrib.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClassTrib(String value) {
                        this.classTrib = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indEscrituracao.
                     * 
                     */
                    @Basic
                    @Column(name = "IND_ESCRITURACAO", scale = 0)
                    public long getIndEscrituracao() {
                        return indEscrituracao;
                    }

                    /**
                     * Define o valor da propriedade indEscrituracao.
                     * 
                     */
                    public void setIndEscrituracao(long value) {
                        this.indEscrituracao = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indDesoneracao.
                     * 
                     */
                    @Basic
                    @Column(name = "IND_DESONERACAO", scale = 0)
                    public long getIndDesoneracao() {
                        return indDesoneracao;
                    }

                    /**
                     * Define o valor da propriedade indDesoneracao.
                     * 
                     */
                    public void setIndDesoneracao(long value) {
                        this.indDesoneracao = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indAcordoIsenMulta.
                     * 
                     */
                    @Basic
                    @Column(name = "IND_ACORDO_ISEN_MULTA", scale = 0)
                    public long getIndAcordoIsenMulta() {
                        return indAcordoIsenMulta;
                    }

                    /**
                     * Define o valor da propriedade indAcordoIsenMulta.
                     * 
                     */
                    public void setIndAcordoIsenMulta(long value) {
                        this.indAcordoIsenMulta = value;
                    }

                    /**
                     * Obt�m o valor da propriedade indSitPJ.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Basic
                    @Column(name = "IND_SIT_PJ", scale = 0)
                    public Long getIndSitPJ() {
                        return indSitPJ;
                    }

                    /**
                     * Define o valor da propriedade indSitPJ.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setIndSitPJ(Long value) {
                        this.indSitPJ = value;
                    }

                    /**
                     * Obt�m o valor da propriedade contato.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato }
                     *     
                     */
                    @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato.class, cascade = {
                        CascadeType.ALL
                    })
                    @JoinColumn(name = "CONTATO_INFO_CADASTRO___0_HJ_0")
                    public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato getContato() {
                        return contato;
                    }

                    /**
                     * Define o valor da propriedade contato.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato }
                     *     
                     */
                    public void setContato(Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato value) {
                        this.contato = value;
                    }

                    /**
                     * Gets the value of the softHouse property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the softHouse property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getSoftHouse().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse }
                     * 
                     * 
                     */
                    @OneToMany(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse.class, cascade = {
                        CascadeType.ALL
                    })
                    @JoinColumn(name = "SOFT_HOUSE_INFO_CADASTRO___0_0")
                    public List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> getSoftHouse() {
                        if (softHouse == null) {
                            softHouse = new ArrayList<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse>();
                        }
                        return this.softHouse;
                    }

                    /**
                     * 
                     * 
                     */
                    public void setSoftHouse(List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> softHouse) {
                        this.softHouse = softHouse;
                    }

                    /**
                     * Obt�m o valor da propriedade infoEFR.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR }
                     *     
                     */
                    @ManyToOne(targetEntity = br.gov.esocial.reinf.schemas.evtinfocontribuinte.v1_03_02.Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR.class, cascade = {
                        CascadeType.ALL
                    })
                    @JoinColumn(name = "INFO_EFR_INFO_CADASTRO___0_H_0")
                    public Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR getInfoEFR() {
                        return infoEFR;
                    }

                    /**
                     * Define o valor da propriedade infoEFR.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR }
                     *     
                     */
                    public void setInfoEFR(Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR value) {
                        this.infoEFR = value;
                    }

                    /**
                     * Obt�m o valor da propriedade hjid.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    @Id
                    @Column(name = "HJID")
                    @GeneratedValue(strategy = GenerationType.AUTO)
                    public Long getHjid() {
                        return hjid;
                    }

                    /**
                     * Define o valor da propriedade hjid.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setHjid(Long value) {
                        this.hjid = value;
                    }

                    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                        if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro)) {
                            return false;
                        }
                        if (this == object) {
                            return true;
                        }
                        final Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro that = ((Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro) object);
                        {
                            String lhsClassTrib;
                            lhsClassTrib = this.getClassTrib();
                            String rhsClassTrib;
                            rhsClassTrib = that.getClassTrib();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "classTrib", lhsClassTrib), LocatorUtils.property(thatLocator, "classTrib", rhsClassTrib), lhsClassTrib, rhsClassTrib)) {
                                return false;
                            }
                        }
                        {
                            long lhsIndEscrituracao;
                            lhsIndEscrituracao = this.getIndEscrituracao();
                            long rhsIndEscrituracao;
                            rhsIndEscrituracao = that.getIndEscrituracao();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indEscrituracao", lhsIndEscrituracao), LocatorUtils.property(thatLocator, "indEscrituracao", rhsIndEscrituracao), lhsIndEscrituracao, rhsIndEscrituracao)) {
                                return false;
                            }
                        }
                        {
                            long lhsIndDesoneracao;
                            lhsIndDesoneracao = this.getIndDesoneracao();
                            long rhsIndDesoneracao;
                            rhsIndDesoneracao = that.getIndDesoneracao();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indDesoneracao", lhsIndDesoneracao), LocatorUtils.property(thatLocator, "indDesoneracao", rhsIndDesoneracao), lhsIndDesoneracao, rhsIndDesoneracao)) {
                                return false;
                            }
                        }
                        {
                            long lhsIndAcordoIsenMulta;
                            lhsIndAcordoIsenMulta = this.getIndAcordoIsenMulta();
                            long rhsIndAcordoIsenMulta;
                            rhsIndAcordoIsenMulta = that.getIndAcordoIsenMulta();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indAcordoIsenMulta", lhsIndAcordoIsenMulta), LocatorUtils.property(thatLocator, "indAcordoIsenMulta", rhsIndAcordoIsenMulta), lhsIndAcordoIsenMulta, rhsIndAcordoIsenMulta)) {
                                return false;
                            }
                        }
                        {
                            Long lhsIndSitPJ;
                            lhsIndSitPJ = this.getIndSitPJ();
                            Long rhsIndSitPJ;
                            rhsIndSitPJ = that.getIndSitPJ();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "indSitPJ", lhsIndSitPJ), LocatorUtils.property(thatLocator, "indSitPJ", rhsIndSitPJ), lhsIndSitPJ, rhsIndSitPJ)) {
                                return false;
                            }
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato lhsContato;
                            lhsContato = this.getContato();
                            Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato rhsContato;
                            rhsContato = that.getContato();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "contato", lhsContato), LocatorUtils.property(thatLocator, "contato", rhsContato), lhsContato, rhsContato)) {
                                return false;
                            }
                        }
                        {
                            List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> lhsSoftHouse;
                            lhsSoftHouse = (((this.softHouse!= null)&&(!this.softHouse.isEmpty()))?this.getSoftHouse():null);
                            List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> rhsSoftHouse;
                            rhsSoftHouse = (((that.softHouse!= null)&&(!that.softHouse.isEmpty()))?that.getSoftHouse():null);
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "softHouse", lhsSoftHouse), LocatorUtils.property(thatLocator, "softHouse", rhsSoftHouse), lhsSoftHouse, rhsSoftHouse)) {
                                return false;
                            }
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR lhsInfoEFR;
                            lhsInfoEFR = this.getInfoEFR();
                            Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR rhsInfoEFR;
                            rhsInfoEFR = that.getInfoEFR();
                            if (!strategy.equals(LocatorUtils.property(thisLocator, "infoEFR", lhsInfoEFR), LocatorUtils.property(thatLocator, "infoEFR", rhsInfoEFR), lhsInfoEFR, rhsInfoEFR)) {
                                return false;
                            }
                        }
                        return true;
                    }

                    public boolean equals(Object object) {
                        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                        return equals(null, null, object, strategy);
                    }

                    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                        int currentHashCode = 1;
                        {
                            String theClassTrib;
                            theClassTrib = this.getClassTrib();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "classTrib", theClassTrib), currentHashCode, theClassTrib);
                        }
                        {
                            long theIndEscrituracao;
                            theIndEscrituracao = this.getIndEscrituracao();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indEscrituracao", theIndEscrituracao), currentHashCode, theIndEscrituracao);
                        }
                        {
                            long theIndDesoneracao;
                            theIndDesoneracao = this.getIndDesoneracao();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indDesoneracao", theIndDesoneracao), currentHashCode, theIndDesoneracao);
                        }
                        {
                            long theIndAcordoIsenMulta;
                            theIndAcordoIsenMulta = this.getIndAcordoIsenMulta();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indAcordoIsenMulta", theIndAcordoIsenMulta), currentHashCode, theIndAcordoIsenMulta);
                        }
                        {
                            Long theIndSitPJ;
                            theIndSitPJ = this.getIndSitPJ();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "indSitPJ", theIndSitPJ), currentHashCode, theIndSitPJ);
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato theContato;
                            theContato = this.getContato();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "contato", theContato), currentHashCode, theContato);
                        }
                        {
                            List<Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse> theSoftHouse;
                            theSoftHouse = (((this.softHouse!= null)&&(!this.softHouse.isEmpty()))?this.getSoftHouse():null);
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "softHouse", theSoftHouse), currentHashCode, theSoftHouse);
                        }
                        {
                            Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR theInfoEFR;
                            theInfoEFR = this.getInfoEFR();
                            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoEFR", theInfoEFR), currentHashCode, theInfoEFR);
                        }
                        return currentHashCode;
                    }

                    public int hashCode() {
                        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                        return this.hashCode(null, strategy);
                    }


                    /**
                     * <p>Classe Java de anonymous complex type.
                     * 
                     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="nmCtt">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="70"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="cpfCtt">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="3"/>
                     *               &lt;maxLength value="11"/>
                     *               &lt;pattern value="[0-9]{3,11}"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="foneFixo" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="13"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="foneCel" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="13"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="email" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="60"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "nmCtt",
                        "cpfCtt",
                        "foneFixo",
                        "foneCel",
                        "email"
                    })
                    @Entity(name = "Reinf$EvtInfoContri$InfoContri$Inclusao$InfoCadastro$Contato")
                    @Table(name = "CONTATO__0")
                    @Inheritance(strategy = InheritanceType.JOINED)
                    public static class Contato
                        implements Equals, HashCode
                    {

                        @XmlElement(required = true)
                        protected String nmCtt;
                        @XmlElement(required = true)
                        protected String cpfCtt;
                        protected String foneFixo;
                        protected String foneCel;
                        protected String email;
                        @XmlAttribute(name = "Hjid")
                        protected Long hjid;

                        /**
                         * Obt�m o valor da propriedade nmCtt.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "NM_CTT", length = 70)
                        public String getNmCtt() {
                            return nmCtt;
                        }

                        /**
                         * Define o valor da propriedade nmCtt.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNmCtt(String value) {
                            this.nmCtt = value;
                        }

                        /**
                         * Obt�m o valor da propriedade cpfCtt.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "CPF_CTT", length = 11)
                        public String getCpfCtt() {
                            return cpfCtt;
                        }

                        /**
                         * Define o valor da propriedade cpfCtt.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCpfCtt(String value) {
                            this.cpfCtt = value;
                        }

                        /**
                         * Obt�m o valor da propriedade foneFixo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "FONE_FIXO", length = 13)
                        public String getFoneFixo() {
                            return foneFixo;
                        }

                        /**
                         * Define o valor da propriedade foneFixo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setFoneFixo(String value) {
                            this.foneFixo = value;
                        }

                        /**
                         * Obt�m o valor da propriedade foneCel.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "FONE_CEL", length = 13)
                        public String getFoneCel() {
                            return foneCel;
                        }

                        /**
                         * Define o valor da propriedade foneCel.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setFoneCel(String value) {
                            this.foneCel = value;
                        }

                        /**
                         * Obt�m o valor da propriedade email.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "EMAIL", length = 60)
                        public String getEmail() {
                            return email;
                        }

                        /**
                         * Define o valor da propriedade email.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEmail(String value) {
                            this.email = value;
                        }

                        /**
                         * Obt�m o valor da propriedade hjid.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Long }
                         *     
                         */
                        @Id
                        @Column(name = "HJID")
                        @GeneratedValue(strategy = GenerationType.AUTO)
                        public Long getHjid() {
                            return hjid;
                        }

                        /**
                         * Define o valor da propriedade hjid.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Long }
                         *     
                         */
                        public void setHjid(Long value) {
                            this.hjid = value;
                        }

                        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                            if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato)) {
                                return false;
                            }
                            if (this == object) {
                                return true;
                            }
                            final Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato that = ((Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.Contato) object);
                            {
                                String lhsNmCtt;
                                lhsNmCtt = this.getNmCtt();
                                String rhsNmCtt;
                                rhsNmCtt = that.getNmCtt();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "nmCtt", lhsNmCtt), LocatorUtils.property(thatLocator, "nmCtt", rhsNmCtt), lhsNmCtt, rhsNmCtt)) {
                                    return false;
                                }
                            }
                            {
                                String lhsCpfCtt;
                                lhsCpfCtt = this.getCpfCtt();
                                String rhsCpfCtt;
                                rhsCpfCtt = that.getCpfCtt();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "cpfCtt", lhsCpfCtt), LocatorUtils.property(thatLocator, "cpfCtt", rhsCpfCtt), lhsCpfCtt, rhsCpfCtt)) {
                                    return false;
                                }
                            }
                            {
                                String lhsFoneFixo;
                                lhsFoneFixo = this.getFoneFixo();
                                String rhsFoneFixo;
                                rhsFoneFixo = that.getFoneFixo();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "foneFixo", lhsFoneFixo), LocatorUtils.property(thatLocator, "foneFixo", rhsFoneFixo), lhsFoneFixo, rhsFoneFixo)) {
                                    return false;
                                }
                            }
                            {
                                String lhsFoneCel;
                                lhsFoneCel = this.getFoneCel();
                                String rhsFoneCel;
                                rhsFoneCel = that.getFoneCel();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "foneCel", lhsFoneCel), LocatorUtils.property(thatLocator, "foneCel", rhsFoneCel), lhsFoneCel, rhsFoneCel)) {
                                    return false;
                                }
                            }
                            {
                                String lhsEmail;
                                lhsEmail = this.getEmail();
                                String rhsEmail;
                                rhsEmail = that.getEmail();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "email", lhsEmail), LocatorUtils.property(thatLocator, "email", rhsEmail), lhsEmail, rhsEmail)) {
                                    return false;
                                }
                            }
                            return true;
                        }

                        public boolean equals(Object object) {
                            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                            return equals(null, null, object, strategy);
                        }

                        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                            int currentHashCode = 1;
                            {
                                String theNmCtt;
                                theNmCtt = this.getNmCtt();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nmCtt", theNmCtt), currentHashCode, theNmCtt);
                            }
                            {
                                String theCpfCtt;
                                theCpfCtt = this.getCpfCtt();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cpfCtt", theCpfCtt), currentHashCode, theCpfCtt);
                            }
                            {
                                String theFoneFixo;
                                theFoneFixo = this.getFoneFixo();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foneFixo", theFoneFixo), currentHashCode, theFoneFixo);
                            }
                            {
                                String theFoneCel;
                                theFoneCel = this.getFoneCel();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foneCel", theFoneCel), currentHashCode, theFoneCel);
                            }
                            {
                                String theEmail;
                                theEmail = this.getEmail();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "email", theEmail), currentHashCode, theEmail);
                            }
                            return currentHashCode;
                        }

                        public int hashCode() {
                            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                            return this.hashCode(null, strategy);
                        }

                    }


                    /**
                     * <p>Classe Java de anonymous complex type.
                     * 
                     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ideEFR">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;pattern value="S|N"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="cnpjEFR" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="8"/>
                     *               &lt;maxLength value="14"/>
                     *               &lt;pattern value="[0-9]{8}|[0-9]{14}"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ideEFR",
                        "cnpjEFR"
                    })
                    @Entity(name = "Reinf$EvtInfoContri$InfoContri$Inclusao$InfoCadastro$InfoEFR")
                    @Table(name = "INFO_EFR__0")
                    @Inheritance(strategy = InheritanceType.JOINED)
                    public static class InfoEFR
                        implements Equals, HashCode
                    {

                        @XmlElement(required = true)
                        protected String ideEFR;
                        protected String cnpjEFR;
                        @XmlAttribute(name = "Hjid")
                        protected Long hjid;

                        /**
                         * Obt�m o valor da propriedade ideEFR.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "IDE_EFR")
                        public String getIdeEFR() {
                            return ideEFR;
                        }

                        /**
                         * Define o valor da propriedade ideEFR.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setIdeEFR(String value) {
                            this.ideEFR = value;
                        }

                        /**
                         * Obt�m o valor da propriedade cnpjEFR.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "CNPJ_EFR", length = 14)
                        public String getCnpjEFR() {
                            return cnpjEFR;
                        }

                        /**
                         * Define o valor da propriedade cnpjEFR.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCnpjEFR(String value) {
                            this.cnpjEFR = value;
                        }

                        /**
                         * Obt�m o valor da propriedade hjid.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Long }
                         *     
                         */
                        @Id
                        @Column(name = "HJID")
                        @GeneratedValue(strategy = GenerationType.AUTO)
                        public Long getHjid() {
                            return hjid;
                        }

                        /**
                         * Define o valor da propriedade hjid.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Long }
                         *     
                         */
                        public void setHjid(Long value) {
                            this.hjid = value;
                        }

                        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                            if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR)) {
                                return false;
                            }
                            if (this == object) {
                                return true;
                            }
                            final Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR that = ((Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.InfoEFR) object);
                            {
                                String lhsIdeEFR;
                                lhsIdeEFR = this.getIdeEFR();
                                String rhsIdeEFR;
                                rhsIdeEFR = that.getIdeEFR();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "ideEFR", lhsIdeEFR), LocatorUtils.property(thatLocator, "ideEFR", rhsIdeEFR), lhsIdeEFR, rhsIdeEFR)) {
                                    return false;
                                }
                            }
                            {
                                String lhsCnpjEFR;
                                lhsCnpjEFR = this.getCnpjEFR();
                                String rhsCnpjEFR;
                                rhsCnpjEFR = that.getCnpjEFR();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "cnpjEFR", lhsCnpjEFR), LocatorUtils.property(thatLocator, "cnpjEFR", rhsCnpjEFR), lhsCnpjEFR, rhsCnpjEFR)) {
                                    return false;
                                }
                            }
                            return true;
                        }

                        public boolean equals(Object object) {
                            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                            return equals(null, null, object, strategy);
                        }

                        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                            int currentHashCode = 1;
                            {
                                String theIdeEFR;
                                theIdeEFR = this.getIdeEFR();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ideEFR", theIdeEFR), currentHashCode, theIdeEFR);
                            }
                            {
                                String theCnpjEFR;
                                theCnpjEFR = this.getCnpjEFR();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cnpjEFR", theCnpjEFR), currentHashCode, theCnpjEFR);
                            }
                            return currentHashCode;
                        }

                        public int hashCode() {
                            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                            return this.hashCode(null, strategy);
                        }

                    }


                    /**
                     * <p>Classe Java de anonymous complex type.
                     * 
                     * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="cnpjSoftHouse">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="3"/>
                     *               &lt;maxLength value="14"/>
                     *               &lt;pattern value="[0-9]{3,14}"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="nmRazao">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="115"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="nmCont">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="70"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="telefone" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="13"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="email" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;minLength value="1"/>
                     *               &lt;maxLength value="60"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "cnpjSoftHouse",
                        "nmRazao",
                        "nmCont",
                        "telefone",
                        "email"
                    })
                    @Entity(name = "Reinf$EvtInfoContri$InfoContri$Inclusao$InfoCadastro$SoftHouse")
                    @Table(name = "SOFT_HOUSE__0")
                    @Inheritance(strategy = InheritanceType.JOINED)
                    public static class SoftHouse
                        implements Equals, HashCode
                    {

                        @XmlElement(required = true)
                        protected String cnpjSoftHouse;
                        @XmlElement(required = true)
                        protected String nmRazao;
                        @XmlElement(required = true)
                        protected String nmCont;
                        protected String telefone;
                        protected String email;
                        @XmlAttribute(name = "Hjid")
                        protected Long hjid;

                        /**
                         * Obt�m o valor da propriedade cnpjSoftHouse.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "CNPJ_SOFT_HOUSE", length = 14)
                        public String getCnpjSoftHouse() {
                            return cnpjSoftHouse;
                        }

                        /**
                         * Define o valor da propriedade cnpjSoftHouse.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCnpjSoftHouse(String value) {
                            this.cnpjSoftHouse = value;
                        }

                        /**
                         * Obt�m o valor da propriedade nmRazao.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "NM_RAZAO", length = 115)
                        public String getNmRazao() {
                            return nmRazao;
                        }

                        /**
                         * Define o valor da propriedade nmRazao.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNmRazao(String value) {
                            this.nmRazao = value;
                        }

                        /**
                         * Obt�m o valor da propriedade nmCont.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "NM_CONT", length = 70)
                        public String getNmCont() {
                            return nmCont;
                        }

                        /**
                         * Define o valor da propriedade nmCont.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNmCont(String value) {
                            this.nmCont = value;
                        }

                        /**
                         * Obt�m o valor da propriedade telefone.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "TELEFONE", length = 13)
                        public String getTelefone() {
                            return telefone;
                        }

                        /**
                         * Define o valor da propriedade telefone.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setTelefone(String value) {
                            this.telefone = value;
                        }

                        /**
                         * Obt�m o valor da propriedade email.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        @Basic
                        @Column(name = "EMAIL", length = 60)
                        public String getEmail() {
                            return email;
                        }

                        /**
                         * Define o valor da propriedade email.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEmail(String value) {
                            this.email = value;
                        }

                        /**
                         * Obt�m o valor da propriedade hjid.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Long }
                         *     
                         */
                        @Id
                        @Column(name = "HJID")
                        @GeneratedValue(strategy = GenerationType.AUTO)
                        public Long getHjid() {
                            return hjid;
                        }

                        /**
                         * Define o valor da propriedade hjid.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Long }
                         *     
                         */
                        public void setHjid(Long value) {
                            this.hjid = value;
                        }

                        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
                            if (!(object instanceof Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse)) {
                                return false;
                            }
                            if (this == object) {
                                return true;
                            }
                            final Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse that = ((Reinf.EvtInfoContri.InfoContri.Inclusao.InfoCadastro.SoftHouse) object);
                            {
                                String lhsCnpjSoftHouse;
                                lhsCnpjSoftHouse = this.getCnpjSoftHouse();
                                String rhsCnpjSoftHouse;
                                rhsCnpjSoftHouse = that.getCnpjSoftHouse();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "cnpjSoftHouse", lhsCnpjSoftHouse), LocatorUtils.property(thatLocator, "cnpjSoftHouse", rhsCnpjSoftHouse), lhsCnpjSoftHouse, rhsCnpjSoftHouse)) {
                                    return false;
                                }
                            }
                            {
                                String lhsNmRazao;
                                lhsNmRazao = this.getNmRazao();
                                String rhsNmRazao;
                                rhsNmRazao = that.getNmRazao();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "nmRazao", lhsNmRazao), LocatorUtils.property(thatLocator, "nmRazao", rhsNmRazao), lhsNmRazao, rhsNmRazao)) {
                                    return false;
                                }
                            }
                            {
                                String lhsNmCont;
                                lhsNmCont = this.getNmCont();
                                String rhsNmCont;
                                rhsNmCont = that.getNmCont();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "nmCont", lhsNmCont), LocatorUtils.property(thatLocator, "nmCont", rhsNmCont), lhsNmCont, rhsNmCont)) {
                                    return false;
                                }
                            }
                            {
                                String lhsTelefone;
                                lhsTelefone = this.getTelefone();
                                String rhsTelefone;
                                rhsTelefone = that.getTelefone();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "telefone", lhsTelefone), LocatorUtils.property(thatLocator, "telefone", rhsTelefone), lhsTelefone, rhsTelefone)) {
                                    return false;
                                }
                            }
                            {
                                String lhsEmail;
                                lhsEmail = this.getEmail();
                                String rhsEmail;
                                rhsEmail = that.getEmail();
                                if (!strategy.equals(LocatorUtils.property(thisLocator, "email", lhsEmail), LocatorUtils.property(thatLocator, "email", rhsEmail), lhsEmail, rhsEmail)) {
                                    return false;
                                }
                            }
                            return true;
                        }

                        public boolean equals(Object object) {
                            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
                            return equals(null, null, object, strategy);
                        }

                        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
                            int currentHashCode = 1;
                            {
                                String theCnpjSoftHouse;
                                theCnpjSoftHouse = this.getCnpjSoftHouse();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cnpjSoftHouse", theCnpjSoftHouse), currentHashCode, theCnpjSoftHouse);
                            }
                            {
                                String theNmRazao;
                                theNmRazao = this.getNmRazao();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nmRazao", theNmRazao), currentHashCode, theNmRazao);
                            }
                            {
                                String theNmCont;
                                theNmCont = this.getNmCont();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nmCont", theNmCont), currentHashCode, theNmCont);
                            }
                            {
                                String theTelefone;
                                theTelefone = this.getTelefone();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "telefone", theTelefone), currentHashCode, theTelefone);
                            }
                            {
                                String theEmail;
                                theEmail = this.getEmail();
                                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "email", theEmail), currentHashCode, theEmail);
                            }
                            return currentHashCode;
                        }

                        public int hashCode() {
                            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
                            return this.hashCode(null, strategy);
                        }

                    }

                }

            }

        }

    }

}
